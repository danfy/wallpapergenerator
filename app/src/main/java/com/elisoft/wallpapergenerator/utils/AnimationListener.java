package com.elisoft.wallpapergenerator.utils;

/**
 * Created by yvorobey on 9/2/2015.
 */
public interface AnimationListener {
    void onAnimationEnd();
}
