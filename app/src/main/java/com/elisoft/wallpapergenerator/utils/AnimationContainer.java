package com.elisoft.wallpapergenerator.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.widget.ImageView;

import com.elisoft.wallpapergenerator.R;

import java.lang.ref.SoftReference;

/**
 * Created by yvorobey on 9/2/2015.
 */
public class AnimationContainer {
    public int FPS = 70;  // animation FPS

    // single instance procedures
    private static AnimationContainer mInstance;

    private AnimationContainer() {
    };

    public static AnimationContainer getInstance() {
        if (mInstance == null)
            mInstance = new AnimationContainer();
        return mInstance;
    }
    public AnimationListener animationListener;

    // animation progress dialog frames
    private int[] mTransparentFrames = {R.drawable.test_1, R.drawable.test_2, R.drawable.test_3, R.drawable.test_4,
            R.drawable.test_5,R.drawable.test_6,R.drawable.test_7,R.drawable.test_8,R.drawable.test_9,R.drawable.test_10,R.drawable.test_11,R.drawable.test_12,
            R.drawable.test_13,R.drawable.test_14,R.drawable.test_15,R.drawable.test_16,R.drawable.test_17,R.drawable.test_18,R.drawable.test_19,R.drawable.test_20,R.drawable.test_21,
            R.drawable.test_22,R.drawable.test_23,R.drawable.test_24,R.drawable.test_25,R.drawable.test_26,R.drawable.test_27,R.drawable.test_28,R.drawable.test_29,R.drawable.test_30,
            R.drawable.test_31,R.drawable.test_32,R.drawable.test_33,R.drawable.test_34,R.drawable.test_35,R.drawable.test_36,R.drawable.test_37,R.drawable.test_38,R.drawable.test_39,
            R.drawable.test_40,R.drawable.test_41,R.drawable.test_42,R.drawable.test_43,R.drawable.test_44,R.drawable.test_45,R.drawable.test_46,R.drawable.test_47,R.drawable.test_48,
            R.drawable.test_49,R.drawable.test_50,R.drawable.test_51,R.drawable.test_52,R.drawable.test_53,R.drawable.test_54,R.drawable.test_55,R.drawable.test_56,R.drawable.test_57,
            R.drawable.test_58,R.drawable.test_59,R.drawable.test_60,R.drawable.test_61,R.drawable.test_62,R.drawable.test_63,R.drawable.test_64,R.drawable.test_65,R.drawable.test_66,
            R.drawable.test_67,R.drawable.test_68,R.drawable.test_69,R.drawable.test_70,R.drawable.test_71,R.drawable.test_72,R.drawable.test_73,R.drawable.test_74,R.drawable.test_75,
            R.drawable.test_76,R.drawable.test_77,R.drawable.test_78,R.drawable.test_79,R.drawable.test_80,R.drawable.test_81,R.drawable.test_82,R.drawable.test_83,R.drawable.test_84,R.drawable.test_85,
            R.drawable.test_86,R.drawable.test_87,R.drawable.test_88,R.drawable.test_89,R.drawable.test_90,R.drawable.test_91,R.drawable.test_92,R.drawable.test_93,R.drawable.test_94,R.drawable.test_95,
            R.drawable.test_96,R.drawable.test_97,R.drawable.test_98,R.drawable.test_99,R.drawable.test_100,R.drawable.test_101,R.drawable.test_102,R.drawable.test_103,R.drawable.test_104,R.drawable.test_105,
            R.drawable.test_106,R.drawable.test_107,R.drawable.test_108, R.drawable.test_109,R.drawable.test_110,R.drawable.test_111,R.drawable.test_112,R.drawable.test_113,R.drawable.test_114, R.drawable.test_115,
            R.drawable.test_116,R.drawable.test_117,R.drawable.test_118,R.drawable.test_119,R.drawable.test_120,R.drawable.test_121,R.drawable.test_122};

    private int[] mStarSkyFrames = {R.drawable.moon_1, R.drawable.moon_2, R.drawable.moon_3, R.drawable.moon_4, R.drawable.moon_5, R.drawable.moon_6, R.drawable.moon_7, R.drawable.moon_8,
        R.drawable.moon_9, R.drawable.moon_10, R.drawable.moon_11, R.drawable.moon_12, R.drawable.moon_13, R.drawable.moon_14, R.drawable.moon_15, R.drawable.moon_16, R.drawable.moon_17,
        R.drawable.moon_18, R.drawable.moon_19, R.drawable.moon_20, R.drawable.moon_21, R.drawable.moon_22, R.drawable.moon_23, R.drawable.moon_24, R.drawable.moon_25, R.drawable.moon_26,
        R.drawable.moon_27, R.drawable.moon_28, R.drawable.moon_29, R.drawable.moon_30, R.drawable.moon_31, R.drawable.moon_32, R.drawable.moon_33, R.drawable.moon_34,R.drawable.moon_35,
        R.drawable.moon_36, R.drawable.moon_37, R.drawable.moon_38, R.drawable.moon_39, R.drawable.moon_40, R.drawable.moon_41, R.drawable.moon_42, R.drawable.moon_43, R.drawable.moon_44,
        R.drawable.moon_45, R.drawable.moon_46, R.drawable.moon_47, R.drawable.moon_48, R.drawable.moon_49, R.drawable.moon_50, R.drawable.moon_51, R.drawable.moon_52, R.drawable.moon_53,
        R.drawable.moon_54, R.drawable.moon_55, R.drawable.moon_56, R.drawable.moon_57, R.drawable.moon_58, R.drawable.moon_59, R.drawable.moon_60, R.drawable.moon_61, R.drawable.moon_62,
        R.drawable.moon_63, R.drawable.moon_64, R.drawable.moon_65, R.drawable.moon_66, R.drawable.moon_67, R.drawable.moon_68, R.drawable.moon_69, R.drawable.moon_70, R.drawable.moon_71,
        R.drawable.moon_72, R.drawable.moon_73, R.drawable.moon_74, R.drawable.moon_75, R.drawable.moon_76, R.drawable.moon_77, R.drawable.moon_78, R.drawable.moon_79, R.drawable.moon_80,
        R.drawable.moon_82
    };
    // animation splash screen frames
//    private int[] mSplashAnimFrames = { R.drawable.logo_ding200480001, R.drawable.logo_ding200480002 };


    /**
     * @param imageView
     * @return progress dialog animation
     */
    public FramesSequenceAnimation createTransparentAnim(ImageView imageView) {

        return new FramesSequenceAnimation(imageView, mTransparentFrames, FPS);
    }

    public FramesSequenceAnimation createStarSkyAnim(ImageView imageView) {
        return new FramesSequenceAnimation(imageView, mStarSkyFrames, FPS);
    }





    /**
     * AnimationPlayer. Plays animation frames sequence in loop
     */
    public class FramesSequenceAnimation {
        private int[] mFrames; // animation frames
        private int mIndex; // current frame
        private boolean mShouldRun; // true if the animation should continue running. Used to stop the animation
        private boolean mIsRunning; // true if the animation currently running. prevents starting the animation twice
        private SoftReference<ImageView> mSoftReferenceImageView; // Used to prevent holding ImageView when it should be dead.
        private Handler mHandler;
        private int mDelayMillis;
//        private OnAnimationStoppedListener mOnAnimationStoppedListener;

        private Bitmap mBitmap = null;
        private BitmapFactory.Options mBitmapOptions;

        public FramesSequenceAnimation(ImageView imageView, int[] frames, int fps) {
            mHandler = new Handler();
            mFrames = frames;
            mIndex = -1;
            mSoftReferenceImageView = new SoftReference<ImageView>(imageView);
            mShouldRun = false;
            mIsRunning = false;
            mDelayMillis = 1000 / fps;

            imageView.setImageResource(mFrames[0]);

            // use in place bitmap to save GC work (when animation images are the same size & type)
            if (Build.VERSION.SDK_INT >= 11) {
                Bitmap bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
                int width = bmp.getWidth();
                int height = bmp.getHeight();
                Bitmap.Config config = bmp.getConfig();
                mBitmap = Bitmap.createBitmap(width, height, config);
                mBitmapOptions = new BitmapFactory.Options();
                // setup bitmap reuse options.
                mBitmapOptions.inBitmap = mBitmap;
                mBitmapOptions.inMutable = true;
                mBitmapOptions.inSampleSize = 1;
            }
        }

        private int getNext() {
            mIndex++;
            if (mIndex >= mFrames.length) {
                mIndex = mFrames.length -1;
                animationListener.onAnimationEnd();
            }
            return mFrames[mIndex];
        }

        /**
         * Starts the animation
         */
        public synchronized void start() {
            mShouldRun = true;
            if (mIsRunning)
                return;

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    ImageView imageView = mSoftReferenceImageView.get();
                    if (!mShouldRun || imageView == null) {
                        mIsRunning = false;
//                        if (animationListener != null) {
//                            animationListener.onAnimationEnd();
//                        }
                        return;
                    }

                    mIsRunning = true;
                    mHandler.postDelayed(this, mDelayMillis);

                    if (imageView.isShown()) {
                        int imageRes = getNext();
                        if (mBitmap != null) { // so Build.VERSION.SDK_INT >= 11
                            Bitmap bitmap = null;
                            try {
                                bitmap = BitmapFactory.decodeResource(imageView.getResources(), imageRes, mBitmapOptions);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (bitmap != null) {
                                imageView.setImageBitmap(bitmap);
                            } else {
                                imageView.setImageResource(imageRes);
                                mBitmap.recycle();
                                mBitmap = null;
                            }
                        } else {
                            imageView.setImageResource(imageRes);
                        }
                    }

                }
            };

            mHandler.post(runnable);
        }

        /**
         * Stops the animation
         */
        public synchronized void stop() {
            mShouldRun = false;
        }
    }
}

