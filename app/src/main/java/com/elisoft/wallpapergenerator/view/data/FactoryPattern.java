package com.elisoft.wallpapergenerator.view.data;

/**
 * Created by mBykov on 24.07.2015.
 */
public class FactoryPattern {
    public PatternItem createPattern (Pattern patternName, int imageResource){
        return new PatternItem(patternName.patternId,patternName.patternTitle, patternName.imgResource);// i will add patterns
    };
}
