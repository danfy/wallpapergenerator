package com.elisoft.wallpapergenerator.view.data;


import com.elisoft.wallpapergenerator.R;

/**
 * Created by mBykov on 24.07.2015.
 */

public enum Group {
    PATTERN_0(R.string.category_new_year, R.drawable.new_year_crop, R.color.blue_600, R.color.blue_200, R.color.blue_800, new Pattern[]{Pattern.P30, Pattern.P31, Pattern.P32, Pattern.P33, Pattern.P34, Pattern.P35}),
    PATTERN_1(R.string.category_textured, R.drawable.space_crop_main, R.color.green_600, R.color.green_200, R.color.green_800, new Pattern[]{Pattern.P19, Pattern.P20, Pattern.P21, Pattern.P22, Pattern.P23, Pattern.P25, Pattern.P26, Pattern.P27, Pattern.P28, Pattern.P29, Pattern.P36}),
    PATTERN_2(R.string.category_material, R.drawable.material_1_crop, R.color.pink_600, R.color.pink_200, R.color.material_status_bar_color,new Pattern[]{Pattern.P1, Pattern.P2, Pattern.P3, Pattern.P4, Pattern.P5, Pattern.P6, Pattern.P7, Pattern.P8}),
    PATTERN_3(R.string.category_transparent, R.drawable.transparent_1_crop,R.color.transparent_main_color,R.color.deep_orange_200, R.color.transparent_status_bar_color,new Pattern[]{Pattern.P9, Pattern.P10, Pattern.P11, Pattern.P24}),
    PATTERN_4(R.string.category_gradient, R.drawable.gradient_crop, R.color.cyan_600, R.color.cyan_200, R.color.gradient_status_bar_color,new Pattern[]{Pattern.P12, Pattern.P14,Pattern.P15, Pattern.P16, Pattern.P17, Pattern.P18}),
    PATTERN_5(R.string.category_star_sky,R.drawable.starsky_crop,R.color.star_sky_main_color, R.color.star_sky_second_color, R.color.star_sky_status_bar_color,new Pattern[]{Pattern.P13});


    int titleStringId,imgResource,categoryColorRes,colorBackground, statusBarColor;
    Pattern [] childrens;
    private Group(int titleStringId, int imgResource, int categoryColorRes,int colorBackground, int statusBarColor, Pattern [] childrens) {
        this.titleStringId = titleStringId;
        this.imgResource=imgResource;
        this.categoryColorRes=categoryColorRes;
        this.colorBackground=colorBackground;
        this.childrens=childrens;
        this.statusBarColor = statusBarColor;
    }
    }
/*
        patternListItems.add(new PatternGroup(getString(R.string.category_gradient), 0, getColor(R.color.cyan_600), getColor(R.color.cyan_200)));
        patternListItems.add(new PatternGroup(getString(R.string.category_star_sky), 0, getColor(R.color.teal_500), getColor(R.color.teal_200)));
*/
