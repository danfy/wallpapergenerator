package com.elisoft.wallpapergenerator.view.data;

import android.content.Context;

/**
 * Created by mBykov on 24.07.2015.
 */
public class FactoryPatternGroup {
    Context context;

    public FactoryPatternGroup(Context context) {
        this.context = context;
    }
    public PatternGroup createGroup (Group group){
       PatternGroup patternGroup=new  PatternGroup(group, context.getResources().getString(group.titleStringId), group.imgResource,context.getResources().getColor(group.categoryColorRes),
               context.getResources().getColor(  group.colorBackground), context.getResources().getColor(group.statusBarColor));// i will add patterns
       FactoryPattern factoryPattern=new FactoryPattern();
       for (Pattern item :group.childrens){
           patternGroup.patternItems.add(factoryPattern.createPattern(item,0));
       }
       return  patternGroup;
   };

}
