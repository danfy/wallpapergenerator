package com.elisoft.wallpapergenerator.view.animation;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 11/6/2015.
 */
public class CustomPatternAnimation extends PatternItemAnimation {
    private final ImageView animatedImage;
    /**
     * Constructor.
     *
     * @param animateFrameLayout - frame layout like container for views that represents icon.
     * @param backgroundFrame    - frame layout is for background.
     */
    public CustomPatternAnimation(FrameLayout animateFrameLayout, FrameLayout backgroundFrame) {
        super(animateFrameLayout, backgroundFrame);
        animatedImage = new ImageView(animateFrameLayout.getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        animateFrameLayout.addView(animatedImage, layoutParams);
        animatedImage.setImageResource(R.drawable.balloon_2);

    }

    @Override
    public void animate() {
        animateFrameLayout.setTranslationX(-300);
        animateFrameLayout.setTranslationY(300);
        animateFrameLayout.animate().setDuration(300).translationX(0).translationY(0).start();

    }

    @Override
    public void restoreView() {
        //Do nothing
    }
}
