package com.elisoft.wallpapergenerator.view.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;

import android.widget.FrameLayout;

/**
 * Created by yvorobey on 7/10/2015.
 */
public class FrameAboveListView extends FrameLayout {
    private static final String TAG = FrameAboveListView.class.getSimpleName();
    private Paint paint;
    private boolean isNeedDrawCircle;
    private float cx, cy, radius;
    private int colorResource;

    public FrameAboveListView(Context context) {
        super(context);
        init();
    }

    public FrameAboveListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FrameAboveListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FrameAboveListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }
    private void init() {
        isNeedDrawCircle = false;
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.RED);
        paint.setStyle(Paint.Style.FILL);
    }
    public void drawCircle(float cx, float cy, float radius, int colorResource) {
        this.cx = cx;
        this.cy = cy;
        this.radius = radius;
        this.colorResource = colorResource;
        isNeedDrawCircle = true;
        invalidate();
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(isNeedDrawCircle) {
            canvas.drawCircle(cx, cy, 150f, paint);
        }
    }
}
