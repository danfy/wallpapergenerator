package com.elisoft.wallpapergenerator.view.views;


import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;


import com.elisoft.wallpapergenerator.R;


/**
 * Created by yvorobey on 7/17/2015.
 */
public class PreLollyPopExpandedFABs extends AbstractExpandedFABs implements View.OnClickListener{
    private FloatingActionButton floatingActionButton;
    public PreLollyPopExpandedFABs(Context context) {
        super(context);
    }

    public PreLollyPopExpandedFABs(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PreLollyPopExpandedFABs(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PreLollyPopExpandedFABs(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected float getMainFAbY() {
        return floatingActionButton.getY();
    }

    @Override
    protected int getMainFabHeight() {
        return floatingActionButton.getHeight();
    }

    @Override
    protected void initMainFab(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.expanded_buttons, this);

        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab_action_main);
    }

    @Override
    protected void setMainFabListener() {
        floatingActionButton.setOnClickListener(this);
    }

    @Override
    protected void setMainFabResource(boolean isExpand) {
        if(isExpand) {
            floatingActionButton.setImageResource(R.drawable.ic_clear_white_24dp);
        } else {
            floatingActionButton.setImageResource(R.drawable.ic_settings_white_24dp);
        }
    }

    @Override
    protected void setMainFabEnabled(boolean isEnabled) {
        floatingActionButton.setEnabled(isEnabled);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == floatingActionButton.getId()) {
            if (isOpen) {
                collapseFAB();
                isOpen = false;
            } else {
                expandFAB();
                isOpen = true;
            }
        } else {
            super.onClick(v);
        }
    }
}
