package com.elisoft.wallpapergenerator.view.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.GridLayout;

/**
 * Created by yvorobey on 9/11/2015.
 */
public class ColorGridLayout extends GridLayout {
    private static final int MAIN_COLOR_COUNT_WITHOUT_BLACK_AND_WHITE = 19;
    private static final int MAIN_COLOR_COUNT = 21;

    public ColorGridLayout(Context context) {
        super(context);
    }

    public ColorGridLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ColorGridLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ColorGridLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void initializeParameters() {

    }

}
