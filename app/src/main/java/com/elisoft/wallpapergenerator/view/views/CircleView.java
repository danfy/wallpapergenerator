package com.elisoft.wallpapergenerator.view.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;

import com.elisoft.wallpapergenerator.R;


/**
 * Created by yvorobey on 7/10/2015.
 */
public class CircleView extends View{
    private static final String TAG = CircleView.class.getSimpleName();
    private float cx;
    private float cy;
    private float radius;
    private int colorResource = Color.RED;
    private Paint paint;
    private Paint shadowPaint;
    private int width;
    private int height;
//    public CircleView(Context context, float cX, float cY, float radius, int colorResource) {
//        super(context);
//        cx = cX;
//        cy = cY;
//        this.radius = radius;
//        this.colorResource = colorResource;
//
//
//    }
    public CircleView(Context context) {
        super(context);
        init();
    }
    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CircleView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        width = getWidth();
        height = getHeight();
        cx = width/2;
        cy = height/2;

    }

    private void init() {
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(colorResource);
        paint.setStyle(Paint.Style.FILL);
        shadowPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        shadowPaint.setStyle(Paint.Style.FILL);
        shadowPaint.setColor(Color.BLACK);
        shadowPaint.setAlpha(30);
        Resources resources = getContext().getResources();
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        radius = resources.getDimension(R.dimen.tool_bar_item_circle_width) *(displayMetrics.densityDpi/160f);

    }
    public void setColor(int color) {
        colorResource =color;
        paint.setColor(colorResource);

    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawCircle(cx,cy, radius, shadowPaint);
        canvas.drawCircle(cx, cy, radius, paint);
    }
}
