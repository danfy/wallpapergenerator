package com.elisoft.wallpapergenerator.view.animation;

import android.media.Image;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.elisoft.wallpapergenerator.R;

/**
 * Class to animate icon of material pattern.
 */
public class MaterialPatternAnimation extends PatternItemAnimation {





    public MaterialPatternAnimation(FrameLayout animateFrameLayout, FrameLayout backgroundFrame) {
        super(animateFrameLayout, backgroundFrame);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        final ImageView imageRectangle = new ImageView(animateFrameLayout.getContext());
        final ImageView imageCircle = new ImageView(animateFrameLayout.getContext());
        animateFrameLayout.addView(imageRectangle, layoutParams);
        animateFrameLayout.addView(imageCircle, layoutParams);
        imageRectangle.setImageResource(R.drawable.rectangle_2);
        imageCircle.setImageResource(R.drawable.ellipse_3);
    }

    @Override
    public void animate() {
        final ScaleAnimation scaleCircle = new ScaleAnimation(0f, 1f, 0f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        scaleCircle.setDuration(300);
        scaleCircle.setStartOffset(300);
        animateFrameLayout.setTranslationY(-300);
        animateFrameLayout.animate().setDuration(450).translationY(0).setInterpolator(new BounceInterpolator()).start();
        animateFrameLayout.getChildAt(1).startAnimation(scaleCircle);
    }

    @Override
    public void restoreView() {
        //Do nothing
    }

}
