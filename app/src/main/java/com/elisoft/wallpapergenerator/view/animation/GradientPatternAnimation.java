package com.elisoft.wallpapergenerator.view.animation;

import android.animation.Animator;

import android.media.Image;
import android.view.ViewGroup;

import android.widget.FrameLayout;
import android.widget.ImageView;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 8/26/2015.
 */
public class GradientPatternAnimation extends PatternItemAnimation {

    private final ImageView imageCircle;
    /**
     * Constructor.
     *
     * @param animateFrameLayout - frame layout like container for views that represents icon.
     */
    public GradientPatternAnimation(FrameLayout animateFrameLayout, FrameLayout backgroundFrame) {
        super(animateFrameLayout, backgroundFrame);
        final ImageView imageShadow = new ImageView(animateFrameLayout.getContext());
        imageCircle = new ImageView(animateFrameLayout.getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        animateFrameLayout.addView(imageShadow, layoutParams);
        animateFrameLayout.addView(imageCircle, layoutParams);
        imageShadow.setImageResource(R.drawable.gradient_ellipse_shadow);
        imageCircle.setImageResource(R.drawable.gradient_ellipse);

    }

    @Override
    public void animate() {
        animateFrameLayout.setTranslationX(300);
        imageCircle.setRotation(180);
        animateFrameLayout.animate().setDuration(300).translationX(0).start();
        imageCircle.animate().setDuration(300).rotation(0).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                backgroundFrame.getChildAt(0).setVisibility(View.GONE);
//                backgroundFrame.getChildAt(1).setBackgroundResource(R.drawable.gradient_background);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();
    }

    @Override
    public void restoreView() {
        //Do nothing
    }
}
