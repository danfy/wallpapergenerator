package com.elisoft.wallpapergenerator.view.views;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;

import com.elisoft.wallpapergenerator.R;

/**
 * Class uses fot expanded buttons. Abstract class for LollyPop and pre-LollyPop expanded buttons.
 */
public abstract class AbstractExpandedFABs extends RelativeLayout implements View.OnClickListener{
    protected static final int ANIMATION_DURATION = 250;
    protected float offset1, offset2, offset3, offset4;
    protected FabWithTextView setAsWallpaperAction, editorAction, shareAction, effectsEditMode;
    protected boolean isOpen = false;

    protected int itemHeight;
    protected OnButtonExpandedClickListener listener;
    protected View view;
    protected int margin;
    public interface OnButtonExpandedClickListener {

        void onSetAsWallpaperClicked();
        void onAddToFavoritesClicked();
        void onShareButtonClicked();
        void onEffectEditModeClicked();
        void onExpandButtons();
        void onCollapseButtons();
    }

    public AbstractExpandedFABs(Context context) {
        super(context);
        init(context);
    }

    public AbstractExpandedFABs(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AbstractExpandedFABs(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AbstractExpandedFABs(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }



    public void setOnButtonExpandedClickListener(OnButtonExpandedClickListener listener) {
        this.listener = listener;
    }

    protected void init(final Context context){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            margin = context.getResources().getDimensionPixelSize(R.dimen.tools_fab_margin);
        }
        initMainFab(context);
        setAsWallpaperAction = (FabWithTextView) view.findViewById(R.id.fab_action_save);
        editorAction = (FabWithTextView) view.findViewById(R.id.fab_action_color_fill);
        shareAction = (FabWithTextView) view.findViewById(R.id.fab_action_share);
        effectsEditMode = (FabWithTextView) view.findViewById(R.id.fab_action_effect_mode);
        this.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                AbstractExpandedFABs.this.getViewTreeObserver().removeOnPreDrawListener(this);
                offset1 = getMainFAbY() - margin+ setAsWallpaperAction.getY();
                itemHeight =getMainFabHeight()/4;
                setAsWallpaperAction.setTranslationY(-offset1);
                setAsWallpaperAction.setVisibility(GONE);
                offset2 = getMainFAbY() - margin + editorAction.getY();

                editorAction.setTranslationY(-offset2);
                editorAction.setVisibility(GONE);
                offset3 = getMainFAbY() - margin + shareAction.getY();

                shareAction.setTranslationY(-offset3);
                shareAction.setVisibility(GONE);
                offset4 = getMainFAbY() - margin + effectsEditMode.getY();
                effectsEditMode.setTranslationY(-offset4);
                effectsEditMode.setVisibility(GONE);
                return false;
            }
        });
        setButtonsListeners();
    }
    protected abstract float getMainFAbY();
    protected abstract int getMainFabHeight();
    protected abstract void initMainFab(final Context context);
    protected abstract void setMainFabListener();
    protected abstract void setMainFabResource(boolean isExpand);
    protected abstract void setMainFabEnabled(boolean isEnabled);

    protected void setButtonsListeners() {
        setMainFabListener();
        setAsWallpaperAction.setFABClickListener(this);
        editorAction.setFABClickListener(this);
        shareAction.setFABClickListener(this);
        effectsEditMode.setFABClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == setAsWallpaperAction.getFAB()) {
            collapseFAB();
            listener.onSetAsWallpaperClicked();
        }
        if(v == editorAction.getFAB()) {
            collapseFAB();
            listener.onAddToFavoritesClicked();
        }
        if(v == shareAction.getFAB()) {
            collapseFAB();
            listener.onShareButtonClicked();
        }
        if(v == effectsEditMode.getFAB()) {
            collapseFAB();
            listener.onEffectEditModeClicked();
        }
    }

    public void expandFAB() {

        isOpen = true;
        setMainFabResource(true);
        setVisibleButtons();
        final AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(createExpandAnimation(setAsWallpaperAction, -offset1, 0), createExpandAnimation(editorAction, -offset2, 0), createExpandAnimation(shareAction, -offset3, 0), createExpandAnimation(effectsEditMode, -offset4, 0));
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                setMainFabEnabled(false);

            }

            @Override
            public void onAnimationEnd(Animator animation) {
//                final AnimatorSet animator = new AnimatorSet();
//                animator.playTogether(createExpandAnimation(setAsWallpaperAction, itemHeight, 0), createExpandAnimation(editorAction, itemHeight, 0), createExpandAnimation(shareAction, itemHeight,0));
//                animator.start();
                listener.onExpandButtons();
                setMainFabEnabled(true);

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        animatorSet.start();
    }

    public void collapseFAB() {

        isOpen = false;
        setMainFabResource(false);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(createCollapseAnimation(setAsWallpaperAction, -offset1), createCollapseAnimation(editorAction, -offset2), createCollapseAnimation(shareAction, -offset3), createCollapseAnimation(effectsEditMode, -offset4));

        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                setMainFabEnabled(false);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                setMainFabEnabled(true);
                setInvisibleButtons();
                listener.onCollapseButtons();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        animatorSet.start();
    }
    private void setVisibleButtons() {
        setAsWallpaperAction.setVisibility(VISIBLE);
        editorAction.setVisibility(VISIBLE);
        shareAction.setVisibility(VISIBLE);
        effectsEditMode.setVisibility(VISIBLE);
    }
    private void setInvisibleButtons() {
        setAsWallpaperAction.setVisibility(GONE);
        editorAction.setVisibility(GONE);
        shareAction.setVisibility(GONE);
        effectsEditMode.setVisibility(GONE);
    }

    private Animator createExpandAnimation(View view, float offset, float offsetTo) {
        Animator animator = ObjectAnimator.ofFloat(view, TRANSLATION_Y, offset, offsetTo);
        animator.setDuration(ANIMATION_DURATION);
        return animator;
    }
    private Animator createCollapseAnimation(View view, float offset) {
        Animator animator = ObjectAnimator.ofFloat(view, TRANSLATION_Y, 0, offset);
        animator.setDuration(ANIMATION_DURATION);
        return  animator;
    }
    public boolean isOpen() {
        return isOpen;
    }
}
