package com.elisoft.wallpapergenerator.view.views.lollypop;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.view.views.AbstractExpandedFABs;

/**
 * Created by yvorobey on 8/28/2015.
 */
public class LollyPopExpandFABs extends AbstractExpandedFABs implements FloatingActionButton.OnCheckedChangeListener {
    private FloatingActionButton floatingActionButton;
    public LollyPopExpandFABs(Context context) {
        super(context);
    }

    public LollyPopExpandFABs(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LollyPopExpandFABs(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LollyPopExpandFABs(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected float getMainFAbY() {
        return floatingActionButton.getY();
    }

    @Override
    protected int getMainFabHeight() {

        return floatingActionButton.getHeight();
    }

    @Override
    protected void initMainFab(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(R.layout.expanded_buttons, this);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab_action_main);
    }

    @Override
    protected void setMainFabListener() {
        floatingActionButton.setOnCheckedChangeListener(this);
    }

    @Override
    protected void setMainFabResource(boolean isExpand) {
        //Do nothing
        if(!isExpand) {
            floatingActionButton.setChecked(false);
        }
    }

    @Override
    protected void setMainFabEnabled(boolean isEnabled) {

            floatingActionButton.setEnabled(isEnabled);

    }

    @Override
    public void onCheckedChanged(FloatingActionButton fab, boolean isChecked) {
            if(isChecked) {
                expandFAB();
            } else {
                collapseFAB();
            }
    }
}
