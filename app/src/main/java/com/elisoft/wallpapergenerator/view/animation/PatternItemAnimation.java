package com.elisoft.wallpapergenerator.view.animation;

import android.widget.FrameLayout;



/**
 * Class is parent for all animation classes of pattern items icon.
 */
public abstract class PatternItemAnimation {
    protected FrameLayout animateFrameLayout;
    protected FrameLayout backgroundFrame;
    /**
     * Constructor.
     * @param animateFrameLayout - frame layout like container for views that represents icon.
     * @param backgroundFrame - frame layout is for background.
     */
    public PatternItemAnimation(FrameLayout animateFrameLayout, FrameLayout backgroundFrame) {
        this.animateFrameLayout = animateFrameLayout;
        this.backgroundFrame = backgroundFrame;
    }


    public abstract void animate();

    public abstract void restoreView();
}
