package com.elisoft.wallpapergenerator.view.animation;

import android.animation.Animator;


import android.view.animation.BounceInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;


import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.utils.AnimationContainer;
import com.elisoft.wallpapergenerator.utils.AnimationListener;

/**
 * Created by yvorobey on 8/26/2015.
 */
public class StarSkyPatternAnimation extends PatternItemAnimation {
    private ImageView imageView;
    /**
     * Constructor.
     *
     * @param animateFrameLayout - frame layout like container for views that represents icon.
     */
    public StarSkyPatternAnimation(FrameLayout animateFrameLayout, FrameLayout backgroundFrame) {
        super(animateFrameLayout, backgroundFrame);
        imageView = new ImageView(animateFrameLayout.getContext());
        animateFrameLayout.addView(imageView);

//        imageView.setImageResource(R.drawable.star_sky_animation);
    }

    @Override
    public void animate() {
//        animateFrameLayout.setTranslationY(-300);
//        animateFrameLayout.animate().setDuration(300).translationY(0).setInterpolator(new BounceInterpolator()).start();
//        ((AnimationDrawable) imageView.getDrawable()).start();
        final AnimationContainer animationContainer = AnimationContainer.getInstance();

        final AnimationContainer.FramesSequenceAnimation framesSequenceAnimation = animationContainer.createStarSkyAnim(imageView);
        animateFrameLayout.setTranslationY(-300);
        animateFrameLayout.animate().setDuration(300).translationY(0).setInterpolator(new BounceInterpolator()).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                framesSequenceAnimation.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        }).start();



        animationContainer.animationListener = new AnimationListener() {
            @Override
            public void onAnimationEnd() {
                framesSequenceAnimation.stop();
            }
        };
    }

    @Override
    public void restoreView() {
        imageView.setImageResource(R.drawable.moon_82);
    }
}
