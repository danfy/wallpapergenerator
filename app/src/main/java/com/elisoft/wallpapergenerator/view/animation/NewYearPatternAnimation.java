package com.elisoft.wallpapergenerator.view.animation;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 12/23/2015.
 */
public class NewYearPatternAnimation extends PatternItemAnimation {
    private final ImageView animatedImageView;

    public NewYearPatternAnimation(FrameLayout animateFrameLayout, FrameLayout backgroundFrame) {
        super(animateFrameLayout, backgroundFrame);
        animatedImageView = new ImageView(animateFrameLayout.getContext());
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        animateFrameLayout.addView(animatedImageView, layoutParams);
        animatedImageView.setImageResource(R.drawable.snowman_snowman);


    }

    @Override
    public void animate() {
        animateFrameLayout.setAlpha(0);
        animateFrameLayout.animate().setDuration(500).alpha(1).start();

    }

    @Override
    public void restoreView() {
        //Do nothing
    }
}
