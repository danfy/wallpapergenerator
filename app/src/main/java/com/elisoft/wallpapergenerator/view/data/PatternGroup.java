package com.elisoft.wallpapergenerator.view.data;

import java.util.ArrayList;

/**
 * Class represents object is used to populate the list adapter.
 * Contains reference to an image, title and color resource.
 */
public class PatternGroup {
    public static String ID_PATTERN_GROUP="ID_PATTERN_GPROUP";
    public Enum GROUP;
    ArrayList <PatternItem> patternItems;

    private String mTitle; //Title of list view item.
    private int mImgResource;   //Image resource for pattern category.
    private int mColorTitle; //Color  for pattern category.
    private int mColorBackground;
    private int mStatusBarColor;


    public PatternGroup(Group GROUP, String title, int imgResource, int categoryColorRes, int colorBackground, int statusBarColor) {
        this.GROUP=GROUP;
        mTitle = title;
        mImgResource = imgResource;
        mColorTitle = categoryColorRes;
        mColorBackground = colorBackground;
        mStatusBarColor = statusBarColor;
        patternItems=new ArrayList<PatternItem>();
    }

    public int getStatusBarColor() {
        return mStatusBarColor;
    }
    public void setStatusBarColor(int statusBarColor) {
        mStatusBarColor = statusBarColor;
    }

    public int getColorBackground() {
        return mColorBackground;
    }

    public void setColorBackground(int colorBackground) {
        this.mColorBackground = colorBackground;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public int getImgResource() {
        return mImgResource;
    }

    public void setImgResource(int imgResource) {
        this.mImgResource = imgResource;
    }

    public int getColorTitleResource() {
        return mColorTitle;
    }

    public void setColorTitleResource(int colorTitleResource) {
        this.mColorTitle = colorTitleResource;
    }
    public ArrayList <PatternItem>  getPatternItems (){
        return  patternItems;
    }
}
