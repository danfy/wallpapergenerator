package com.elisoft.wallpapergenerator.view.views;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.elisoft.wallpapergenerator.R;

/**
 * Class for pattern items in grid view
 */
public class GridViewPatternItem extends RelativeLayout {
    private static final String TAG = GridViewPatternItem.class.getSimpleName();
    private  CircleView circleView;
    public GridViewPatternItem(Context context) {
        super(context);
    }

    public GridViewPatternItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridViewPatternItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GridViewPatternItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {

        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    public void animateClick() {
        circleView = new CircleView(getContext());
        circleView.setColor(R.color.red_300);

        circleView.animate().scaleX(5f).scaleY(5f).setDuration(300).setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                removeCircle();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }
    public void removeCircle() {
        removeView(circleView);
    }
}
