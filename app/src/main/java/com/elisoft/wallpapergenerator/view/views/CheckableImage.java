package com.elisoft.wallpapergenerator.view.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 8/25/2015.
 */
public class CheckableImage extends FrameLayout implements Checkable {
    private boolean isChecked = false;
    private ImageView imageWallpaper;
    private FrameLayout checkedImage;

    public CheckableImage(Context context) {
        super(context);
        init(context);
    }

    public CheckableImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CheckableImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CheckableImage(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.checkable_image, this);
        imageWallpaper = (ImageView) view.findViewById(R.id.image_wallpaper);
        checkedImage = (FrameLayout) view.findViewById(R.id.image_checked);
    }

    @Override
    public void setChecked(boolean checked) {
        if(checked!=isChecked) {
            isChecked = checked;
        }
        if(isChecked) {
            checkedImage.setVisibility(View.VISIBLE);
        } else {
            checkedImage.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked);
    }

    public ImageView getImageWallpaper() {
        return imageWallpaper;
    }
    public void setScaleType(ImageView.ScaleType scaleType) {
        imageWallpaper.setScaleType(scaleType);
    }

}
