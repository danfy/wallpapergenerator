package com.elisoft.wallpapergenerator.view.data;

/**
 * Created by yvorobey on 7/1/2015.
 */
public class PatternItem {
    public static String ID_PATTERN="ID_PATTERN";

    private int mId;
    private String mTitle;
    private int mImageResource;


     PatternItem(int id,String title, int imageResource) {
         this.mId = id;
        mTitle = title;
        mImageResource = imageResource;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public int getImageResource() {
        return mImageResource;
    }

    public void setImageResource(int imageResource) {
        this.mImageResource = imageResource;
    }
}
