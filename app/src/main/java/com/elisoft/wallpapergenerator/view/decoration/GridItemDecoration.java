package com.elisoft.wallpapergenerator.view.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 7/13/2015.
 */
public class GridItemDecoration extends RecyclerView.ItemDecoration {
    private int spaceBetween;

    public GridItemDecoration(int space) {
        spaceBetween = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.bottom = spaceBetween;


//        if(parent.getChildAdapterPosition(view) <2) {
//            outRect.top = spaceBetween;
//        }
        if(parent.getChildAdapterPosition(view)%2 ==0) {
            outRect.left = spaceBetween;
            outRect.right = spaceBetween/2;
        } else {
            outRect.left = spaceBetween/2;
            outRect.right = spaceBetween;
        }


    }

}
