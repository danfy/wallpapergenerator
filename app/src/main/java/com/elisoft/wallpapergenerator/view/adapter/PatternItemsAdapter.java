package com.elisoft.wallpapergenerator.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.elisoft.wallpapergenerator.ItemClickListener;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.favorites.utils.ImageResizer;
import com.elisoft.wallpapergenerator.view.data.PatternItem;

/**
 * Created by yvorobey on 7/1/2015.
 */
public class PatternItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = PatternItemsAdapter.class.getSimpleName();
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private boolean isFirstCreate = true;
    private List<PatternItem> list;
    public ItemClickListener mItemClickListener;
    private ImageResizer imageResizer;
    private int width, height;
    public PatternItemsAdapter(Context context, List<PatternItem> list) {
        this.list = list;
        imageResizer = new ImageResizer(context, 0,0);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if(i == TYPE_ITEM) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pattern_item_view, viewGroup, false);
            return new PatternItemHolder(view);
        }else if(i == TYPE_HEADER) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.empty_header, viewGroup, false);
            return new HeaderHolder(view);
        }
        return null;

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int i) {


        if(viewHolder instanceof PatternItemHolder) {
            final PatternItem patternItem = getItem(i);
            final PatternItemHolder patternItemHolder = (PatternItemHolder) viewHolder;
            patternItemHolder.title.setText(patternItem.getTitle());
//        viewHolder.patternImage.setImageResource(patternItem.getImageResource());
            if(isFirstCreate) {
                patternItemHolder.patternImage.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        patternItemHolder.patternImage.getViewTreeObserver().removeOnPreDrawListener(this);
                        width = patternItemHolder.patternImage.getWidth();
                        height = patternItemHolder.patternImage.getHeight();
                        imageResizer.setImageSize(width,height);
                        imageResizer.loadImage(patternItem.getImageResource(), patternItemHolder.patternImage);
                        isFirstCreate = false;
                        return false;
                    }
                });
            } else {
                imageResizer.setImageSize(width, height);
                imageResizer.loadImage(patternItem.getImageResource(), patternItemHolder.patternImage);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public boolean isHeader(int position) {
        return position==0 || position == 1;
    }
    @Override
    public int getItemViewType(int position) {
        if(isHeader(position)) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }
    private PatternItem getItem(int position) {
        return list.get(position-2);
    }
    
    @Override
    public int getItemCount() {
        return list.size() +2;
    }



    class PatternItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private ImageView patternImage;
        public PatternItemHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title_item_pattern);
            patternImage = (ImageView) itemView.findViewById(R.id.img_item_pattern);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            mItemClickListener.onItemClick(v, getAdapterPosition()-2);
            notifyItemChanged(getAdapterPosition() -2);
        }
    }
    class HeaderHolder extends RecyclerView.ViewHolder {

        public HeaderHolder(View itemView) {
            super(itemView);
        }
    }
}
