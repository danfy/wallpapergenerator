package com.elisoft.wallpapergenerator.view.animation;

import android.support.v7.widget.RecyclerView;

import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;

/**
 * Created by yvorobey on 7/13/2015.
 */
public class GridItemAnimation extends Animation {
    private RecyclerView recyclerView;
    private long duration;
    private long step;


    private ScaleAnimation scaleAnimation;
    public GridItemAnimation(RecyclerView view) {
        recyclerView = view;

        scaleAnimation = new ScaleAnimation(0f,1f,0f,1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);

    }

    @Override
    public void setDuration(long durationMillis) {
        super.setDuration(durationMillis);
        duration = durationMillis;
        step = duration/3;
        scaleAnimation.setDuration(step);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        if(interpolatedTime <step) {
            recyclerView.getChildAt(0).setAnimation(scaleAnimation);
            scaleAnimation.start();
            recyclerView.postInvalidate();
        }
        if(interpolatedTime>step && interpolatedTime<step*2) {
            recyclerView.getChildAt(1).setAnimation(scaleAnimation);
            recyclerView.getChildAt(2).setAnimation(scaleAnimation);
            scaleAnimation.start();
            recyclerView.postInvalidate();
        }
        if(interpolatedTime>=step*2) {
            recyclerView.getChildAt(3).setAnimation(scaleAnimation);
            recyclerView.getChildAt(4).setAnimation(scaleAnimation);
            recyclerView.getChildAt(5).setAnimation(scaleAnimation);
            scaleAnimation.start();
            recyclerView.postInvalidate();
        }
    }

    @Override
    public void start() {
        super.start();


    }
}
