package com.elisoft.wallpapergenerator.view.animation;

import android.widget.FrameLayout;
import android.widget.ImageView;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.utils.AnimationContainer;
import com.elisoft.wallpapergenerator.utils.AnimationListener;

/**
 * Created by yvorobey on 8/25/2015.
 */
public class TransparentPatternAnimation extends PatternItemAnimation  {
    private ImageView imageView;
    /**
     * Constructor.
     *
     * @param animateFrameLayout - frame layout like container for views that represents icon.
     */
    public TransparentPatternAnimation(FrameLayout animateFrameLayout, FrameLayout backgroundFrame) {
        super(animateFrameLayout, backgroundFrame);
        imageView = new ImageView(animateFrameLayout.getContext());


        animateFrameLayout.addView(imageView);
//        AsyncLoadDrawable asyncLoadDrawable = new AsyncLoadDrawable(animateFrameLayout.getContext(), imageView);
//
//        asyncLoadDrawable.execute(R.drawable.test_animation);
//        imageView.setImageResource(R.drawable.test_animation);


    }

    @Override
    public void animate() {

        final AnimationContainer animationContainer = AnimationContainer.getInstance();
        final AnimationContainer.FramesSequenceAnimation framesSequenceAnimation = animationContainer.createTransparentAnim(imageView);
        framesSequenceAnimation.start();
        animationContainer.animationListener = new AnimationListener() {
            @Override
            public void onAnimationEnd() {
                framesSequenceAnimation.stop();
            }
        };
//        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getDrawable();
//        animationDrawable.start();
    }

    @Override
    public void restoreView() {
        imageView.setImageResource(R.drawable.test_122);
    }


}
