package com.elisoft.wallpapergenerator.view.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.R;


/**
 * Created by yvorobey on 8/3/2015.
 */
public class ScrollableFrame extends FrameLayout {
    private static final String TAG = ScrollableFrame.class.getSimpleName();
    private FrameLayout frameLayout;
    private TextView textView;
    private FrameLayout animatedFrame;
    private RecyclerView recyclerView;
    private FrameLayout.LayoutParams layoutParams;


    private int maxTranslationVisible = 0;


    public ScrollableFrame(Context context) {
        super(context);
        init(context);
    }

    public ScrollableFrame(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScrollableFrame(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ScrollableFrame(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }
    private void init(Context context) {
        layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        maxTranslationVisible = context.getResources().getDimensionPixelSize(R.dimen.tool_bar_item_height);

    }
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        frameLayout = (FrameLayout) getChildAt(1);
        textView = (TextView) findViewById(R.id.title_pattern);
        animatedFrame = (FrameLayout) findViewById(R.id.img_pattern_type);
        recyclerView = (RecyclerView) getChildAt(0);
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                frameLayout.setTranslationY(frameLayout.getTranslationY() - dy);

                frameLayout.setAlpha((maxTranslationVisible + frameLayout.getTranslationY()) / maxTranslationVisible);
//                textView.setScaleX((maxTranslationVisible + frameLayout.getTranslationY()) / maxTranslationVisible);
//                textView.setScaleY((maxTranslationVisible + frameLayout.getTranslationY()) / maxTranslationVisible);
//                animatedFrame.setScaleX((maxTranslationVisible + frameLayout.getTranslationY()) / maxTranslationVisible);
//                animatedFrame.setScaleY((maxTranslationVisible + frameLayout.getTranslationY()) / maxTranslationVisible);

            }
        });
    }

    public float getTopFrameTranslationY() {
        return frameLayout.getTranslationY();
    }


}
