package com.elisoft.wallpapergenerator.view.animation;

import android.widget.FrameLayout;

/**
 * Created by yvorobey on 8/26/2015.
 */
public class PatternItemAnimFactory {
    private static final int NEW_YEAR_GROUP = 0;
    private static final int CUSTOM_GROUP = 1;
    private static final int MATERIAL_GROUP = 2;
    private static final int TRANSPARENT_GROUP = 3;
    private static final int GRADIENT_GROUP = 4;
    private static final int STAR_GROUP = 5;
    public static PatternItemAnimation getAnimation(int id, FrameLayout animatedFrameLayout, FrameLayout backgroundFrame) {
        PatternItemAnimation patternItemAnimation = null;
        if(id == MATERIAL_GROUP) {
            patternItemAnimation = new MaterialPatternAnimation(animatedFrameLayout, backgroundFrame);
        } else if(id == TRANSPARENT_GROUP) {
            patternItemAnimation = new TransparentPatternAnimation(animatedFrameLayout,backgroundFrame);
        } else if(id == GRADIENT_GROUP) {
            patternItemAnimation = new GradientPatternAnimation(animatedFrameLayout,backgroundFrame);
        } else if( id == STAR_GROUP) {
            patternItemAnimation = new StarSkyPatternAnimation(animatedFrameLayout,backgroundFrame);
        } else if(id == CUSTOM_GROUP) {
            patternItemAnimation = new CustomPatternAnimation(animatedFrameLayout, backgroundFrame);
        } else if(id == NEW_YEAR_GROUP) {
            patternItemAnimation = new NewYearPatternAnimation(animatedFrameLayout, backgroundFrame);
        }

        return patternItemAnimation;
    }
}
