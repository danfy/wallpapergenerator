package com.elisoft.wallpapergenerator.view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import com.elisoft.wallpapergenerator.ItemClickListener;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.favorites.utils.ImageResizer;
import com.elisoft.wallpapergenerator.view.data.PatternGroup;


/**
 * Created by yvorobey on 6/30/2015.
 */
public class CategoryPatternsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = CategoryPatternsAdapter.class.getSimpleName();
    private List<PatternGroup> mData;

    public ItemClickListener mItemClickListener;


    private ImageResizer imageResizer;
    public CategoryPatternsAdapter(Context context, int layoutViewResourceId, List<PatternGroup> mData) {
        this.mData = mData;


        imageResizer = new ImageResizer(context, 0,0);


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        final View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pattern_category_item, viewGroup, false);


        return new ViewHolder(v);
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final PatternGroup record = mData.get(position);
        final ViewHolder hold =(ViewHolder)holder;
        hold.relativeLayout.setBackgroundColor(record.getColorTitleResource());
        hold.titleTextView.setText(record.getTitle());
//        hold.imageView.setImageResource(record.getImgResource());
        ViewTreeObserver viewTreeObserver = hold.imageView.getViewTreeObserver();
        viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                hold.imageView.getViewTreeObserver().removeOnPreDrawListener(this);


                imageResizer.setImageSize(hold.imageView.getWidth(), hold.imageView.getHeight());
                imageResizer.loadImage(record.getImgResource(), hold.imageView);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imageView;
        TextView titleTextView;
        RelativeLayout relativeLayout;
        private ImageView icon;
        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = (TextView) itemView.findViewById(R.id.title_pattern_category);
            imageView = (ImageView) itemView.findViewById(R.id.img_pattern_category);
            relativeLayout=(RelativeLayout)itemView.findViewById(R.id.item_category_container);
            itemView.setOnClickListener(this);
        }



        @Override
        public void onClick(View v) {
            mItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

/*    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PatternGroup object = mData.get(position);

        if(convertView ==null) {
            convertView = mLayoutInflater.inflate(R.layout.pattern_category_item, parent, false);
        }
        ImageView imageView = (ImageView)convertView.findViewById(R.id.img_pattern_category);
        TextView titleTextView = (TextView) convertView.findViewById(R.id.title_pattern_category);
        RelativeLayout relativeLayout = (RelativeLayout) convertView.findViewById(R.id.item_category_container);
        relativeLayout.setBackgroundColor(object.getColorTitleResource());
        titleTextView.setText(object.getTitle());
        return convertView;
    }*/

    private void animateViewWhenCreate(View view) {
        view.setAlpha(0);
        view.setTranslationY(mTranslation);
        long startTime = 300;
        view.animate().alpha(1).translationY(0).setStartDelay(200).setInterpolator(new DecelerateInterpolator()).setDuration(startTime).start();


    }
    private float mTranslation = 500;
    public void setTranslation(boolean up) {
      if(up) {
          mTranslation = -500;
      } else {
          mTranslation = 500;
      }
    }


}
