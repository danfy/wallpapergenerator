package com.elisoft.wallpapergenerator.view.data;


import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.wallpapergenerator.R;

/**
 * Created by mBykov on 24.07.2015.
 */
public enum  Pattern {
    //Material design group
    P1 ("Pattern1", PatternsIds.BOTTOM_TRIANGLES_PATTERN, R.drawable.material_1_crop),
    P2 ("Pattern2",PatternsIds.BOTTOM_TRIANGLES_WITH_CIRCLE_PATTERN,R.drawable.material_2_crop),
    P3 ("Pattern3",PatternsIds.RECTANGLES_FROM_TOP_PATTERN, R.drawable.material_3_crop),
    P4 ("Pattern4",PatternsIds.RECTANGLES_FROM_TOP_WITH_CIRCLE_PATTERN, R.drawable.material_4_crop),
    P5 ("Pattern5",PatternsIds.CIRCLE_PATTERN, R.drawable.material_5_crop),
    P6 ("Pattern6",PatternsIds.CROSSING_RECTANGLES_PATTERN, R.drawable.material_6_crop),
    P7 ("Pattern7",PatternsIds.CROSSING_RECTANGLES_WITH_CIRCLE_PATTERN, R.drawable.material_7_crop),
    P8 ("Pattern8",PatternsIds.TRIANGLES_FROM_ONE_ANGLE, R.drawable.material_8_crop),

    //Transparent shapes group
    P9 ("Pattern1",PatternsIds.TRANSPARENT_CIRCLES, R.drawable.transparent_1_crop),
    P10("Pattern2",PatternsIds.BUBBLES, R.drawable.transparent_2_crop),
    P11("Pattern3", PatternsIds.SQUARE_BUBBLES,R.drawable.transparent_3_crop),
    P24("Pattern4", PatternsIds.NON_FLAT_SQUARE_BUBBLES, R.drawable.transparent_4_crop),

    //Gradient group
    P12("Pattern1", PatternsIds.RADIAL_GRADIENT,R.drawable.gradient_crop),
    P14("Pattern2", PatternsIds.COLORED_RADIAL_GRADIENT, R.drawable.gradient_2_crop),
    P15("Pattern3", PatternsIds.HORIZONTAL_LINEAR_GRADIENT, R.drawable.gradient_3_crop),
    P16("Pattern4", PatternsIds.COLOR_HORIZONTAL_LINEAR_GRADIENT, R.drawable.gradient_4_crop),
    P17("Pattern5", PatternsIds.VERTICAL_LINEAR_GRADIENT, R.drawable.gradient_5_crop),
    P18("Pattern6", PatternsIds.COLOR_VERTICAL_LINEAR_GRADIENT, R.drawable.gradient_6_crop),
    //Star sky group
    P13 ("Pattern1", PatternsIds.STARS_SKY,R.drawable.starsky_crop),


    P19("Pattern1", PatternsIds.MOUNTAINS_WITH_ICE, R.drawable.mountains_with_ice_crop),
    P20("Pattern2", PatternsIds.MOUNTAINS_WITH_MOON, R.drawable.mountains_with_moon_crop),
    P21("Pattern3", PatternsIds.BALLOONS_PATTERN, R.drawable.balloons_crop),
    P22("Pattern4", PatternsIds.SPACE_SHIP_PATTERN, R.drawable.space_crop_main),
    P23("Pattern5", PatternsIds.HILLS_PATTERN, R.drawable.hills_crop),
    P25("Pattern6", PatternsIds.PARK_PATTERN, R.drawable.park_crop),
    P26("Pattern7", PatternsIds.BEACH_PATTERN, R.drawable.beach_crop),
    P27("Pattern8", PatternsIds.LIGHT_HOUSE, R.drawable.light_house_crop),
    P28("Pattern9", PatternsIds.NATURE_PATTERN, R.drawable.nature_crop),
    P29("Pattern10", PatternsIds.MOUNTAINS_AND_SEA, R.drawable.mountains_and_see_crop),

    P30("Pattern1", PatternsIds.SNOWMAN_PATTERN, R.drawable.snowman_crop),
    P31("Pattern2", PatternsIds.ABSTRACT_CHRISTMAS_TREE, R.drawable.abstract_cristmas_tree_crop),
    P32("Pattern3", PatternsIds.CHRISTMAS_CITY, R.drawable.christmas_city_crop),
    P33("Pattern4", PatternsIds.CHRISTMAS_TREE, R.drawable.christmas_tree_crop),
    P34("Pattern5", PatternsIds.CHRISTMAS_MOUNTAINS, R.drawable.christmas_mountains_crop),
    P35("Pattern6", PatternsIds.CHRISTMAS_ROOM, R.drawable.christmas_room_crop),
    P36("Pattern7", PatternsIds.SUNSET, R.drawable.sunset_crop);



    int patternId, imgResource;
    String patternTitle;

    private Pattern(String patternTitle,int patternId, int imgResource) {
        this.patternTitle = patternTitle;
        this.patternId = patternId;
        this.imgResource = imgResource;
    }
}
