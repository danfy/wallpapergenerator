package com.elisoft.wallpapergenerator.view.views.lollypop;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Outline;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.Checkable;
import android.widget.FrameLayout;

/**
 * Class represents Floating action button lolly pop style.
 * Use this class only for lolly pop and higher.
 */
public class FloatingActionButton extends FrameLayout implements Checkable {

    public static interface OnCheckedChangeListener {
        void onCheckedChanged(FloatingActionButton fab, boolean isChecked);
    }

    private static final int[] CHECKED_STATE_SET = {
            android.R.attr.state_checked
    };

    private boolean mChecked;
    private OnCheckedChangeListener mOnCheckedChangeListener;


    public FloatingActionButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0,0);
    }

    public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context,attrs,defStyleAttr,0);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr);
        setClickable(true);
        setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0,0,getWidth(), getHeight());
            }
        });
        setClipToOutline(true);
    }

    public FloatingActionButton(Context context) {
        this(context, null, 0, 0);

    }

    @Override
    public void setChecked(boolean checked) {
        if(checked == mChecked) {
            return;
        }
        mChecked = checked;
        refreshDrawableState();
        if(mOnCheckedChangeListener!=null) {
            mOnCheckedChangeListener.onCheckedChanged(this,checked);
        }
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        mOnCheckedChangeListener = listener;
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }
    public boolean performClick() {
        toggle();
        return super.performClick();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        invalidateOutline();
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        }
        return drawableState;
    }
}
