package com.elisoft.wallpapergenerator.view.views;

import android.content.Context;
import android.graphics.Canvas;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * Created by yvorobey on 7/2/2015.
 */
public class PatternCategoryCardView extends CardView {

    private static final String TAG = PatternCategoryCardView.class.getSimpleName();

    public PatternCategoryCardView(Context context) {
        super(context);


    }

    public PatternCategoryCardView(Context context, AttributeSet attrs) {
        super(context, attrs);


    }

    public PatternCategoryCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        initViews();
    }

    private void initViews() {


    }




    @Override
    protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

    }

}
