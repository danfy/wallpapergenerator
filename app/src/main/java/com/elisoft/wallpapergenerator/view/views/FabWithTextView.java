package com.elisoft.wallpapergenerator.view.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.R;


/**
 * Created by yvorobey on 7/21/2015.
 */
public class FabWithTextView extends RelativeLayout {
    private TextView actionTitle;
    private View fabView;

    public FabWithTextView(Context context) {
        super(context);
    }

    public FabWithTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FabWithTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public FabWithTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.FabWithTextView, 0, 0);
        String actionText = typedArray.getString(R.styleable.FabWithTextView_action_title);
        int color = typedArray.getColor(R.styleable.FabWithTextView_fab_button_color, Color.WHITE);
        int srcReference = typedArray.getResourceId(R.styleable.FabWithTextView_fab_button_img, R.drawable.ic_settings_white_24dp);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout relativeLayout = (RelativeLayout) layoutInflater.inflate(R.layout.fab_with_text, this,true);

        actionTitle = (TextView) relativeLayout.findViewById(R.id.title_action);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fabView= (com.elisoft.wallpapergenerator.view.views.lollypop.FloatingActionButton)relativeLayout.findViewById(R.id.fab_button);
            final ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.fab_icon);
            imageView.setImageResource(srcReference);
            int fabBackground = typedArray.getResourceId(R.styleable.FabWithTextView_fab_lolly_background, R.drawable.fab_tools_background);
            ((com.elisoft.wallpapergenerator.view.views.lollypop.FloatingActionButton)fabView).setBackgroundResource(fabBackground);
        } else {
            fabView = (FloatingActionButton) relativeLayout.findViewById(R.id.fab_button);
            ((FloatingActionButton)fabView).setImageResource(srcReference);

//        floatingActionButton.setBackgroundDrawable(new ColorDrawable(color));
            ((FloatingActionButton)fabView).setBackgroundTintList(new ColorStateList(new int[][]{new int[]{0}}, new int[]{color}));
        }
        typedArray.recycle();
        actionTitle.setText(actionText);




    }

    public void setFABClickListener(OnClickListener onClickListener) {
        fabView.setOnClickListener(onClickListener);
    }


    public View getFAB() {
        return fabView;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }
}
