package com.elisoft.wallpapergenerator.favorites.app;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;


import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.app.AbstractShareActivity;
import com.elisoft.wallpapergenerator.app.AppApplication;
import com.google.android.gms.analytics.HitBuilders;

/**
 * Created by yvorobey on 7/28/2015.
 */
public class ActivityGallery extends AbstractShareActivity {
    private static final String TAG = "ImageGridActivity";

    @Override
    protected void setItemId() {
        mItemId = R.id.my_wallpapers;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppApplication.sendGoogleAnalyticsScreenName("Screen : " + getString(R.string.screen_my_wallpapers));
        if(checkPermission(PERMISSION_WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_EXTERNAL_STORAGE)) {
            initializeFragment();
        }

    }

    @Override
    protected void permissionWriteExternalStorageGranted() {
        initializeFragment();
    }

    @Override
    protected void permissionWriteExternalStorageNotGranted() {
        this.finish();
    }

    private void initializeFragment() {
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        if (getSupportFragmentManager().findFragmentByTag(TAG) == null) {
            final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.gallery_fragment_container, new ImageGridFragment(), TAG);
            ft.commitAllowingStateLoss();
        }
    }
}
