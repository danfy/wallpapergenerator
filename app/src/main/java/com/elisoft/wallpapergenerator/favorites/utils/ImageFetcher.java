/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.elisoft.wallpapergenerator.favorites.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * A simple subclass of {@link ImageResizer} that fetches and resizes images fetched from a URL.
 */
public class ImageFetcher extends ImageResizer {

    private static final int HTTP_CACHE_SIZE = 10 * 1024 * 1024; // 10MB
    private static final String WALLPAPER_CACHE_DIR = "wallpapers";
    private static final int IO_BUFFER_SIZE = 8 * 1024;

    private DiskLruCache mWallpaperDiskCache;
    private File mWallpaperCacheDir;
    private boolean mWallpaperDiskCacheStarting = true;
    private final Object mWallpaperDiskCacheLock = new Object();
    private static final int DISK_CACHE_INDEX = 0;

    /**
     * Initialize providing a target image width and height for the processing images.
     *
     * @param context
     * @param imageWidth
     * @param imageHeight
     */
    public ImageFetcher(Context context, int imageWidth, int imageHeight) {
        super(context, imageWidth, imageHeight);
        init(context);
    }

    /**
     * Initialize providing a single target image size (used for both width and height);
     *
     * @param context
     * @param imageSize
     */
    public ImageFetcher(Context context, int imageSize) {
        super(context, imageSize);
        init(context);
    }

    private void init(Context context) {
//        checkConnection(context);
        mWallpaperCacheDir = ImageCache.getDiskCacheDir(context, WALLPAPER_CACHE_DIR);
    }

    @Override
    protected void initDiskCacheInternal() {
        super.initDiskCacheInternal();
        initWallpaperDiskCache();
    }

    private void initWallpaperDiskCache() {
        if (!mWallpaperCacheDir.exists()) {
            mWallpaperCacheDir.mkdirs();
        }
        synchronized (mWallpaperDiskCacheLock) {
            if (ImageCache.getUsableSpace(mWallpaperCacheDir) > HTTP_CACHE_SIZE) {
                try {
                    mWallpaperDiskCache = DiskLruCache.open(mWallpaperCacheDir, 1, 1, HTTP_CACHE_SIZE);

                } catch (IOException e) {
                    mWallpaperDiskCache = null;
                }
            }
            mWallpaperDiskCacheStarting = false;
            mWallpaperDiskCacheLock.notifyAll();
        }
    }

    @Override
    protected void clearCacheInternal() {
        super.clearCacheInternal();
        synchronized (mWallpaperDiskCacheLock) {
            if (mWallpaperDiskCache != null && !mWallpaperDiskCache.isClosed()) {
                try {
                    mWallpaperDiskCache.delete();

                } catch (IOException e) {
                   e.printStackTrace();
                }
                mWallpaperDiskCache = null;
                mWallpaperDiskCacheStarting = true;
                initWallpaperDiskCache();
            }
        }
    }

    @Override
    protected void flushCacheInternal() {
        super.flushCacheInternal();
        synchronized (mWallpaperDiskCacheLock) {
            if (mWallpaperDiskCache != null) {
                try {
                    mWallpaperDiskCache.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void closeCacheInternal() {
        super.closeCacheInternal();
        synchronized (mWallpaperDiskCacheLock) {
            if (mWallpaperDiskCache != null) {
                try {
                    if (!mWallpaperDiskCache.isClosed()) {
                        mWallpaperDiskCache.close();
                        mWallpaperDiskCache = null;

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

//    /**
//    * Simple network connection check.
//    *
//    * @param context
//    */
//    private void checkConnection(Context context) {
//        final ConnectivityManager cm =
//                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
//        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
//
//
//        }
//    }


    /**
     * The main process method, which will be called by the ImageWorker in the AsyncTask background
     * thread.
     *
     * @param data The data to load the bitmap, in this case, a regular http URL
     * @return The downloaded and resized bitmap
     */
    private Bitmap processBitmap(String data) {


        final String key = data;
        FileDescriptor fileDescriptor = null;
        FileInputStream fileInputStream = null;
        File file = new File(data);
        DiskLruCache.Snapshot snapshot;
        synchronized (mWallpaperDiskCacheLock) {
            // Wait for disk cache to initialize
            while (mWallpaperDiskCacheStarting) {
                try {
                    mWallpaperDiskCacheLock.wait();
                } catch (InterruptedException e) {}
            }

            if (mWallpaperDiskCache != null) {
                try {
                    snapshot = mWallpaperDiskCache.get(key);
                    if (snapshot == null) {

                        DiskLruCache.Editor editor = mWallpaperDiskCache.edit(key);
                        if (editor != null) {
                            if (downloadUrlToStream(data,
                                    editor.newOutputStream(DISK_CACHE_INDEX))) {
                                editor.commit();
                            } else {
                                editor.abort();
                            }
                        }
                        snapshot = mWallpaperDiskCache.get(key);
                    }
                    if (snapshot != null) {
                        fileInputStream =
                                (FileInputStream) snapshot.getInputStream(DISK_CACHE_INDEX);
                        fileDescriptor = fileInputStream.getFD();
                    }
                } catch (IOException e) {

                } catch (IllegalStateException e) {

                } finally {
                    if (fileDescriptor == null && fileInputStream != null) {
                        try {
                            fileInputStream.close();
                        } catch (IOException e) {}
                    }
                }
            }
        }

        Bitmap bitmap = null;
        if (fileDescriptor != null) {
            bitmap = decodeSampledBitmapFromDescriptor(fileDescriptor, mImageWidth,
                    mImageHeight, getImageCache());
        }
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e) {}
        }
        return bitmap;
    }

    @Override
    protected Bitmap processBitmap(Object data) {
        return processBitmap(String.valueOf(data));
    }

    /**
     * Download a bitmap from a URL and write the content to an output stream.
     *
     * @param urlString The URL to fetch
     * @return true if successful, false otherwise
     */
    public boolean downloadUrlToStream(String urlString, OutputStream outputStream) throws IOException {
//        disableConnectionReuseIfNecessary();
//        HttpURLConnection urlConnection = null;
        FileInputStream fileIO = null;
        BufferedOutputStream out = null;
        BufferedInputStream in = null;

        try {
//            final URL url = new URL(urlString);
            final File file = new File(urlString);
//            urlConnection = (HttpURLConnection) url.openConnection();
            fileIO = new FileInputStream(file);
            in = new BufferedInputStream(fileIO, IO_BUFFER_SIZE);
            out = new BufferedOutputStream(outputStream, IO_BUFFER_SIZE);

            int b;
            while ((b = in.read()) != -1) {
                out.write(b);
            }
            return true;
        } catch (final IOException e) {

        } finally {
            if(fileIO!=null) {
                fileIO.close();
            }
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (final IOException e) {}
        }
        return false;
    }

    /**
     * Workaround for bug pre-Froyo, see here for more info:
     * http://android-developers.blogspot.com/2011/09/androids-http-clients.html
     */
    public static void disableConnectionReuseIfNecessary() {
        // HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");
        }
    }
}
