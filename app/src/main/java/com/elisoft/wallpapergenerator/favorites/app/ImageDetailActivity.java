/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.elisoft.wallpapergenerator.favorites.app;

import android.annotation.TargetApi;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.graphics.Point;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;


import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.Toast;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.app.AbstractShareActivity;
import com.elisoft.wallpapergenerator.app.AppApplication;
import com.elisoft.wallpapergenerator.favorites.FavoriteSaveLoad;
import com.elisoft.wallpapergenerator.favorites.utils.WallpaperSdFetcher;
import com.google.android.gms.analytics.HitBuilders;


public class ImageDetailActivity extends AbstractShareActivity implements OnClickListener {
    private static final String IMAGE_CACHE_DIR = "images";
    public static final String EXTRA_IMAGE = "extra_image";

    private ImagePagerAdapter mAdapter;
    private WallpaperSdFetcher mImageFetcher;
    private ViewPager mPager;
    private FavoriteSaveLoad favoriteSaveLoad;


    @Override
    protected void setItemId() {

    }

    @TargetApi(VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.image_detail_pager);
        favoriteSaveLoad = new FavoriteSaveLoad();
        // Fetch screen height and width, to use as our max size when loading images as this
        // activity runs full screen
        final DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int height = displayMetrics.heightPixels;
        final int width = displayMetrics.widthPixels;

        // For this sample we'll use half of the longest width to resize our images. As the
        // image scaling ensures the image is larger than this, we should be left with a
        // resolution that is appropriate for both portrait and landscape. For best image quality
        // we shouldn't divide by 2, but this will use more memory and require a larger memory
        // cache.
        final int longest = (height > width ? height : width) / 2;
        ArrayList<String> paths = favoriteSaveLoad.getChildrenPathAsList();
//        ImageCache.ImageCacheParams cacheParams =
//                new ImageCache.ImageCacheParams(this, IMAGE_CACHE_DIR);
//        cacheParams.setMemCacheSizePercent(0.25f); // Set memory cache to 25% of app memory

        // The ImageFetcher takes care of loading images into our ImageView children asynchronously
        mImageFetcher = new WallpaperSdFetcher(this, longest);
//        mImageFetcher.addImageCache(getSupportFragmentManager(), null);
        mImageFetcher.setImageFadeIn(false);

        // Set up ViewPager and backing adapter
        mAdapter = new ImagePagerAdapter(getSupportFragmentManager(), paths);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.setPageMargin((int) getResources().getDimension(R.dimen.horizontal_page_margin));
        mPager.setOffscreenPageLimit(2);

        // Set up activity to go full screen
//        getWindow().addFlags(LayoutParams.FLAG_FULLSCREEN);

        // Enable some additional newer visibility and ActionBar features to create a more
        // immersive photo viewing experience

            final android.support.v7.app.ActionBar actionBar = getSupportActionBar();
            if(actionBar!=null) {
                // Hide title text and set home as up
                actionBar.setDisplayShowTitleEnabled(false);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
            // Hide and show the ActionBar as the visibility changes
//            mPager.setOnSystemUiVisibilityChangeListener(
//                    new View.OnSystemUiVisibilityChangeListener() {
//                        @Override
//                        public void onSystemUiVisibilityChange(int vis) {
//                            if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
//                                if(actionBar!=null)
//                                actionBar.hide();
//                            } else {
//                                if(actionBar!=null)
//                                actionBar.show();
//                            }
//                        }
//                    });

            // Start low profile mode and hide ActionBar
//            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
//        if(actionBar!=null)
//            actionBar.hide();


        // Set the current item based on the extra passed in to this activity
        final int extraCurrentItem = getIntent().getIntExtra(EXTRA_IMAGE, -1);
        if (extraCurrentItem != -1) {
            mPager.setCurrentItem(extraCurrentItem);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mImageFetcher.setExitTasksEarly(false);
        AppApplication.sendGoogleAnalyticsScreenName("Screen : " + getString(R.string.screen_my_wallpapers_item));
    }

    @Override
    protected void onPause() {
        super.onPause();
        mImageFetcher.setExitTasksEarly(true);
        mImageFetcher.flushCache();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mImageFetcher.closeCache();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
//                NavUtils.navigateUpFromSameTask(this);
                onBackPressed();
                return true;
            case R.id.action_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(ImageDetailActivity.this);
                builder.setMessage(getString(R.string.text_delete_single_item_dialog));
                builder.setPositiveButton(getString(R.string.text_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String filepath = mAdapter.getData().get(mPager.getCurrentItem());
                        final File file = new File(filepath);
                        AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_delete), "Delete in gallery item name : " + file.getName());
                        if (file.delete()) {
                            Toast.makeText(ImageDetailActivity.this, getString(R.string.text_file) +" " + file.getName() + " " + getString(R.string.text_has_been_deleted), Toast.LENGTH_SHORT).show();
                            mAdapter.removeView(mPager, mPager.getCurrentItem());

                        }
                    }
                });
                builder.setNegativeButton(getString(R.string.text_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();


                return true;
            case R.id.set_as_wallpaper:
                AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_set_wallpaper_in_gallery), "Set wallpaper from gallery file : " + mAdapter.pathFiles.get(mPager.getCurrentItem()));
                WallpaperManager wallpaperManager= WallpaperManager.getInstance(ImageDetailActivity.this);;
                Display display = getWindowManager().getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                int screenHeight = point.y;
                Bitmap bitmap = BitmapFactory.decodeFile(mAdapter.pathFiles.get(mPager.getCurrentItem()));
                int width = bitmap.getWidth();
                width = (width*screenHeight)/bitmap.getHeight();
                try {
                    wallpaperManager.setBitmap(Bitmap.createScaledBitmap(bitmap, width, screenHeight,true));
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    bitmap.recycle();
                }

//                try {
//                    Bitmap bitmap = forceDecodeBitmap(mAdapter.pathFiles.get(mPager.getCurrentItem()));
//                    wallpaperManager.setBitmap(bitmap);
//                    System.gc();
//                    wallpaperManager.clear();
//                    wallpaperManager.setStream(new FileInputStream(mAdapter.pathFiles.get(mPager.getCurrentItem())));
//                    Uri uri = Uri.fromFile(new File(mAdapter.pathFiles.get(mPager.getCurrentItem())));
//                    wallpaperManager.forgetLoadedWallpaper();
//                    wallpaperManager.clear();
//                    ImageDetailActivity.this.setWallpaper(new FileInputStream(mAdapter.pathFiles.get(mPager.getCurrentItem())));

//                    Display display = getWindowManager().getDefaultDisplay();
//                    Point size = new Point();
//                    display.getSize(size);
//                    int screenHeight = size.y;
//                    Bitmap bitmap = BitmapFactory.decodeFile(mAdapter.pathFiles.get(mPager.getCurrentItem()));
//                    int width = bitmap.getWidth();
//                    width = (width*screenHeight)/bitmap.getHeight();
//                    wallpaperManager.setBitmap(Bitmap.createScaledBitmap(bitmap, width, screenHeight, true));

//
//                    final FileInputStream fileInputStream = new FileInputStream(mAdapter.pathFiles.get(mPager.getCurrentItem()));
//                    wallpaperManager.setStream(fileInputStream);
//                    fileInputStream.close();
//                    Toast.makeText(ImageDetailActivity.this, getString(R.string.text_image_set_as_wallpaper), Toast.LENGTH_SHORT).show();
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//
//                }
//                finally {
//                    wallpaperManager = null;
//                }



               return  true;


//            case R.id.clear_cache:
//                mImageFetcher.clearCache();
//                Toast.makeText(
//                        this, R.string.clear_cache_complete_toast, Toast.LENGTH_SHORT).show();
//                return true;
            case R.id.action_share:
                AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_share_in_gallery_item),"Share in gallery item file : " + mAdapter.getData().get(mPager.getCurrentItem()));

                //TODO : change String text with link to APP in Google Play
                shareSingleObject("This image created with WallpaperGenerator http://www.elinext.com/", mAdapter.getData().get(mPager.getCurrentItem()));
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_wallpaper_view, menu);



        return super.onCreateOptionsMenu(menu);
    }



    /**
     * Called by the ViewPager child fragments to load images via the one ImageFetcher
     */
    public WallpaperSdFetcher getImageFetcher() {
        return mImageFetcher;
    }

    @Override
    protected void permissionWriteExternalStorageGranted() {

    }

    @Override
    protected void permissionWriteExternalStorageNotGranted() {

    }


    /**
     * The main adapter that backs the ViewPager. A subclass of FragmentStatePagerAdapter as there
     * could be a large number of items in the ViewPager and we don't want to retain them all in
     * memory at once but create/destroy them on the fly.
     */
    private class ImagePagerAdapter extends FragmentStatePagerAdapter {
        private final ArrayList<String> pathFiles;


        public ImagePagerAdapter(FragmentManager fm, ArrayList<String> pathFiles) {
            super(fm);
            this.pathFiles = pathFiles;
        }

        public ArrayList<String> getData() {
            return pathFiles;
        }
        @Override
        public int getCount() {
            return pathFiles.size();
        }

        @Override
        public Fragment getItem(int position) {
            return ImageDetailFragment.newInstance(pathFiles.get(position));
        }


        public int removeView(ViewPager viewPager, int position) {
            viewPager.setAdapter(null);
            pathFiles.remove(position);
            viewPager.setAdapter(this);
            return position;
        }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
    }

}

    /**
     * Set on the ImageView in the ViewPager children fragments, to enable/disable low profile mode
     * when the ImageView is touched.
     */
    @TargetApi(VERSION_CODES.HONEYCOMB)
    @Override
    public void onClick(View v) {
        final int vis = mPager.getSystemUiVisibility();
        if ((vis & View.SYSTEM_UI_FLAG_LOW_PROFILE) != 0) {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        } else {
            mPager.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(ImageDetailActivity.this, ActivityGallery.class));

        super.onBackPressed();

    }
    private Bitmap forceDecodeBitmap(String path) throws FileNotFoundException {
        Bitmap bmp = null;

        while(bmp == null){
            bmp = BitmapFactory.decodeStream(new FileInputStream(path));
        }

        return bmp;
    }

}
