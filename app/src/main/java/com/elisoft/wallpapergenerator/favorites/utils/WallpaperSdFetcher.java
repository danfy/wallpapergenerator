package com.elisoft.wallpapergenerator.favorites.utils;

import android.content.Context;
import android.graphics.Bitmap;

/**
 * Created by yvorobey on 7/28/2015.
 */
public class WallpaperSdFetcher extends ImageResizer {

    /**
     * Initialize providing a target image width and height for the processing images.
     *
     * @param context
     * @param imageWidth
     * @param imageHeight
     */
    public WallpaperSdFetcher(Context context, int imageWidth, int imageHeight) {
        super(context, imageWidth, imageHeight);
    }

    /**
     * Initialize providing a single target image size (used for both width and height);
     *
     * @param context
     * @param imageSize
     */
    public WallpaperSdFetcher(Context context, int imageSize) {
        super(context, imageSize);
    }

    private Bitmap processBitmap(String path) {
        Bitmap bitmap = null;

        if(path !=null) {
            bitmap = decodeSampledBitmapFromFile(path, mImageWidth, mImageHeight, null);

        }
        return bitmap;
    }

    @Override
    protected Bitmap processBitmap(Object data) {
        return processBitmap(String.valueOf(data));
    }
}
