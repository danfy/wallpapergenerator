package com.elisoft.wallpapergenerator.favorites;

import android.os.Environment;

import java.io.File;
import java.util.ArrayList;

import com.elisoft.wallpapergenerator.wall.wallpapers.Config;

/**
 * Class saves and loads bitmap to sd card directory for gallery.
 */
public class FavoriteSaveLoad {
    /**
     * Method checks if external storage is writable.
     * @return true - if external storage is writable, false otherwise.
     */
    private boolean isExternalStorageEnviable() {
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)) {
            return  true;
        } else {
            return false;
        }
    }

    /**
     * Get folder to save bitmap favorite wallpaper
     * @return
     */
    public String getFolder() {
        String folder = null;
        if (isExternalStorageEnviable()) {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString()+"/"+ Config.getFolderName());
            file.mkdirs();
            if (file.exists()) {
                folder = file.getPath();
            }
        } else {
            folder = Environment.getRootDirectory().getPath();
        }
        return folder;}

    /**
     * Return file to save wallpaper.
     * @param patternId - id of current pattern. Need for analytics.
     * @return - File object to save wallpaper.
     */
    public File getFileToSave(int patternId) {
        File f = new File(getFolder(), Config.getWallpaperName()+ "_" + patternId+ "."+Config.getWallpaperFormarString());
        return f;
    }

    public String[] getChildrenPath() {
        String path = getFolder();
        File file = new File(path);
       File[] files = file.listFiles() ;
        final String[] filesPath = new String[files.length];
        for (int i = 0; i< files.length; i++) {

            filesPath[i] = files[i].getAbsolutePath();
        }
        return filesPath;
    }
    public ArrayList<String> getChildrenPathAsList() {
        String path = getFolder();
        if(path == null) {
            return new ArrayList<>();
        }
        File file = new File(path);
        File[] files = file.listFiles();

        final ArrayList<String> arrayList = new ArrayList<>();
        if(files == null) {
            return arrayList;
        }
        for (int i = 0; i< files.length; i++) {
            arrayList.add(files[i].getAbsolutePath());
        }
        return arrayList;
    }



}
