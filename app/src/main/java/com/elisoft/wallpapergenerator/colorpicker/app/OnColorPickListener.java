package com.elisoft.wallpapergenerator.colorpicker.app;

/**
 * Created by yvorobey on 9/14/2015.
 */
public interface OnColorPickListener {
    void onPickColor(int color, int mode);

}
