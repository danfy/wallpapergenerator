package com.elisoft.wallpapergenerator.colorpicker.app;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import android.view.Gravity;

import android.view.Window;

import android.widget.RelativeLayout;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.colorpicker.ColorPickerBuilder;

/**
 * Created by yvorobey on 9/11/2015.
 */
public class ColorPickerFragmentDialog extends DialogFragment {
    public static final String BUNDLE_PICK_COLOR = "pick";
    public static final int BUNDLE_SINGLE_COLOR_CLICKED = 0;
    public static final int BUNDLE_FIRST_COLOR_CLICKED = 1;
    public static final int BUNDLE_SECOND_COLOR_CLICKED = 2;

    private OnColorPickListener onColorPickListener;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final ColorPickerBuilder colorPickerBuilder = new ColorPickerBuilder(getActivity());
        final int clickedMode = getArguments().getInt(BUNDLE_PICK_COLOR);
        builder.setView(colorPickerBuilder.build());
        builder.setTitle(getString(R.string.text_choose_color));
        builder.setPositiveButton(getString(R.string.text_select), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               if(colorPickerBuilder.getColorCheckedItem()!=null) {
                   onColorPickListener.onPickColor(colorPickerBuilder.getColorCheckedItem().getMainColor(), clickedMode);
               }

            }
        });
        Dialog dialog = builder.create();
        Window window = dialog.getWindow();
        window.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            onColorPickListener = (OnColorPickListener) activity;
        } catch (ClassCastException e) {

        }

    }
}
