package com.elisoft.wallpapergenerator.colorpicker.adapter;

import android.content.Context;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;


import com.elisoft.wallpapergenerator.colorpicker.data.ColorItem;
import com.elisoft.wallpapergenerator.colorpicker.view.ColorView;


import java.util.ArrayList;

/**
 * Created by yvorobey on 9/14/2015.
 */
public class MainColorAdapter extends BaseAdapter {
    private ArrayList<ColorItem> colorItems;
    private Context mContext;

    public MainColorAdapter(Context context, ArrayList<ColorItem> colorItemArrayList) {
        mContext = context;

        colorItems = colorItemArrayList;

    }


    @Override
    public int getCount() {
        return colorItems.size();
    }

    @Override
    public Object getItem(int position) {
        return colorItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ColorItem colorItem = (ColorItem) getItem(position);
        ColorView colorView = new ColorView(mContext, colorItem.getMainColor(), colorItem.getSecondColor());

        return colorView;
    }
}
