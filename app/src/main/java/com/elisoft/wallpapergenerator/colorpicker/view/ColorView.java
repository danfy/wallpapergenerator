package com.elisoft.wallpapergenerator.colorpicker.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Checkable;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 9/11/2015.
 */
public class ColorView extends View implements Checkable {

    private int mColor;
    private int mColorStroke;
    private Paint paint;
    private Paint strokePaint;
    private Paint checkedPaint;
    private int width;
    private int height;
    private float cx;
    private float cy;
    private float radius;
    private boolean isChecked = false;

    public ColorView(Context context) {
        super(context);
        mColor = 0;
        mColorStroke = 0;
        init();
    }

    public ColorView(Context context, int color, int colorStroke) {
        super(context);
        mColor = color;
        mColorStroke = colorStroke;
        init();
    }

    public ColorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mColor = 0;
        mColorStroke = 0;
        init();
    }

    public ColorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mColor = 0;
        mColorStroke = 0;
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ColorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mColor = 0;
        mColorStroke = 0;
        init();
    }

    private void init() {
        height = width = getContext().getResources().getDimensionPixelSize(R.dimen.color_picker_item);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(mColor);

        paint.setStyle(Paint.Style.FILL);
        strokePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        strokePaint.setStrokeWidth(1);
        strokePaint.setColor(mColorStroke);
        strokePaint.setStyle(Paint.Style.STROKE);

        checkedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        checkedPaint.setColor(Color.WHITE);
        checkedPaint.setStyle(Paint.Style.FILL);


    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        setMeasuredDimension(width, height);

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        cx = (right - left)/2;
        cy = (bottom - top)/2;
        radius = width/2;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawCircle(cx, cy, radius, paint);
        canvas.drawCircle(cx, cy, radius-2, strokePaint);
        if(isChecked) {
            canvas.drawCircle(cx, cy, radius/2, checkedPaint);
        }
    }

    @Override
    public void setChecked(boolean checked) {
        isChecked = checked;
        invalidate();
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public boolean performClick() {
        setChecked(true);
        return super.performClick();

    }

    @Override
    public void toggle() {
        isChecked = !isChecked;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

//        if(event.getAction() == MotionEvent.ACTION_DOWN) {
//            if(isChecked) {
//                setChecked(false);
//            } else {
//                setChecked(true);
//            }
//
//        }
        return super.onTouchEvent(event);
    }

    public void setMainColor(int color) {
        mColor = color;
        paint.setColor(mColor);
    }
    public void setStrokeColor(int color) {
        mColorStroke = color;
        paint.setColor(mColorStroke);
    }
}
