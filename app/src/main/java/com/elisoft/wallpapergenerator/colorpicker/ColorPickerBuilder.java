package com.elisoft.wallpapergenerator.colorpicker;

import android.content.Context;


import android.view.Gravity;

import android.view.View;


import android.widget.AbsListView;

import android.widget.GridView;
import android.widget.RelativeLayout;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.colorpicker.adapter.MainColorAdapter;
import com.elisoft.wallpapergenerator.colorpicker.data.ColorItem;

import java.util.ArrayList;


/**
 * Created by yvorobey on 9/11/2015.
 */
public class ColorPickerBuilder {
    private final GridView gridView;
    private final RelativeLayout relativeLayout;
    private MainColorAdapter mainColorAdapter;
    public ColorPickerBuilder(Context context) {
        relativeLayout = (RelativeLayout) View.inflate(context, R.layout.grid_color_picker, null);
        gridView = (GridView)relativeLayout.findViewById(R.id.grid_color_picker) ;
        gridView.setNumColumns(context.getResources().getInteger(R.integer.column_count_color_palette));
        gridView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        setGridParams(context);
    }

    public RelativeLayout build() {
        return relativeLayout;
    }

    public GridView getGridColorView() {
        return gridView;
    }
    private void setGridParams(Context context) {
        ArrayList<ColorItem> colorItems = new ArrayList<>();
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.red_500), getColorFromResource(context, R.color.red_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.pink_500), getColorFromResource(context, R.color.pink_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.purple_500), getColorFromResource(context, R.color.purple_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.deep_purple_500), getColorFromResource(context, R.color.deep_purple_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.indigo_500), getColorFromResource(context,R.color.indigo_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.blue_500), getColorFromResource(context, R.color.blue_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.light_blue_500), getColorFromResource(context, R.color.light_blue_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.cyan_500), getColorFromResource(context,R.color.cyan_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.teal_500), getColorFromResource(context, R.color.teal_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.green_500), getColorFromResource(context, R.color.green_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.light_green_500), getColorFromResource(context, R.color.light_green_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.lime_500), getColorFromResource(context, R.color.lime_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.yellow_500), getColorFromResource(context, R.color.yellow_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.amber_500), getColorFromResource(context, R.color.amber_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.orange_500), getColorFromResource(context, R.color.orange_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.deep_orange_500), getColorFromResource(context, R.color.deep_orange_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.brown_500), getColorFromResource(context, R.color.brown_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.grey_500), getColorFromResource(context, R.color.grey_700)));
        colorItems.add(new ColorItem(getColorFromResource(context, R.color.blue_grey_500), getColorFromResource(context, R.color.blue_grey_700)));


        mainColorAdapter = new MainColorAdapter(context, colorItems);

        gridView.setAdapter(mainColorAdapter);
        gridView.setGravity(Gravity.CENTER);

        gridView.setVerticalSpacing(10);
        gridView.setHorizontalSpacing(10);
//        GridLayout.Spec row1 = GridLayout.spec(0);
//        GridLayout.Spec row2 = GridLayout.spec(1);
//        GridLayout.Spec row3 = GridLayout.spec(2);
//        GridLayout.Spec row4 = GridLayout.spec(3);
//
//        GridLayout.Spec col1 = GridLayout.spec(0);
//        GridLayout.Spec col2 = GridLayout.spec(1);
//        GridLayout.Spec col3 = GridLayout.spec(2);
//        GridLayout.Spec col4 = GridLayout.spec(3);
//        GridLayout.Spec col5 = GridLayout.spec(4);
//        GridLayout.Spec col6 = GridLayout.spec(5);

//        ColorView firstColor = new ColorView(context, getColorFromResource(context, R.color.red_500), getColorFromResource(context, R.color.red_800));
//        addViewToGrid(firstColor, row1, col1);
//
//        ColorView secondColor = new ColorView(context, getColorFromResource(context, R.color.pink_500), getColorFromResource(context, R.color.pink_800));
//        addViewToGrid(secondColor, row1, col2);
//
//        ColorView thirdColor = new ColorView(context, getColorFromResource(context,R.color.purple_500),getColorFromResource(context, R.color.purple_800));
//        addViewToGrid(thirdColor, row1, col3);
//
//        ColorView fourthColor = new ColorView(context, getColorFromResource(context, R.color.deep_purple_500),getColorFromResource(context, R.color.deep_purple_800));
//        addViewToGrid(fourthColor, row1, col4);
//
//        ColorView fifthColor = new ColorView(context, getColorFromResource(context, R.color.indigo_500),getColorFromResource(context, R.color.indigo_800));
//        addViewToGrid(fifthColor, row1, col5);
//
//        ColorView sixthColor = new ColorView(context, getColorFromResource(context, R.color.blue_500), getColorFromResource(context, R.color.blue_800));
//        addViewToGrid(sixthColor, row1, col6);
//
//        ColorView seventhColor = new ColorView(context, getColorFromResource(context, R.color.light_blue_500),getColorFromResource(context, R.color.light_blue_800));
//        addViewToGrid(seventhColor, row2, col1);
//
//        ColorView eighthColor = new ColorView(context, getColorFromResource(context, R.color.cyan_500), getColorFromResource(context, R.color.cyan_800));
//        addViewToGrid(eighthColor, row2, col2);
//
//        ColorView ninthColor = new ColorView(context, getColorFromResource(context, R.color.teal_500),getColorFromResource(context, R.color.teal_800));
//        addViewToGrid(ninthColor, row2, col3);
//
//        ColorView tenthColor = new ColorView(context, getColorFromResource(context, R.color.green_500),getColorFromResource(context, R.color.green_800));
//        addViewToGrid(tenthColor, row2, col4);
//
//        ColorView eleventhColor = new ColorView(context, getColorFromResource(context, R.color.light_green_500), getColorFromResource(context, R.color.light_green_800));
//        addViewToGrid(eleventhColor, row2, col5);
//
//        ColorView twelfthColor  = new ColorView(context, getColorFromResource(context, R.color.lime_500),getColorFromResource(context, R.color.lime_800));
//        addViewToGrid(twelfthColor, row2, col6);
//
//        ColorView thirteenthColor = new ColorView(context, getColorFromResource(context, R.color.yellow_500), getColorFromResource(context, R.color.yellow_800));
//        addViewToGrid(thirteenthColor, row3,col1);
//
//        ColorView fourteenthColor = new ColorView(context, getColorFromResource(context, R.color.amber_500), getColorFromResource(context, R.color.amber_800));
//        addViewToGrid(fourteenthColor, row3, col2);
//
//        ColorView fifteenthColor = new ColorView(context, getColorFromResource(context, R.color.orange_500), getColorFromResource(context, R.color.orange_800));
//        addViewToGrid(fifteenthColor, row3, col3);
//
//        ColorView sixteenthColor = new ColorView(context, getColorFromResource(context, R.color.deep_orange_500), getColorFromResource(context, R.color.deep_orange_800));
//        addViewToGrid(sixteenthColor, row3, col4);
//
//        ColorView seventeenthColor = new ColorView(context, getColorFromResource(context,R.color.brown_500), getColorFromResource(context, R.color.brown_800));
//        addViewToGrid(seventeenthColor, row3, col5);
//
//        ColorView eighteenthColor = new ColorView(context, getColorFromResource(context, R.color.grey_500), getColorFromResource(context, R.color.grey_800));
//        addViewToGrid(eighteenthColor, row3, col6);
//
//        ColorView nineteenthColor = new ColorView(context, getColorFromResource(context, R.color.blue_grey_500), getColorFromResource(context, R.color.blue_grey_800));
//        addViewToGrid(nineteenthColor, row4,col1);

    }

    private int getColorFromResource(Context context, int colorResource) {
        return context.getResources().getColor(colorResource);
    }

    public ColorItem getColorCheckedItem() {
        if(gridView.getCheckedItemPosition()!= GridView.INVALID_POSITION) {
            return (ColorItem) mainColorAdapter.getItem(gridView.getCheckedItemPosition());
        } else {
            return null;
        }
    }

//    private void addViewToGrid(View view, GridLayout.Spec rowSpec, GridLayout.Spec columnSpec) {
//        gridLayoutColor.addView(view, new GridLayout.LayoutParams(rowSpec, columnSpec));
//    }
}
