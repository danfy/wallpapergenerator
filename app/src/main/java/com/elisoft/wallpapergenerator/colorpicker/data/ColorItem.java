package com.elisoft.wallpapergenerator.colorpicker.data;

import java.util.ArrayList;

/**
 * Created by yvorobey on 9/14/2015.
 */
public class ColorItem {
    private int mainColor;
    private int secondColor;
    private ArrayList<ColorItem> childColors;

    public ColorItem() {

    }

    public ColorItem(int mainColor, int secondColor) {
        this.mainColor = mainColor;
        this.secondColor = secondColor;
    }

    public int getMainColor() {
        return mainColor;
    }

    public void setMainColor(int mainColor) {
        this.mainColor = mainColor;
    }

    public int getSecondColor() {
        return secondColor;
    }

    public void setSecondColor(int secondColor) {
        this.secondColor = secondColor;
    }

    public ArrayList<ColorItem> getChildColors() {
        return childColors;
    }

    public void setChildColors(ArrayList<ColorItem> childColors) {
        this.childColors = childColors;
    }
}
