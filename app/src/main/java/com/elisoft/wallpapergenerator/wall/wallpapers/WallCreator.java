package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLException;
import android.os.Environment;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.IntBuffer;

import javax.microedition.khronos.opengles.GL10;

import com.elisoft.wallpapergenerator.favorites.FavoriteSaveLoad;

/**
 * Created by mBykov on 23.06.2015.
 */
public class WallCreator {

    Context context;
    Bitmap[] pazzle;
    int w,h;
    private int mUpdaterCode;
    private int patternId;

    private FavoriteSaveLoad favoriteSaveLoad;
    public WallCreator(Context context) {
        this.context = context;
        pazzle = new Bitmap[4];
        favoriteSaveLoad = new FavoriteSaveLoad();
    }


    public void setWH(int w,int h) {
        this.w = w;
        this.h = h;
    }

    public void setUpdaterCode(int code, int patternId) {
        this.mUpdaterCode = code;
        this.patternId = patternId;
    }

    public  Bitmap savePixels(int x, int y, int w, int h, GL10 gl) {
        int bitmapBuffer[] = new int[w * h];
        int bitmapSource[] = new int[w * h];
        IntBuffer intBuffer = IntBuffer.wrap(bitmapBuffer);
        intBuffer.position(0);

        try {
            GLES20.glReadPixels(x, y, w, h, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, intBuffer);
            int offset1, offset2;
            for (int i = 0; i < h; i++) {
                offset1 = i * w;
                offset2 = (h - i - 1) * w;
                for (int j = 0; j < w; j++) {
                    int texturePixel = bitmapBuffer[offset1 + j];
                    int blue = (texturePixel >> 16) & 0xff;
                    int red = (texturePixel << 16) & 0x00ff0000;
                    int pixel = (texturePixel & 0xff00ff00) | red | blue;
                    bitmapSource[offset2 + j] = pixel;
                }
            }
        } catch (GLException e) {
            return null;
        }
        return Bitmap.createBitmap(bitmapSource, w, h, Bitmap.Config.ARGB_8888);
    }

    public void saveImage(Bitmap bitmap, int pos) {
        pazzle[pos] = bitmap;
        if (pos == pazzle.length - 1) {
            saveBigPicture();
        }

    }
    void saveBigPicture() {
        Bitmap bitmap = createBigBitmap();
        Canvas canvas = new Canvas(bitmap);
        int bitmapChoice=0;
        for (int i=0;i<2;i++) {
            for (int j = 0; j < 2; j++) {
                if(!pazzle[bitmapChoice].isRecycled()) {
                    canvas.drawBitmap(pazzle[bitmapChoice], pazzle[0].getWidth() * j, pazzle[0].getHeight() * i, new Paint());
                    bitmapChoice++;
                }
            }
        }
        saveToFile(bitmap);
    }
    Bitmap createBigBitmap() {
        int height = pazzle[0].getHeight() *2;
        int width = pazzle[0].getWidth() * 2;
        return Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    }
   public void saveToFile(Bitmap bitmap) {

        File f = favoriteSaveLoad.getFileToSave(patternId);
        try {
            boolean isCreated =  f.createNewFile();
            if (isCreated) {
                FileOutputStream out = new FileOutputStream(f.getPath());
                bitmap.compress(Config.getWallpaperFormar(), Config.getWallpaperQuality(), out);
                out.close();
                galleryAddPic(f.getAbsolutePath());
                updateUI(State.OK,f.getPath(),mUpdaterCode);
            } else updateUI(State.OK,f.getPath(),mUpdaterCode);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void galleryAddPic(String photoPath) {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(photoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        context.sendBroadcast(mediaScanIntent);
//        updateUI(State.OK,photoPath,mUpdaterCode);
    }

    void updateUI(State state,String imgName, int updaterCode) {

    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }


}
