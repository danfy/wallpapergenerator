package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.content.Context;
import android.content.res.Configuration;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.os.Build;
import android.util.Log;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.components.Scene;
import com.elisoft.glrendering.history.ScenesStack;
import com.elisoft.glrendering.shapes.AbstractShape;
import com.elisoft.glrendering.wallpapers.categories.WallpaperFactory;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;
import com.elisoft.wallpapergenerator.app.AppApplication;

import java.util.HashMap;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;



/**
 * Class represents Wallpaper OpenGL renderer.
 */
public class WallRenderer extends GLSurfaceViewRenderer {

    public interface HistoryListener {
        void isBackHistoryEnabled();
        void isForwardHistoryEnabled();
        void isBackHistoryDisabled();
        void isForwardHistoryDisabled();

    }
    private HistoryListener historyListener;
    private boolean isGenerateScene = false;
    private boolean isRestore = false;
    private float ratio = 0f;
    int i=0;

    private ScenesStack scenesStack;
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mCurrentItemMatrix = new float[16];
    private WallpaperPattern currentPattern;
    private AbstractShape currentShape;
    private Scene currentScene;
    private Context mContext;
    private boolean isChanged = false;
    //    private final float[] mViewMatrix = new float[16];
    //    private final float[] mMVPMatrix = new float[16];
        private int width;
        private int height;
    private volatile boolean isBusy = false;

    /**
     * Constructor of WallRenderer
     * @param context - Context.
     * @param progress - Reference to Updater interface.
     * @param patternId - Pattern id. Value from PatternsIds class fields.
     */
    public WallRenderer(Context context, Updater progress, int patternId) {
        super(context, progress);

        mContext=context;
        currentPattern = WallpaperFactory.getWallpaperById(patternId, context);
        scenesStack = ScenesStack.getInstance(null,0,0);

    }

    public WallRenderer(Context context, Updater progress, int patternId, String[] scenes, int currentSceneId, int countOfScenes){
        super(context, progress);
        mContext=context;
        currentPattern = WallpaperFactory.getWallpaperById(patternId, context);
        scenesStack = ScenesStack.getInstance(scenes, currentSceneId, countOfScenes);
    }



    public void setOnHistoryListener(HistoryListener historyListener) {
        this.historyListener = historyListener;
    }

    public void setContext(Context context) {
        if(mContext == null) {
            mContext = context;
        }
    }

    private void initializeScene() {


            if(!isRestore) {
                currentScene = currentPattern.generateScene();
                scenesStack.add(currentScene.saveScene(mContext));
            } else {

                popUpCurrentScene();
            }
    }
    public void setTouchFromActivity(float x, float y) {
        if(currentScene == null) {
            return;
        }
        HashMap<Integer, Layer> layerHashMap = currentScene.getLayerHashMap();
        AbstractShape shape = null;
        for(Layer layer: layerHashMap.values()) {
             shape= layer.onScreenClicked(x, y);
            if (shape != null) {
                currentShape = shape;
            }
        }


    }
    public void setScaleFromActivity(float scale) {

        if(currentShape!=null) {
            currentShape.scale(scale);
        }
        setIsChanged(true);

    }

    public void setIsChanged(boolean isChanged) {
        this.isChanged = isChanged;
    }

    public void setPositionChangedFromActivity(float dx, float dy , float dz) {
        if(currentShape!=null) {
            if(ratio == 0) {
                ratio = 0.9f;
            }
            currentShape.movePosition(2.0f * ratio * dx / width, 2.0f * (-dy / height), dz);
        }
        setIsChanged(true);
    }
    private boolean isSavedScene = false;

    private void popUpCurrentScene() {

        isSavedScene = true;
        currentScene = Scene.loadScene(mContext, scenesStack.getCurrentElement());
    }

    public void showPreviousSavedScene() {

        currentScene = Scene.loadScene(mContext, scenesStack.getPrevious());
        if(scenesStack.isPreviousHistoryEnabled()) {
            historyListener.isBackHistoryEnabled();
        } else {
            historyListener.isBackHistoryDisabled();
        }
        if(scenesStack.isNextHistoryEnabled()) {
            historyListener.isForwardHistoryEnabled();
        } else {
            historyListener.isForwardHistoryDisabled();
        }

        isSavedScene = true;
    }

    public boolean isBusy() {
        return isBusy;
    }

    public void showNextSavedScene() {

        currentScene = Scene.loadScene(mContext, scenesStack.getNext());
        if(scenesStack.isNextHistoryEnabled()) {
            historyListener.isForwardHistoryEnabled();
        } else {
            historyListener.isForwardHistoryDisabled();
        }
        if(scenesStack.isPreviousHistoryEnabled()) {
            historyListener.isBackHistoryEnabled();
        } else {
            historyListener.isBackHistoryDisabled();
        }

        isSavedScene =true;
    }
    public void generateScene(){
//        currentScene = currentPattern.generateScene();

        isGenerateScene = true;
        isRestore = false;
        historyListener.isBackHistoryEnabled();
        historyListener.isForwardHistoryDisabled();
    }

    @Override
    protected float[] getMatrix() {
//         Draw background color
        isBusy = true;
        i++;

            if(isChanged) {
                return mProjectionMatrix;

            }
            if (isGenerateScene) {

                initializeScene();
                isGenerateScene = false;

            }
            if (isSavedScene) {
                currentScene.initializeShapesFromFile(AppApplication.getContext());
                isSavedScene = false;

            } else {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    currentScene = Scene.loadScene(mContext, scenesStack.getCurrentElement());
                    currentScene.initializeShapesFromFile(AppApplication.getContext());
                }
            }
        GLES20.glClearColor(currentScene.getBackgroundColor().getRed(), currentScene.getBackgroundColor().getGreen(), currentScene.getBackgroundColor().getBlue(), currentScene.getBackgroundColor().getAlpha());

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        return mProjectionMatrix;
    }

    @Override
    protected void drawFrame(float[] matrix) {


        currentScene.draw(matrix);


        isSavedScene = false;
        isBusy = false;
    }

    @Override
    public void onSurfaceChanged(int width, int height) {


//        this.width = width;
//        this.height = height;
        GLES20.glViewport(0, 0, width, height);
        ratio = (float)width/(float)height;
        if(mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Matrix.orthoM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, -1, 1);

        } else {
            Matrix.orthoM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, -1, 1);

        }


//
//        currentPattern.setWidth(w);
//        currentPattern.setHeight(h);

    }
    public float getRatio() {
        return ratio;
    }
    public void setHeight(int height) {
        this.height = height;
        currentPattern.setHeight(height);
    }

    public void setWidth(int width) {
        this.width = width;
        currentPattern.setWidth(width);
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

        initializeScene();


    }
    public void isRestore() {
        isRestore = true;

    }

    public ScenesStack getScenesStack() {
        return scenesStack;
    }
}
