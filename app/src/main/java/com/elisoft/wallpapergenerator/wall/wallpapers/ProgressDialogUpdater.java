package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.app.Dialog;

import android.content.Context;
import android.graphics.Bitmap;

import android.widget.Toast;

import com.elisoft.wallpapergenerator.R;


/**
 * Created by mBykov on 01.07.2015.
 */


public class ProgressDialogUpdater implements Updater {
    private static final String TAG = ProgressDialogUpdater.class.getSimpleName();

    Dialog mDialog;
    Context context;

    public ProgressDialogUpdater(Context context, Dialog dialog) {
        this.context = context;

        mDialog = dialog;
    }

    @Override
    public void requestRender() {

    }

    @Override
    public void beforeShow(Bitmap backgroung) {

    }

    public void show() {
//        mProgressDialog.setProgress(0);
//        mProgressDialog.setMax(100);
        mDialog.show();

    }

//    public void update(final int persents) {
//        mProgressDialog.setProgress(persents);
//    }

    public void close() {
        mDialog.dismiss();
    }

    public void finish(State status, final String filePath, int updaterCode) {
        if(updaterCode == Updater.UPDATE_ADD_FAVORITES) {
            Toast.makeText(context, context.getString(R.string.text_image_added_to_favorite), Toast.LENGTH_SHORT).show();
        }
        if(updaterCode == Updater.UPDATE_SET_WALLPAPER) {
            Toast.makeText(context, context.getString(R.string.text_image_set_as_wallpaper), Toast.LENGTH_SHORT).show();
        }

//        Toast.makeText(context, filePath, Toast.LENGTH_SHORT).show();
    }


}
