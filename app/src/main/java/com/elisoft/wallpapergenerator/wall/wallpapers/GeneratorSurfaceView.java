package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.content.Context;
import android.opengl.EGLDisplay;
import android.opengl.GLSurfaceView;
import android.os.Parcelable;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;


import com.elisoft.glrendering.EGLConfigEditor;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.OnTouchScreenListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.PositionChangeListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.ScaleChangeListener;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLContext;

/**
 * Class represents view for OpenGL rendering.
 */
public class GeneratorSurfaceView extends GLSurfaceView {
    private static final String TAG = GeneratorSurfaceView.class.getSimpleName();
    private ScaleChangeListener scaleChangeListener;
    private PositionChangeListener positionChangeListener;
    private ScaleGestureDetector mScaleDetector;
    private OnTouchScreenListener onTouchScreenListener;
    private float scaleFactor = 1.0f;
    private float mLastTouchX;
    private float mLastTouchY;
    private int mActivePointerId = MotionEvent.INVALID_POINTER_ID;

    public GeneratorSurfaceView(Context context) {
        super(context);
        init();
        initGestureDetector(context);
    }

    public GeneratorSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        initGestureDetector(context);
    }
    private void initGestureDetector(Context context) {
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }

    public void setScaleChangeListener(ScaleChangeListener scaleChangeListener) {
        this.scaleChangeListener = scaleChangeListener;
    }

    public void setPositionChangeListener(PositionChangeListener positionChangeListener) {
        this.positionChangeListener = positionChangeListener;
    }

    public void setOnTouchScreenListener(OnTouchScreenListener onTouchScreenListener) {
        this.onTouchScreenListener = onTouchScreenListener;
    }

    private void init() {
        setEGLContextClientVersion(2);
        //Comment below line for test in emulator
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        javax.microedition.khronos.egl.EGLDisplay eglDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        EGLConfigEditor eglConfigEditor = new EGLConfigEditor();
        if(eglConfigEditor.chooseConfig(egl10, eglDisplay) !=null) {
            setEGLConfigChooser(new EGLConfigEditor());
        }

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {

        super.onRestoreInstanceState(state);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);

        final  int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case MotionEvent.ACTION_DOWN:{

                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                final float x = MotionEventCompat.getX(event, pointerIndex);
                final float y = MotionEventCompat.getY(event, pointerIndex);
                mLastTouchX = x;
                mLastTouchY = y;
                mActivePointerId = MotionEventCompat.getPointerId(event, 0);
                if(onTouchScreenListener!=null) {
                    onTouchScreenListener.onTouchScreenCoordinates(x, y, 0.0f);
                }
                break;
            }
            case MotionEvent.ACTION_MOVE: {

                final int pointerIndex = MotionEventCompat.findPointerIndex(event, mActivePointerId);
                final float x = MotionEventCompat.getX(event, pointerIndex);
                final float y = MotionEventCompat.getY(event, pointerIndex);
                final float dx = x - mLastTouchX;
                final float dy = y - mLastTouchY;
                if(positionChangeListener!=null) {
                    positionChangeListener.onPositionChanged(dx, dy, 0.0f);
                }

                mLastTouchY = y;
                mLastTouchX = x;
                break;
            }
            case MotionEvent.ACTION_UP:

                mActivePointerId = MotionEvent.INVALID_POINTER_ID;

                break;
            case MotionEvent.ACTION_CANCEL: {

                mActivePointerId = MotionEvent.INVALID_POINTER_ID;
                break;
            }
            case MotionEvent.ACTION_POINTER_UP:
                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                final int pointerId = MotionEventCompat.getPointerId(event, pointerIndex);

                if(pointerId == mActivePointerId) {
                    final int newPointerIndex = pointerIndex ==0 ?1:0;
                    mLastTouchX = MotionEventCompat.getX(event, newPointerIndex);
                    mLastTouchY = MotionEventCompat.getY(event, newPointerIndex);
                    mActivePointerId = MotionEventCompat.getPointerId(event, newPointerIndex);
                }
                break;
        }
        return true;
    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor =detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 1.5f));
            if(scaleChangeListener!=null) {
                scaleChangeListener.onScaleChanged(scaleFactor);
            }
            return true;
        }
    }
}
