package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;

/**
 * Created by mBykov on 01.07.2015.
 */
public class UpdateProgress implements Updater{

    Context context;
    Updater mUpdater;
    Handler handler;

    public UpdateProgress(Context context, Updater updater) {
        this.context = context;
        mUpdater=updater;
        handler=new Handler(Looper.getMainLooper());

    }

    @Override
    public void requestRender() {
        handler.post(new Runnable() {
            @Override
            public void run() {
                mUpdater.requestRender();
            }
        });
    }

    public void beforeShow(final Bitmap backgroung){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mUpdater.beforeShow(backgroung);
                }
            });
    }

    public void show(){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mUpdater.show();
                }
            });


    }
//    public void update(final int persents){
//            handler.post(new Runnable() {
//                @Override
//                public void run() {
//                    mUpdater.update(persents);
//                }
//            });
//    }
    public void close(){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mUpdater.close();
                }
            });
    }


    public void finish(final State status, final String filePath, final int updaterCode){
            handler.post(new Runnable() {
                @Override
                public void run() {
                    mUpdater.finish(status, filePath, updaterCode);
                }
            });
    }
}
