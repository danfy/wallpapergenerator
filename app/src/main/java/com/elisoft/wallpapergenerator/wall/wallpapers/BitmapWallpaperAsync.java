package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;

import com.elisoft.wallpapergenerator.favorites.utils.ImageFetcher;

/**
 * Created by yvorobey on 7/29/2015.
 */
public class BitmapWallpaperAsync extends AsyncTask<String, Void, Bitmap> {

        private int width, height;
        private WallpaperManager wallpaperManager;
        private Context context;

        public BitmapWallpaperAsync(Context context, WallpaperManager wallpaperManager, int width, int height) {
            this.context = context;
            this.wallpaperManager = wallpaperManager;
            this.width = width;
            this.height = height;
        }
        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = ImageFetcher.decodeSampledBitmapFromFile(params[0], width, height, null);
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            try {
                wallpaperManager.setBitmap(bitmap);
                Toast.makeText(context, "Image set as wallpaper", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(context," Image couldn't set as wallpaper", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(bitmap);
        }
}
