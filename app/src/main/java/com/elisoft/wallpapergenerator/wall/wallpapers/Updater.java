package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.graphics.Bitmap;

/**
 * Created by mBykov on 01.07.2015.
 */
public interface Updater {
     public static final int UPDATE_ADD_FAVORITES = 0;
     public static final int UPDATE_SET_WALLPAPER = 1;
     public static final int UPDATE_SHARE_IMAGE = 2;
     public static final int UPDATE_GENERATE = 3;
     public static final int UPDATE_ENTER_EFFECT_MODE = 4;
     void requestRender();
     void beforeShow(Bitmap backgroung);
     void show();
//     void update(final int persents);
     void close();
     void finish(State status, final String filePath, int updaterCode);
}
