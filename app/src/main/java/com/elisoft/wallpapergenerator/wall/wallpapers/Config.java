package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.graphics.Bitmap;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mBykov on 23.06.2015.
 */
public class Config {
    private static SimpleDateFormat simpleDateFormat=new SimpleDateFormat("-ddMMyy-hhmmss.SSS");
    private static Bitmap.CompressFormat format=Bitmap.CompressFormat.PNG;
    private static int quality=100; // 0-100
    private static final String WALLPAPER_FOLDER_NAME_FIELD=".Wallpapers";
    public static  String getWallpaperName(){
        return  "Wallpaper"+simpleDateFormat.format( new Date() ) ;
    };
    public static Bitmap.CompressFormat getWallpaperFormar(){
        return  format ;
    };

    public static String getWallpaperFormarString(){
        return  format.toString() ;
    };
    public static int getWallpaperQuality(){
        return  quality ;
    };
    public static String getFolderName (){
        return WALLPAPER_FOLDER_NAME_FIELD;
    }
}
