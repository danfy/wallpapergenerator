package com.elisoft.wallpapergenerator.wall.wallpapers;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;


import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by mBykov on 29.06.2015.
 */
public abstract class GLSurfaceViewRenderer implements GLSurfaceView.Renderer {
    protected WallCreator wallCreator;
    Context context;
    Updater mUpdateProgress;
    int w,h;
    float scale;
    float [] bufferMaxrix;

    AtomicInteger i=new AtomicInteger(101);
    //AtomicBoolean stop=new AtomicBoolean(true);

    public GLSurfaceViewRenderer(Context context, Updater progress) {
        this.context=context;
        mUpdateProgress=progress;
        wallCreator=new WallCreator(context){
            @Override
            void updateUI(State state, String imgName, int updaterCode) {
                super.updateUI(state,imgName,updaterCode);

                mUpdateProgress.close();
                mUpdateProgress.finish(state, imgName,updaterCode);
            }
        };
    }



    @Override
    public void onDrawFrame(GL10 gl) {


        float[] matrix=getMatrix();


            switch (i.get()){
                case 100:i.set(101);
                    mUpdateProgress.show();
                    clearScreen();
                    drawFrame(matrix);
                    bufferMaxrix= Arrays.copyOf(matrix,matrix.length);
                    Bitmap bitmap = wallCreator.savePixels(0,0,w,h,gl);
                        mUpdateProgress.beforeShow(bitmap);
                    wallCreator.saveToFile(bitmap);
                    bitmap.recycle();
                    mUpdateProgress.requestRender();
                    break;
                case 0: i.set(1);
                    mUpdateProgress.show();
                    clearScreen();
                    Matrix.multiplyMM(bufferMaxrix, 0, bufferMaxrix, 0, getScaleMatrixMatrix(2), 0);
                    Matrix.translateM(bufferMaxrix, 0, 0.5f * scale, -0.5f, 0f);
                    drawFrame(bufferMaxrix);
                        wallCreator.saveImage(wallCreator.savePixels(0, 0, w, h, gl), 0);
                    mUpdateProgress.requestRender();
                    break;
                case 1:i.set(2);
//                    mUpdateProgress.update(25);
                    clearScreen();
                    //Matrix.multiplyMM(matrix, 0, matrix, 0, getScaleMatrixMatrix(2), 0);
                    Matrix.translateM(bufferMaxrix, 0, -1f * scale, 0f, 0f);
                    drawFrame(bufferMaxrix);
                    wallCreator.saveImage(wallCreator.savePixels(0, 0, w, h, gl), 1);
                    mUpdateProgress.requestRender();
                    break;
                case 2: i.set(3);
//                    mUpdateProgress.update(50);
                    clearScreen();
                    //Matrix.multiplyMM(bufferMaxrix, 0, bufferMaxrix, 0, getScaleMatrixMatrix(2), 0);
                    Matrix.translateM(bufferMaxrix, 0, 1f * scale, 1f, 0f);
                    drawFrame(bufferMaxrix);
                        wallCreator.saveImage(wallCreator.savePixels(0, 0, w, h, gl), 2);
                    mUpdateProgress.requestRender();
                    break;
                case 3:i.set(101);
//                    mUpdateProgress.update(75);
                    clearScreen();
                    //Matrix.multiplyMM(matrix, 0, matrix, 0, getScaleMatrixMatrix(2), 0);
                    Matrix.translateM(bufferMaxrix, 0, -1f * scale, 0f, 0f);
                    drawFrame(bufferMaxrix);
//                    mUpdateProgress.update(90);
                        wallCreator.saveImage(wallCreator.savePixels(0, 0, w, h, gl), 3);
                    mUpdateProgress.requestRender();
                    break;
                default:

                    clearScreen();
                    drawFrame(matrix);

                    break;
            }



    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        w=width;h=height;
        switch (context.getApplicationContext().getResources().getConfiguration().orientation){
            case Configuration.ORIENTATION_LANDSCAPE:
                scale = (float) width / height;
                break;
            case Configuration.ORIENTATION_PORTRAIT:
                scale = (float) width / height;
                break;
        }
        wallCreator.setWH(width, height);
        onSurfaceChanged(width, height);
    }



    public void setBufferFrame(int updaterCode, int patternId) {
        if (i.get()==101){
            i.set(100);
        }
        wallCreator.setUpdaterCode(updaterCode, patternId);
    }

    public void setGenerate(int updaterCode, int patternId) {
        wallCreator.setUpdaterCode(updaterCode, patternId);
    }

    protected void clearScreen(){
    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
}
    abstract protected float [] getMatrix();
    protected abstract void drawFrame(float[] matrix);

    public Context getContext() {
        return context;
    }
    public float [] getScaleMatrixMatrix (float s){
        return new float[] {1*s,0,0,0,
                0,1*s,0,0,
                0,0,1*s,0,
                0,0,0,1};
    }


    public abstract void  onSurfaceChanged(int width, int height);
}
