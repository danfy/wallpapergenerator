package com.elisoft.wallpapergenerator.app;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

/**
 * Created by yvorobey on 1/12/2016.
 */
public abstract class PermissionCheckActivity extends AbstractActivity {
    protected static final int REQUEST_PERMISSION_EXTERNAL_STORAGE = 1;
    protected static final String PERMISSION_WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;

    /**
     * Method check if permission is on or off.
     * @param permission - String values of permission. Example: Manifest.permission.WRITE_EXTERNAL_STORAGE.
     * @param requestCode - Request code for ask to enable permission.
     * @return true - if permission is enabled, false otherwise.
     */
    protected boolean checkPermission(String permission, int requestCode) {
        int permissionCheck = ContextCompat.checkSelfPermission(PermissionCheckActivity.this, permission);
        if(permissionCheck!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PermissionCheckActivity.this,
                    new String[]{permission},
                    requestCode);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    permissionWriteExternalStorageGranted();
                } else {
                    permissionWriteExternalStorageNotGranted();
                }
                break;

            }
        }
    }

    protected abstract void permissionWriteExternalStorageGranted();

    protected abstract void permissionWriteExternalStorageNotGranted();
}
