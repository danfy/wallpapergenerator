package com.elisoft.wallpapergenerator.app;

import android.animation.Animator;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;


import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;

import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;

import android.widget.FrameLayout;


import android.widget.TextView;

import java.util.ArrayList;

import com.elisoft.wallpapergenerator.ItemClickListener;
import com.elisoft.wallpapergenerator.WallpaperGeneratorActivity;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.view.adapter.PatternItemsAdapter;


import com.elisoft.wallpapergenerator.view.animation.PatternItemAnimFactory;
import com.elisoft.wallpapergenerator.view.animation.PatternItemAnimation;

import com.elisoft.wallpapergenerator.view.data.FactoryPatternGroup;
import com.elisoft.wallpapergenerator.view.data.Group;
import com.elisoft.wallpapergenerator.view.data.PatternGroup;
import com.elisoft.wallpapergenerator.view.data.PatternItem;
import com.elisoft.wallpapergenerator.view.decoration.GridItemDecoration;
import com.elisoft.wallpapergenerator.view.views.CircleView;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


/**
 * Created by yvorobey on 7/1/2015.
 */
public class PatternItemsFragment extends Fragment {

    public static final String BUNDLE_CY = "centerY";
    public static final String BUNDLE_COLOR = "color";
    public static final String BUNDLE_TITLE = "title";
    public static final String BUNDLE_HEIGHT_VIEW = "height";
    public static final String BUNDLE_Y = "y";
    public static final String BUNDLE_BACKGROUND = "background";
    public static final String BUNDLE_ID_PATTERN_GROUP = "id_pattern_group";
    public static final String SAVED_TOP_FRAME_TRANSLATION_Y = "top_frame_translation_y";
    private float cy;
    private PatternGroup group;
    private int color;
    private String title;
    private Toolbar toolbar;
    Context context;
    private ArrayList<PatternItem> patternItems;
    private RecyclerView gridView;
    private OnAnimationBackEndedListener listener;
    private FrameLayout frameLayout;
    private TextView textView;
    private FrameLayout animatedIcon;
    private Toolbar actionToolBar;
    private int group_number;
    private boolean isSavedInstanceState = false;


    public interface OnAnimationBackEndedListener {
        void onAnimationBackEnded();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
//        setRetainInstance(true);
        context=getActivity();

        Bundle bundle = getArguments();

        if (bundle != null) {

            cy = bundle.getFloat(BUNDLE_CY);
            title = bundle.getString(BUNDLE_TITLE);
            color = bundle.getInt(BUNDLE_COLOR);



            group_number = bundle.getInt(BUNDLE_ID_PATTERN_GROUP);

        }
        super.onCreate(savedInstanceState);



        group= new FactoryPatternGroup (getActivity()).createGroup(Group.values()[group_number]);
        color = group.getColorTitleResource();
        patternItems = new ArrayList<>();
        for (PatternItem item:group.getPatternItems()){
            patternItems.add(item);
        }
     }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnAnimationBackEndedListener) activity;

      //  ((AppCompatActivity)activity).getSupportActionBar().hide();
    }

    @Override
    public void onResume() {
        super.onResume();
//        listener = (OnAnimationBackEndedListener) getActivity();

            AppApplication.sendGoogleAnalyticsScreenName("Screen : " + getString(R.string.screen_pattern_items));

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(savedInstanceState!=null) {
            isSavedInstanceState = true;
        }
        final View view = inflater.inflate(R.layout.patterns_item_layout, container, false);
        toolbar = (Toolbar)view.findViewById(R.id.item_tool_bar);
        frameLayout = (FrameLayout) view.findViewById(R.id.frame_tool);
        toolbar.setVisibility(View.GONE);
        actionToolBar = (Toolbar) view.findViewById(R.id.item_action_bar);
        actionToolBar.setVisibility(View.GONE);
        actionToolBar.setFocusable(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(actionToolBar);


        final ActionBar supportActionBar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        if(supportActionBar!=null) {
            supportActionBar.setDisplayShowTitleEnabled(false);
            supportActionBar.setDisplayHomeAsUpEnabled(true);
        }
        textView= (TextView) view.findViewById(R.id.title_pattern);

        animatedIcon = (FrameLayout) view.findViewById(R.id.img_pattern_type);
        gridView = (RecyclerView) view.findViewById(R.id.grid_items);
        final PatternItemsAdapter patternItemsAdapter = new PatternItemsAdapter(getActivity(),patternItems);
        patternItemsAdapter.mItemClickListener = new ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
//                ((GridViewPatternItem)view).animateClick();

                final PatternItem patternItem = patternItems.get(position);
                    AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_click),"Clicked : " + patternItem.getTitle() + " in category " + group.getTitle());
                    Intent intent = new Intent(getActivity(), WallpaperGeneratorActivity.class);
                intent.putExtra(WallpaperGeneratorActivity.BUNDLE_PATTERN_ID, patternItem.getId());
                startActivity(intent);
            }
        };
        gridView.setVisibility(View.INVISIBLE);
        gridView.setAdapter(patternItemsAdapter);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);

        gridView.setLayoutManager(gridLayoutManager);
        final int space =getResources().getDimensionPixelSize(R.dimen.space_between_pattern_items);
        gridView.addItemDecoration(new GridItemDecoration(space));

        textView.setText(title);
        final CircleView circleView = (CircleView) frameLayout.getChildAt(0);
        circleView.setColor(color);
        if(isSavedInstanceState) {
            toolbar.setVisibility(View.VISIBLE);
            actionToolBar.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.VISIBLE);
            frameLayout.setTranslationY(savedInstanceState.getFloat(SAVED_TOP_FRAME_TRANSLATION_Y));
            circleView.setScaleX(20f);
            circleView.setScaleY(20f);
            PatternItemAnimFactory.getAnimation(group_number, animatedIcon, frameLayout).restoreView();
            isSavedInstanceState = false;
        }else {
            frameLayout.setTranslationY(cy);
            gridView.setVisibility(View.GONE);




//                ((CircleView) frameLayout.getChildAt(0)).setColor(color);

            frameLayout.animate().setDuration(150).translationY(0).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {

                    final ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 20f, 1f, 20f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
                    scaleAnimation.setDuration(300);
                    scaleAnimation.setInterpolator(new AccelerateInterpolator());
                    scaleAnimation.setFillAfter(true);
                    scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {


                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                            toolbar.setVisibility(View.VISIBLE);
                            actionToolBar.setVisibility(View.VISIBLE);
                            final ScaleAnimation scaleIcon = new ScaleAnimation(0f, 1f, 0f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
                            scaleIcon.setDuration(300);
                            scaleIcon.setStartOffset(300);

                            PatternItemAnimation patternItemAnimation = PatternItemAnimFactory.getAnimation(group_number, animatedIcon, frameLayout);
                            if (patternItemAnimation != null) {
                                patternItemAnimation.animate();
                            }


                            textView.startAnimation(scaleIcon);

                            gridView.setVisibility(View.VISIBLE);
                            gridView.setTranslationY(500);
                            gridView.setAlpha(0);
                            gridView.animate().setDuration(300).translationY(0).alpha(1).setInterpolator(new DecelerateInterpolator()).start();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    frameLayout.getChildAt(0).startAnimation(scaleAnimation);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();

        }

        return view;
    }


    @Override
    public void onStart() {

        super.onStart();

    }


    public void onBackPressed() {
        actionToolBar.setVisibility(View.GONE);
        final CircleView circleView = (CircleView) frameLayout.getChildAt(0);
        ScaleAnimation scaleAnimation = new ScaleAnimation(20f, 1f, 20f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(300);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                frameLayout.animate().setDuration(150).translationY(cy).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        listener.onAnimationBackEnded();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                }).start();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
          circleView.setAnimation(scaleAnimation);
          circleView.startAnimation(scaleAnimation);

          gridView.animate().setDuration(300).translationY(500).alpha(0).start();


        toolbar.animate().setDuration(150).alpha(0).start();
        gridView.animate().setDuration(150).translationY(500).alpha(0).start();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putFloat(SAVED_TOP_FRAME_TRANSLATION_Y, frameLayout.getTranslationY());

    }
}

