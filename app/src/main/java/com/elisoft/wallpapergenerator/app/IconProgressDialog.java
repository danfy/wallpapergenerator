package com.elisoft.wallpapergenerator.app;

import android.app.Dialog;
import android.content.Context;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;

import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.R;




/**
 * Created by yvorobey on 9/3/2015.
 */
public class IconProgressDialog extends Dialog {
    private ImageView imageLayerOne;
    private ImageView imageLayerTwo;
    private TextView textView;
    public IconProgressDialog(Context context) {
        super(context, R.style.TransparentProgressDialog);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.gravity = Gravity.CENTER;

        setTitle(null);
        setCancelable(false);
        setOnCancelListener(null);
        RelativeLayout relativeLayout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.animated_icon_view, null);
        RelativeLayout.LayoutParams layoutParams1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        imageLayerOne = (ImageView) relativeLayout.findViewById(R.id.icon_layer_1);
        imageLayerTwo = (ImageView) relativeLayout.findViewById(R.id.icon_layer_2);
        textView = (TextView) relativeLayout.findViewById(R.id.textProgress);

        addContentView(relativeLayout, layoutParams1);


    }
    public void setText(String message) {
        textView.setText(message);
    }

    @Override
    public void show() {
        super.show();
        RotateAnimation animation = new RotateAnimation(0.0f, 360f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        RotateAnimation animation1 = new RotateAnimation(0.0f, -360f,Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        animation.setRepeatMode(Animation.INFINITE);
        animation.setDuration(2000);
        animation1.setInterpolator(new LinearInterpolator());
        animation1.setDuration(2000);
        animation1.setRepeatMode(Animation.INFINITE);

        imageLayerOne.startAnimation(animation);
        imageLayerTwo.startAnimation(animation1);

    }
}
