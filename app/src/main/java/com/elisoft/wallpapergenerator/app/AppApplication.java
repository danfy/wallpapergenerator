package com.elisoft.wallpapergenerator.app;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.os.Build;


import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;

import com.google.android.gms.analytics.Tracker;

/**
 * Main class of application.
 *
 */
public class AppApplication extends Application {
    private static AppApplication sInstance;
    /**
     * Analytics singleton. The field is set in onCreate method.
     */
    private static GoogleAnalytics analytics;
    /**
     * Default app tracker.
     */
    private static Tracker tracker;

    /**
     * Access to the global Analytics singleton. If this method returns null you forgot to either
     * set android:name="&lt;this.class.name&gt;" attribute on your application element in
     * AndroidManifest.xml or you are not setting this.analytics field in onCreate method override.
     */
    public static GoogleAnalytics analytics() {
        return analytics;
    }

    /**
     * The default app tracker. If this method returns null you forgot to either set
     * android:name="&lt;this.class.name&gt;" attribute on your application element in
     * AndroidManifest.xml or you are not setting this.tracker field in onCreate method override.
     */
    public static Tracker tracker() {
        return tracker;
    }


    private static boolean isLollyPopAndHigher = false;

    public  AppApplication() {
        sInstance = this;

    }
    public static Context getContext() {
        return sInstance;
    }



    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public void onCreate() {
        //TODO: Comment below lines for release
//        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
//                .detectAll()  // or .detectAll() for all detectable problems
//                .penaltyLog()
//                .penaltyDialog()
//
//                .build());
//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                .detectAll()
//                .penaltyLog()
//
//
//                .penaltyDeath()
//                .build());
        super.onCreate();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            isLollyPopAndHigher = true;
        }


        analytics = GoogleAnalytics.getInstance(this);
//        analytics.setDryRun(true);
//        analytics.getLogger().setLogLevel(Logger.LogLevel.VERBOSE);
        tracker = analytics.newTracker("UA-66806802-1");
        // Provide unhandled exceptions reports. Do that first after creating the tracker
        tracker.enableExceptionReporting(true);

        // Enable Remarketing, Demographics & Interests reports
        // https://developers.google.com/analytics/devguides/collection/android/display-features
        tracker.enableAdvertisingIdCollection(true);

        // Enable automatic activity tracking for your app
        tracker.enableAutoActivityTracking(true);

        tracker.send(new HitBuilders.ScreenViewBuilder().setCustomDimension(1, null).build());
    }
    public static boolean isLollyPopAndHigher() {
        return isLollyPopAndHigher;
    }

    public static void sendGoogleAnalyticsEvent(String category, String action) {
        if(tracker!=null) {
            tracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .build());
        }
    }
    public static void sendGoogleAnalyticsScreenName(String screenName) {
        if(tracker!=null) {
            tracker.setScreenName(screenName);
            tracker.send(new HitBuilders.AppViewBuilder().build());
        }
    }

}
