package com.elisoft.wallpapergenerator.app;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;


import com.elisoft.wallpapergenerator.R;
import com.google.android.gms.analytics.HitBuilders;

import java.io.File;
import java.util.ArrayList;

/**
 * Abstract class for shareSingleObject image. Child activities which needs to shareSingleObject must inherits from this.
 */
public abstract class AbstractShareActivity extends PermissionCheckActivity {
    private static final String TAG = AbstractShareActivity.class.getSimpleName();
    private static final String LINK_TO_APP_GP = "market://details?id="; //Link to app from Android APP;
    private static final String LINK_TO_APP_BROWSER = "http://play.google.com/store/apps/details?id="; //Link to app with browser.

    private static final String TYPE_IMAGE = "image/*";

    private ArrayList<Uri> pathsFileToSend;
    /**
     * Send Intent to shareSingleObject single object.
     * @param text - Text to shareSingleObject.
     * @param filePath - Path of file to shareSingleObject.
     */
    protected void shareSingleObject(@Nullable String text, @Nullable String filePath) {

        initShareSingleIntent(text, filePath);
    }

    /**
     * Initialize shareSingleObject Intent.
     * @param text - Text to shareSingleObject.
     * @param filePath - Path of file to shareSingleObject.
     */
    private void initShareSingleIntent(@Nullable String text, @Nullable String filePath) {

        Intent intent  = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        StringBuilder builder = new StringBuilder();
        builder.append(getString(R.string.share_text));
        builder.append(LINK_TO_APP_BROWSER);
        builder.append(getString(R.string.invite_link));


            intent.putExtra(Intent.EXTRA_TEXT, builder.toString());

        if(filePath!=null) {
            intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(filePath)));
        }
        intent.setType(TYPE_IMAGE);

        startActivity(Intent.createChooser(intent, getString(R.string.text_share)));
    }

    /**
     * Initialize shareMultiple Intent.
     * @param textToSend - Text to shareMultiple.
     * @param listUriToSend - Path collection of files to share.
     */
    private void initShareMultipleIntent(@Nullable String textToSend, ArrayList<Uri> listUriToSend) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND_MULTIPLE);
        StringBuilder builder = new StringBuilder();
        builder.append(getString(R.string.share_text));
        builder.append(LINK_TO_APP_BROWSER);
        builder.append(getString(R.string.invite_link));
        intent.putExtra(Intent.EXTRA_TEXT, builder.toString());

        if(listUriToSend!=null) {
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, listUriToSend);

        }
        intent.setType(TYPE_IMAGE);

        startActivity(Intent.createChooser(intent, getString(R.string.text_share)));

    }

    /**
     * Method adds filePath to Collection for send
     * @param filePath - Path of file to add fo send.
     */
    protected void addItemToSend(String filePath) {
        if(pathsFileToSend == null) {
            pathsFileToSend = new ArrayList<>();
        }

        pathsFileToSend.add(Uri.fromFile(new File(filePath)));
    }

    /**
     * Method removes filePath from Collection fo send.
     * @param filePath - Path of file to remove.
     */
    protected void removeItemToSend(String filePath) {
        if(pathsFileToSend == null) {
            pathsFileToSend = new ArrayList<>();
        }
        if(pathsFileToSend.remove(Uri.fromFile(new File(filePath))));
    }


    /**
     * Method send Intent with collection of paths files.
     * @param textToSend - Text to send with collection.
     */
    public void shareImageCollection(String textToSend) {
            if(pathsFileToSend == null) {

                return;
            }

            initShareMultipleIntent(textToSend, pathsFileToSend);
            pathsFileToSend = null;
    }
    /**
     * Method send Intent with collection of paths files.
     * @param textToSend - Text to send with collection.
     * @param pathList - List of paths to send
     */
    public void shareImageCollection(String textToSend, ArrayList<String>pathList) {
        for(int i = 0; i< pathList.size(); i++) {
            AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_share_in_gallery),"Share item in gallery : " + pathList.get(i));
            addItemToSend(pathList.get(i));
        }
        initShareMultipleIntent(textToSend, pathsFileToSend);
        pathsFileToSend = null;

    }



}
