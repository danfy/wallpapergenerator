package com.elisoft.wallpapergenerator.app;

import android.app.Activity;
import android.app.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;


import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;

import java.util.ArrayList;
import java.util.List;

import com.elisoft.wallpapergenerator.ItemClickListener;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.view.adapter.CategoryPatternsAdapter;
import com.elisoft.wallpapergenerator.view.data.FactoryPatternGroup;
import com.elisoft.wallpapergenerator.view.data.Group;
import com.elisoft.wallpapergenerator.view.data.PatternGroup;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;


/**
 * Class represents fragment that holds categories of patterns.
 */
public class PatternsFragment extends Fragment {

    private static final int Y_TRANSLATION = 500;       //Value of translation by y coordinate of list item for start animation.
    private static final int TRANSLATE_ANIMATION_DURATION = 500;  //Value used for time to animate list item (in milliseconds).
    private static final int SCALE_ANIMATION_DURATION = 300;      //Time of scale animation.
    private List<PatternGroup> patternListItems;
    private CategoryPatternsAdapter categoryPatternsAdapter;
    private OnItemListClicked listener;
    private float centerY;
    private int color;

    private RecyclerView listView;
    private boolean isNeedAnimation = true;
    int clickedPosition = 0;
    private android.content.Context context;
    private LinearLayoutManager layoutManager;
    private boolean isItemClicked = false;

    /**
     * Listener for send event to activity.
     */
    public interface OnItemListClicked {
        /**
         * Method calls when item in list of pattern categories was clicked.
         *
         * @param positionY - Y coordinate of center of view.
         * @param color     - Color that represents pattern category.
         * @param title     - Title of pattern category.
         */
        void onItemClicked(float positionY, int color, int backgroundColor, int statusBarColor, String title, int heightOfView, float y, int patternGroupId);
    }

    public void setIsNeedAnimation(boolean isNeedAnimation) {
        this.isNeedAnimation = isNeedAnimation;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
//        setRetainInstance(true);
        /* Populate list of category patterns*/
        patternListItems = new ArrayList<>();
        FactoryPatternGroup factoryPatternGroup= new FactoryPatternGroup(context);
        for (Group group: Group.values()){
            patternListItems.add(factoryPatternGroup.createGroup(group));
        }
        setHasOptionsMenu(true);
    }

    private int getColor(int resource) {
        return getActivity().getResources().getColor(resource);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_patterns, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        final View view = inflater.inflate(R.layout.patterns_fragment_layout, container, false);

        Toolbar toolbar = (Toolbar)view.findViewById(R.id.testToolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        listView = (RecyclerView) view.findViewById(R.id.list_patterns);
        layoutManager = new LinearLayoutManager(context);
        listView.setLayoutManager(layoutManager);
        categoryPatternsAdapter = new CategoryPatternsAdapter(getActivity(), 0, patternListItems);

        listView.setAdapter(categoryPatternsAdapter);

        if (isNeedAnimation) {
            final ViewTreeObserver viewTreeObserver = listView.getViewTreeObserver();
            viewTreeObserver.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    int lastPosition = layoutManager.findLastVisibleItemPosition();
                    int firstPosition = layoutManager.findFirstVisibleItemPosition();

                   int count = 0;
                    for (int i = firstPosition; i < lastPosition+1; i++) {
                        View view = listView.getChildAt(i);
                        if(view == null){
                            return false;
                        }
                        view.setTranslationY(Y_TRANSLATION);
                        view.setAlpha(0);
                        view.animate().setStartDelay(count * 100).setDuration(TRANSLATE_ANIMATION_DURATION).alpha(1).translationY(0).start();
                        count += 2;
                    }
                    listView.getViewTreeObserver().removeOnPreDrawListener(this);
                    return false;
                }
            });
        }
        categoryPatternsAdapter.mItemClickListener=new ItemClickListener() {
            @Override
            public void onItemClick(final View view, final int position) {
                if(!isItemClicked) {
                    isItemClicked = true;

                    animateHideItem(position, view);
                }


            }};


/*        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, final int position, long id) {
                clickedPosition = position;

                ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 0f, 1f, 0f, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
                scaleAnimation.setDuration(SCALE_ANIMATION_DURATION);
                final PatternGroup patternListItem = (PatternGroup) parent.getItemAtPosition(position);

                listView.setBackgroundColor(patternListItem.getColorBackground());
                centerY = view.getY() + view.getHeight() / 4;
                color = patternListItem.getColorTitleResource();
                scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        listener.onItemClicked(centerY, color, patternListItem.getColorBackground(), patternListItem.getTitle(), view.getHeight(), view.getY());

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                view.startAnimation(scaleAnimation);


            }
        });*/
        return view;
    }

    public OnItemListClicked getListener() {
        return listener;
    }

    @Override
    public void onAttach(Activity activity) {
        listener = (OnItemListClicked) activity;
        super.onAttach(activity);

    }

    @Override
    public void onResume() {
        super.onResume();
        // ((AppCompatActivity) getActivity()).getSupportActionBar().show();

            AppApplication.sendGoogleAnalyticsScreenName("Screen : " + getString(R.string.screen_pattern_categories));

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(listView!=null) {
            outState.putInt("ScrollX", listView.getScrollX());
            outState.putInt("ScrollY", listView.getScrollY());
        }
    }

    private void animateHideItem(final int position, final View view) {

        clickedPosition = position;
        final PatternGroup patternGroup = (PatternGroup) patternListItems.get(position);
//                final Intent intent=new Intent(getActivity(), TestActivity2.class);
//                android.util.Log.d(getClass().getSimpleName(), patternListItems.size() + "");
//                android.util.Log.d(getClass().getSimpleName(), position + "");
//                intent.putExtra(PatternGroup.ID_PATTERN_GROUP, patternGroup.GROUP.ordinal());

           AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_click),"Category : " + patternGroup.getTitle());


        ScaleAnimation scaleAnimation = new ScaleAnimation(1f, 0f, 1f, 0f, Animation.RELATIVE_TO_SELF, (float) 0.5, Animation.RELATIVE_TO_SELF, (float) 0.5);
        scaleAnimation.setDuration(SCALE_ANIMATION_DURATION);

        listView.setBackgroundColor(patternGroup.getColorBackground());

        centerY = view.getY() + view.getHeight() / 4;

        color = patternGroup.getColorTitleResource();
//                intent.putExtra(PatternItemsFragment.BUNDLE_COLOR, color);
//                intent.putExtra(PatternItemsFragment.BUNDLE_CY, centerY);

        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                        startActivity(intent);
                isItemClicked = false;
                listener.onItemClicked(centerY, color, patternGroup.getColorBackground(),patternGroup.getStatusBarColor(), patternGroup.getTitle(),  view.getHeight(), view.getY(), patternGroup.GROUP.ordinal());

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(scaleAnimation);
    }

    public void animateItem(final int position) {
        isItemClicked = false;
        ScaleAnimation scaleAnimation = new ScaleAnimation(0f, 1f, 0f, 1f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f, ScaleAnimation.RELATIVE_TO_SELF, 0.5f);
        scaleAnimation.setDuration(150);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                listView.setBackgroundColor(getActivity().getResources().getColor(R.color.white));
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        listView.getChildAt(clickedPosition).startAnimation(scaleAnimation);
//        listView.getChildAt(clickedPosition).animate().setDuration(150).scaleX(1.1f).scaleY(1.1f).setListener(new Animator.AnimatorListener() {
//            @Override
//            public void onAnimationStart(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                listView.getChildAt(clickedPosition).animate().setDuration(150).scaleX(1f).scaleY(1).start();
//            }
//
//            @Override
//            public void onAnimationCancel(Animator animation) {
//
//            }
//
//            @Override
//            public void onAnimationRepeat(Animator animation) {
//
//            }
//        }).start();
//        listView.invalidate();
    }
}
