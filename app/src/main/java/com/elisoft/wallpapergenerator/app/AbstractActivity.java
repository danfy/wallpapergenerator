package com.elisoft.wallpapergenerator.app;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.view.MenuItem;
import android.widget.Toast;


import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.PatternsActivity;
import com.elisoft.wallpapergenerator.favorites.app.ActivityGallery;
import com.elisoft.wallpapergenerator.settings.SettingsActivity;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.appinvite.AppInviteInvitation;

/**
 * Abstract activity for activities with navigation drawer menu in App.
 */
public abstract class AbstractActivity extends AppCompatActivity {
    private static final String TAG = AbstractActivity.class.getSimpleName();
    private static final  int REQUEST_INVITE = 109;
    protected Toolbar toolbar;
    protected NavigationView navigationView;        //Reference to navigation view with id R.id.navigation_view.
    protected DrawerLayout drawerLayout;            //Reference to drawer layout with id R.id.drawer_layout.
    protected int mItemId = -1;



    /**
     * Abstract method for initialize mItemId protected field.
     */
    protected abstract void setItemId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        setItemId();
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        if(navigationView!=null){
            if(mItemId!=-1) {

                navigationView.getMenu().findItem(mItemId).setChecked(true);
            }
            setUpNavigationView(navigationView);

        }

        toolbar = (Toolbar) findViewById(R.id.app_toolbar);

        if(toolbar!=null) {

            setSupportActionBar(toolbar);
        }
    }

    private void setUpNavigationView(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                Intent intent = null;

                switch (menuItem.getItemId()) {
                    case R.id.patterns:
                        intent = new Intent(AbstractActivity.this, PatternsActivity.class);
                        break;
                    case R.id.my_wallpapers:
                        intent = new Intent(AbstractActivity.this, ActivityGallery.class);
                        break;
                    case R.id.settings:
                        intent = new Intent(AbstractActivity.this, SettingsActivity.class);
                        break;
                }

                menuItem.setChecked(true);
                drawerLayout.closeDrawers();
                if (intent != null && mItemId != menuItem.getItemId()) {
                    startActivity(intent);
                    AbstractActivity.this.finish();
                }
                return false;
            }
        });
    }

    /**
     * Method call for invite using App Invite Service from Google Play Service
     */
    protected void onInviteClicked() {
        AppApplication.sendGoogleAnalyticsEvent("Invite","On action invite");
        Intent intent = new AppInviteInvitation.IntentBuilder(getString(R.string.invite_title)).setMessage(getString(R.string.invite_message)).setDeepLink(Uri.parse(getString(R.string.invite_link))).build();
        try {
            startActivityForResult(intent, REQUEST_INVITE);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(AbstractActivity.this, getString(R.string.text_invite_cannot_be_sent), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode ==REQUEST_INVITE) {
            if(resultCode == RESULT_OK) {
                AppApplication.sendGoogleAnalyticsEvent("Invite result","Action invite result");
            }else {
                //TODO: Sending failed need to show dialog
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStop() {
        if(navigationView!=null) {
            if (mItemId != -1) {
                if(navigationView.getMenu()!=null && navigationView.getMenu().findItem(mItemId)!=null) {
                    navigationView.getMenu().findItem(mItemId).setChecked(false);
                }

            }

        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }
}
