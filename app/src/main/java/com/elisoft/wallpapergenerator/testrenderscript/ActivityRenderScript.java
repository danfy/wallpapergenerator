package com.elisoft.wallpapergenerator.testrenderscript;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.Matrix3f;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.renderscript.ScriptIntrinsicColorMatrix;
import android.renderscript.ScriptIntrinsicConvolve5x5;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;


import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.app.AbstractActivity;

/**
 * Created by yvorobey on 8/4/2015.
 */
public class ActivityRenderScript extends AbstractActivity{
    public static final String PATH_EXTRA = "file_path";
    private static final int NUM_BITMAPS = 2;
    private int mCurrentBitmap = 0;
    private Bitmap mBitmapIn;
    private Bitmap[] mBitmapsOut;
    private ImageView imageView;
    private SeekBar seekBar;
    private RenderScript mRS;
    private Allocation mInAllocation;
    private Allocation[] mOutAllocation;

    private ScriptIntrinsicBlur mScriptBlur;
    private ScriptIntrinsicConvolve5x5 mScriptConvolve;
    private ScriptIntrinsicColorMatrix mScriptMatrix;

    private final int MODE_BLUR = 0;
    private final int MODE_CONVOLVE =1;
    private final int MODE_COLORMATRIX = 2;
    private int mFilterMode = MODE_BLUR;
    private RenderScriptTask mLatestTask = null;

    private String filePath;


    @Override
    protected void setItemId() {
        //Do nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.renderscript);
        filePath = getIntent().getStringExtra(PATH_EXTRA);
        mBitmapIn = loadBitmap(filePath);
        mBitmapsOut = new Bitmap[NUM_BITMAPS];
        for (int i =0; i<NUM_BITMAPS; ++i) {
            mBitmapsOut[i] = Bitmap.createBitmap(mBitmapIn.getWidth(), mBitmapIn.getHeight(), mBitmapIn.getConfig());
        }
        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageBitmap(mBitmapsOut[mCurrentBitmap]);
        mCurrentBitmap+=(mCurrentBitmap+1)%NUM_BITMAPS;

        seekBar = (SeekBar) findViewById(R.id.seek_bar);
        seekBar.setProgress(50);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateImage(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        createScript();
        mFilterMode = MODE_BLUR;
        updateImage(50);
    }

    private void createScript() {
        mRS = RenderScript.create(this);
        mInAllocation = Allocation.createFromBitmap(mRS, mBitmapIn);
        mOutAllocation = new Allocation[NUM_BITMAPS];
        for (int i = 0; i< NUM_BITMAPS; ++i) {
            mOutAllocation[i] = Allocation.createFromBitmap(mRS, mBitmapsOut[i]);
        }
        mScriptBlur = ScriptIntrinsicBlur.create(mRS, Element.U8_4(mRS));
        mScriptConvolve = ScriptIntrinsicConvolve5x5.create(mRS, Element.U8_4(mRS));
        mScriptMatrix = ScriptIntrinsicColorMatrix.create(mRS);

    }


    private void performFilter(Allocation inAllocation, Allocation outAllocation, Bitmap bitmapOut, float value) {
        switch (mFilterMode) {
            case MODE_BLUR:
                mScriptBlur.setRadius(25);

                mScriptBlur.setInput(inAllocation);
                mScriptBlur.forEach(outAllocation);
                break;
            case MODE_CONVOLVE:
                float f1 = value;
                float f2 = 1.0f - f1;

                float coefficients[] = {-f1 * 2, 0, -f1, 0, 0, 0, -f2 * 2, -f2, 0,
                        0, -f1, -f2, 1, f2, f1, 0, 0, f2, f2 * 2, 0, 0, 0, f1, 0,
                        f1 * 2,};
                mScriptConvolve.setCoefficients(coefficients);
                mScriptConvolve.setInput(inAllocation);
                mScriptConvolve.forEach(outAllocation);
                break;
            case MODE_COLORMATRIX: {
                float cos = (float) Math.cos((double) value);
                float sin = (float) Math.sin((double) value);
                Matrix3f mat = new Matrix3f();
                mat.set(0, 0, (float) (.299 + .701 * cos + .168 * sin));
                mat.set(1, 0, (float) (.587 - .587 * cos + .330 * sin));
                mat.set(2, 0, (float) (.114 - .114 * cos - .497 * sin));
                mat.set(0, 1, (float) (.299 - .299 * cos - .328 * sin));
                mat.set(1, 1, (float) (.587 + .413 * cos + .035 * sin));
                mat.set(2, 1, (float) (.114 - .114 * cos + .292 * sin));
                mat.set(0, 2, (float) (.299 - .3 * cos + 1.25 * sin));
                mat.set(1, 2, (float) (.587 - .588 * cos - 1.05 * sin));
                mat.set(2, 2, (float) (.114 + .886 * cos - .203 * sin));
                mScriptMatrix.setColorMatrix(mat);

                mScriptMatrix.forEach(inAllocation, outAllocation);
            }
            break;
        }
        outAllocation.copyTo(bitmapOut);
    }

    private float getFilterParameter(int i ) {
        float f = .0f;
        switch (mFilterMode) {
            case MODE_BLUR: {
                final float max = 25.0f;
                final float min = 1.f;
                f = (float) ((max - min) *( i/100.0) + min);
            }
            break;
            case MODE_CONVOLVE: {
                final float max = 2.f;
                final float min = 0.f;
                f = (float) ((max - min) * (i / 100.0) + min);
            }
            break;
            case MODE_COLORMATRIX: {
                final float max = (float) Math.PI;
                final float min = (float) -Math.PI;
                f = (float) ((max - min) * (i / 100.0) + min);
            }
            break;
        }
        return f;

    }

    private Bitmap loadBitmap(String path) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        return BitmapFactory.decodeFile(path, options);
    }

    private class RenderScriptTask extends AsyncTask<Float, Integer, Integer> {
        Boolean issued = false;


        protected Integer doInBackground(Float... values) {
            int index = -1;
            if (isCancelled() == false) {
                issued = true;
                index = mCurrentBitmap;

                performFilter(mInAllocation, mOutAllocation[index], mBitmapsOut[index], values[0]);
                mCurrentBitmap = (mCurrentBitmap + 1) % NUM_BITMAPS;
            }
            return index;
        }

        void updateView(Integer result) {
            if (result != -1) {
                // Request UI update
                imageView.setImageBitmap(mBitmapsOut[result]);
                imageView.invalidate();
            }
        }

        protected void onPostExecute(Integer result) {
            updateView(result);
        }

        protected void onCancelled(Integer result) {
            if (issued) {
                updateView(result);
            }
        }
    }

    private void updateImage(int progress) {
        float f = getFilterParameter(progress);

        if (mLatestTask != null)
            mLatestTask.cancel(false);
        mLatestTask = new RenderScriptTask();

        mLatestTask.execute(f);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.renderscript, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_blur:{
                mFilterMode = MODE_BLUR;
                updateImage(seekBar.getProgress());
            }
            break;
            case R.id.action_convolve:{
                mFilterMode = MODE_CONVOLVE;
                updateImage(seekBar.getProgress());
            }
            break;
            case R.id.action_color:{
                mFilterMode = MODE_COLORMATRIX;
                updateImage(seekBar.getProgress());
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}
