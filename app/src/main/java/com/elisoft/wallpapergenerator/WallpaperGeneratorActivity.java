package com.elisoft.wallpapergenerator;



import com.elisoft.glrendering.history.ScenesStack;
import com.elisoft.wallpapergenerator.app.AbstractShareActivity;
import com.elisoft.wallpapergenerator.app.AppApplication;
import com.elisoft.wallpapergenerator.app.IconProgressDialog;
import com.elisoft.wallpapergenerator.app.PermissionCheckActivity;
import com.elisoft.wallpapergenerator.mediaeffectmode.app.ImageEffectsActivity;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.OnTouchScreenListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.PositionChangeListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.ScaleChangeListener;
import com.elisoft.wallpapergenerator.view.views.AbstractExpandedFABs;
import com.elisoft.wallpapergenerator.view.views.PreLollyPopExpandedFABs;
import com.elisoft.wallpapergenerator.view.views.lollypop.LollyPopExpandFABs;
import com.elisoft.wallpapergenerator.wall.wallpapers.GeneratorSurfaceView;
import com.elisoft.wallpapergenerator.wall.wallpapers.ProgressDialogUpdater;
import com.elisoft.wallpapergenerator.wall.wallpapers.State;
import com.elisoft.wallpapergenerator.wall.wallpapers.UpdateProgress;
import com.elisoft.wallpapergenerator.wall.wallpapers.Updater;
import com.elisoft.wallpapergenerator.wall.wallpapers.WallRenderer;

import android.Manifest;
import android.animation.Animator;
import android.app.WallpaperManager;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.os.Bundle;


import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;



import java.io.IOException;


/**
 * Class represent activity for generate wallpapers.
 */
public class WallpaperGeneratorActivity extends AbstractShareActivity implements PreLollyPopExpandedFABs.OnButtonExpandedClickListener, WallRenderer.HistoryListener {


    public static final String BUNDLE_PATTERN_ID = "pattern_id";

    private static final int FAB_GENERATE_ANIMATION_DURATION = 150;

    private static final int ACTION_SHARE = 3;
    private static final int ACTION_ADD_TO_FAVORITES =4;
    private static final int ACTION_SET_AS_WALLPAPER = 5;
    private static final int ACTION_EFFECT_MODE = 6;

    private GeneratorSurfaceView editorGLSurfaceView;               //View for rendering.
    private WallRenderer renderer;                                  //OpenGL render.
    private IconProgressDialog mProgressDialog;
    private UpdateProgress mUpdateProgress;
    private ImageView imageView;
    private View fabGenerate;
    private AbstractExpandedFABs relativeLayout;
    private int currentPatternId = 0;
    private RelativeLayout layout;
    private RelativeLayout historyRelative;
    private ImageButton imageButtonBack;
    private ImageButton imageButtonForward;
    private ImageView fabIcon;
    private boolean isSavedInstance;
    private boolean isAnimationStarted = false;
    private View animatedActionView;
    private int currentAction = 0;

    @Override
    protected void setItemId() {
        //Do nothing
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.generator_layout);
        currentPatternId = getIntent().getIntExtra(BUNDLE_PATTERN_ID, 0);
        if(getSupportActionBar() !=null) {

            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        initializeViews();

        mProgressDialog=createProgressDialog();

        mUpdateProgress=new UpdateProgress(this, new ProgressDialogUpdater(this, mProgressDialog){
            @Override
            public void requestRender() {
                editorGLSurfaceView.requestRender();
            }

            @Override
            public void beforeShow(Bitmap backgroung) {
                super.beforeShow(backgroung);
                // to do...
                //editorGLSurfaceView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageBitmap(backgroung);
            }

            @Override
            public void close() {
                super.close();

            }

            @Override
            public void show() {
                super.show();

//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
            }

            @Override
            public void finish(State status, String filePath, int updaterCode) {
                super.finish(status, filePath, updaterCode);

//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

                if(updaterCode == Updater.UPDATE_SET_WALLPAPER) {
//                    WallpaperSettings.saveURLToPreferences(WallpaperGeneratorActivity.this, filePath);
//                    Intent intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
//                    intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, new ComponentName(WallpaperGeneratorActivity.this, WallpaperServiceGenerator.class));
//                    startActivity(intent);

                    WallpaperManager wallpaperManager = WallpaperManager.getInstance(WallpaperGeneratorActivity.this);


                Display display = getWindowManager().getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                int screenHeight = point.y;
                Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                int width = bitmap.getWidth();


                width = (width*screenHeight)/bitmap.getHeight();
                try {
                    wallpaperManager.setBitmap(Bitmap.createScaledBitmap(bitmap, width, screenHeight,true));
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    bitmap.recycle();
                }

//                    try {
//                        final FileInputStream fileInputStream = new FileInputStream(filePath);
//                        wallpaperManager.setStream(fileInputStream);
//                        fileInputStream.close();
//                    } catch (FileNotFoundException e) {
//
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                }
                if(updaterCode == Updater.UPDATE_SHARE_IMAGE) {

                    shareSingleObject("", filePath);
                }



                imageView.setImageResource(android.R.color.transparent);
                imageView.setVisibility(View.GONE);
                editorGLSurfaceView.setVisibility(View.VISIBLE);
                if(updaterCode == Updater.UPDATE_ENTER_EFFECT_MODE) {
                    Intent intent = new Intent(WallpaperGeneratorActivity.this, ImageEffectsActivity.class);
                    intent.putExtra(ImageEffectsActivity.STRING_PATH, filePath);
                    startActivity(intent);
                    return;
                }
            }
        });





        fabGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(renderer.isBusy()) {
                    return;
                }
                renderer.setIsChanged(false);


                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    fabIcon.setRotation(-180);
                    fabIcon.animate().setDuration(150).rotation(0).start();
                }
                AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_button_generate), "Generate wallpaper pattern id : " + currentPatternId);
                renderer.generateScene();
                renderer.setGenerate(Updater.UPDATE_GENERATE, currentPatternId);
                editorGLSurfaceView.requestRender();
                animateActionView();

            }
        });

        initializeForeground();
        if(savedInstanceState == null) {
            renderer=new WallRenderer(this,mUpdateProgress,currentPatternId);
        } else {
            renderer = new WallRenderer(this, mUpdateProgress, currentPatternId, savedInstanceState.getStringArray("KEY_FILES_ARRAY"), savedInstanceState.getInt("KEY_CURRENT_SCENE_ID"), savedInstanceState.getInt("KEY_COUNT_SCENES"));
        }
        renderer.setOnHistoryListener(this);
        editorGLSurfaceView.setRenderer(renderer);
        editorGLSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        editorGLSurfaceView.setOnTouchScreenListener(new OnTouchScreenListener() {
            @Override
            public void onTouchScreenCoordinates(float x, float y, float z) {



                    x= 2* renderer.getRatio()*(x- editorGLSurfaceView.getWidth()/2)/editorGLSurfaceView.getWidth();

                    y = -2*(y-editorGLSurfaceView.getHeight()/2)/editorGLSurfaceView.getHeight();

                renderer.setTouchFromActivity(x,y);
            }
        });
        editorGLSurfaceView.setPositionChangeListener(new PositionChangeListener() {
            @Override
            public void onPositionChanged(float dx, float dy, float dz) {
                renderer.setPositionChangedFromActivity(dx, dy, dz);
                editorGLSurfaceView.requestRender();
            }
        });
        editorGLSurfaceView.setScaleChangeListener(new ScaleChangeListener() {
            @Override
            public void onScaleChanged(float scale) {
                renderer.setScaleFromActivity(scale);
                editorGLSurfaceView.requestRender();
            }
        });





    }

    private void initializeViews() {

        int fabHalf = 0;
        int fabHalfGenerate = 0;
        editorGLSurfaceView = (GeneratorSurfaceView) findViewById(R.id.rendering_view);
        editorGLSurfaceView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                editorGLSurfaceView.getViewTreeObserver().removeOnPreDrawListener(this);
                renderer.setWidth(editorGLSurfaceView.getWidth());
                renderer.setHeight(editorGLSurfaceView.getHeight());
                return false;
            }
        });
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
            relativeLayout = (LollyPopExpandFABs) findViewById(R.id.expanded_menu);
            fabHalf = getResources().getDimensionPixelSize(R.dimen.fab_size)/2 + getResources().getDimensionPixelSize(R.dimen.tools_fab_margin);
        } else {
            relativeLayout = (PreLollyPopExpandedFABs) findViewById(R.id.expanded_menu);


            fabHalf = getResources().getDimensionPixelSize(R.dimen.size_fab_pre_lolly_pop) / 2;
            fabHalfGenerate = getResources().getDimensionPixelSize(R.dimen.size_fab_pre_lolly_pop_generate)/2;
        }

        int toolbarHeight = getResources().getDimensionPixelSize(R.dimen.tool_bar_generate_size);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(0, toolbarHeight - fabHalf, 0, 0);
        relativeLayout.setLayoutParams(layoutParams);
        relativeLayout.setOnButtonExpandedClickListener(this);

        fabGenerate =findViewById(R.id.fab_generate);

        int controlHeight = getResources().getDimensionPixelSize(R.dimen.history_controls_height);



        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP) {
            fabIcon = (ImageView) ((ViewGroup)fabGenerate).getChildAt(0);
        } else {
            RelativeLayout.LayoutParams generateFabLayout = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            generateFabLayout.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            generateFabLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            generateFabLayout.setMargins(0, 0, 0, controlHeight - fabHalfGenerate);
            fabGenerate.setLayoutParams(generateFabLayout);
        }




        layout=(RelativeLayout)findViewById(R.id.container);

        historyRelative = (RelativeLayout) findViewById(R.id.generator_history);


        imageButtonBack = (ImageButton) historyRelative.findViewById(R.id.button_history_back);
        imageButtonBack.setEnabled(false);
        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (renderer.isBusy()) {
                    return;
                }
//                if(lastHistoryClickTime!=0 && (System.currentTimeMillis() - lastHistoryClickTime)>500) {


                renderer.setIsChanged(false);

                renderer.showPreviousSavedScene();
                editorGLSurfaceView.requestRender();
//                }
//                lastHistoryClickTime = System.currentTimeMillis();

            }
        });
        imageButtonForward = (ImageButton) historyRelative.findViewById(R.id.button_history_forward);
        imageButtonForward.setEnabled(false);
        imageButtonForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(lastHistoryClickTime!=0 && (System.currentTimeMillis()- lastHistoryClickTime)>500) {
                if (renderer.isBusy()) {
                    return;
                }
                renderer.setIsChanged(false);

                renderer.showNextSavedScene();
                editorGLSurfaceView.requestRender();
//                }
//                lastHistoryClickTime = System.currentTimeMillis();

            }
        });

    }
    private void showFabGenerate() {

        historyRelative.animate().setDuration(FAB_GENERATE_ANIMATION_DURATION).translationY(0).start();


        fabGenerate.animate().setStartDelay(FAB_GENERATE_ANIMATION_DURATION).setDuration(FAB_GENERATE_ANIMATION_DURATION).scaleX(1f).scaleY(1f).start();


        fabGenerate.setEnabled(true);
    }
    private void hideFabGenerate() {

        fabGenerate.animate().setDuration(FAB_GENERATE_ANIMATION_DURATION).scaleX(0f).scaleY(0f).start();

        historyRelative.animate().setStartDelay(FAB_GENERATE_ANIMATION_DURATION).setDuration(FAB_GENERATE_ANIMATION_DURATION).
                        translationY(historyRelative.getHeight()).start();


        fabGenerate.setEnabled(false);
    }

    private void initializeForeground() {
        imageView=new ImageView(this);
        imageView.setVisibility(View.GONE);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);

        layout.addView(imageView, layoutParams);
    }

    private void showForeGround() {
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageResource(R.color.transparent_white);
    }
    private void hideForeGround() {
        imageView.setImageResource(android.R.color.transparent);
        imageView.setVisibility(View.GONE);
    }

    private IconProgressDialog createProgressDialog() {
//        ProgressDialog dialog =new ProgressDialog(this);
//        dialog.setMessage("Save wallpaper");
//        dialog.setIndeterminate(false);
//        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
//        dialog.setCancelable(false);


        return new IconProgressDialog(this);
    }

    private void animateActionView() {
        if(!isAnimationStarted) {
            isAnimationStarted = true;
            animatedActionView.animate().setDuration(300).scaleX(1.5f).scaleY(1.5f).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    animatedActionView.animate().setDuration(300).scaleY(1.0f).scaleX(1.0f).setListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            isAnimationStarted = false;
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {

                        }
                    }).start();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            }).start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_generator, menu);
        final MenuItem menuItem = menu.findItem(R.id.set_as_wallpaper);
//        ImageView imageView = (ImageView) View.inflate(WallpaperGeneratorActivity.this, R.layout.animated_icon_view,null);
        menuItem.setActionView(R.layout.animated_action_item);
        animatedActionView = menuItem.getActionView();
        if(animatedActionView!=null) {
            animatedActionView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    WallpaperGeneratorActivity.this.onOptionsItemSelected(menuItem);
                }
            });
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(relativeLayout.isOpen()) {
            relativeLayout.collapseFAB();
        }
        switch (item.getItemId()){
            case R.id.set_as_wallpaper:
                currentAction = ACTION_SET_AS_WALLPAPER;
                executeAction(currentAction);

                break;
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_favorite:
                currentAction = ACTION_ADD_TO_FAVORITES;
                executeAction(currentAction);
                break;
            case R.id.action_share:
                currentAction = ACTION_SHARE;
                executeAction(currentAction);
                break;
        }

        return true;
    }

    private void actionSetAsWallpaper() {
        AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_set_wallpaper_in_generator), "Set wallpaper in generator pattern id : " + currentPatternId);
        mProgressDialog.setText(getString(R.string.text_set_as_wallpaper_progress));
        renderer.setBufferFrame(Updater.UPDATE_SET_WALLPAPER, currentPatternId);
        editorGLSurfaceView.requestRender();
    }

    private void actionAddToFavorites() {
        AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_add_to_favorites), "Add to favorite pattern id : " + currentPatternId);
        mProgressDialog.setText(getString(R.string.text_add_to_favorite_progress));
        renderer.setBufferFrame(Updater.UPDATE_ADD_FAVORITES, currentPatternId);
        editorGLSurfaceView.requestRender();
    }
    private void actionShare() {
        AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_share_in_generator), "Share in generator pattern id : " + currentPatternId);
        mProgressDialog.setText(getString(R.string.text_prepare_image_for_share_progress));
        renderer.setBufferFrame(Updater.UPDATE_SHARE_IMAGE, currentPatternId);
        editorGLSurfaceView.requestRender();
    }

    private void actionEffectMode() {
        AppApplication.sendGoogleAnalyticsEvent(getString(R.string.analytics_category_effects_editor), "Effect editor in generator pattern id : " + currentPatternId);
        mProgressDialog.setText(getString(R.string.text_prepare_image_for_effect_progress));
        renderer.setBufferFrame(Updater.UPDATE_ENTER_EFFECT_MODE, currentPatternId);
        editorGLSurfaceView.requestRender();
//        ColorPickerFragmentDialog colorPickerFragmentDialog = new ColorPickerFragmentDialog();
//        colorPickerFragmentDialog.show(getFragmentManager(), "Dialog");
    }

    @Override
    protected void onPause() {
        super.onPause();
        editorGLSurfaceView.onPause();

    }



    @Override
    protected void onResume() {
        super.onResume();
        renderer.setContext(this);
        if(isSavedInstance) {
            renderer.isRestore();
        }
        editorGLSurfaceView.onResume();
        AppApplication.sendGoogleAnalyticsScreenName("Screen : " + getString(R.string.screen_wallpaper_generator));

    }
    @Override
    protected void onDestroy() {
        if(mProgressDialog!=null) {
            mProgressDialog.dismiss();
        }


        //TODO: Remove this line in realise

        super.onDestroy();
    }


    @Override
    protected void onStop() {
        super.onStop();
        if(renderer!=null) {
            renderer.setIsChanged(false);
        }
    }

    @Override
    public void isBackHistoryEnabled() {

        imageButtonBack.setEnabled(true);
    }

    @Override
    public void isForwardHistoryEnabled() {


        imageButtonForward.setEnabled(true);
    }

    @Override
    public void isBackHistoryDisabled() {
        imageButtonBack.setEnabled(false);
    }

    @Override
    public void isForwardHistoryDisabled() {
        imageButtonForward.setEnabled(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        isSavedInstance = true;
        outState.putBoolean("SAVED", isSavedInstance);
        outState.putStringArray("KEY_FILES_ARRAY", renderer.getScenesStack().getStackFiles());
        outState.putInt("KEY_CURRENT_SCENE_ID" , renderer.getScenesStack().getCurrentId());
        outState.putInt("KEY_COUNT_SCENES", renderer.getScenesStack().getElementsCount());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        isSavedInstance = savedInstanceState.getBoolean("SAVED",false);

    }



    @Override
    public void onSetAsWallpaperClicked() {
        currentAction = ACTION_SET_AS_WALLPAPER;
        executeAction(currentAction);
    }

    @Override
    public void onAddToFavoritesClicked() {
        currentAction = ACTION_ADD_TO_FAVORITES;
        executeAction(currentAction);
    }

    @Override
    public void onShareButtonClicked() {
        currentAction = ACTION_SHARE;
        executeAction(currentAction);
    }

    @Override
    public void onEffectEditModeClicked() {
        currentAction = ACTION_EFFECT_MODE;
        executeAction(currentAction);
    }

    @Override
    public void onExpandButtons() {
        hideFabGenerate();
        showForeGround();
    }

    @Override
    public void onCollapseButtons() {
        hideForeGround();
        showFabGenerate();
    }

    private void executeAction(int action) {
        if(checkPermission(PERMISSION_WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_EXTERNAL_STORAGE)) {
            switch (action) {
                case ACTION_SET_AS_WALLPAPER:
                    actionSetAsWallpaper();
                    break;
                case ACTION_ADD_TO_FAVORITES:
                    actionAddToFavorites();
                    break;
                case ACTION_EFFECT_MODE:
                    actionEffectMode();
                    break;
                case ACTION_SHARE:
                    actionShare();
                    break;
            }
        }
    }


    @Override
    protected void permissionWriteExternalStorageGranted() {
        executeAction(currentAction);
    }

    @Override
    protected void permissionWriteExternalStorageNotGranted() {

    }
}
