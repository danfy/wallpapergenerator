package com.elisoft.wallpapergenerator;


import android.app.FragmentTransaction;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;


import android.view.View;
import android.view.Window;
import android.view.WindowManager;



import com.crashlytics.android.Crashlytics;

import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.wallpapergenerator.app.AbstractActivity;

import com.elisoft.wallpapergenerator.app.PatternItemsFragment;
import com.elisoft.wallpapergenerator.app.PatternsFragment;
import com.elisoft.wallpapergenerator.view.data.Pattern;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;


import io.fabric.sdk.android.Fabric;

/**
 * Created by yvorobey on 6/30/2015.
 */
public class PatternsActivity extends AbstractActivity implements PatternsFragment.OnItemListClicked, PatternItemsFragment.OnAnimationBackEndedListener {

    private FragmentTransaction fragmentTransaction;
    private PatternsFragment patternsFragment;
    private PatternItemsFragment patternItemsFragment;

    private static final String PATTERN_CATEGORY_TAG = "pattern_category";
    private static final String PATTERN_ITEMS_TAG = "pattern_items";
    @Override
    protected void setItemId() {
        mItemId = R.id.patterns;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fabric.with(this, new Crashlytics());

        setContentView(R.layout.navigation_drawer_test);


          if(savedInstanceState!=null) {
            return;
        }
        if (getFragmentManager().getBackStackEntryCount()==0){
            fragmentTransaction = getFragmentManager().beginTransaction();
            patternsFragment = new PatternsFragment();
//            patternItemsFragment = new PatternItemsFragment();
            patternsFragment.setIsNeedAnimation(true);
            fragmentTransaction.replace(R.id.fragment_container, patternsFragment, PATTERN_CATEGORY_TAG);
//            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        } else {


            patternsFragment= (PatternsFragment) getFragmentManager().findFragmentByTag(PATTERN_CATEGORY_TAG);
            patternItemsFragment= (PatternItemsFragment) getFragmentManager().findFragmentByTag(PATTERN_ITEMS_TAG);
            patternsFragment.setIsNeedAnimation(false);

        }

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest builder = new AdRequest.Builder().addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("24EA53AE834292F82188B1CAB5F40BF5").addTestDevice("35C2C9BA094C7093349EE8461076A0E2").addTestDevice("3AF8E5E55DE1218FAC5C62F9BC1C4DC4").
                addTestDevice("2E057647F6C48F2D2EF81DB51921740F").build();

        adView.loadAd(builder);



    }






    @Override
    public void onBackPressed() {

        if(getFragmentManager().getBackStackEntryCount()>0) {
            if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.setStatusBarColor(getResources().getColor(R.color.color_status_bar));
            }
            patternItemsFragment= (PatternItemsFragment) getFragmentManager().findFragmentByTag(PATTERN_ITEMS_TAG);
            patternItemsFragment.onBackPressed();
           // getFragmentManager().popBackStack();
        } else {
            actOnBackPressed();
        }
    }

    public void actOnBackPressed(){
        super.onBackPressed();
    }
    @Override
    public void onAnimationBackEnded() {


       /* getFragmentManager().beginTransaction().show(patternsFragment).commit();
        getFragmentManager().beginTransaction().remove(patternItemsFragment).commit();*/
        //patternsFragment= (PatternsFragment) getFragmentManager().findFragmentByTag(PatternsFragment.class.getSimpleName());

    //    getSupportActionBar().show();

        try {
            getFragmentManager().popBackStack();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
        patternsFragment  = (PatternsFragment) getFragmentManager().findFragmentByTag(PATTERN_CATEGORY_TAG);
        if(patternsFragment == null) {
            patternsFragment = new PatternsFragment();
            getFragmentManager().beginTransaction().replace(R.id.fragment_container, patternsFragment, PATTERN_CATEGORY_TAG).commitAllowingStateLoss();
        }
        patternsFragment.setIsNeedAnimation(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            patternItemsFragment = (PatternItemsFragment) getFragmentManager().findFragmentByTag(PATTERN_ITEMS_TAG);
            if(patternItemsFragment!=null && patternItemsFragment.isVisible()) {

                onBackPressed();
            } else {
                if(drawerLayout!=null) {
                    drawerLayout.openDrawer(Gravity.LEFT);
                }
            }

        }
        if(item.getItemId() == R.id.action_send_invite) {
            onInviteClicked();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClicked(float positionY, int color, int backgroundColor, int statusBarColor, String title, int heightOfView, float y, int patternItemId) {
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(statusBarColor);
        }
        patternItemsFragment = new PatternItemsFragment();
        Bundle bundle = new Bundle();
        bundle.putFloat(PatternItemsFragment.BUNDLE_CY, positionY);
        bundle.putInt(PatternItemsFragment.BUNDLE_COLOR, color);
        bundle.putString(PatternItemsFragment.BUNDLE_TITLE, title);
        bundle.putInt(PatternItemsFragment.BUNDLE_HEIGHT_VIEW, heightOfView);
        bundle.putFloat(PatternItemsFragment.BUNDLE_Y, y);
        bundle.putInt(PatternItemsFragment.BUNDLE_BACKGROUND, backgroundColor);
        bundle.putInt(PatternItemsFragment.BUNDLE_ID_PATTERN_GROUP, patternItemId);

        patternItemsFragment.setArguments(bundle);

        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, patternItemsFragment, PATTERN_ITEMS_TAG);
        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.hide(patternsFragment);

        fragmentTransaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
