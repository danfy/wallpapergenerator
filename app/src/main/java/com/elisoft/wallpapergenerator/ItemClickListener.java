package com.elisoft.wallpapergenerator;

import android.view.View;

/**
 * Created by mBykov on 23.07.2015.
 */
public interface ItemClickListener {
    public void onItemClick(View view, int position);

}
