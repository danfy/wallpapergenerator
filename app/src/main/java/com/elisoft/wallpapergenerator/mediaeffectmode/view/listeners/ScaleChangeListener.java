package com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners;

/**
 * Created by yvorobey on 9/22/2015.
 */
public interface ScaleChangeListener {
     void onScaleChanged (float scale);
}
