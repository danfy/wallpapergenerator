package com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners;

/**
 * Created by yvorobey on 9/14/2015.
 */
public interface OnColorClickListener {
    void onColorClicked();
    void onFirstColorClicked();
    void onSecondColorClicked();
}
