package com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners;

/**
 * Created by yvorobey on 9/24/2015.
 */
public interface OnTouchScreenListener {

    public void onTouchScreenCoordinates(float x, float y, float z);
}
