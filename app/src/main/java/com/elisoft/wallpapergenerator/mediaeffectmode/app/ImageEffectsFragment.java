package com.elisoft.wallpapergenerator.mediaeffectmode.app;


import android.app.Activity;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.media.effect.Effect;
import android.media.effect.EffectContext;
import android.media.effect.EffectFactory;
import android.net.Uri;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;


import com.elisoft.wallpapergenerator.ItemClickListener;
import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.app.IconProgressDialog;
import com.elisoft.wallpapergenerator.colorpicker.app.ColorPickerFragmentDialog;
import com.elisoft.wallpapergenerator.mediaeffectmode.adapter.EffectModeAdapter;
import com.elisoft.wallpapergenerator.mediaeffectmode.utils.DefaultEffectValues;
import com.elisoft.wallpapergenerator.mediaeffectmode.utils.TextureRenderer;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.EffectControlFactory;

import com.elisoft.wallpapergenerator.mediaeffectmode.view.EffectsSurfaceView;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.decoration.HorizontalListDecoration;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.EffectsValueChangeListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols.AbstractEffectControl;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.OnColorClickListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.PositionChangeListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.ScaleChangeListener;
import com.elisoft.wallpapergenerator.wall.wallpapers.ProgressDialogUpdater;
import com.elisoft.wallpapergenerator.wall.wallpapers.UpdateProgress;
import com.elisoft.wallpapergenerator.wall.wallpapers.WallCreator;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by yvorobey on 9/9/2015.
 */
public class ImageEffectsFragment extends Fragment implements GLSurfaceView.Renderer, EffectsValueChangeListener, OnColorClickListener {
    private static final int REQUEST_PICK_FROM_GALLERY = 1;
    private static final String STATE_CURRENT_EFFECT = "current_effect";
    private EffectsSurfaceView mImageEffectView;
    private int[] mTextures = new int[2];
    private EffectContext mEffectContext;
    private Effect mEffect;
    private TextureRenderer mTexRenderer = new TextureRenderer();
    private int mImageWidth;
    private int mImageHeight;
    private boolean mInitialized = false;
    private int mCurrentEffect;
    private String pathFile;
    private RecyclerView recyclerView;
    private FrameLayout.LayoutParams layoutParams;
    private AbstractEffectControl valueView;
    private boolean isEffectModeChanged = true;
    private int tintColor;
    private int firstColor;
    private int secondColor;
    private WallCreator wallCreator;
    private UpdateProgress updateProgress;
    private boolean isSave = false;
    private FrameLayout effectSettingsContainer;
    private ArrayList<TextureRenderer> textureRenderes;
    private TextureRenderer mCurrentTexture;
    private float[] matrix = new float[16];


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firstColor = getActivity().getResources().getColor(R.color.yellow_500);
        tintColor = getActivity().getResources().getColor(R.color.purple_500);
        textureRenderes = new ArrayList<>();
        textureRenderes.add(mTexRenderer);
        mCurrentTexture = mTexRenderer;
        secondColor = getActivity().getResources().getColor(R.color.grey_500);
        wallCreator = new WallCreator(getActivity());
        final android.support.v7.app.ActionBar actionBar =  ((AppCompatActivity) getActivity()).getSupportActionBar();
               if(actionBar!=null) {
                   actionBar.setDisplayHomeAsUpEnabled(true);
               }
        IconProgressDialog iconProgressDialog = createProgressDialog();
        updateProgress = new UpdateProgress(getActivity(),new ProgressDialogUpdater(getActivity(), iconProgressDialog) {
            @Override
            public void close() {
                super.close();
                Toast.makeText(getActivity(), getString(R.string.text_image_added_to_favorite), Toast.LENGTH_SHORT).show();
            }
        });
        Matrix.setIdentityM(matrix, 0);

        setHasOptionsMenu(true);
    }

    private IconProgressDialog createProgressDialog() {

        IconProgressDialog iconProgressDialog = new IconProgressDialog(getActivity());
        iconProgressDialog.setText(getString(R.string.text_saving));
        return iconProgressDialog;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        effectSettingsContainer = (FrameLayout) inflater.inflate(R.layout.fragment_image_effects, container, false);
        return effectSettingsContainer;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        recyclerView = (RecyclerView) view.findViewById(R.id.list_effect_mode);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
//        final Button button = (Button) view.findViewById(R.id.button_add_photo);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sendIntentImagePick();
//            }
//        });
        final EffectModeAdapter effectModeAdapter = new EffectModeAdapter(getActivity());
        effectModeAdapter.mItemClickListener = new ItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                setCurrentEffect(view.getId());
                if(!isEffectModeChanged) {
                    return;
                }
                effectSettingsContainer.removeView(valueView);

                valueView = EffectControlFactory.getEffectControl(getActivity(), mCurrentEffect);

                if(valueView!=null) {
                    effectSettingsContainer.addView(valueView, layoutParams);

                    valueView.setEffectsValueChangeListener(ImageEffectsFragment.this);
                    valueView.setOnColorClickListener(ImageEffectsFragment.this);
                    valueView.setFirstColor(firstColor);
                    valueView.setSecondColor(secondColor);
                    valueView.setSingleColor(tintColor);
                }
                mImageEffectView.requestRender();
            }
        };
        recyclerView.setAdapter(effectModeAdapter);
        recyclerView.addItemDecoration(new HorizontalListDecoration(2));

        //        recyclerView.addItemDecoration(new HorizontalListDecoration(getResources().getDimensionPixelSize(R.dimen.space_between_pattern_items)));
//        viewStub = (ViewStub) view.findViewById(R.id.view_effect_settings);

        layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.BOTTOM;
        layoutParams.setMargins(0,0,0, getActivity().getResources().getDimensionPixelSize(R.dimen.recycle_view_effect_mode_height));

        mImageEffectView = (EffectsSurfaceView) view.findViewById(R.id.image_effects_view);
        mImageEffectView.setEGLContextClientVersion(2);
        mImageEffectView.setRenderer(this);
        mImageEffectView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
        mImageEffectView.setScaleChangeListener(new ScaleChangeListener() {
            @Override
            public void onScaleChanged(float scale) {

//                Matrix.scaleM(matrix, 0, scale, scale, 1.0f);
//                mImageEffectView.requestRender();
            }
        });
        mImageEffectView.setPositionChangeListener(new PositionChangeListener() {
            @Override
            public void onPositionChanged(float dx, float dy, float dz) {


//                Matrix.translateM(matrix, 0, dx / (mImageEffectView.getWidth() * mImageEffectView.getScaleFactor()), -dy / (mImageEffectView.getHeight() * mImageEffectView.getScaleFactor()), dz);
//                Log.d("TAG" , " dx : " + dx/mImageEffectView.getWidth() + " , dy : " + dy/mImageEffectView.getHeight() + "scale " + mImageEffectView.getScale());

//                mImageEffectView.requestRender();
            }
        });
        if(null != savedInstanceState && savedInstanceState.containsKey(STATE_CURRENT_EFFECT)) {
            setCurrentEffect(savedInstanceState.getInt(STATE_CURRENT_EFFECT));
        } else {
            setCurrentEffect(R.id.none);
        }

        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_CURRENT_EFFECT, mCurrentEffect);
    }

    private void setCurrentEffect(int effect) {
        if(mCurrentEffect!=effect) {
            isEffectModeChanged = true;
        }
        mCurrentEffect = effect;
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        if(mTexRenderer!=null) {
            mTexRenderer.updateViewSize(width, height);
        }
    }

    private void sendIntentImagePick() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        try {
            startActivityForResult(intent, REQUEST_PICK_FROM_GALLERY);
        } catch (ActivityNotFoundException e) {

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_PICK_FROM_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {

                pathFile = getImagePathFromUri(data.getData());
                mInitialized = false;


                mImageEffectView.requestRender();
            }
        }

    }
    private String getImagePathFromUri(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri, projection,null,null,null);
        String picturePath = "";
        if(cursor!=null) {
           int columnIndex = cursor.getColumnIndex(projection[0]);
            cursor.moveToFirst();
            picturePath = cursor.getString(columnIndex);
            cursor.close();
        }

        return picturePath;
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        if(!mInitialized) {
            mEffectContext = EffectContext.createWithCurrentGlContext();
            mTexRenderer.init();
            if(loadTextures()) {
                mInitialized = true;
            } else {
                mInitialized = false;
            }
        }
        if(mCurrentEffect!=R.id.none) {
            if(isEffectModeChanged) {
                initEffect();
                isEffectModeChanged = false;
            }
            applyEffect();
        }
        renderResult();

        if(isSave) {
            updateProgress.show();
            int x = (int)mImageEffectView.getX();
            int y = (int)mImageEffectView.getY();

            Bitmap bitmap = wallCreator.savePixels(x, y, mImageEffectView.getWidth(), mImageEffectView.getHeight(), gl);
            wallCreator.saveToFile(bitmap);
            bitmap.recycle();
            isSave = false;
            updateProgress.close();
        }

    }
    private boolean loadTextures(){
        GLES20.glGenTextures(2, mTextures, 0);
        Bitmap bitmap = BitmapFactory.decodeFile(pathFile);
        if(bitmap != null) {
            mImageWidth = bitmap.getWidth();
            mImageHeight = bitmap.getHeight();
            mTexRenderer.updateTextureSize(mImageWidth, mImageHeight);

            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
            initTexParams();
            return true;
        }
        return false;

    }

    public void requestRendering() {
        mImageEffectView.requestRender();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.media_effects, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        setCurrentEffect(item.getItemId());
//        mImageEffectView.requestRender();
        if(item.getItemId() == R.id.action_save) {
            isSave = true;
            mImageEffectView.requestRender();
        }
        if(item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }

    private void initEffect() {
        EffectFactory effectFactory = mEffectContext.getFactory();
        if(mEffect!=null) {
            mEffect.release();
        }
        switch (mCurrentEffect) {
            case R.id.none:
                break;
            case R.id.autofix:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_AUTOFIX);
                mEffect.setParameter("scale", DefaultEffectValues.AUTOFIX_SCALE);

                break;

            case R.id.bw:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_BLACKWHITE);
                mEffect.setParameter("black", DefaultEffectValues.BLACK_WHITE_BLACK_VALUE);
                mEffect.setParameter("white", DefaultEffectValues.BLACK_WHITE_WHITE_VALUE);
                break;

            case R.id.brightness:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_BRIGHTNESS);
                mEffect.setParameter("brightness", DefaultEffectValues.BRIGHTNESS);
                break;

            case R.id.contrast:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_CONTRAST);
                mEffect.setParameter("contrast", DefaultEffectValues.CONTRAST);
                break;

            case R.id.crossprocess:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_CROSSPROCESS);
                break;

            case R.id.documentary:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_DOCUMENTARY);
                break;

            case R.id.duotone:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_DUOTONE);
                mEffect.setParameter("first_color", firstColor);
                mEffect.setParameter("second_color", secondColor);
                break;

            case R.id.filllight:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_FILLLIGHT);
                mEffect.setParameter("strength", DefaultEffectValues.FILL_LIGHT_STRENGTH);
                break;

            case R.id.fisheye:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_FISHEYE);
                mEffect.setParameter("scale", DefaultEffectValues.FISH_EYE_SCALE);
                break;

            case R.id.flipvert:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_FLIP);
                mEffect.setParameter("vertical", true);
                break;

            case R.id.fliphor:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_FLIP);
                mEffect.setParameter("horizontal", true);
                break;

            case R.id.grain:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_GRAIN);
                mEffect.setParameter("strength", DefaultEffectValues.GRAIN_STRENGTH);
                break;

            case R.id.grayscale:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_GRAYSCALE);
                break;

            case R.id.lomoish:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_LOMOISH);
                break;

            case R.id.negative:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_NEGATIVE);
                break;

            case R.id.posterize:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_POSTERIZE);
                break;

            case R.id.rotate:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_ROTATE);
                mEffect.setParameter("angle", 180);
                break;

            case R.id.saturate:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_SATURATE);
                mEffect.setParameter("scale", DefaultEffectValues.SATURATE_SCALE);
                break;

            case R.id.sepia:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_SEPIA);
                break;

            case R.id.sharpen:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_SHARPEN);
                break;

            case R.id.temperature:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_TEMPERATURE);
                mEffect.setParameter("scale", DefaultEffectValues.TEMPERATURE_SCALE);
                break;

            case R.id.tint:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_TINT);
                mEffect.setParameter("tint", tintColor);
                break;

            case R.id.vignette:
                mEffect = effectFactory.createEffect(EffectFactory.EFFECT_VIGNETTE);
                mEffect.setParameter("scale", DefaultEffectValues.VIGNETTE_SCALE);
                break;

            default:
                break;
        }
    }
    private void applyEffect() {
        mEffect.apply(mTextures[0], mImageWidth, mImageHeight, mTextures[1]);
    }
    private void renderResult() {
        if(mCurrentEffect!=R.id.none) {
            mTexRenderer.renderTexture(mTextures[1], matrix);
        } else {
            mTexRenderer.renderTexture(mTextures[0], matrix);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        pathFile = activity.getIntent().getStringExtra(ImageEffectsActivity.STRING_PATH);

    }

    private  void initTexParams() {
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
                GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
                GLES20.GL_CLAMP_TO_EDGE);
    }

    @Override
    public void onSetFloatValue(String parameter, float scale) {


            if (mEffect != null) {
                try {
                    mEffect.setParameter(parameter, scale);
                    mImageEffectView.requestRender();
                } catch (IllegalArgumentException e) {

                }
            }
        }


    @Override
    public void onSetIntegerValue(String parameter, int value) {

        if(mEffect!=null) {
            try {
                mEffect.setParameter(parameter, value);
                mImageEffectView.requestRender();
            } catch (IllegalArgumentException e) {

            }
        }
    }

    @Override
    public void onSetBlackValue(float blackValue) {
        if(mEffect!=null) {
            try {
                mEffect.setParameter("black", blackValue);
                mImageEffectView.requestRender();
            } catch (IllegalArgumentException e) {

            }
        }
    }

    @Override
    public void onSetWhiteValue(float whiteValue) {
        if(mEffect!=null) {
            try {
                mEffect.setParameter("white", whiteValue);
                mImageEffectView.requestRender();
            }catch (IllegalArgumentException e) {

            }
        }
    }


    private void setEffectParameter(String parameter, float value) {
        mEffect.setParameter(parameter, value);
    }


    @Override
    public void onColorClicked() {
        showDialog(ColorPickerFragmentDialog.BUNDLE_SINGLE_COLOR_CLICKED);
    }
    @Override
    public void onFirstColorClicked() {
        showDialog(ColorPickerFragmentDialog.BUNDLE_FIRST_COLOR_CLICKED);
    }

    @Override
    public void onSecondColorClicked() {
        showDialog(ColorPickerFragmentDialog.BUNDLE_SECOND_COLOR_CLICKED);
    }

    private void showDialog(int bundleClickMode) {
        ColorPickerFragmentDialog colorPickerFragmentDialog = new ColorPickerFragmentDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(ColorPickerFragmentDialog.BUNDLE_PICK_COLOR, bundleClickMode);
        colorPickerFragmentDialog.setArguments(bundle);
        colorPickerFragmentDialog.show(getFragmentManager(), "DialogColor");
    }


    public void setTint(int color) {
        tintColor = color;
        valueView.setSingleColor(color);
        if(mEffect!=null) {
            try {
                mEffect.setParameter("tint", color);
                mImageEffectView.requestRender();
            } catch (IllegalArgumentException e) {

            }
        }
    }
    public void setFirstColor(int color) {
        firstColor = color;
        valueView.setFirstColor(color);
        if(mEffect!=null) {
            try {
                mEffect.setParameter("first_color", color);
                mImageEffectView.requestRender();
            } catch (IllegalArgumentException e) {

            }
        }
    }
    public void setSecondColor(int color) {
        secondColor = color;
        valueView.setSecondColor(color);
        if(mEffect!=null) {
            try {
                mEffect.setParameter("second_color", color);
                mImageEffectView.requestRender();
            } catch (IllegalArgumentException e) {

            }
        }
    }
}
