package com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 9/14/2015.
 */
public class EffectControlWithTwoColor extends AbstractEffectControl {
    protected ImageView firstColorImg, secondColorImg;
    protected TextView firstColorTitle, secondColorTitle;

    public EffectControlWithTwoColor(Context context) {
        super(context);
    }

    public EffectControlWithTwoColor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EffectControlWithTwoColor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EffectControlWithTwoColor(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.effect_control_two_colors,this);
        firstColorImg= (ImageView) view.findViewById(R.id.img_color);
        firstColorImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorClickListener.onFirstColorClicked();
            }
        });
        firstColorTitle = (TextView)view.findViewById(R.id.text_setting_effect);
        firstColorTitle.setText(context.getString(R.string.first_color_value));
        secondColorImg = (ImageView)view.findViewById(R.id.img_second_color);
        secondColorImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onColorClickListener.onSecondColorClicked();
            }
        });
        secondColorTitle = (TextView) view.findViewById(R.id.text_setting_effect_second);
        secondColorTitle.setText(context.getString(R.string.second_color_value));

    }

    @Override
    public void setFirstColor(int color) {
        firstColorImg.setBackgroundColor(color);
    }

    @Override
    public void setSecondColor(int color) {
        secondColorImg.setBackgroundColor(color);
    }

    @Override
    public void setSingleColor(int color) {
        //Do nothing
    }
}
