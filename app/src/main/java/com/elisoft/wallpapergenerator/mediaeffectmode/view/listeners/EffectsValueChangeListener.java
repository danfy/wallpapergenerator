package com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners;

/**
 * Created by yvorobey on 9/10/2015.
 */
public interface EffectsValueChangeListener {
    void onSetFloatValue(String parameter, float value);
    void onSetIntegerValue(String parameter, int value);
    void onSetBlackValue(float blackValue);
    void onSetWhiteValue(float whiteValue);
//    void onSetFirstColor(int firstColorValue);
//    void onSetSecondColor(int secondColorValue);

}
