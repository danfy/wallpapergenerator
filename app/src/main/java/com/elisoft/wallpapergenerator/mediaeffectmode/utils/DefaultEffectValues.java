package com.elisoft.wallpapergenerator.mediaeffectmode.utils;

/**
 * Created by yvorobey on 9/11/2015.
 */

public class DefaultEffectValues {

    public static final float BLACK_WHITE_BLACK_VALUE = .1f;
    public static final float BLACK_WHITE_WHITE_VALUE = .7f;

    public static final float AUTOFIX_SCALE = 0.0f;

    public static final float BRIGHTNESS = 2.0f;

    public static final float CONTRAST = 1.4f;

    public static final float FILL_LIGHT_STRENGTH = .8f;

    public static final float FISH_EYE_SCALE = 0.5f;

    public static final float GRAIN_STRENGTH = 1.0f;

    public static final float SATURATE_SCALE = 0.5f;

    public static final float TEMPERATURE_SCALE = 0.9f;

    public static final float VIGNETTE_SCALE = 0.5f;

}
