package com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.R;



/**
 * Created by yvorobey on 9/10/2015.
 */
public class EffectControlWithTwoSeekBar extends AbstractEffectControl {
    protected TextView titleSeekBarFirstValue;
    protected SeekBar valueSeekBarFirstValue;
    protected TextView titleSeekBarSecondValue;
    protected SeekBar valueSeekBarSecondValue;
    protected String secondParameter;

    public EffectControlWithTwoSeekBar(Context context) {
        super(context);
    }

    public EffectControlWithTwoSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EffectControlWithTwoSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EffectControlWithTwoSeekBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.effect_control_two_seekbar,this);
        titleSeekBarFirstValue = (TextView) view.findViewById(R.id.text_effect_first_value);
        titleSeekBarSecondValue = (TextView) view.findViewById(R.id.text_effect_second_value);

        valueSeekBarFirstValue = (SeekBar) view.findViewById(R.id.seekBar_first_value);
        valueSeekBarFirstValue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mListener == null) {
                    return;
                }
                mListener.onSetBlackValue((float)progress/scaleFactor);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        valueSeekBarSecondValue =(SeekBar) view.findViewById(R.id.seekBar_second_value);
        valueSeekBarSecondValue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mListener == null) {
                    return;
                }
                mListener.onSetWhiteValue((float)progress/scaleFactor);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void setFirstColor(int color) {
        //Do nothing
    }

    @Override
    public void setSecondColor(int color) {
        //Do nothing

    }

    @Override
    public void setSingleColor(int color) {
        //Do nothing

    }

    public void setTextFirstValue(String text) {
        titleSeekBarFirstValue.setText(text);
    }
    public void setTextSecondValue(String text) {
        titleSeekBarSecondValue.setText(text);
    }


    public void setSecondParameter(String secondParameter) {
        this.secondParameter = secondParameter;
    }


    public void setFirstSeekProgress(float progress) {
        valueSeekBarFirstValue.setProgress((int)(progress * scaleFactor));
    }
    public void setSecondSeekProgress(float progress) {
        valueSeekBarSecondValue.setProgress((int) (progress * scaleFactor));
    }

}
