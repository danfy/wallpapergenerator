package com.elisoft.wallpapergenerator.mediaeffectmode.view.decoration;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by yvorobey on 9/10/2015.
 */
public class HorizontalListDecoration extends RecyclerView.ItemDecoration {
    private int spaceBetween;

    public HorizontalListDecoration(final int space) {
        this.spaceBetween = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.bottom = 2;
        outRect.left = 2;
        outRect.right = 2;
        outRect.top = 2;

    }
}
