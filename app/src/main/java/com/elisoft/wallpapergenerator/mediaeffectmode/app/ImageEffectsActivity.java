package com.elisoft.wallpapergenerator.mediaeffectmode.app;

import android.app.FragmentTransaction;
import android.os.Bundle;


import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.app.AbstractActivity;
import com.elisoft.wallpapergenerator.app.AbstractShareActivity;
import com.elisoft.wallpapergenerator.app.AppApplication;
import com.elisoft.wallpapergenerator.colorpicker.app.ColorPickerFragmentDialog;
import com.elisoft.wallpapergenerator.colorpicker.app.OnColorPickListener;
import com.elisoft.wallpapergenerator.favorites.app.ImageGridFragment;

/**
 * Created by yvorobey on 9/9/2015.
 */
public class ImageEffectsActivity extends AbstractShareActivity implements OnColorPickListener {
    private static final String TAG = ImageEffectsActivity.class.getSimpleName();
    public static final String STRING_PATH = "string_path_file";
    private ImageEffectsFragment imageEffectsFragment;

    @Override
    protected void setItemId() {
        //Do nothing
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_effects);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AppApplication.sendGoogleAnalyticsScreenName("ImageEffectsActivity");
        if(checkPermission(PERMISSION_WRITE_EXTERNAL_STORAGE, REQUEST_PERMISSION_EXTERNAL_STORAGE)) {
            initializeFragment();
        }
    }

    @Override
    public void onPickColor(int color, int clickedMode) {
        if(clickedMode == ColorPickerFragmentDialog.BUNDLE_SINGLE_COLOR_CLICKED) {
            imageEffectsFragment.setTint(color);
        }else {
            if(clickedMode == ColorPickerFragmentDialog.BUNDLE_FIRST_COLOR_CLICKED) {
                imageEffectsFragment.setFirstColor(color);
            } else {
                imageEffectsFragment.setSecondColor(color);
            }
        }
    }
    private void initializeFragment() {

        imageEffectsFragment = (ImageEffectsFragment) getFragmentManager().findFragmentByTag(TAG);
        if ( imageEffectsFragment== null) {
            imageEffectsFragment = new ImageEffectsFragment();
            final FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.add(R.id.edit_image_fragment_container, imageEffectsFragment, TAG);
            ft.commitAllowingStateLoss();
        } else {
            imageEffectsFragment.requestRendering();
        }

    }


    @Override
    protected void permissionWriteExternalStorageGranted() {
        initializeFragment();
    }

    @Override
    protected void permissionWriteExternalStorageNotGranted() {
        this.finish();
    }
}
