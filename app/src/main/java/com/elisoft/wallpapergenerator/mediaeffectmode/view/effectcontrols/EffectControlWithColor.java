package com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 9/14/2015.
 */
public class EffectControlWithColor extends AbstractEffectControl {
    protected ImageView imageView;
    protected TextView textView;

    public EffectControlWithColor(Context context) {
        super(context);
    }

    public EffectControlWithColor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EffectControlWithColor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EffectControlWithColor(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.effect_control_one_color,this);
        imageView = (ImageView) view.findViewById(R.id.img_color);
        imageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onColorClickListener!=null) {
                    onColorClickListener.onColorClicked();
                }
            }
        });
        textView = (TextView)view.findViewById(R.id.text_setting_effect);
        textView.setText(context.getString(R.string.color_value));
    }

    @Override
    public void setFirstColor(int color) {
        //Do nothing
    }

    @Override
    public void setSecondColor(int color) {
        //Do nothing

    }

    @Override
    public void setSingleColor(int color) {
        imageView.setBackgroundColor(color);
    }


}
