package com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols;

import android.content.Context;
import android.util.AttributeSet;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.EffectsValueChangeListener;

/**
 * Created by yvorobey on 9/10/2015.
 */
public class EffectControlWithOneSeekBar extends AbstractEffectControl {

    protected TextView titleSeekBar;
    protected SeekBar valueSeekBar;

    public EffectControlWithOneSeekBar(Context context) {
        super(context);
    }

    public EffectControlWithOneSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EffectControlWithOneSeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public EffectControlWithOneSeekBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }


    @Override
    protected void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.effect_control_one_seekbar,this);
        titleSeekBar = (TextView) view.findViewById(R.id.text_setting_effect);
        valueSeekBar = (SeekBar) view.findViewById(R.id.seekBar);
        valueSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(mListener == null) {
                    return;
                }
                if (parameter.equals("angle")) {

                    mListener.onSetIntegerValue(parameter, 90 * progress);
                } else {
                    mListener.onSetFloatValue(parameter, (float) progress / scaleFactor);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    public void setFirstColor(int color) {
        //Do nothing
    }

    @Override
    public void setSecondColor(int color) {
        //Do nothing
    }

    @Override
    public void setSingleColor(int color) {
        //Do nothing

    }

    public void setText(String text) {
        titleSeekBar.setText(text);
    }

    public void setSeekProgress(float progress) {

        valueSeekBar.setProgress((int) (progress * scaleFactor));
    }
    public void setSeekMax(int max) {
        valueSeekBar.setMax(max);
    }
}
