package com.elisoft.wallpapergenerator.mediaeffectmode.view;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.PositionChangeListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.ScaleChangeListener;

/**
 * Created by yvorobey on 9/22/2015.
 */
public class EffectsSurfaceView extends GLSurfaceView {
    private static final String TAG = EffectsSurfaceView.class.getSimpleName();

    ScaleChangeListener scaleChangeListener;
    PositionChangeListener positionChangeListener;

    private ScaleGestureDetector mScaleDetector;

    private float scaleFactor = 1.0f;

    private float mLastTouchX;
    private float mLastTouchY;
    private int mActivePointerId = MotionEvent.INVALID_POINTER_ID;
    public EffectsSurfaceView(Context context) {
        super(context);
        initGestureDetector(context);

    }

    public EffectsSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initGestureDetector(context);

    }
    private void initGestureDetector(Context context) {
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
    }

    public void setScaleChangeListener(ScaleChangeListener scaleChangeListener) {
        this.scaleChangeListener = scaleChangeListener;
    }

    public void setPositionChangeListener(PositionChangeListener positionChangeListener) {
        this.positionChangeListener = positionChangeListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mScaleDetector.onTouchEvent(event);

        final  int action = MotionEventCompat.getActionMasked(event);
        switch (action) {
            case MotionEvent.ACTION_DOWN:{
                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                final float x = MotionEventCompat.getX(event, pointerIndex);
                final float y = MotionEventCompat.getY(event, pointerIndex);
                mLastTouchX = x;
                mLastTouchY = y;
                mActivePointerId = MotionEventCompat.getPointerId(event, 0);
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                final int pointerIndex = MotionEventCompat.findPointerIndex(event, mActivePointerId);
                final float x = MotionEventCompat.getX(event, pointerIndex);
                final float y = MotionEventCompat.getY(event, pointerIndex);
                final float dx = x - mLastTouchX;
                final float dy = y - mLastTouchY;

                positionChangeListener.onPositionChanged(dx, dy, 0.0f);

                mLastTouchY = y;
                mLastTouchX = x;
                break;
            }
            case MotionEvent.ACTION_UP:
                mActivePointerId = MotionEvent.INVALID_POINTER_ID;
                break;
            case MotionEvent.ACTION_CANCEL: {
                mActivePointerId = MotionEvent.INVALID_POINTER_ID;
                break;
            }
            case MotionEvent.ACTION_POINTER_UP:
                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                final int pointerId = MotionEventCompat.getPointerId(event, pointerIndex);

                if(pointerId == mActivePointerId) {
                    final int newPointerIndex = pointerIndex ==0 ?1:0;
                    mLastTouchX = MotionEventCompat.getX(event, newPointerIndex);
                    mLastTouchY = MotionEventCompat.getY(event, newPointerIndex);
                    mActivePointerId = MotionEventCompat.getPointerId(event, newPointerIndex);
                }
                break;
        }
        return true;
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scaleFactor =detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 1.5f));
            scaleChangeListener.onScaleChanged(scaleFactor);
            return true;
        }
    }


}
