package com.elisoft.wallpapergenerator.mediaeffectmode.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.elisoft.wallpapergenerator.ItemClickListener;
import com.elisoft.wallpapergenerator.R;

/**
 * Created by yvorobey on 9/10/2015.
 */
public class EffectModeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static int[] stringResources = {R.string.none, R.string.autofix, R.string.bw, R.string.brightness, R.string.contrast, R.string.crossprocess, R.string.documentary, R.string.duotone, R.string.filllight, R.string.fisheye, R.string.flipvert, R.string.fliphor, R.string.grain, R.string.grayscale, R.string.lomoish, R.string.negative, R.string.posterize, R.string.rotate, R.string.saturate, R.string.sepia, R.string.sharpen, R.string.temperature, R.string.tint, R.string.vignette};
    private static int[] ids = {R.id.none, R.id.autofix, R.id.bw, R.id.brightness, R.id.contrast, R.id.crossprocess, R.id.documentary, R.id.duotone, R.id.filllight, R.id.fisheye, R.id.flipvert, R.id.fliphor, R.id.grain, R.id.grayscale, R.id.lomoish, R.id.negative, R.id.posterize, R.id.rotate, R.id.saturate, R.id.sepia, R.id.sharpen, R.id.temperature, R.id.tint, R.id.vignette};
    private static int[] drawables = {R.drawable.ic_clear_white_18dp,R.drawable.ic_brightness_auto_white_18dp,R.drawable.ic_filter_b_and_w_white_18dp,R.drawable.ic_brightness_5_white_18dp,R.drawable.ic_brightness_6_white_18dp,R.drawable.ic_tonality_white_18dp,R.drawable.ic_camera_roll_white_18dp,R.drawable.ic_invert_colors_white_18dp,R.drawable.ic_flare_white_18dp,R.drawable.ic_panorama_fish_eye_white_18dp,R.drawable.ic_flip_vertical_white_18dp,R.drawable.ic_flip_horizontal_white_18dp,R.drawable.ic_grain_white_18dp,R.drawable.ic_brightness_2_white_18dp,R.drawable.ic_filter_tilt_shift_white_18dp,R.drawable.ic_image_white_18dp,R.drawable.ic_broken_image_white_18dp, R.drawable.ic_rotate_right_white_18dp,R.drawable.ic_wb_incandescent_white_18dp,R.drawable.ic_texture_white_18dp,R.drawable.ic_straighten_white_18dp,R.drawable.ic_exposure_white_18dp,R.drawable.ic_tonality_white_18dp,R.drawable.ic_vignette_white_18dp};
    public ItemClickListener mItemClickListener;
    private Context mContext;


    public EffectModeAdapter(Context context){
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_effect_mode,parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;


        viewHolder.itemView.setId(ids[position]);
        viewHolder.iconEffect.setImageResource(drawables[position]);
        viewHolder.effectTitle.setText(mContext.getString(stringResources[position]));
    }

    @Override
    public int getItemCount() {
        return stringResources.length;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView effectTitle;
        ImageView iconEffect;
        CardView cardView;
        public ViewHolder(View itemView) {
            super(itemView);
            effectTitle = (TextView) itemView.findViewById(R.id.item_effect_text);
            iconEffect = (ImageView) itemView.findViewById(R.id.icon_effect);
            cardView = (CardView) itemView.findViewById(R.id.card_mode_effect);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener!=null) {
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
            notifyItemChanged(getAdapterPosition());

        }
    }
}
