package com.elisoft.wallpapergenerator.mediaeffectmode.view;

import android.content.Context;

import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.mediaeffectmode.utils.DefaultEffectValues;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols.AbstractEffectControl;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols.EffectControlWithColor;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols.EffectControlWithOneSeekBar;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols.EffectControlWithTwoColor;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols.EffectControlWithTwoSeekBar;

/**
 * Created by yvorobey on 9/10/2015.
 */
public class EffectControlFactory {


    public static AbstractEffectControl getEffectControl(Context context, int effectId) {
        AbstractEffectControl abstractEffectControl = null;
        switch (effectId) {
            case R.id.autofix:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.scale_value));
                abstractEffectControl.setParameter("scale");
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.AUTOFIX_SCALE);
                break;
            case R.id.bw:
                abstractEffectControl = new EffectControlWithTwoSeekBar(context);
                ((EffectControlWithTwoSeekBar)abstractEffectControl).setTextFirstValue(context.getString(R.string.black_value));
                ((EffectControlWithTwoSeekBar)abstractEffectControl).setTextSecondValue(context.getString(R.string.white_value));
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithTwoSeekBar) abstractEffectControl).setFirstSeekProgress(DefaultEffectValues.BLACK_WHITE_BLACK_VALUE);
                ((EffectControlWithTwoSeekBar) abstractEffectControl).setSecondSeekProgress(DefaultEffectValues.BLACK_WHITE_WHITE_VALUE);


                break;
            case R.id.brightness:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.brightness_value));
                abstractEffectControl.setParameter("brightness");
                abstractEffectControl.setScaleFactor(20);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.BRIGHTNESS);
                break;
            case R.id.contrast:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.contrast_value));
                abstractEffectControl.setParameter("contrast");
                abstractEffectControl.setScaleFactor(20);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.CONTRAST);
                break;

            case R.id.duotone:
                abstractEffectControl = new EffectControlWithTwoColor(context);

                break;
            case R.id.filllight:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.strength_value));
                abstractEffectControl.setParameter("strength");
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.FILL_LIGHT_STRENGTH);
                break;
            case R.id.fisheye:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.scale_value));
                abstractEffectControl.setParameter("scale");
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.FISH_EYE_SCALE);
                break;

            case R.id.grain:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.strength_value));
                abstractEffectControl.setParameter("strength");
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.GRAIN_STRENGTH);
                break;

            case R.id.tint:
                abstractEffectControl = new EffectControlWithColor(context);

                break;
            case R.id.saturate:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.scale_value));
                abstractEffectControl.setParameter("scale");
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.SATURATE_SCALE);
                break;
            case R.id.sharpen:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.scale_value));
                abstractEffectControl.setParameter("scale");
                abstractEffectControl.setScaleFactor(100);

                break;
            case R.id.temperature:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.scale_value));
                abstractEffectControl.setParameter("scale");
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.TEMPERATURE_SCALE);
                break;
            case R.id.vignette:
                abstractEffectControl = new EffectControlWithOneSeekBar(context);
                ((EffectControlWithOneSeekBar)abstractEffectControl).setText(context.getString(R.string.scale_value));
                abstractEffectControl.setParameter("scale");
                abstractEffectControl.setScaleFactor(100);
                ((EffectControlWithOneSeekBar) abstractEffectControl).setSeekProgress(DefaultEffectValues.VIGNETTE_SCALE);
                break;
        }
        return abstractEffectControl;
    }

}
