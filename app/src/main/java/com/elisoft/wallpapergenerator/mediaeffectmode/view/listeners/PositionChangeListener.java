package com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners;

/**
 * Created by yvorobey on 9/23/2015.
 */
public interface PositionChangeListener {
    public void onPositionChanged(float dx, float dy, float dz);
}
