package com.elisoft.wallpapergenerator.mediaeffectmode.view.effectcontrols;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;

import android.widget.RelativeLayout;


import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.EffectsValueChangeListener;
import com.elisoft.wallpapergenerator.mediaeffectmode.view.listeners.OnColorClickListener;

/**
 * Created by yvorobey on 9/10/2015.
 */
public abstract class AbstractEffectControl extends RelativeLayout {
    protected int scaleFactor;
    protected String parameter;
    protected EffectsValueChangeListener mListener;
    protected OnColorClickListener onColorClickListener;

    public AbstractEffectControl(Context context) {
        super(context);
        init(context);
    }

    public AbstractEffectControl(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AbstractEffectControl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public AbstractEffectControl(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }
    public void setEffectsValueChangeListener(EffectsValueChangeListener effectsValueChangeListener) {
        mListener = effectsValueChangeListener;
    }

    public void setOnColorClickListener(OnColorClickListener onColorClickListener) {
        this.onColorClickListener = onColorClickListener;
    }

    protected abstract void init(Context context);

    public void setScaleFactor(int scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    public abstract void setFirstColor(int color);
    public abstract void setSecondColor(int color);
    public abstract void setSingleColor(int color);
}
