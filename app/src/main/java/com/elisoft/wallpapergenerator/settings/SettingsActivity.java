package com.elisoft.wallpapergenerator.settings;

import android.os.Bundle;
import android.view.MenuItem;


import com.elisoft.wallpapergenerator.R;
import com.elisoft.wallpapergenerator.app.AbstractActivity;
import com.elisoft.wallpapergenerator.app.AppApplication;
import com.google.android.gms.analytics.HitBuilders;

/**
 * Created by yvorobey on 7/30/2015.
 */
public class SettingsActivity extends AbstractActivity {

    @Override
    protected void setItemId() {
        mItemId = R.id.settings;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        if(getSupportActionBar()!=null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        AppApplication.sendGoogleAnalyticsScreenName("Screen : " + getString(R.string.screen_settings));
    }
}
