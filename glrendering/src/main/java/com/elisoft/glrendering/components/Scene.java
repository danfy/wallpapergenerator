package com.elisoft.glrendering.components;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.elisoft.glrendering.utils.FloatColor;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;



/**
 * Class holds layers for drawing
 */
public class Scene implements Serializable{
    private static final long serialVersionUID = 8743212345L;
    // Background color. Use in GLES20.glClearColor(float, float, float, float).
    private FloatColor backgroundColor;
    private transient static final String FIRST_FILE ="first.ser", SECOND_FILE = "second.ser", THIRD_FILE = "third.ser", FOURTH_FILE = "fourth.ser";
    private transient static int countSave = 0;
    private int levelCount = 0;
    private int nextInsertedIndex = 0;
    private HashMap<Integer, Layer> layerHashMap;

    public Scene() {
        layerHashMap = new HashMap<>();
    }
    public void addLayer(Layer layer) {
        layerHashMap.put(nextInsertedIndex, layer);
        levelCount++;
        nextInsertedIndex++;
    }
    public void draw() {
        for(Layer layer: layerHashMap.values()) {
            layer.drawLayer();
        }
    }
    public void draw(float[] mMVPMatrix) {
        for (Layer layer: layerHashMap.values()) {
            layer.drawLayer(mMVPMatrix);
        }
    }
    public void clear() {
        layerHashMap.clear();
        nextInsertedIndex= 0;
    }

    public FloatColor getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(FloatColor backgroundColor) {
        this.backgroundColor = backgroundColor;
    }
    public String saveScene(Context context) {
        String fileName = "";
        countSave++;
        if(countSave ==5) {
            countSave =1;
        }
        switch (countSave) {
            case 1: fileName = FIRST_FILE;
                break;
            case 2: fileName = SECOND_FILE;
                break;
            case 3: fileName = THIRD_FILE;
                break;
            case 4: fileName = FOURTH_FILE;
        }
        try {
            FileOutputStream fileOutputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(this);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileName;
    }
    public static Scene loadScene(Context context, String fileName) {
        Scene scene = null;

        try {
            FileInputStream fileInputStream = context.openFileInput(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            scene = (Scene) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return scene;
    }
    public void initializeShapesFromFile(Context context) {
        for(Layer layer: layerHashMap.values()) {
            layer.initializeShapesFromFile(context);
        }
    }
    public HashMap<Integer, Layer> getLayerHashMap() {
        return layerHashMap;
    }
}
