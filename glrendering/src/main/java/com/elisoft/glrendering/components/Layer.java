package com.elisoft.glrendering.components;

import android.content.Context;
import android.graphics.drawable.shapes.Shape;
import android.opengl.GLES20;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

import com.elisoft.glrendering.shapes.AbstractShape;

/**
 * Class represents one level in openGL rendering environment
 * Layer holds shapes which is positioned on the same level
 */
public class Layer implements Serializable {

    private static final long serialVersionUID = 9312456631L;

    private ArrayList<AbstractShape> arrayListShapes;

    public Layer() {
        arrayListShapes = new ArrayList<>();
    }
    public void addShape(AbstractShape shape) {
        arrayListShapes.add(shape);
    }
    public void removeShape(AbstractShape shape) {
        if(arrayListShapes.contains(shape)) {
            arrayListShapes.remove(shape);
        }
    }
    public void drawLayer() {
        for(AbstractShape shape : arrayListShapes) {
            shape.draw();
        }
    }
    public void drawLayer(float[] mMVPMatrix) {

        for(AbstractShape shape : arrayListShapes) {
            if (shape.isShadow()) {

                GLES20.glEnable(GLES20.GL_BLEND);
                GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
                shape.draw(mMVPMatrix);
                GLES20.glDisable(GLES20.GL_BLEND);
            } else if (shape.isTransparent()) {
                GLES20.glEnable(GLES20.GL_BLEND);

                GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);
                GLES20.glBlendEquation(GLES20.GL_FUNC_ADD);

                shape.draw(mMVPMatrix);
                GLES20.glDisable(GLES20.GL_BLEND);
            } else {
                shape.draw(mMVPMatrix);
            }
        }




    }
    public AbstractShape onScreenClicked(float x, float y) {
        if(arrayListShapes.size() == 0) {
            return null;
        }
        for (int i = arrayListShapes.size()-1; i>=0; i--) {
            AbstractShape abstractShape = arrayListShapes.get(i);
            if(abstractShape.checkIfPointInShape(x,y)) {
                return abstractShape;
            }

                                          }
        return null;
    }

    public void initializeShapesFromFile(Context context) {
        for (AbstractShape shape : arrayListShapes) {
            shape.initializeShapeFromFile(context);
        }
    }
}
