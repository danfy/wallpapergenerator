package com.elisoft.glrendering.history;


import android.util.Log;

/**
 * Created by yvorobey on 6/23/2015.
 */
public class ScenesStack {


   private static final int STACK_DEFAULT_SIZE = 4;   //Default size of stack.
   private  String[] stackFileNames;             // Stack array of String.

   private int currentId;
   private int elementsCount;
   private int sizeOfStack;
   private boolean isNextHistoryEnabled = false;
   private boolean isPreviousHistoryEnabled = false;
   private static ScenesStack sInstance;

   public static ScenesStack getInstance(String[] fileNames, int currentId, int elementsCount) {
      if(sInstance == null) {
         if(fileNames == null) {
            sInstance = new ScenesStack();
         } else {
            sInstance = new ScenesStack(fileNames, currentId, elementsCount);
         }

      }
      if(fileNames != null){
         sInstance.setFilesName(fileNames, currentId,elementsCount);
      } else {
         sInstance.initializeEmptyStack();
      }
      return sInstance;
   }


   private void setFilesName(String[] fileNames, int currentId, int elementsCount) {
      stackFileNames = fileNames;
      this.currentId = currentId;
      this.elementsCount = elementsCount;
   }

   private void initializeEmptyStack() {
      stackFileNames = new String[STACK_DEFAULT_SIZE];
      sizeOfStack = STACK_DEFAULT_SIZE;
      elementsCount = 0;
      currentId = 0;
   }
   private ScenesStack(String[] fileNames, int currentElementID, int elementsCount) {
      setFilesName(fileNames, currentElementID, elementsCount);
   }


   /**
    * Default constructor without <p>size</p> argument. Set size of stack to 3.
    */
   private ScenesStack() {

       initializeEmptyStack();


   }



   /**
    * Method add String to the end of the stack.
    * @param fileName - String object of fileName
    */
   public void add(String fileName) {
      sizeOfStack = stackFileNames.length;
      if (elementsCount == sizeOfStack) {
         replaceElements();
         stackFileNames[sizeOfStack - 1] = fileName;
         currentId = sizeOfStack-1;
      } else {
         stackFileNames[elementsCount] = fileName;
         currentId = elementsCount;
         elementsCount++;

      }
      if(currentId>0) {
         isPreviousHistoryEnabled = true;
      }
      isNextHistoryEnabled=false;

   }

   /**
    * Method returns previous elements in stack from current position
    * @return String object in stack previously from current position
    */
   public String getPrevious() {
      currentId--;
      String previous = stackFileNames[currentId];
      if(currentId ==0) {
        isPreviousHistoryEnabled =false;
      }
      isNextHistoryEnabled = true;

      return previous;
   }

   /**
    * Method returns next elements in stack from current position
    * @return String object in stack next from current position
    */
   public String getNext() {
      currentId++;
      String next = stackFileNames[currentId];
      if(currentId == elementsCount-1) {
        isNextHistoryEnabled = false;
      }
      isPreviousHistoryEnabled = true;

      return next;
   }
   private void replaceElements() {
      for(int i =0; i<stackFileNames.length -1; i++) {
         stackFileNames[i] = stackFileNames[i+1];
      }
   }

   public String getCurrentElement() {
      return stackFileNames[currentId];
   }

   public boolean isPreviousHistoryEnabled() {


      return isPreviousHistoryEnabled;
   }

   public boolean isNextHistoryEnabled() {

      return isNextHistoryEnabled;
   }

   public String[] getStackFiles() {
      return stackFileNames;
   }
   public int getCurrentId() {
      return currentId;
   }
   public int getElementsCount() {
      return elementsCount;
   }
}
