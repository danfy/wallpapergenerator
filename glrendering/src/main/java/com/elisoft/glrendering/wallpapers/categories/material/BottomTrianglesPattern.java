package com.elisoft.glrendering.wallpapers.categories.material;


import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.ShadowTriangle;
import com.elisoft.glrendering.shapes.Triangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;


public class BottomTrianglesPattern extends WallpaperPattern {

    private static final float MIN_X = -1.0f;
    private static final float MAX_X = 1.0f;
    private static final float MIN_Y = -0.3f;
    private static final float MAX_Y = 0.7f;


    protected BottomTrianglesPattern(Context context, int id) {
        super(context, id);
        setBackgroundColor(getColor());
    }

    public BottomTrianglesPattern(Context context) {
        super(context, PatternsIds.BOTTOM_TRIANGLES_PATTERN);
        setBackgroundColor(getColor());
    }
    @Override
    protected Layer initializeFirstLayer() {
        Layer firstLayer = new Layer();
        Triangle triangle = generateTriangle(0.1f, getColor(), 1.0f) ;
        ShadowTriangle triangleShadow = triangle.generateShadow();
        firstLayer.addShape(triangleShadow);
        firstLayer.addShape(triangle);
        return firstLayer;
    }
    @Override
    protected Layer initializeSecondLayer() {
        Layer secondLayer = new Layer();
        Triangle triangle = generateTriangle(0.2f, getColor(), 0.8f);
        ShadowTriangle triangleShadow = triangle.generateShadow();
        secondLayer.addShape(triangleShadow);
        secondLayer.addShape(triangle);
        return secondLayer;
    }
    protected Layer initializeThirdLayer() {
        Layer thirdLayer = new Layer();
        Triangle triangle = generateTriangle(0.3f, getColor(), 0.6f);
        ShadowTriangle triangleShadow = triangle.generateShadow();
        thirdLayer.addShape(triangleShadow);
        thirdLayer.addShape(triangle);
        return thirdLayer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        Layer fourthLayer = new Layer();
        Triangle triangle = generateTriangle(0.4f, getColor(), 0.5f);
        ShadowTriangle triangleShadow = triangle.generateShadow();
        fourthLayer.addShape(triangleShadow);
        fourthLayer.addShape(triangle);
        return fourthLayer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }

    private Triangle generateTriangle(float z, FloatColor floatColor, float zoom) {
        Random random = new Random();
        float randomX =  random.nextFloat() *(MAX_X - MIN_X) +MIN_X;
        float randomY =random.nextFloat() *(MAX_Y - MIN_Y)*zoom + MIN_Y;
        Vertex vertexTop = new Vertex(randomX, randomY,z);
        float half = 0;
        if(randomY>0) {
            half = Math.abs(1f+randomY);
        } else {
            half = Math.abs(1f - Math.abs(randomY));
        }
        Vertex vertexBottomLeft = new Vertex(randomX - half, -1.0f, z);
        Vertex vertexBottomRight = new Vertex(randomX + half, -1.0f, z );
        Triangle triangle = new Triangle(vertexTop, vertexBottomLeft, vertexBottomRight);
        triangle.setColor(floatColor);
        return  triangle;

    }



}
