package com.elisoft.glrendering.wallpapers.categories.transparent;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.GradientRadialRectangle;
import com.elisoft.glrendering.shapes.Rectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;



/**
 * Created by yvorobey on 6/29/2015.
 */
public class SquareBubbles extends WallpaperPattern {
    protected boolean isFlat =true;

    public SquareBubbles(Context context, boolean isFlat) {
        super(context, PatternsIds.SQUARE_BUBBLES);
        this.isFlat = isFlat;
        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());

    }

    public SquareBubbles(Context context, int width, int height) {
        super(context, PatternsIds.SQUARE_BUBBLES);
        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());
        this.width = width;
        this.height =height;
    }

    @Override
    protected Layer initializeFirstLayer() {
        return generateFirstLayer();
    }

    @Override
    protected Layer initializeSecondLayer() {
        return generateSecondLayer(0.1f);
    }

    @Override
    protected Layer initializeThirdLayer() {
        return generateThirdLayer(0.2f);
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }

    private Layer generateFirstLayer() {
        Layer layer = new Layer();
        FloatColor firstColor = palete.getSomeColor();
        FloatColor secondColor = FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.black));
        GradientRadialRectangle radialGradient = new GradientRadialRectangle(width,height,firstColor,secondColor, 0.5f, 0.5f, 0.7f);
        layer.addShape(radialGradient);
        return layer;
    }
    private Layer generateSecondLayer(float z) {
        Layer layer = new Layer();
        Random random = new Random();
        float minX = -0.8f;
        float maxX  = 0.8f;
        float minY = -0.5f;
        float maxY = 0.5f;
        float maxWidth = 0.2f;
        float minWidth = 0.1f;
        float x = 0;
        float y = 0;
        float width = 0;
        FloatColor floatColor = FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.white));
        for (int i = 0; i<20; i++) {
            x = random.nextFloat()*(maxX - minX) + minX;
            y = random.nextFloat() *(maxY - minY) + minY;
            width = random.nextFloat()*(maxWidth - minWidth) +minWidth;
            layer.addShape(generateRectangle(x, y, z, width, floatColor, 0.5f));
        }
        return layer;
    }
    private Layer generateThirdLayer(float z) {
        Layer layer = new Layer();
        Random random = new Random();
        float minX = -0.8f;
        float maxX  = 0.8f;
        float minY = -0.5f;
        float maxY = 0.5f;
        float maxRadius = 0.6f;
        float minRadius = 0.3f;
        float x = 0;
        float y = 0;
        float radius = 0;

        FloatColor floatColor = FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.white));
        for (int i = 0; i<5; i++) {
            x = random.nextFloat()*(maxX - minX) + minX;
            y = random.nextFloat() *(maxY - minY) + minY;
            radius = random.nextFloat()*(maxRadius - minRadius) +minRadius;
            layer.addShape(generateRectangle(x, y, z, radius, floatColor, 0.3f));
        }
        return layer;
    }
    private Rectangle generateRectangle(float centerX, float centerY, float centerZ, float width, FloatColor floatColor, float alpha) {
        Vertex centerVertex = new Vertex(centerX, centerY, centerZ);

        Rectangle rectangle = new Rectangle(centerVertex, width, width, 45, isFlat);
        floatColor.setAlpha(alpha);
        rectangle.setIsTransparent(true);
        rectangle.setColor(floatColor);
        return rectangle;
    }
}
