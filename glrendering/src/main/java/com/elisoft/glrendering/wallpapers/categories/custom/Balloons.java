package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 9/15/2015.
 */
public class Balloons extends WallpaperPattern {
    private static final float WIDTH_HEIGHT_RATION_BALLOON =1.4f; //    height /width of balloon texture.
    private final Random random;
    private static final float MAX_WIDTH = 0.7f;
    private static final float MIN_WIDTH = 0.1f;
    public Balloons(Context context) {
        super(context, PatternsIds.BALLOONS_PATTERN);
        random = new Random();
    }




    @Override
    protected Layer initializeFirstLayer() {



        Vertex topLeft = new Vertex(-1.0f, 1.0f, 0.0f);
        Vertex bottomLeft = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRight = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRight = new Vertex(1.0f, 1.0f, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTag("Sky");
        texturedRectangle.setTextResourceId(R.drawable.sky_background_port, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {



        Vertex topLeft = new Vertex(-1.0f, -0.33f, 0.0f);
        Vertex bottomLeft = new Vertex(-1.0f, -0.8f, 0.0f);
        Vertex bottomRight = new Vertex(1.0f, -0.8f, 0.0f);
        Vertex topRight = new Vertex(1.0f, -0.33f, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setIsClickable(true);
        texturedRectangle.setTag("cloud1");
        texturedRectangle.setTextResourceId(R.drawable.cloud_1_port, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        Vertex topLeftSecond = new Vertex(-1.0f, -0.43f, 0.0f);
        Vertex bottomLeftSecond = new Vertex(-1.0f, -0.9f, 0.0f);
        Vertex bottomRightSecond = new Vertex(1.0f, -0.9f, 0.0f);
        Vertex topRightSecond = new Vertex(1.0f, -0.43f, 0.0f);
        TexturedRectangle texturedRectangleSecond = new TexturedRectangle(topLeftSecond, bottomLeftSecond, bottomRightSecond, topRightSecond);
        texturedRectangleSecond.setIsClickable(true);
        texturedRectangleSecond.setTextResourceId(R.drawable.cloud_2_port, mContext);
        texturedRectangleSecond.setTag("cloud2");
        layer.addShape(texturedRectangleSecond);

        Vertex topLeftThird = new Vertex(-1.0f, -0.53f, 0.0f);
        Vertex bottomLeftThird= new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightThird = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightThird = new Vertex(1.0f, -0.53f, 0.0f);
        TexturedRectangle texturedRectangleThird = new TexturedRectangle(topLeftThird, bottomLeftThird, bottomRightThird, topRightThird);
        texturedRectangleThird.setIsClickable(true);
        texturedRectangleThird.setTextResourceId(R.drawable.cloud_3_port, mContext);
        texturedRectangleThird.setTag("cloud3");

        layer.addShape(texturedRectangleThird);
        return layer;


    }

    @Override
    protected Layer initializeThirdLayer() {
        float minX = -0.5f;
        float maxX = 0.5f;
        float minY = -0.5f;
        float maxY = 0.5f;
        float maxWidth;

        float centerX = random.nextFloat()*(0.7f - (-0.7f)) + (-0.0f);
        float centerY = random.nextFloat()*(0.2f - (-0.2f)) + (-0.2f);
        float width = maxWidth= random.nextFloat()*(MAX_WIDTH/2.5f - MIN_WIDTH) +MIN_WIDTH;
        float height = width*WIDTH_HEIGHT_RATION_BALLOON;


        Vertex fourthCenterBalloon = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangleFourth = new TexturedRectangle(fourthCenterBalloon, width, height,0);
        texturedRectangleFourth.setIsClickable(true);
        texturedRectangleFourth.setTextResourceId(R.drawable.balloon_4_high, mContext);
        texturedRectangleFourth.setTag("ballon4");
        Layer layer = new Layer();
        layer.addShape(texturedRectangleFourth);





        centerX = random.nextFloat()*(0.7f - (0.0f)) + (0.0f);
        centerY = random.nextFloat()*(0.8f - 0.5f) + 0.5f;
        width = random.nextFloat()*(MAX_WIDTH/2 - MIN_WIDTH) +MIN_WIDTH;
        height = width*WIDTH_HEIGHT_RATION_BALLOON;
        if(maxWidth<width)
            maxWidth = width;

        Vertex thirdCenterBalloon = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangleThird = new TexturedRectangle(thirdCenterBalloon, width, height, 0);
        texturedRectangleThird.setIsClickable(true);
        texturedRectangleThird.setTextResourceId(R.drawable.ballon_3_hight, mContext);
        texturedRectangleThird.setTag("ballon3");
        layer.addShape(texturedRectangleThird);


         centerX = random.nextFloat()*(-0.7f - (-0.3f)) + (-0.3f);
         centerY = random.nextFloat()*(0.8f - 0.0f) + 0.0f;
         width = random.nextFloat()*(MAX_WIDTH/1.5f - MIN_WIDTH*2) + MIN_WIDTH*2;
         height = width*WIDTH_HEIGHT_RATION_BALLOON;

        if(maxWidth<width) {
            maxWidth = width;
        }

        Vertex secondBalloonCenter = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangleSecond = new TexturedRectangle(secondBalloonCenter, width, height, 0);
        texturedRectangleSecond.setIsClickable(true);
        texturedRectangleSecond.setTextResourceId(R.drawable.ballon_2_high, mContext);
        texturedRectangleSecond.setTag("ballon2");
        layer.addShape(texturedRectangleSecond);



        centerX = random.nextFloat()*(maxX -minX) + minX;
        centerY = random.nextFloat()*(maxY - minY) + minY;
        width = random.nextFloat()*(MAX_WIDTH - maxWidth*1.1f) + maxWidth*1.1f;
         height = width*WIDTH_HEIGHT_RATION_BALLOON;
        Vertex vertex = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(vertex,width,height,0);
        texturedRectangle.setIsClickable(true);
        texturedRectangle.setTextResourceId(R.drawable.balloon_1, mContext);
        texturedRectangle.setTag("ballon1");

        layer.addShape(texturedRectangle);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {

//          float ratio = 0.58f;
//
//          float centerX = random.nextFloat()*(0.0f - (-0.4f)) + (-0.4f);
//          float centerY = random.nextFloat()*(0.0f - (-0.4f)) + (-0.4f);
//          float width = random.nextFloat() *(0.3f - (0.1f)) + (0.1f);
//          float height = width*ratio;
//
//          Vertex centerFirstCloud = new Vertex(centerX, centerY, 0.0f);
//
//          TexturedRectangle rectangle = new TexturedRectangle(centerFirstCloud, width, height, 0);
//          rectangle.setTextResourceId(R.drawable.cloud_6, mContext);
//            Layer layer = new Layer();
//          layer.addShape(rectangle);
//
//
//          ratio = 0.62f;
//
//          centerX = random.nextFloat()*(0.3f - (-0.3f)) + (-0.3f);
//          centerY = random.nextFloat()*(0.4f - 0.0f) + (0.0f);
//          width = random.nextFloat() *(0.6f - (0.3f)) + (0.3f);
//          height = width*ratio;
//          Vertex centerSecondCloud = new Vertex(centerX, centerY, 0.0f);
//          TexturedRectangle secondCloudRect = new TexturedRectangle(centerSecondCloud, width, height, 0);
//          secondCloudRect.setTextResourceId(R.drawable.cloud_4, mContext);
//        layer.addShape(secondCloudRect);

//        Vertex topLeft = new Vertex(-0.2f, 0.2f, 0.0f);
//        Vertex bottomLeft = new Vertex(-0.2f, -0.36f, 0.0f);
//        Vertex bottomRight = new Vertex(0.2f, -0.36f,0.0f);
//        Vertex topRight = new Vertex(0.2f, 0.2f, 0.0f);
//        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
//        texturedRectangle.setTextResourceId(R.drawable.cloud_4, mContext);
//        Layer layer = new Layer();
//        layer.addShape(texturedRectangle);

//        Vertex topLeftSecond = new Vertex(0.1f, 0.2f, 0.0f);
//        Vertex bottomLeftSecond = new Vertex(0.1f, -0.1f, 0.0f);
//        Vertex bottomRightSecond = new Vertex(0.48f, -0.1f,0.0f);
//        Vertex topRightSecond = new Vertex(0.48f, 0.2f, 0.0f);
//        TexturedRectangle texturedRectangleSecond = new TexturedRectangle(topLeftSecond, bottomLeftSecond, bottomRightSecond, topRightSecond);
//        texturedRectangleSecond.setTextResourceId(R.drawable.cloud_4, mContext);
//
//        layer.addShape(texturedRectangleSecond);

        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();

    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
