package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 11/10/2015.
 */
public class LightHouse extends WallpaperPattern {

    public LightHouse(Context context) {
        super(context, PatternsIds.LIGHT_HOUSE);
    }

    @Override
    protected Layer initializeFirstLayer() {
        Vertex topLeftVertex = new Vertex(-1.0f, 1.0f, 0.0f);
        Vertex bottomLeftVertex = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightVertex = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightVertex = new Vertex(1.0f, 1.0f, 0.0f);

        TexturedRectangle background = new TexturedRectangle(topLeftVertex, bottomLeftVertex, bottomRightVertex, topRightVertex);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.light_house_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);

        Vertex topLeftSeaVertex = new Vertex(-1.0f, 0.0f, 0.0f);
        Vertex bottomLeftSeaVertex = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightSeaVertex = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightSeaVertex = new Vertex(1.0f, 0.0f, 0.0f);

        TexturedRectangle sea = new TexturedRectangle(topLeftSeaVertex, bottomLeftSeaVertex, bottomRightSeaVertex, topRightSeaVertex);
        sea.setIsClickable(true);
        sea.setTextResourceId(R.drawable.light_house_sea, mContext);
        layer.addShape(sea);

        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float secondWaveRatio = 6.355f;
        float height = 0.2f;
        float width = height*secondWaveRatio;
        float centerX = 0.2f;
        float centerY = -0.02f +height/2f;
        TexturedRectangle secondWave = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        secondWave.setIsClickable(true);
        secondWave.setTextResourceId(R.drawable.light_house_wave_2, mContext);
        Layer layer = new Layer();
        layer.addShape(secondWave);

        float lightHouseRatio = 1.27f;
        height = 0.9f;
        width = height*lightHouseRatio;
        centerX = .2f;
        centerY = -0.02f +height/2f;
        TexturedRectangle lightHouse = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        lightHouse.setIsClickable(true);
        lightHouse.setTextResourceId(R.drawable.light_house_lighthouse, mContext);
        layer.addShape(lightHouse);
        float firstWaveRatio = 6.36f;
        height = 0.1f;
        width = height*firstWaveRatio;
        centerX = 0.1f;
        centerY = -0.02f + height/2f;
        TexturedRectangle firstWave = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        firstWave.setIsClickable(true);
        firstWave.setTextResourceId(R.drawable.light_house_wave_1, mContext);
        layer.addShape(firstWave);

        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float boatRatio = 0.73f;
        Random random = new Random();
        float centerX = random.nextFloat()*(0.6f -(-0.6f)) + (-0.6f);
        float centerY = random.nextFloat()*(-0.2f - (-0.7f)) + (-0.7f);
        float width = random.nextFloat()*(0.4f - (0.3f)) + (0.3f);
        float height = width/boatRatio;

        TexturedRectangle boat = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        boat.setIsClickable(true);
        boat.setTextResourceId(R.drawable.light_house_boat, mContext);
        Layer layer = new Layer();
        layer.addShape(boat);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
