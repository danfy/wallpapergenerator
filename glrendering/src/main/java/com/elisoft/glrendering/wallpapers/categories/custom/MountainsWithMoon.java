package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 9/24/2015.
 */
public class MountainsWithMoon extends WallpaperPattern {

    private final Random random;
    private float topWaterCoordinate;
    public MountainsWithMoon(Context context) {
        super(context, PatternsIds.MOUNTAINS_WITH_MOON);
        random = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        Vertex topLeft = new Vertex(-1.0f, 1.0f, 0.0f);
        Vertex bottomLeft = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRight = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRight = new Vertex(1.0f, 1.0f, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTag("Sky");
        texturedRectangle.setTextResourceId(R.drawable.mountains_moon_sky, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float centerX = random.nextFloat()*(0.7f- (-0.7f)) +(-0.7f);
        float centerY = random.nextFloat()*(0.7f- 0.3f) + 0.3f;
        float width = random.nextFloat()*(0.4f - 0.2f) + 0.2f;
        float height = width;

        Vertex centerVertex = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(centerVertex, width, height,0f);
        texturedRectangle.setTag("Moon");
        texturedRectangle.setTextResourceId(R.drawable.mountains_moon_moon, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);
        return layer;


    }

    @Override
    protected Layer initializeThirdLayer() {
        topWaterCoordinate = random.nextFloat()*(0.2f -(-0.2f)) + (-0.2f);

        Vertex topLeft = new Vertex(-1.0f, topWaterCoordinate, 0.0f);
        Vertex bottomLeft = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRight = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRight = new Vertex(1.0f, topWaterCoordinate, 0.0f);

        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setTag("Water");
        texturedRectangle.setTextResourceId(R.drawable.mountains_moon_see, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);
        return layer;



    }

    @Override
    protected Layer initializeFourthLayer() {
        float mainMountainsRatio = 4.811f;
        float height = random.nextFloat()*(0.5f - 0.3f) + 0.3f;
        float width = height*mainMountainsRatio;
        Vertex bottomLeft = new Vertex(-width/2, topWaterCoordinate,0.0f);
        Vertex bottomRight = new Vertex(width/2, topWaterCoordinate, 0.0f);
        Vertex topLeft = new Vertex(-width/2, topWaterCoordinate+ height, 0.0f);
        Vertex topRight = new Vertex(width/2,topWaterCoordinate + height, 0.0f);

        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setTag("Mountains");
        texturedRectangle.setTextResourceId(R.drawable.mountains_moon_mountains, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        float firstIceRatio = 1.6f;
        float secondIceRatio = 1.9f;
        float thirdIceRatio = 0.88f;
        float fourthIceRatio = 1.15f;
        height = random.nextFloat() *(0.4f - 0.2f) + 0.2f;
        width = firstIceRatio*height;
        float centerX = random.nextFloat()*(0.7f -(-0.7f)) +( -0.7f);
        Vertex centerVertexFirst = new Vertex(centerX, topWaterCoordinate, 0.0f);


//        Vertex bottomLeft = new Vertex(-width/2, topWaterCoordinate, 0.0f);
//        Vertex bottomRight = new Vertex(width/2, topWaterCoordinate, 0.0f);
//        Vertex topLeft = new Vertex(-width/2, topWaterCoordinate + height, 0.0f);
//        Vertex topRight = new Vertex(width/2, topWaterCoordinate + height, 0.0f);

        TexturedRectangle texturedRectangleIceOne = new TexturedRectangle(centerVertexFirst, width, height, 0);
        texturedRectangleIceOne.setTag("Ice one");
        texturedRectangleIceOne.setTextResourceId(R.drawable.mountains_moon_ice_1, mContext);
        layer.addShape(texturedRectangleIceOne);

        height = random.nextFloat() *(0.3f - 0.2f) + 0.2f;
        width = secondIceRatio*height;
        centerX = random.nextFloat()*(0.7f - ( -0.7f))  + (-0.7f);
        Vertex centerVertexTwo = new Vertex(centerX, topWaterCoordinate + height/2, 0.0f);

        TexturedRectangle textureRectangleIceTwo = new TexturedRectangle(centerVertexTwo, width, height, 0);
        textureRectangleIceTwo.setTag("Ice two");
        textureRectangleIceTwo.setTextResourceId(R.drawable.mountains_moon_ice_2, mContext);
        layer.addShape(textureRectangleIceTwo);

        height = random.nextFloat()*(0.3f - 0.2f) + 0.2f;
        width = thirdIceRatio*height;
        centerX = random.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        Vertex centerVertexThird = new Vertex(centerX, topWaterCoordinate, 0.0f);
        TexturedRectangle texturedRectangleIceThird = new TexturedRectangle(centerVertexThird, width, height, 0);
        texturedRectangleIceThird.setTag("Ice third");
        texturedRectangleIceThird.setTextResourceId(R.drawable.mountains_moon_ice_3, mContext);
        layer.addShape(texturedRectangleIceThird);


        height = random.nextFloat()*(0.3f - 0.2f) + 0.2f;
        width = fourthIceRatio*height;
        centerX = random.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        Vertex centerVertexFourth = new Vertex(centerX, topWaterCoordinate, 0.0f);
        TexturedRectangle texturedRectangleIceFourth = new TexturedRectangle(centerVertexFourth, width, height, 0);
        texturedRectangleIceFourth.setTag("Ice fourth");
        texturedRectangleIceFourth.setTextResourceId(R.drawable.mountains_moon_ice_4, mContext);
        layer.addShape(texturedRectangleIceFourth);
         return layer;
    }

    @Override
    protected Layer initializeFifthLayer() {


        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
