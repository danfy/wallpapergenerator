package com.elisoft.glrendering.wallpapers.categories.gradient;

import android.content.Context;
import android.util.Log;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.GradientRadialRectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;


/**
 * Class represent radial gradient
 */
public class RadialGradient extends WallpaperPattern {
    private static final String TAG = RadialGradient.class.getSimpleName();

    public RadialGradient(Context context) {
        super(context, PatternsIds.RADIAL_GRADIENT);
//        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());

    }
    protected RadialGradient(Context context, int patternId) {
        super(context, patternId);

    }

    public RadialGradient(Context context, int width, int height) {
        super(context, PatternsIds.RADIAL_GRADIENT);
        this.width = width;
        this.height = height;
//        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());
    }



    @Override
    protected Layer initializeFirstLayer() {

        Layer layer = new Layer();
        layer.addShape(initializeBackgroundRectangle(width, height,FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.black))));
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeThirdLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }

    protected GradientRadialRectangle initializeBackgroundRectangle(float resolutionX, float resolutionY, FloatColor secondColor) {

        FloatColor firstColor = palete.getSomeColor();

        GradientRadialRectangle gradientRadialRectangle = new GradientRadialRectangle(resolutionX, resolutionY,firstColor, secondColor, 0.5f, 0.5f, 0.8f);

        return gradientRadialRectangle;
    }


}
