package com.elisoft.glrendering.wallpapers.categories.stars;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Circle;
import com.elisoft.glrendering.shapes.GradientRadialRectangle;
import com.elisoft.glrendering.shapes.StarsSky;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;



/**
 * Created by yvorobey on 6/29/2015.
 */
public class StarsSkyPattern extends WallpaperPattern {

    private final Random random;

    public StarsSkyPattern(Context context) {
        super(context, PatternsIds.STARS_SKY);
        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());
        random = new Random();
    }
    public StarsSkyPattern(Context context, int width, int height) {
        super(context, PatternsIds.STARS_SKY);
        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());
        this.width = width;
        this.height = height;
        random = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        return generateFirstLayer();
    }

    @Override
    protected Layer initializeSecondLayer() {
        return generateSecondLayer();
    }

    @Override
    protected Layer initializeThirdLayer() {
        return generateThirdLayer();
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }

    @Override
    protected void initializeNewBackgroundColor() {
        setBackgroundColor(palete.getSomeColor());
    }
    private Layer generateFirstLayer() {
        Layer layer = new Layer();
        FloatColor firstColor = palete.getSomeColor();
        FloatColor secondColor = FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.black));
        GradientRadialRectangle gradientRadialRectangle = new GradientRadialRectangle(width, height, firstColor, secondColor, random.nextFloat()*(1.0f - 0.0f) + 0.0f, random.nextFloat()*(1.0f - 0.0f) + 0.0f, 0.7f);
        layer.addShape(gradientRadialRectangle);
        return layer;
    }
    private Layer generateSecondLayer() {
        Layer layer = new Layer();
        StarsSky starsSky = new StarsSky(200);
        layer.addShape(starsSky);
        return layer;
    }
    private Layer generateThirdLayer() {
        Layer layer = new Layer();

        for(int i = 0; i<20; i++) {
            Circle circle =new Circle(new Vertex(random.nextFloat()*(0.8f-(-0.8f)) + (-0.8f), random.nextFloat()*(0.8f-(-0.8f)) +(-0.8f), 0.1f), 20, 0.007f);
            circle.setColor(FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.white)));
            layer.addShape(circle);

        }
        return layer;
    }
}
