package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 9/24/2015.
 */
public class MountainsWithIce extends WallpaperPattern {
    private final Random random;
    private static final float MAX_WIDTH = 0.7f;
    private static final float MIN_WIDTH = 0.1f;
    private float topWaterCoordinate;
    public MountainsWithIce(Context context) {
        super(context, PatternsIds.MOUNTAINS_WITH_ICE);
        random = new Random();
    }


    @Override
    protected Layer initializeFirstLayer() {
        Vertex topLeft = new Vertex(-1.0f, 1.0f, 0.0f);
        Vertex bottomLeft = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRight = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRight = new Vertex(1.0f, 1.0f, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTag("Sky");
        texturedRectangle.setTextResourceId(R.drawable.sky_mountains_ice, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {


        topWaterCoordinate = random.nextFloat()*(0.2f -(-0.2f)) + (-0.2f);

        Vertex topLeft = new Vertex(-1.0f, topWaterCoordinate, 0.0f);
        Vertex bottomLeft = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRight = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRight = new Vertex(1.0f, topWaterCoordinate, 0.0f);

        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setTag("Water");
        texturedRectangle.setTextResourceId(R.drawable.sea_mountains_ice, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);





        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        //Mountains
        float mountainsRatio = 9.27f;
        float height = random.nextFloat()*(0.3f - 0.2f) +0.2f;
        float width = height*mountainsRatio;
        Vertex bottomLeft = new Vertex(-width/2, topWaterCoordinate,0.0f);
        Vertex bottomRight = new Vertex(width/2, topWaterCoordinate, 0.0f);
        Vertex topLeft = new Vertex(-width/2, topWaterCoordinate+height, 0.0f);
        Vertex topRight = new Vertex(width/2,topWaterCoordinate + height, 0.0f);

        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeft, bottomLeft, bottomRight, topRight);
        texturedRectangle.setTag("Mountains");
        texturedRectangle.setTextResourceId(R.drawable.mountains_ice, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        float firstIceRatio = 1.44f;
        float secondIceRatio =1.14f;
        float width = random.nextFloat()*(0.5f - 0.2f) + 0.2f;
        float height = width/firstIceRatio;
        float centerX = random.nextFloat()*(0.7f -( -0.7f)) + (-0.7f);
        float centerY = random.nextFloat()*((topWaterCoordinate - 0.1f - height) - (-1.0f +0.1f + height)) + (-1.0f + 0.1f + height);


        Vertex centerVertex = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangleFirst = new TexturedRectangle(centerVertex, width,height,0);
        texturedRectangleFirst.setTextResourceId(R.drawable.mountaint_ice_1, mContext);
        texturedRectangleFirst.setTag("Ice one");
        Layer layer = new Layer();
        layer.addShape(texturedRectangleFirst);

        width = random.nextFloat()*(0.5f - 0.2f) + 0.2f;
        height = width/secondIceRatio;
        centerX = random.nextFloat()*(0.7f -(-0.7f)) + (-0.7f);
        centerY = random.nextFloat()*((topWaterCoordinate-0.1f - height) - (-1.0f + 0.1f + height)) + (-1.0f + 0.1f + height);

        Vertex secondCenterVertex = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangleSecond = new TexturedRectangle(secondCenterVertex, width, height,0);
        texturedRectangleSecond.setTextResourceId(R.drawable.mountaints_ice_2, mContext);
        texturedRectangleSecond.setTag("Ice two");
        layer.addShape(texturedRectangleSecond);
        return layer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
