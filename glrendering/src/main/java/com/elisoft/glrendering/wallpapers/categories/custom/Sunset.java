package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 1/11/2016.
 */
public class Sunset extends WallpaperPattern {
    private final Random mRandom;

    public Sunset(Context context) {
        super(context, PatternsIds.SUNSET);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.56f;
        float centerX = 0.0f;
        float centerY = 0.0f;

        float width = 2.0f;
        float height = width/backgroundRatio;
        TexturedRectangle texturedRectangle = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTextResourceId(R.drawable.sunset_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        centerX = mRandom.nextFloat()*(0.7f - (-0.7f)) +(-0.7f);
        centerY = mRandom.nextFloat()*(0.9f - (0.1f)) + (0.1f);
        width = mRandom.nextFloat()*(0.6f - (0.4f)) + 0.4f;
        height = width;
        TexturedRectangle sun = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        sun.setIsClickable(true);
        sun.setTextResourceId(R.drawable.sunset_sun, mContext);
        layer.addShape(sun);

        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float thirdMountainRatio =1.37f;
        float centerX = mRandom.nextFloat()*(0.0f - (-0.7f)) +(-0.7f);
        float height = 0.5f;
        float width = height*thirdMountainRatio;
        float centerY = -0.1f+height/2f;

        TexturedRectangle thirdMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        thirdMountain.setIsClickable(true);
        thirdMountain.setTextResourceId(R.drawable.sunset_mountain_3, mContext);
        Layer layer = new Layer();
        layer.addShape(thirdMountain);

        float secondMountainRatio = 1.62f;

        centerX = mRandom.nextFloat()*(0.0f - (-0.7f)) + (-0.7f);
        height = 0.6f;
        width = height*secondMountainRatio;
        centerY = -0.1f + height/2f;

        TexturedRectangle secondMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        secondMountain.setIsClickable(true);
        secondMountain.setTextResourceId(R.drawable.sunset_mountain_2, mContext);
        layer.addShape(secondMountain);

        float firstMountainRatio = 1.24f;
        centerX = mRandom.nextFloat()*(0.7f - 0.2f) + 0.2f;
        height = 0.55f;
        width = height*firstMountainRatio;
        centerY = -0.1f + height/2f;

        TexturedRectangle firstMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        firstMountain.setIsClickable(true);
        firstMountain.setTextResourceId(R.drawable.sunset_mountain, mContext);
        layer.addShape(firstMountain);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float seaRatio = 1.7f;
        float height = 1.0f;
        float centerX = 0.0f;
        float centerY = 0.0f - height/2f;
        float width = height*seaRatio;
        TexturedRectangle sea = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        sea.setIsClickable(true);
        sea.setTextResourceId(R.drawable.sunset_sea, mContext);
        Layer layer = new Layer();
        layer.addShape(sea);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        float secondBoatRatio = 0.74f;
        float centerX = mRandom.nextFloat()*(0.6f - 0.1f) +0.1f;
        float centerY = mRandom.nextFloat()*(-0.1f - (-0.8f)) + (-0.8f);
        float height = mRandom.nextFloat()*(0.2f - 0.1f) +0.1f;
        float width = height*secondBoatRatio;
        TexturedRectangle secondBoat = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        secondBoat.setIsClickable(true);
        secondBoat.setTextResourceId(R.drawable.sunset_boat_2, mContext);
        Layer layer = new Layer();
        layer.addShape(secondBoat);

        float firstBoatRatio = 0.74f;
        centerX = mRandom.nextFloat()*(-0.1f - (-0.6f)) +(-0.6f);
        centerY = mRandom.nextFloat()*(-0.1f - (-0.8f)) + (-0.8f);
        height = mRandom.nextFloat()*(0.4f - 0.3f ) + 0.3f;
        width = height*firstBoatRatio;
        TexturedRectangle firstBoat = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0 );
        firstBoat.setIsClickable(true);
        firstBoat.setTextResourceId(R.drawable.sunset_boat, mContext);
        layer.addShape(firstBoat);

        return layer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
