package com.elisoft.glrendering.wallpapers.categories.newyearholiday;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 12/24/2015.
 */
public class ChristmasMountains extends WallpaperPattern {
    private final Random mRandom;
    public ChristmasMountains(Context context) {
        super(context, PatternsIds.CHRISTMAS_MOUNTAINS);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.563f;
        float centerX = 0.0f;
        float centerY = 0.5f;
        float width = 2f;
        float height = width/backgroundRatio;
        TexturedRectangle background = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.christmans_mountains_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);

        float moonRatio = 1.34f;
        centerX = 0.0f;
        centerY = 0.5f;
        height = 1.3f;
        width = height*moonRatio;
        TexturedRectangle moon = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        moon.setIsClickable(true);
        moon.setTextResourceId(R.drawable.christmans_mountains_moon, mContext);
        layer.addShape(moon);

        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float fourthMountainsRatio = 0.83f;
        float centerX = 0.0f;
        float centerY = mRandom.nextFloat()*(-0.5f - (-0.7f)) + (-0.7f);
        float width = 2f;
        float height = width/fourthMountainsRatio;
        TexturedRectangle fourthMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        fourthMountain.setIsClickable(true);
        fourthMountain.setTextResourceId(R.drawable.christmans_mountains_mountain_4, mContext);
        Layer layer = new Layer();
        layer.addShape(fourthMountain);

        float thirdMountainsRatio = 0.92f;
        centerX = 0.0f;
        centerY = mRandom.nextFloat()*(-0.6f - (-0.9f)) + (-0.9f);
        width = 2f;
        height = width/thirdMountainsRatio;
        TexturedRectangle thirdMountains = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        thirdMountains.setIsClickable(true);
        thirdMountains.setTextResourceId(R.drawable.christmans_mountains_mountain_3, mContext);
        layer.addShape(thirdMountains);

        float firstMountainRatio = 1.62f;
        centerX = mRandom.nextFloat()*(-0.3f - (-0.6f)) + (-0.6f);

        height = mRandom.nextFloat()*(1.0f - 0.8f ) + 0.8f;
        centerY = -(1.0f - height/2);
        width = height* firstMountainRatio;
        TexturedRectangle firstMountains = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        firstMountains.setIsClickable(true);
        firstMountains.setTextResourceId(R.drawable.christmans_mountains_mountain_1, mContext);
        layer.addShape(firstMountains);

        float secondMountainRatio = 1.63f;
        centerX = mRandom.nextFloat()*(0.6f - (0.3f)) + 0.3f;
        height = mRandom.nextFloat()*(1.2f - 0.8f) +0.8f;
        centerY = -(1.0f - height/2);
        width = height*secondMountainRatio;
        TexturedRectangle secondMountains = new TexturedRectangle(new Vertex(centerX, centerY,0.0f), width, height,0);
        secondMountains.setIsClickable(true);
        secondMountains.setTextResourceId(R.drawable.christmans_mountains_mountain_2, mContext);
        layer.addShape(secondMountains);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
