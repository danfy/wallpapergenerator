package com.elisoft.glrendering.wallpapers.service;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by yvorobey on 12/29/2015.
 */
public class WallpaperSettings {
    public static final String WALL_SETTINGS_PREF_NAME = "Wallpaper_settings";
    public static final String FILE_URL_VALUE = "file_url";

    public static void saveURLToPreferences(Context context, String fileUrl) {
        SharedPreferences.Editor editor = context.getSharedPreferences(WALL_SETTINGS_PREF_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(FILE_URL_VALUE, fileUrl);
        editor.apply();
    }
    public static String getURLFromPreferences(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(WALL_SETTINGS_PREF_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(FILE_URL_VALUE, "");
    }
}
