package com.elisoft.glrendering.wallpapers.categories.gradient;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.LinearGradientRectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;

/**
 * Created by yvorobey on 9/1/2015.
 */
public class ColoredHorLinearGradient extends HorLinearGradient {

    public ColoredHorLinearGradient(Context context) {
        super(context, PatternsIds.COLOR_HORIZONTAL_LINEAR_GRADIENT);
        palete.setColorRange(ColorRange.BASICS);

    }

    @Override
    protected FloatColor initializeFirstColor() {
        return palete.getSomeColor();
    }

    @Override
    protected FloatColor initializeSecondColor() {
        return palete.getSomeColor();
    }




}
