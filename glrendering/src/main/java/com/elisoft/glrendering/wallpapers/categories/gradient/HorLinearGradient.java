package com.elisoft.glrendering.wallpapers.categories.gradient;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.HorizontalGradientRectangle;
import com.elisoft.glrendering.shapes.LinearGradientRectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;

/**
 * Created by yvorobey on 8/31/2015.
 */
public class HorLinearGradient extends LinearGradient{

    public HorLinearGradient(Context context) {
        super(context, PatternsIds.HORIZONTAL_LINEAR_GRADIENT);
//        palete.setColorRange(ColorRange.BASICS);
    }
    protected HorLinearGradient(Context context, int patternId) {
        super(context, patternId);
    }

    @Override
    protected FloatColor initializeFirstColor() {
        return palete.getSomeColor();
    }

    @Override
    protected FloatColor initializeSecondColor() {
        return FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.black));
    }

    @Override
    protected LinearGradientRectangle initializeLinearGradientShape(float resolutionX, float resolutionY, FloatColor firstColor, FloatColor secondColor) {
        HorizontalGradientRectangle horizontalGradientRectangle = new HorizontalGradientRectangle(resolutionX,resolutionY,firstColor,secondColor);
        return horizontalGradientRectangle;
    }

    @Override
    protected Layer initializeSecondLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeThirdLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }


}
