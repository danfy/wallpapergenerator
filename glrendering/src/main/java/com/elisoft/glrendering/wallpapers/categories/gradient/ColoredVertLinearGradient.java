package com.elisoft.glrendering.wallpapers.categories.gradient;

import android.content.Context;

import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;

/**
 * Created by yvorobey on 9/1/2015.
 */
public class ColoredVertLinearGradient extends VertLinearGradient {


    public ColoredVertLinearGradient(Context context) {
        super(context, PatternsIds.COLOR_VERTICAL_LINEAR_GRADIENT);
//        palete.setColorRange(ColorRange.BASICS);
    }

    @Override
    protected FloatColor initializeFirstColor() {
        return palete.getSomeColor();
    }

    @Override
    protected FloatColor initializeSecondColor() {
        return palete.getSomeColor();
    }
}
