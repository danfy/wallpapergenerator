package com.elisoft.glrendering.wallpapers.categories.newyearholiday;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 12/24/2015.
 */
public class AbstractCristmasTree extends WallpaperPattern {

    private final Random mRandom;

    public AbstractCristmasTree(Context context) {
        super(context, PatternsIds.ABSTRACT_CHRISTMAS_TREE);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.56f;
        float centerX = 0.0f;
        float centerY = 0.0f;
        float width = 2f;
        float height = width/backgroundRatio;
        TexturedRectangle background = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.abstract_tree_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float treeRatio = 0.79f;
        float centerX = 0.0f;
        float centerY = -0.2f;
        float height = 1.2f;
        float width = height * treeRatio;
        TexturedRectangle tree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        tree.setIsClickable(true);
        tree.setTextResourceId(R.drawable.abstract_tree_christmas_tree, mContext);
        Layer layer = new Layer();
        layer.addShape(tree);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float firstToyRatio = 0.065f;
        float centerX = mRandom.nextFloat()*(-0.2f - (-0.6f)) + (-0.6f);

        float height = mRandom.nextFloat()*(1.0f - 0.7f) + 0.7f;
        float width = height*firstToyRatio;
        float centerY = 1.0f - height/2f;
        TexturedRectangle firstToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        firstToy.setIsClickable(true);
        firstToy.setTextResourceId(R.drawable.abstract_tree_toy, mContext);
        Layer layer = new Layer();
        layer.addShape(firstToy);

        float secondToyRatio = 0.062f;
        centerX = mRandom.nextFloat()*(0.6f - 0.2f) + 0.2f;
        height = mRandom.nextFloat()*(1.2f - 0.7f) +0.7f;
        width = height*secondToyRatio;
        centerY = 1.0f - height/2f;
        TexturedRectangle secondToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        secondToy.setIsClickable(true);
        secondToy.setTextResourceId(R.drawable.abstract_tree_toy_2, mContext);
        layer.addShape(secondToy);

        float starRatio =0.24f;
        centerX = mRandom.nextFloat()*(0.2f - (-0.2f)) + (-0.2f);
        height = mRandom.nextFloat()*(0.8f -0.6f) + 0.6f;
        width = height*starRatio;
        centerY = 1.0f - height/2f;
        TexturedRectangle star = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        star.setIsClickable(true);
        star.setTextResourceId(R.drawable.abstract_tree_star, mContext);
        layer.addShape(star);

        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
