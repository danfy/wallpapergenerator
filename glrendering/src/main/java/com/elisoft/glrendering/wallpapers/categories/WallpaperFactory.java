package com.elisoft.glrendering.wallpapers.categories;


import android.content.Context;


import com.elisoft.glrendering.wallpapers.categories.custom.Balloons;
import com.elisoft.glrendering.wallpapers.categories.custom.Beach;
import com.elisoft.glrendering.wallpapers.categories.custom.Hills;
import com.elisoft.glrendering.wallpapers.categories.custom.LightHouse;
import com.elisoft.glrendering.wallpapers.categories.custom.MountainsAndSea;
import com.elisoft.glrendering.wallpapers.categories.custom.MountainsWithIce;
import com.elisoft.glrendering.wallpapers.categories.custom.MountainsWithMoon;
import com.elisoft.glrendering.wallpapers.categories.custom.Nature;
import com.elisoft.glrendering.wallpapers.categories.custom.Park;
import com.elisoft.glrendering.wallpapers.categories.custom.SpaceShip;
import com.elisoft.glrendering.wallpapers.categories.custom.Sunset;
import com.elisoft.glrendering.wallpapers.categories.gradient.ColoredHorLinearGradient;
import com.elisoft.glrendering.wallpapers.categories.gradient.ColoredRadialGradient;
import com.elisoft.glrendering.wallpapers.categories.gradient.ColoredVertLinearGradient;
import com.elisoft.glrendering.wallpapers.categories.gradient.HorLinearGradient;
import com.elisoft.glrendering.wallpapers.categories.gradient.RadialGradient;
import com.elisoft.glrendering.wallpapers.categories.gradient.VertLinearGradient;
import com.elisoft.glrendering.wallpapers.categories.material.BottTriangAddCirclePattern;
import com.elisoft.glrendering.wallpapers.categories.material.BottomTrianglesPattern;
import com.elisoft.glrendering.wallpapers.categories.material.CirclesPattern;
import com.elisoft.glrendering.wallpapers.categories.material.CrossRectanglesPattern;
import com.elisoft.glrendering.wallpapers.categories.material.CrossRectanglesWithCirclePattern;
import com.elisoft.glrendering.wallpapers.categories.material.RectanglesFromTop;
import com.elisoft.glrendering.wallpapers.categories.material.RectanglesFromTopWithCircle;
import com.elisoft.glrendering.wallpapers.categories.material.TrianglesFromOneAngle;
import com.elisoft.glrendering.wallpapers.categories.newyearholiday.AbstractCristmasTree;
import com.elisoft.glrendering.wallpapers.categories.newyearholiday.ChristmasCity;
import com.elisoft.glrendering.wallpapers.categories.newyearholiday.ChristmasMountains;
import com.elisoft.glrendering.wallpapers.categories.newyearholiday.ChristmasRoom;
import com.elisoft.glrendering.wallpapers.categories.newyearholiday.ChristmasTree;
import com.elisoft.glrendering.wallpapers.categories.newyearholiday.Snowman;
import com.elisoft.glrendering.wallpapers.categories.stars.StarsSkyPattern;
import com.elisoft.glrendering.wallpapers.categories.transparent.Bubbles;
import com.elisoft.glrendering.wallpapers.categories.transparent.SquareBubbles;
import com.elisoft.glrendering.wallpapers.categories.transparent.TransparentCircles;

/**
 * Created by yvorobey on 7/24/2015.
 */
public class WallpaperFactory {
    /**
     * Method returns WallpaperPattern object by id.
     * @param patternId - Pattern id. PatternsIds class holds all ids values of WallpaperPatterns.
     * @param context - Context.
     * @return WallpaperPattern object by id.
     */
    public static WallpaperPattern getWallpaperById(int patternId, Context context) {
        WallpaperPattern wallpaperPattern = null;
        switch (patternId) {
            case PatternsIds.BOTTOM_TRIANGLES_PATTERN:
                wallpaperPattern = new BottomTrianglesPattern(context);
                break;
            case PatternsIds.BOTTOM_TRIANGLES_WITH_CIRCLE_PATTERN:
                wallpaperPattern = new BottTriangAddCirclePattern(context);
                break;
            case PatternsIds.RECTANGLES_FROM_TOP_PATTERN:
                wallpaperPattern = new RectanglesFromTop(context);
                break;
            case PatternsIds.RECTANGLES_FROM_TOP_WITH_CIRCLE_PATTERN:
                wallpaperPattern = new RectanglesFromTopWithCircle(context);
                break;
            case PatternsIds.CIRCLE_PATTERN:
                wallpaperPattern = new CirclesPattern(context);
                break;
            case PatternsIds.CROSSING_RECTANGLES_PATTERN:
                wallpaperPattern = new CrossRectanglesPattern(context);
                break;
            case PatternsIds.CROSSING_RECTANGLES_WITH_CIRCLE_PATTERN:
                wallpaperPattern = new CrossRectanglesWithCirclePattern(context);
                break;
            case PatternsIds.TRIANGLES_FROM_ONE_ANGLE:
                wallpaperPattern = new TrianglesFromOneAngle(context);
                break;
            case PatternsIds.TRANSPARENT_CIRCLES:
                wallpaperPattern = new TransparentCircles(context);
                break;
            case PatternsIds.BUBBLES:
                wallpaperPattern = new Bubbles(context);
                break;
            case PatternsIds.STARS_SKY:
                wallpaperPattern = new StarsSkyPattern(context);
                break;
            case PatternsIds.SQUARE_BUBBLES:
                wallpaperPattern = new SquareBubbles(context, true);
                break;
            case PatternsIds.RADIAL_GRADIENT:
                wallpaperPattern = new RadialGradient(context);
                break;
            case PatternsIds.COLORED_RADIAL_GRADIENT:
                wallpaperPattern = new ColoredRadialGradient(context);
                break;
            case PatternsIds.HORIZONTAL_LINEAR_GRADIENT:
                wallpaperPattern = new HorLinearGradient(context);
                break;
            case PatternsIds.COLOR_HORIZONTAL_LINEAR_GRADIENT:
                wallpaperPattern = new ColoredHorLinearGradient(context);
                break;
            case PatternsIds.VERTICAL_LINEAR_GRADIENT:
                wallpaperPattern = new VertLinearGradient(context);
                break;
            case PatternsIds.COLOR_VERTICAL_LINEAR_GRADIENT:
                wallpaperPattern = new ColoredVertLinearGradient(context);
                break;
            case PatternsIds.BALLOONS_PATTERN:
                wallpaperPattern = new Balloons(context);
                break;
            case PatternsIds.MOUNTAINS_WITH_ICE:
                wallpaperPattern = new MountainsWithIce(context);
                break;
            case PatternsIds.MOUNTAINS_WITH_MOON:
                wallpaperPattern = new MountainsWithMoon(context);
                break;
            case PatternsIds.SPACE_SHIP_PATTERN:
                wallpaperPattern = new SpaceShip(context);
                break;
            case PatternsIds.HILLS_PATTERN:
                wallpaperPattern = new Hills(context);
                break;
            case PatternsIds.NON_FLAT_SQUARE_BUBBLES:
                wallpaperPattern = new SquareBubbles(context, false);
                break;
            case PatternsIds.PARK_PATTERN:
                wallpaperPattern = new Park(context);
                break;
            case PatternsIds.BEACH_PATTERN:
                wallpaperPattern = new Beach(context);
                break;
            case PatternsIds.LIGHT_HOUSE:
                wallpaperPattern = new LightHouse(context);
                break;
            case PatternsIds.NATURE_PATTERN:
                wallpaperPattern = new Nature(context);
                break;
            case PatternsIds.MOUNTAINS_AND_SEA:
                wallpaperPattern = new MountainsAndSea(context);
                break;
            case PatternsIds.SNOWMAN_PATTERN:
                wallpaperPattern = new Snowman(context);
                break;
            case PatternsIds.ABSTRACT_CHRISTMAS_TREE:
                wallpaperPattern = new AbstractCristmasTree(context);
                break;
            case PatternsIds.CHRISTMAS_CITY:
                wallpaperPattern = new ChristmasCity(context);
                break;
            case PatternsIds.CHRISTMAS_TREE:
                wallpaperPattern = new ChristmasTree(context);
                break;
            case PatternsIds.CHRISTMAS_MOUNTAINS:
                wallpaperPattern = new ChristmasMountains(context);
                break;
            case PatternsIds.CHRISTMAS_ROOM:
                wallpaperPattern = new ChristmasRoom(context);
                break;
            case PatternsIds.SUNSET:
                wallpaperPattern = new Sunset(context);
                break;
        }
        return wallpaperPattern;
    }
}
