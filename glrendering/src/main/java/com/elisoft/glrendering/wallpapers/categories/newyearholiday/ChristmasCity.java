package com.elisoft.glrendering.wallpapers.categories.newyearholiday;

import android.content.Context;


import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;



import java.util.Random;

/**
 * Created by yvorobey on 12/24/2015.
 */
public class ChristmasCity extends WallpaperPattern {
    private final Random mRandom;
    public ChristmasCity(Context context) {
        super(context, PatternsIds.CHRISTMAS_CITY);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.58f;
        float centerX = 0.0f;
        float centerY = 0.3f;
        float width = 2f;
        float height = width/backgroundRatio;
        TexturedRectangle background = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.cristmas_city_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);

        float firstStarRatio = 1.85f;
        centerX = mRandom.nextFloat()*(0.3f - (-0.3f)) + (-0.3f);
        centerY = mRandom.nextFloat()*(0.7f - 0.5f) + 0.5f;
        height = 0.5f;
        width = height* firstStarRatio;
        TexturedRectangle firstStar = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        firstStar.setIsClickable(true);
        firstStar.setTextResourceId(R.drawable.cristmas_city_star, mContext);
        layer.addShape(firstStar);

        float secondStarRatio = 1.5f;
        centerX = mRandom.nextFloat()*(0.3f - (-0.3f)) + (-0.3f);
        centerY = mRandom.nextFloat()*(0.8f - 0.6f) + 0.6f;
        height= 0.5f;
        width = height* secondStarRatio;
        TexturedRectangle secondStar = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        secondStar.setIsClickable(true);
        secondStar.setTextResourceId(R.drawable.cristmas_city_star2, mContext);
        layer.addShape(secondStar);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(0.7f - 0.4f) + 0.4f;
        height = 0.5f;
        width = height;
        TexturedRectangle moon = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        moon.setIsClickable(true);
        moon.setTextResourceId(R.drawable.cristmas_city_moon, mContext);
        layer.addShape(moon);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float firstCityRatio = 1.63f;
        float height = 0.7f;
        float width = height*firstCityRatio;
        float centerX = 0.3f;
        float centerY = -(0.65f - height/2f);
        TexturedRectangle fisrtCity = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        fisrtCity.setIsClickable(true);
        fisrtCity.setTextResourceId(R.drawable.cristmas_city_city, mContext);
        Layer layer = new Layer();
        layer.addShape(fisrtCity);

        float secondCityRatio = 0.51f;
        height = 0.5f;
        width = height*secondCityRatio;
        centerX = -0.5f;
        centerY = -(0.65f - height/2f);
        TexturedRectangle secondCity = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        secondCity.setIsClickable(true);
        secondCity.setTextResourceId(R.drawable.cristmas_city_city_2, mContext);
        layer.addShape(secondCity);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float thirdHillRatio = 1.6f;
        float height = 0.8f;
        float width = height*thirdHillRatio;
        float centerX = -0.1f;
        float centerY = -0.6f;
        TexturedRectangle thirdHill = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        thirdHill.setIsClickable(true);
        thirdHill.setTextResourceId(R.drawable.cristmas_city_hill_3, mContext);
        Layer layer = new Layer();
        layer.addShape(thirdHill);

        float secondHillRatio = 2.172f;
        height = 0.6f;
        width = height* secondHillRatio;
        centerX = 0.1f;
        centerY = -0.7f;
        TexturedRectangle secondHill = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        secondHill.setIsClickable(true);
        secondHill.setTextResourceId(R.drawable.cristmas_city_hill_2, mContext);
        layer.addShape(secondHill);

        float firstHillRatio = 1.87f;
        height = 0.5f;
        width = height*firstHillRatio;
        centerX = -0.3f;
        centerY = -0.8f;
        TexturedRectangle firstHill = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        firstHill.setIsClickable(true);
        firstHill.setTextResourceId(R.drawable.cristmas_city_hill, mContext);
        layer.addShape(firstHill);

        float santaRatio = 1.28f;
        height = 0.2f;
        width = height*santaRatio;
        centerX = mRandom.nextFloat()*(0.0f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.5f - (-0.65f)) + (-0.65f);
        TexturedRectangle santa = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        santa.setIsClickable(true);
        santa.setTextResourceId(R.drawable.cristmas_city_santa, mContext);
        layer.addShape(santa);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
