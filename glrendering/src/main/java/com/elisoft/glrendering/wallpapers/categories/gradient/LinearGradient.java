package com.elisoft.glrendering.wallpapers.categories.gradient;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.LinearGradientRectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

/**
 * Created by yvorobey on 8/31/2015.
 */
public abstract class LinearGradient extends WallpaperPattern {

    public LinearGradient(Context context, int id) {
        super(context, id);
    }

    protected abstract FloatColor initializeFirstColor();
    protected abstract FloatColor initializeSecondColor();

    @Override
    protected Layer initializeFirstLayer() {
        Layer layer = new Layer();
        layer.addShape(initializeLinearGradientShape(width,height, initializeFirstColor(), initializeSecondColor()));
        return layer;
    }
    protected abstract LinearGradientRectangle initializeLinearGradientShape(float resolutionX, float resolutionY, FloatColor firstColor, FloatColor secondColor);

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
