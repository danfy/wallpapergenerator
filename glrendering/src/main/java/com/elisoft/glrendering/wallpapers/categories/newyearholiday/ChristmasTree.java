package com.elisoft.glrendering.wallpapers.categories.newyearholiday;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 12/24/2015.
 */
public class ChristmasTree extends WallpaperPattern {
    private final Random mRandom;
    public ChristmasTree(Context context) {
        super(context, PatternsIds.CHRISTMAS_TREE);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.56f;
        float centerX = 0.0f;
        float centerY = -0.3f;
        float width = 2.0f;
        float height = width/backgroundRatio;
        TexturedRectangle background = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.christmas_tree_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);

        float firstMountRatio = 2.51f;
        centerX = mRandom.nextFloat()*(0.8f- 0.5f) + 0.5f;
        centerY = -0.6f;
        height = mRandom.nextFloat()*(0.5f - 0.3f) + 0.3f;
        width = height* firstMountRatio;
        TexturedRectangle firstMount = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        firstMount.setIsClickable(true);
        firstMount.setTextResourceId(R.drawable.christmas_tree_mount, mContext);
        layer.addShape(firstMount);

        float secondMountRatio =2.14f;
        centerX = mRandom.nextFloat()*(-0.5f-(-0.8f)) + (-0.8f);
        centerY = -0.6f;
        height = mRandom.nextFloat()*(0.5f - 0.3f) + 0.3f;
        width = height*secondMountRatio;
        TexturedRectangle secondMount = new TexturedRectangle(new Vertex(centerX, centerY,0.0f), width, height,0);
        secondMount.setIsClickable(true);
        secondMount.setTextResourceId(R.drawable.christmas_tree_mount_2, mContext);
        layer.addShape(secondMount);

        float snowRatio = 5.55f;
        height = 0.5f;
        width = height*snowRatio;
        centerX = 0.0f;
        centerY = -(1.0f - height/2f);
        TexturedRectangle snow = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        snow.setIsClickable(true);
        snow.setTextResourceId(R.drawable.christmas_tree_snow, mContext);
        layer.addShape(snow);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float treeRatio = 0.79f;
        float height = 1.2f;
        float width = height*treeRatio;
        float centerX = 0.0f;
        float centerY = 0.0f;
        TexturedRectangle tree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        tree.setIsClickable(false);
        tree.setTextResourceId(R.drawable.christmas_tree_tree, mContext);
        Layer layer = new Layer();
        layer.addShape(tree);

        float blueGiftRatio = 0.67f;
        height = 0.2f;
        width = height* blueGiftRatio;
        centerX = mRandom.nextFloat()*(0.5f - (-0.5f)) + (-0.5f);
        centerY = -(0.7f - height/2f);
        TexturedRectangle blueGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        blueGift.setIsClickable(true);
        blueGift.setTextResourceId(R.drawable.christmas_tree_gift_bl, mContext);
        layer.addShape(blueGift);

        float greenGiftRatio = 1.31f;
        height = 0.15f;
        width = height*greenGiftRatio;
        centerX = mRandom.nextFloat()*(0.5f - (-0.5f)) + (-0.5f);
        centerY = -(0.7f - height/2f);
        TexturedRectangle greenGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        greenGift.setIsClickable(true);
        greenGift.setTextResourceId(R.drawable.christmas_tree_gift_green, mContext);
        layer.addShape(greenGift);

        float orangeGiftRatio = 0.39f;
        height = 0.25f;
        width = height*orangeGiftRatio;
        centerX = mRandom.nextFloat()*(0.5f - (-0.5f)) + (-0.5f);
        centerY = -(0.7f - height/2f);
        TexturedRectangle orangeGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        orangeGift.setIsClickable(true);
        orangeGift.setTextResourceId(R.drawable.christmas_tree_gift_orange, mContext);
        layer.addShape(orangeGift);

        float redGiftRatio = 0.81f;
        height = 0.2f;
        width = height*redGiftRatio;
        centerX = mRandom.nextFloat()*(0.5f - (-0.5f)) + (-0.5f);
        centerY = -(0.7f - height/2f);
        TexturedRectangle redGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        redGift.setIsClickable(true);
        redGift.setTextResourceId(R.drawable.christmas_tree_gift_red, mContext);
        layer.addShape(redGift);

        centerX = 0.0f;
        centerY = 0.65f;
        width = 0.2f;
        height = width;
        TexturedRectangle star = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        star.setIsClickable(true);
        star.setTextResourceId(R.drawable.christmas_tree_star, mContext);
        layer.addShape(star);



        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = 0.3f;
        width = mRandom.nextFloat()*(0.06f - 0.04f) + 0.04f;
        height = width;
        TexturedRectangle secondToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        secondToy.setIsClickable(true);
        secondToy.setTextResourceId(R.drawable.christmas_tree_toy_2, mContext);
        layer.addShape(secondToy);

        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = 0.2f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle fourthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        fourthToy.setIsClickable(true);
        fourthToy.setTextResourceId(R.drawable.christmas_tree_toy_4, mContext);
        layer.addShape(fourthToy);

        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = 0.1f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle fifthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        fifthToy.setIsClickable(true);
        fifthToy.setTextResourceId(R.drawable.christmas_tree_toy_5, mContext);
        layer.addShape(fifthToy);

        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = 0.0f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle sixthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        sixthToy.setIsClickable(true);
        sixthToy.setTextResourceId(R.drawable.christmas_tree_toy_6, mContext);
        layer.addShape(sixthToy);


        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = -0.10f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle seventhToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        seventhToy.setIsClickable(true);
        seventhToy.setTextResourceId(R.drawable.christmas_tree_toy_7, mContext);
        layer.addShape(seventhToy);

        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = -0.2f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle engthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        engthToy.setIsClickable(true);
        engthToy.setTextResourceId(R.drawable.christmas_tree_toy_8, mContext);
        layer.addShape(engthToy);

        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = -0.3f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle ninethToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        ninethToy.setIsClickable(true);
        ninethToy.setTextResourceId(R.drawable.christmas_tree_toy_9, mContext);
        layer.addShape(ninethToy);

        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = -0.35f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle tenthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        tenthToy.setIsClickable(true);
        tenthToy.setTextResourceId(R.drawable.christmas_tree_toy_10, mContext);
        layer.addShape(tenthToy);

        centerX = mRandom.nextFloat()*(0.15f - (-0.15f)) + (-0.15f);
        centerY = -0.3f;
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle eleventhToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        eleventhToy.setIsClickable(true);
        eleventhToy.setTextResourceId(R.drawable.christmas_tree_toy_11, mContext);
        layer.addShape(eleventhToy);


        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {

        float centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        float centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        float width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        float height = width;
        TexturedRectangle fifteenthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        fifteenthToy.setIsClickable(true);
        fifteenthToy.setTextResourceId(R.drawable.christmas_tree_toy_15, mContext);
        Layer layer = new Layer();
        layer.addShape(fifteenthToy);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle engthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        engthToy.setIsClickable(true);
        engthToy.setTextResourceId(R.drawable.christmas_tree_toy_8, mContext);
        layer.addShape(engthToy);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle ninethToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        ninethToy.setIsClickable(true);
        ninethToy.setTextResourceId(R.drawable.christmas_tree_toy_9, mContext);
        layer.addShape(ninethToy);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle tenthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        tenthToy.setIsClickable(true);
        tenthToy.setTextResourceId(R.drawable.christmas_tree_toy_10, mContext);
        layer.addShape(tenthToy);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle eleventhToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        eleventhToy.setIsClickable(true);
        eleventhToy.setTextResourceId(R.drawable.christmas_tree_toy_11, mContext);
        layer.addShape(eleventhToy);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle sixteenthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        sixteenthToy.setIsClickable(true);
        sixteenthToy.setTextResourceId(R.drawable.christmas_tree_toy_16, mContext);

        layer.addShape(sixteenthToy);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle seventeenthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        seventeenthToy.setIsClickable(true);
        seventeenthToy.setTextResourceId(R.drawable.christmas_tree_toy_17, mContext);

        layer.addShape(seventeenthToy);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(-0.7f - (-0.9f)) + (-0.9f);
        width = mRandom.nextFloat()*(0.1f - 0.08f) + 0.08f;
        height = width;
        TexturedRectangle eighteenthToy = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        eighteenthToy.setIsClickable(true);
        eighteenthToy.setTextResourceId(R.drawable.christmas_tree_toy_18, mContext);

        layer.addShape(seventeenthToy);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
