package com.elisoft.glrendering.wallpapers.categories.material;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Circle;
import com.elisoft.glrendering.shapes.ShadowCircle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;

import java.util.Random;



/**
 * Created by yvorobey on 6/17/2015.
 */
public class BottTriangAddCirclePattern extends BottomTrianglesPattern {
    private static final float RADIUS = 0.2f;
    private static final float MAX_Y_CIRCLE = 1.0f - RADIUS;
    private static final float MIN_Y_CIRCLE = 0.7f - RADIUS;
    private static final float MIN_X_CIRCLE = -1.0f;
    private static final float MAX_X_CIRCLE = 1.0f;

    public BottTriangAddCirclePattern(Context context) {
        super(context, PatternsIds.BOTTOM_TRIANGLES_WITH_CIRCLE_PATTERN);
    }

    @Override
    protected Layer initializeFifthLayer() {
        Layer fifthLayer = new Layer();

        Circle circle = generateCircle(0.5f, getColor());
        ShadowCircle circleShadow= circle.generateShadow();
        fifthLayer.addShape(circleShadow);
        fifthLayer.addShape(circle);
        return fifthLayer;
    }
    private Circle generateCircle(float z, FloatColor floatColor) {
        Random random = new Random();
        float cx = random.nextFloat()*(MAX_X_CIRCLE - MIN_X_CIRCLE) + MIN_X_CIRCLE;
        float cy = 1.0f;
        float radius = random.nextFloat()*(1.0f - 0.5f) +0.5f;
        Vertex centerVertex = new Vertex(cx,cy, z);
        Circle shape = new Circle(centerVertex, 100, radius);
        shape.setColor(floatColor);
        return shape;
    }
}
