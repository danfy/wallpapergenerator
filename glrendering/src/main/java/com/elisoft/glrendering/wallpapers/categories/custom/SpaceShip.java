package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 9/28/2015.
 */
public class SpaceShip extends WallpaperPattern {
    private final Random random;
    public SpaceShip(Context context) {
        super(context, PatternsIds.SPACE_SHIP_PATTERN);
        random= new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        Vertex vertexTopLeft = new Vertex(-1.0f, 1.77f, 0.0f);
        Vertex vertexBottomLeft = new Vertex(-1.0f, -1.77f, 0.0f);
        Vertex vertexBottomRight = new Vertex(1.0f, -1.77f, 0.0f);
        Vertex vertexTopRight = new Vertex(1.0f, 1.77f, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(vertexTopLeft, vertexBottomLeft, vertexBottomRight, vertexTopRight);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTextResourceId(R.drawable.space_sky, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float centerX = random.nextFloat()*(0.8f -(-0.8f)) + (-0.8f);
        float centerY = random.nextFloat()*(0.8f - 0.2f) + 0.2f;
        float width = random.nextFloat()*(0.5f - 0.3f) + 0.3f;
        float height = width;
        Vertex centerVertex = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangle= new TexturedRectangle(centerVertex, width, height, 0f);
        texturedRectangle.setTextResourceId(R.drawable.space_moon, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float centerX = random.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        float centerY = random.nextFloat()*(-0.3f - (-0.8f)) + (-0.8f);
        float width = random.nextFloat()*(0.7f - 0.5f) + 0.5f;
        float height = width;
        Vertex centerVertex = new Vertex(centerX, centerY, 0.0f);
        TexturedRectangle texturedRectangle = new TexturedRectangle(centerVertex, width, height, 0.0f);
        texturedRectangle.setTextResourceId(R.drawable.space_sun, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        float spaceShipRatio = 2.07f;
        float centerX = random.nextFloat()*(0.5f - (-0.5f)) + (-0.5f);
        float centerY = random.nextFloat()*(0.4f - (-0.4f)) + (-0.4f);
        float height = random.nextFloat() *(0.6f - (0.4f)) + (0.4f);
        float width = height*spaceShipRatio;
        Vertex centerVertex = new Vertex(centerX, centerY, 0f);
        TexturedRectangle texturedRectangle= new TexturedRectangle(centerVertex, width, height, 0f);
        texturedRectangle.setTextResourceId(R.drawable.space_spaceship, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);
        return layer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
