package com.elisoft.glrendering.wallpapers.categories;

/**
 * Class holds constants for different patterns.
 */
public class PatternsIds {
    //Material category
    public static final int BOTTOM_TRIANGLES_PATTERN = 0;
    public static final int BOTTOM_TRIANGLES_WITH_CIRCLE_PATTERN = 1;
    public static final int RECTANGLES_FROM_TOP_PATTERN = 2;
    public static final int RECTANGLES_FROM_TOP_WITH_CIRCLE_PATTERN = 3;
    public static final int CIRCLE_PATTERN = 4;
    public static final int CROSSING_RECTANGLES_PATTERN = 5;
    public static final int CROSSING_RECTANGLES_WITH_CIRCLE_PATTERN = 6;
    public static final int TRIANGLES_FROM_ONE_ANGLE = 7;
    //End Material category

    //Transparent Shapes Category
    public static final int TRANSPARENT_CIRCLES = 8;
    public static final int BUBBLES = 10;
    public static final int SQUARE_BUBBLES = 15;
    public static final int NON_FLAT_SQUARE_BUBBLES = 27;

    //End Transparent Shapes Category

    //Gradient Category
    public static final int RADIAL_GRADIENT = 16;
    public static final int COLORED_RADIAL_GRADIENT = 17;
    public static final int HORIZONTAL_LINEAR_GRADIENT = 18;
    public static final int COLOR_HORIZONTAL_LINEAR_GRADIENT = 19;
    public static final int VERTICAL_LINEAR_GRADIENT = 20;
    public static final int COLOR_VERTICAL_LINEAR_GRADIENT = 21;
    //End Gradient Category

    //Star Sky category
    public static final int STARS_SKY = 13;
    //End Star Sky category


    public static final int BALLOONS_PATTERN = 22;
    public static final int MOUNTAINS_WITH_ICE =23;
    public static final int MOUNTAINS_WITH_MOON  =24;
    public static final int SPACE_SHIP_PATTERN = 25;
    public static final int HILLS_PATTERN = 26;
    public static final int PARK_PATTERN = 28;
    public static final int BEACH_PATTERN = 29;
    public static final int LIGHT_HOUSE = 30;
    public static final int NATURE_PATTERN = 31;
    public static final int MOUNTAINS_AND_SEA = 32;
    public static final int SUNSET = 39;

    //New Year
    public static final int SNOWMAN_PATTERN = 33;
    public static final int ABSTRACT_CHRISTMAS_TREE = 34;
    public static final int CHRISTMAS_CITY = 35;
    public static final int CHRISTMAS_TREE = 36;
    public static final int CHRISTMAS_MOUNTAINS = 37;
    public static final int CHRISTMAS_ROOM = 38;


//    public static final int SQUARES = 17;
//    public static final int ANALOG_LINES = 18;
//    public static final int CROSSING_CIRCLES_BORDER = 9;
//    public static final int WATER_FLOW = 11;
//    public static final int WAVE_CIRCLES = 12;
//    public static final int WAVES = 14;
}
