package com.elisoft.glrendering.wallpapers.categories.newyearholiday;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 12/23/2015.
 */
public class Snowman extends WallpaperPattern {
    private final Random mRandom;

    public Snowman(Context context) {
        super(context, PatternsIds.SNOWMAN_PATTERN);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.563f;
        float centerX = 0.0f;
        float centerY = 0.3f;
        float width = 2f;
        float height = width/backgroundRatio;

        TexturedRectangle background = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.snowman_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);


        centerX = mRandom.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        centerY = mRandom.nextFloat()*(0.7f - (0.2f)) + (0.2f);
        width = mRandom.nextFloat()*(0.5f - 0.3f) +0.3f;
        height = width;

        TexturedRectangle sun = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        sun.setIsClickable(true);
        sun.setTextResourceId(R.drawable.snowman_sun, mContext);
        layer.addShape(sun);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float firstCloudRatio = 2.22f;
        float centerX = mRandom.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        float centerY = 0.7f;
        float width = mRandom.nextFloat()*(0.3f - 0.2f) + 0.2f;
        float height = width/firstCloudRatio;
        TexturedRectangle firstCloud = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        firstCloud.setIsClickable(true);
        firstCloud.setTextResourceId(R.drawable.snowman_cloud_1, mContext);
        Layer layer = new Layer();
        layer.addShape(firstCloud);

        centerX = mRandom.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        centerY = 0.5f;
        width = mRandom.nextFloat()*(0.3f - 0.2f) + 0.2f;
        height = width/firstCloudRatio;
        TexturedRectangle secondCloud = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height ,0);
        secondCloud.setIsClickable(true);
        secondCloud.setTextResourceId(R.drawable.snowman_cloud_2, mContext);
        layer.addShape(secondCloud);

        centerX = mRandom.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        centerY = 0.3f;
        width = mRandom.nextFloat()*(0.3f - 0.2f) + 0.2f;
        height = width/firstCloudRatio;
        TexturedRectangle thirdCloud = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        thirdCloud.setIsClickable(true);
        thirdCloud.setTextResourceId(R.drawable.snowman_cloud_3, mContext);
        layer.addShape(thirdCloud);

        float mountRatio =2.51f;
        height = 0.7f;
        width = height* mountRatio;
        centerX = 0.0f;
        centerY = -(1.0f -height/2f);
        TexturedRectangle mount = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f) , width, height, 0);
        mount.setIsClickable(true);
        mount.setTextResourceId(R.drawable.snowman_mount, mContext);
        layer.addShape(mount);

        float thirdTreeRatio = 0.56f;
        height = 0.2f;
        width = height*thirdTreeRatio;
        centerX = -0.6f;
        centerY = -0.8f + height/2;
        TexturedRectangle thirdTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        thirdTree.setIsClickable(true);
        thirdTree.setTextResourceId(R.drawable.snowman_tree_3, mContext);
        layer.addShape(thirdTree);

        float secondTreeRatio = 0.55f;
        height = 0.25f;
        width = height*secondTreeRatio;
        centerX = -0.65f;
        centerY = -0.8f + height/2f;
        TexturedRectangle secondTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        secondTree.setIsClickable(true);
        secondTree.setTextResourceId(R.drawable.snowman_tree_2, mContext);
        layer.addShape(secondTree);
        return layer;

    }

    @Override
    protected Layer initializeThirdLayer() {
        float mainTreeRatio = 0.72f;
        float height = 1.0f;
        float width = height*mainTreeRatio;
        float centerX = 0.0f;
        float centerY = -0.3f;
        TexturedRectangle mainTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        mainTree.setIsClickable(true);
        mainTree.setTextResourceId(R.drawable.snowman_tree, mContext);
        Layer layer = new Layer();
        layer.addShape(mainTree);

        float secondGiftRatio = 0.64f;
        height = 0.12f;
        width = height*secondGiftRatio;
        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = -0.8f + height/2f;
        TexturedRectangle secondGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        secondGift.setIsClickable(true);
        secondGift.setTextResourceId(R.drawable.snowman_gift_2, mContext);
        layer.addShape(secondGift);


        float firstGiftRatio = 0.662f;
        height = 0.15f;
        width = height* firstGiftRatio;
        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = -0.8f +height/2f;
        TexturedRectangle firstGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        firstGift.setIsClickable(true);
        firstGift.setTextResourceId(R.drawable.snowman_gift, mContext);
        layer.addShape(firstGift);

        float thirdGiftRatio = 0.86f;
        height = 0.18f;
        width = height * thirdGiftRatio;
        centerX = mRandom.nextFloat()*(0.6f - ( -0.6f)) + (-0.6f);
        centerY = -0.8f + height/2f;
        TexturedRectangle thirdGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        thirdGift.setIsClickable(true);
        thirdGift.setTextResourceId(R.drawable.snowman_gift_3, mContext);
        layer.addShape(thirdGift);

        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        float snowmanRatio = 0.79f;
        float height = 0.4f;
        float width = height*snowmanRatio;
        float centerX = mRandom.nextFloat()*(0.6f - 0.2f) + 0.2f;
        float centerY = -0.8f + height/2f;
        TexturedRectangle snowman = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        snowman.setIsClickable(true);
        snowman.setTextResourceId(R.drawable.snowman_snowman, mContext);
        Layer layer = new Layer();
        layer.addShape(snowman);

        float snowRatio = 7.53f;
        height = 0.25f;
        width = height* snowRatio;
        centerX = 0.0f;
        centerY = -(1.0f - height/2f);
        TexturedRectangle snow = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        snow.setIsClickable(true);
        snow.setTextResourceId(R.drawable.snowman_snow, mContext);
        layer.addShape(snow);
        return layer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
