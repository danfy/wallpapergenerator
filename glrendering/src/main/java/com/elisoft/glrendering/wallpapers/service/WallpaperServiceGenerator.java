package com.elisoft.glrendering.wallpapers.service;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceHolder;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * Created by yvorobey on 12/29/2015.
 */
public class WallpaperServiceGenerator extends android.service.wallpaper.WallpaperService {
    @Override
    public Engine onCreateEngine() {
        return new WallpaperEngine();
    }
    private class WallpaperEngine extends Engine{
        private int LANDSCAPE = 0;
        private int PORTRAIT = 1;
        private final SurfaceHolder.Callback callback = new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                draw();
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
                draw();
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        };

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            draw();
            surfaceHolder.addCallback(callback);

        }

        @Override
        public void onDestroy() {
            this.getSurfaceHolder().removeCallback(callback);
            super.onDestroy();
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            if(visible) {
                draw();
            }
        }
        private void draw() {
            if(!isVisible()) {
                return;
            }
            Canvas canvas = null;
            try {
                canvas = getSurfaceHolder().lockCanvas();
                if(canvas!=null) {
                    int portrait = canvas.getHeight() > canvas.getWidth() ? PORTRAIT
                            : LANDSCAPE;

                    Bitmap bm = BitmapFactory
                            .decodeStream(new FileInputStream(WallpaperSettings.getURLFromPreferences(getApplicationContext())));




                    int screenHeight = canvas.getHeight();
                    int width = bm.getWidth();
                    width = (width*screenHeight)/bm.getHeight();
                    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
                    canvas.drawBitmap(bm,null, new RectF(0,0,width, screenHeight), paint);
//                    if (bm != null) {
//                            Matrix matrix = new Matrix();
//                            matrix.setRectToRect(
//                                    new RectF(0, 0, bm.getWidth(), bm.getHeight()),
//                                    new RectF(0, 0, bm.getWidth(), bm.getHeight()),
//                                    Matrix.ScaleToFit.CENTER);
//                        canvas.drawBitmap(bm, matrix, null);
//                    }
                }
            } catch (FileNotFoundException e) {

            } finally {
                if (canvas != null)
                    getSurfaceHolder().unlockCanvasAndPost(canvas);
            }
        }
    }
}
