package com.elisoft.glrendering.wallpapers.categories.material;

import android.content.Context;


import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Rectangle;
import com.elisoft.glrendering.shapes.ShadowRectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;


/**
 * Class represents pattern with crossing rectangles
 */
public class CrossRectanglesPattern extends WallpaperPattern {
    private static final String TAG = CrossRectanglesPattern.class.getSimpleName();
    private static final float MAX_WIDTH_RECTANGLE = 1.5f;
    private static final float MIN_WIDTH_RECTANGLE = 1.0f;
    protected static final float MAX_Y = 1.0f;
    protected static final float MIN_Y =-1.0f;
    protected CrossRectanglesPattern(Context context, int id) {
        super(context, id);
        setBackgroundColor(getColor());

    }

    public CrossRectanglesPattern(Context context) {
        super(context, PatternsIds.CROSSING_RECTANGLES_PATTERN);
        setBackgroundColor(getColor());

    }

    @Override
    protected Layer initializeFirstLayer() {
        Layer layer = new Layer();
        Rectangle rectangle = generateFirstRectangle(0.1f, getColor());
        ShadowRectangle shadowRectangle = rectangle.generateShadow();
        layer.addShape(shadowRectangle);
        layer.addShape(rectangle);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        Layer layer = new Layer();
        Rectangle rectangle = generateSecondRectangle(0.2f, getColor());
        ShadowRectangle shadowRectangle = rectangle.generateShadow();
        layer.addShape(shadowRectangle);
        layer.addShape(rectangle);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        Layer layer = new Layer();
        Rectangle rectangle = generateThirdRectangle(0.3f, getColor());
        ShadowRectangle shadowRectangle = rectangle.generateShadow();
        layer.addShape(shadowRectangle);
        layer.addShape(rectangle);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        Layer layer = new Layer();
        Rectangle rectangle = generateFourthRectangle(0.4f, getColor());
        ShadowRectangle shadowRectangle = rectangle.generateShadow();
        layer.addShape(shadowRectangle);
        layer.addShape(rectangle);
        return layer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }

    private Rectangle generateFirstRectangle(float z, FloatColor floatColor) {
        Random random = new Random();
        float dx = random.nextFloat()*(MAX_WIDTH_RECTANGLE - MIN_WIDTH_RECTANGLE) + MIN_WIDTH_RECTANGLE;

        Vertex vertexTop = new Vertex(-1.0f - dx, MIN_Y, z);
        Vertex vertexBottomLeft = new Vertex(-1.0f, MIN_Y, z);
        Vertex vertexBottomRight = new Vertex(1.0f, MAX_Y, z);
        Vertex vertexTopRight = new Vertex(1.0f -dx, MAX_Y, z);

        Rectangle rectangle = new Rectangle(vertexTop, vertexBottomLeft, vertexBottomRight, vertexTopRight);
        rectangle.setColor(floatColor);

        return rectangle;

    }
    private Rectangle generateSecondRectangle(float z, FloatColor floatColor) {

        Random random = new Random();
        float topLeftX = random.nextFloat()*(-0.5f - (-4.0f)) + (-4.0f);
        float width = random.nextFloat()*(1.0f - (0.5f)) + (0.5f);
        float dx = 0;
        if(topLeftX< -1.0f) {
            dx= - (Math.abs(topLeftX) - 1.0f);
        } else {
            dx = 1.0f - Math.abs(topLeftX);
        }



        Vertex vertexTopLeft = new Vertex(topLeftX, MIN_Y, z);
        Vertex vertexBottomLeft = new Vertex(topLeftX +width, MIN_Y, z);
        Vertex vertexBottomRight = new Vertex(1.0f + dx + width, MAX_Y, z);
        Vertex vertexTopRight = new Vertex(1.0f + dx, MAX_Y,z);

        Rectangle rectangle = new Rectangle(vertexTopLeft, vertexBottomLeft, vertexBottomRight, vertexTopRight);
        rectangle.setColor(floatColor);
        return rectangle;
    }
    private Rectangle generateThirdRectangle(float z, FloatColor floatColor) {
        float minX = -1.5f;
        float maxX = -0.5f;

        Random random = new Random();
        float width = random.nextFloat()*(1.0f - 0.3f) + (0.3f);
        float topLeftX = random.nextFloat()*(maxX - minX) + minX;

        float dx = 0;
        if(topLeftX< -1.0f) {
            dx= - (Math.abs(topLeftX) - 1.0f);
        } else {
            dx = 1.0f -Math.abs(topLeftX);
        }
        Vertex vertexBottomLeft = new Vertex(topLeftX, MAX_Y,z);
        Vertex vertexTopLeft = new Vertex(topLeftX + width, MAX_Y, z);
        Vertex vertexBottomRight = new Vertex(1.0f + dx, MIN_Y, z);
        Vertex vertexTopRight = new Vertex(1.0f + dx + width, MIN_Y, z);
        Rectangle rectangle = new Rectangle(vertexTopLeft, vertexBottomLeft, vertexBottomRight, vertexTopRight);
        rectangle.setColor(floatColor);
        return rectangle;
    }
    private Rectangle generateFourthRectangle(float z, FloatColor floatColor) {
        float maxX = 1.0f;
        float minX = 0.0f;
        Random random = new Random();
        float topRightX = random.nextFloat()*(maxX - minX) + minX;
        float width = random.nextFloat() *(0.8f - 0.4f) + (0.4f);
        float dx = 1.0f - topRightX;

        Vertex vertexTopRight = new Vertex(topRightX, MIN_Y, z);
        Vertex vertexBottomRight = new Vertex(topRightX - width, MIN_Y, z);
        Vertex vertexTopLeft = new Vertex(-1.0f - dx - width, MAX_Y, z);
        Vertex vertexBottomLeft = new Vertex(-1.0f - dx, MAX_Y, z);
        Rectangle rectangle = new Rectangle(vertexTopLeft, vertexBottomLeft, vertexTopRight, vertexBottomRight);
        rectangle.setColor(floatColor);
        return rectangle;
    }
}
