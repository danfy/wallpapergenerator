package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 11/9/2015.
 */
public class Park extends WallpaperPattern {

    private final Random mRandom;

    public Park(Context context) {
        super(context, PatternsIds.PARK_PATTERN);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        Vertex topLeftVertex = new Vertex(-1.0f, 1.0f, 0.0f);
        Vertex bottomLeftVertex = new Vertex(-1.0f, -0.8f, 0.0f);
        Vertex bottomRightVertex = new Vertex(1.0f, -0.8f, 0.0f);
        Vertex topRightVertex = new Vertex(1.0f, 1.0f, 0.0f);

        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeftVertex, bottomLeftVertex, bottomRightVertex, topRightVertex);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTextResourceId(R.drawable.park_background_2, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        Vertex topLeftVertexGround = new Vertex(-1.0f, -0.45f, 0.0f);
        Vertex bottomLeftVertexGround = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightVertexGround = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightVertexGround = new Vertex(1.0f, -0.45f, 0.0f);

        TexturedRectangle texturedRectangleGround = new TexturedRectangle(topLeftVertexGround, bottomLeftVertexGround, bottomRightVertexGround, topRightVertexGround);
        texturedRectangleGround.setIsClickable(false);
        texturedRectangleGround.setTextResourceId(R.drawable.park_background_hill, mContext);
        layer.addShape(texturedRectangleGround);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float firstBuildingRatio = 0.2954f;
        float centerX = mRandom.nextFloat()*(-0.1f -(-0.4f)) +(-0.4f);
        float centerY = mRandom.nextFloat()*(0.2f -(0.1f)) + 0.1f ;
        float width = mRandom.nextFloat()*(0.5f - (0.4f)) + 0.4f;
        float height = width/firstBuildingRatio;
        TexturedRectangle firstBuilding = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        firstBuilding.setIsClickable(true);
        firstBuilding.setTextResourceId(R.drawable.park_building_1, mContext);
        Layer layer = new Layer();
        layer.addShape(firstBuilding);

        float thirdBuildingRatio =0.1213f;
        centerX = mRandom.nextFloat()*(0.6f-0.3f) + 0.3f;
        centerY = mRandom.nextFloat()*(0.1f - (-0.1f) + (-0.1f));
        width = 0.15f;
        height = width/thirdBuildingRatio;
        TexturedRectangle thirdBuilding = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        thirdBuilding.setIsClickable(true);
        thirdBuilding.setTextResourceId(R.drawable.park_building_3, mContext);
        layer.addShape(thirdBuilding);


        float wealRatio = 0.86f;
        centerX = mRandom.nextFloat()*(0.5f - 0.3f) + 0.3f;
        centerY = mRandom.nextFloat()*(-0.3f - (-0.4f)) + (-0.4f);
        width = 0.5f;
        height = width/wealRatio;
        TexturedRectangle weal = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f),width, height, 0);
        weal.setIsClickable(true);
        weal.setTextResourceId(R.drawable.park_weal, mContext);
        layer.addShape(weal);

        float treeRatio = 1.22f;

        centerX = mRandom.nextFloat()*(0.65f - 0.6f) + 0.6f;
        centerY = mRandom.nextFloat()*(-0.7f -(-0.5f)) + (-0.5f);
        width = 0.1f;
        height = width/treeRatio;
        TexturedRectangle tree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        tree.setIsClickable(true);
        tree.setTextResourceId(R.drawable.park_tree_3, mContext);
        layer.addShape(tree);

        float treesRatio = 1.32f;
        centerX = mRandom.nextFloat()*(0.1f - 0.f) + 0.f;
        centerY = -0.6f;
        width = 0.2f;
        height = width/treesRatio;
        TexturedRectangle trees = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        trees.setIsClickable(true);
        trees.setTextResourceId(R.drawable.park_tree_4, mContext);
        layer.addShape(trees);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {

        Vertex topLeftVertex = new Vertex(-1.0f, -0.3f, 0.0f);
        Vertex bottomLeftVertex = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightVertex = new Vertex(0.7f, -1.0f, 0.0f);
        Vertex topRightVertex = new Vertex(0.7f, -0.3f, 0.0f);


        TexturedRectangle firstHill = new TexturedRectangle(topLeftVertex, bottomLeftVertex, bottomRightVertex, topRightVertex);
        firstHill.setIsClickable(true);
        firstHill.setTextResourceId(R.drawable.park_hill_2, mContext);
        Layer layer = new Layer();
        layer.addShape(firstHill);

        float buildingRatio = 0.413f;
        float centerX = mRandom.nextFloat()*(-0.55f - (-0.65f)) + (-0.65f);
        float centerY = mRandom.nextFloat()*(-0.1f + (-0.2f)) + (-0.2f);
        float width = 0.4f;
        float height = width/buildingRatio;

        TexturedRectangle building = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        building.setIsClickable(true);
        building.setTextResourceId(R.drawable.park_building_2, mContext);
        layer.addShape(building);

        float firstTreeRatio = 1.264f;
        centerX = -0.1f;
        centerY = -0.6f;
        width = 0.15f;
        height = width/firstTreeRatio;

        TexturedRectangle firstTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        firstTree.setIsClickable(true);
        firstTree.setTextResourceId(R.drawable.park_tree_2, mContext);
        layer.addShape(firstTree);

        float secondTreeRatio = 1.27f;
        centerX = -0.3f;
        centerY = -0.7f;
        width = 0.2f;
        height = width/secondTreeRatio;

        TexturedRectangle secondTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        secondTree.setIsClickable(true);
        secondTree.setTextResourceId(R.drawable.park_tree_1, mContext);
        layer.addShape(secondTree);

        Vertex topLeftSecHillVertex = new Vertex(-0.1f, -0.6f, 0.0f);
        Vertex bottomLeftSecHellVertex = new Vertex(-0.1f, -1.0f, 0.0f);
        Vertex bottomRightSecHellVertex = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightSecHellVertex = new Vertex(1.0f, -0.6f, 0.0f);

        TexturedRectangle secondHill = new TexturedRectangle(topLeftSecHillVertex, bottomLeftSecHellVertex, bottomRightSecHellVertex, topRightSecHellVertex);
        secondHill.setIsClickable(true);
        secondHill.setTextResourceId(R.drawable.park_hill_1, mContext);
        layer.addShape(secondHill);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {

        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
