package com.elisoft.glrendering.wallpapers.categories.material;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Circle;
import com.elisoft.glrendering.shapes.ShadowCircle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;


/**
 * Created by yvorobey on 6/18/2015.
 */
public class CirclesPattern extends WallpaperPattern {
    private static final float MAX_AXIS = 0.8f;
    private static final float MIN_AXIS = -0.8f;
    private static final float MAX_RADIUS = 1.0f;
    private static final float MIN_RADIUS = 0.5f;
    protected Vertex lastCircleVertex;

    protected  CirclesPattern(Context context, int id) {
        super(context,id);
    }
    public CirclesPattern(Context context) {
        super(context, PatternsIds.CIRCLE_PATTERN);
        setBackgroundColor(getColor());

    }

    @Override
    protected Layer initializeFirstLayer() {
        Layer firstLayer = new Layer();
        Circle circle = generateCircle(0.1f, getColor(), 1.0f);
        ShadowCircle shadowCircle = circle.generateShadow();
        firstLayer.addShape(shadowCircle);
        firstLayer.addShape(circle);

        return firstLayer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        Layer secondLayer = new Layer();
        Circle circle = generateCircle(0.2f, getColor(),1.0f);
        ShadowCircle shadowCircle = circle.generateShadow();
       secondLayer.addShape(shadowCircle);
        secondLayer.addShape(circle);
        return secondLayer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        Layer thirdLayer = new Layer();
        Circle circle = generateCircle(0.3f, getColor(),0.8f);
        ShadowCircle shadowCircle = circle.generateShadow();
        thirdLayer.addShape(shadowCircle);
        thirdLayer.addShape(circle);
        return thirdLayer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        Layer fourthLayer = new Layer();
        Circle circle = generateCircle(0.4f, getColor(),0.5f);
        ShadowCircle shadowCircle = circle.generateShadow();
        fourthLayer.addShape(shadowCircle);
        fourthLayer.addShape(circle);
        return fourthLayer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
    private Circle generateCircle(float z, FloatColor floatColor, float scale) {
        Random random = new Random();

        float centerX = random.nextFloat()*(MAX_AXIS - MIN_AXIS) + MIN_AXIS;
        float centerY = random.nextFloat()*(MAX_AXIS - MIN_AXIS) + MIN_AXIS;
        float radius = random.nextFloat()*(MAX_RADIUS - MIN_RADIUS)*scale + MIN_RADIUS*scale;
        Vertex centerVertex = new Vertex(centerX,centerY, z);
        lastCircleVertex = centerVertex;
        Circle circle = new Circle(centerVertex, 100, radius);
        circle.setColor(floatColor);
        return circle;

    }
}
