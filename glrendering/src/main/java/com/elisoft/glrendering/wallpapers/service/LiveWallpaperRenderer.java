package com.elisoft.glrendering.wallpapers.service;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by yvorobey on 12/29/2015.
 */
public class LiveWallpaperRenderer implements GLSurfaceView.Renderer {
    private final Context mContext;

    public LiveWallpaperRenderer(Context context) {
        mContext = context;
    }

    @Override
    public void onSurfaceCreated(GL10 gl10, EGLConfig eglConfig) {

    }

    @Override
    public void onSurfaceChanged(GL10 gl10, int i, int i1) {

    }

    @Override
    public void onDrawFrame(GL10 gl10) {

    }



}
