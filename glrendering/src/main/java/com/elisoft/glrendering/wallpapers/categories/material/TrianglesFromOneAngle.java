package com.elisoft.glrendering.wallpapers.categories.material;

import android.content.Context;


import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.ShadowTriangle;
import com.elisoft.glrendering.shapes.Triangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;



/**
 * Created by yvorobey on 6/22/2015.
 */
public class TrianglesFromOneAngle extends WallpaperPattern {
    private static final String TAG = TrianglesFromOneAngle.class.getSimpleName();
    private float fromX, fromY;
    private float dx;
    protected TrianglesFromOneAngle(Context context, int id) {
        super(context, id);
        setBackgroundColor(getColor());

    }
    public TrianglesFromOneAngle(Context context) {
        super(context, PatternsIds.TRIANGLES_FROM_ONE_ANGLE);
        setBackgroundColor(getColor());

    }

    @Override
    protected Layer initializeFirstLayer() {
        Layer layer = new Layer();
        Triangle triangle = generateFirstTriangle(0.1f, getColor());
        ShadowTriangle shadowTestTriangle = triangle.generateShadow();
        layer.addShape(shadowTestTriangle);
        layer.addShape(triangle);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        Layer layer = new Layer();
        Triangle triangle = generateSecondTriangle(0.2f, getColor());
        ShadowTriangle shadowTriangle = triangle.generateShadow();
        layer.addShape(shadowTriangle);
        layer.addShape(triangle);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        Layer layer =new Layer();
        Triangle triangle = generateThirdTriangle(0.3f, getColor());
        ShadowTriangle shadowTriangle = triangle.generateShadow();
        layer.addShape(shadowTriangle);
        layer.addShape(triangle);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        Layer layer = new Layer();
        Triangle triangle = generateFourthTriangle(0.4f, getColor());
        ShadowTriangle shadowTriangle = triangle.generateShadow();
        layer.addShape(shadowTriangle);
        layer.addShape(triangle);
        return layer;
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);

    }
    private Triangle generateFirstTriangle(float z, FloatColor floatColor){
        Random random = new Random();
        int corner = random.nextInt((4-1)+1) + 1;
        dx = (random.nextFloat()*(1.0f - 0.0f) + 0.0f);

        switch (corner) {
           case 1: fromX = -1.0f;
                   fromY = 1.0f;
                   dx = 2.0f +dx;
               break;
            case 2: fromX = -1.0f;
                   fromY = -1.0f;
                   dx = 2.0f +dx;
                break;
            case 3: fromX = 1.0f;
                    fromY = -1.0f;
                    dx = -2.0f-dx;
                break;
            case 4: fromX = 1.0f;
                    fromY = 1.0f;
                    dx =-2.0f -dx;
                break;
        }

        Vertex top = new Vertex(fromX, fromY, z);
        Vertex vertexBottomLeft = new Vertex( fromX, -fromY, z);
        Vertex vertexBottomRight = new Vertex(fromX + dx, -fromY,z);
        Triangle triangle = new Triangle(top, vertexBottomLeft, vertexBottomRight);
        triangle.setColor(floatColor);
        return triangle;
    }
    private Triangle generateSecondTriangle(float z, FloatColor floatColor) {
        Random random = new Random();

        Vertex top = new Vertex(fromX, fromY, z);
        Vertex vertexBottomLeft = new Vertex(fromX, -fromY, z);
        if(fromX>0) {
            dx = dx + random.nextFloat()*(1.0f - 0.8f) + 0.8f;
        } else {
            dx = dx/2 - random.nextFloat()*(1.0f - 0.8f) + 0.8f;
        }

        Vertex vertexBottomRight = new Vertex(fromX + dx, -fromY, z);
        Triangle triangle = new Triangle(top, vertexBottomLeft, vertexBottomRight);
        triangle.setColor(floatColor);
        return triangle;
    }
    private Triangle generateThirdTriangle(float z, FloatColor floatColor) {
        Random random = new Random();
        Vertex vertexTop = new Vertex(fromX, fromY, z);
        Vertex vertexBottomLeft = new Vertex(fromX, -fromY, z);
        if(fromX>0) {
            dx = dx + random.nextFloat()*(1.0f - 0.8f) + 0.8f;

        }else {
            dx = dx/2 - random.nextFloat()*(1.0f -0.8f) + 0.8f;
        }
        Vertex vertexBottomRight = new Vertex(fromX + dx, -fromY, z);
        Triangle triangle = new Triangle(vertexTop, vertexBottomLeft, vertexBottomRight);
        triangle.setColor(floatColor);
        return triangle;
    }
    private Triangle generateFourthTriangle(float z, FloatColor floatColor) {
        Random random = new Random();
        Vertex vertexTop = new Vertex(fromX, fromY, z);
        Vertex vertexBottomLeft = new Vertex(fromX, -fromY, z);
        if(fromX>0) {
            dx = dx + random.nextFloat()*(1.0f - 0.8f) + 0.8f;

        }else {
            dx = dx/2 -random.nextFloat()*(1.0f - 0.8f) + 0.8f;
        }
        Vertex vertexBottomRight = new Vertex(fromX + dx, -fromY, z);
        Triangle triangle = new Triangle(vertexTop, vertexBottomLeft, vertexBottomRight);
        triangle.setColor(floatColor);
        return triangle;
    }
}
