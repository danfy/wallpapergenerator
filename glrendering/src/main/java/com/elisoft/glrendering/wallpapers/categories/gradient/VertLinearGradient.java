package com.elisoft.glrendering.wallpapers.categories.gradient;

import android.content.Context;
import android.graphics.Color;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.LinearGradientRectangle;
import com.elisoft.glrendering.shapes.VerticalGradientRectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;

/**
 * Created by yvorobey on 9/1/2015.
 */
public class VertLinearGradient extends LinearGradient {

    public VertLinearGradient(Context context) {
        super(context, PatternsIds.VERTICAL_LINEAR_GRADIENT);
        palete.setColorRange(ColorRange.BASICS);
    }
    protected VertLinearGradient(Context context, int patternId) {
        super(context, patternId);
    }

    @Override
    protected FloatColor initializeFirstColor() {
        return palete.getSomeColor();
    }

    @Override
    protected FloatColor initializeSecondColor() {
        return FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.black));
    }

    @Override
    protected LinearGradientRectangle initializeLinearGradientShape(float resolutionX, float resolutionY, FloatColor firstColor, FloatColor secondColor) {
        VerticalGradientRectangle verticalGradientRectangle = new VerticalGradientRectangle(resolutionX,resolutionY,firstColor,secondColor);
        return verticalGradientRectangle;
    }

    @Override
    protected Layer initializeSecondLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeThirdLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }
}
