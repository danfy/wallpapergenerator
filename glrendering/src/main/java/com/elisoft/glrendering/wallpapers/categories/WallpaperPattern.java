package com.elisoft.glrendering.wallpapers.categories;

import android.content.Context;


import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.components.Scene;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.colors.Palete;


/**
 * Abstract class for patterns.
 */
public abstract class WallpaperPattern  {
    // Id of pattern. PatternsIds class holds all ids.
    protected int mId;
    protected Context mContext;
    protected Scene scene;
    protected Palete palete;
    protected int width;
    protected int height;




    public void setWidth(int width) {
        this.width = width;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    public WallpaperPattern(Context context, int id) {
        this.mId = id;
        mContext = context;
        scene = new Scene();
        palete=new Palete(context);
    }

    protected FloatColor getColor (){
        return palete.getSomeColor();
    }
    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }

    /**
     * Method returns FloatColor object of background color.
     * @return FloatColor object of background color.
     */
    public FloatColor getBackgroundColor() {
        return scene.getBackgroundColor();
    }

    /**
     * Set background color
     * @param backgroundColor - FloatColor object for background color.
     */
    public void setBackgroundColor(FloatColor backgroundColor) {
        scene.setBackgroundColor(backgroundColor);

    }
    protected abstract Layer initializeFirstLayer();
    protected abstract Layer initializeSecondLayer();
    protected abstract Layer initializeThirdLayer();
    protected abstract Layer initializeFourthLayer();
    protected abstract Layer initializeFifthLayer();

    /**
     * Method generate Scene object for rendering. Must be call before draw.
     */
    public Scene generateScene() {

        scene.clear();
        initializeNewBackgroundColor();
        scene.addLayer(initializeFirstLayer());
        scene.addLayer(initializeSecondLayer());
        scene.addLayer(initializeThirdLayer());
        scene.addLayer(initializeFourthLayer());
        scene.addLayer(initializeFifthLayer());
        return scene;
    }
    public abstract void draw(float[] mMVPMatrix);
    protected void initializeNewBackgroundColor() {
        setBackgroundColor(palete.getSomeColor());
    }
    /**
     * Draw scene.
     * @param mMVPMatrix
     */
    protected void drawScene(float[] mMVPMatrix) {
        scene.draw(mMVPMatrix);
    }

    public Scene getScene() {
        return scene;
    }


}
