package com.elisoft.glrendering.wallpapers.categories.material;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.AbstractShape;
import com.elisoft.glrendering.shapes.Rectangle;
import com.elisoft.glrendering.shapes.ShadowRectangle;
import com.elisoft.glrendering.shapes.ShadowTriangle;
import com.elisoft.glrendering.shapes.Triangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;


/**
 * Class represents pattern with rectangles from top
 */
public class RectanglesFromTop extends WallpaperPattern {
    private AbstractShape nextShape;
    private AbstractShape shape;
    private Vertex dependVertex;
    private Vertex secondDependVertex;

    protected RectanglesFromTop(Context context, int id) {

        super(context, id);
        setBackgroundColor(getColor());

    }

    public RectanglesFromTop(Context context) {
        super(context, PatternsIds.RECTANGLES_FROM_TOP_PATTERN);
        setBackgroundColor(getColor());

    }
    private Triangle generateTriangle(float z, FloatColor floatColor) {
        Random random = new Random();
        float startX = 0.0f;
        float startY = -1.0f;
        float dx = random.nextFloat()*(0.5f -(-0.5f)) + (-0.5f);
        float dy = random.nextFloat()*(0.0f - (-0.1f)) + (-0.1f);
        Vertex vertexBottomLeft = new Vertex(startX + dx , startY + dy, z);
        Vertex vertexTop = new Vertex(vertexBottomLeft.getCoordinateX() -(1.0f +Math.abs(startY+dy)), 1.0f, z);
        Vertex vertexBottomRight = new Vertex(vertexBottomLeft.getCoordinateX() + (1.0f +Math.abs(startY+dy)), 1.0f , z);
        Triangle triangle = new Triangle(vertexTop, vertexBottomLeft, vertexBottomRight);
        triangle.setColor(floatColor);
        nextShape = generateSecondTriangle(0.2f, getColor(), dy, vertexBottomLeft.getCoordinateX());
        return triangle;

    }
    private Triangle generateSecondTriangle(float z, FloatColor floatColor, float dy, float bottomLeftX) {
        Vertex vertexBottomLeft = new Vertex(bottomLeftX - Math.abs(dy), -1.0f, z);
        Vertex vertexTop = new Vertex(vertexBottomLeft.getCoordinateX() - (1.0f + Math.abs(vertexBottomLeft.getCoordinateY())), 1.0f, z);
        Vertex vertexBottomRight = new Vertex(vertexBottomLeft.getCoordinateX() + (1.0f + Math.abs(vertexBottomLeft.getCoordinateY())), 1.0f,z);
        Triangle triangle = new Triangle(vertexTop, vertexBottomLeft, vertexBottomRight);
        dependVertex = vertexBottomLeft;
        triangle.setColor(floatColor);
        shape = generaThirdTriangle(0.3f, getColor(), vertexTop, vertexBottomLeft, vertexBottomRight);

        return triangle;

    }
    private Triangle generaThirdTriangle(float z, FloatColor floatColor, Vertex topVertex, Vertex bottomLeftVertex, Vertex bottomRightVertex) {
        float dYMax = 1.5f;
        float dYMin = 0.5f;
        Random random = new Random();
        float dy = random.nextFloat()*(dYMax - dYMin) + dYMin;
        Vertex vertexBottomLeft = new Vertex(bottomLeftVertex.getCoordinateX(), bottomLeftVertex.getCoordinateY() +dy, z);
        Vertex vertexTop = new Vertex(bottomLeftVertex.getCoordinateX()- (2.0f-dy), 1.0f, z);
        Vertex vertexBottomRight = new Vertex(bottomLeftVertex.getCoordinateX() +(2.0f - dy), 1.0f, z);
        secondDependVertex = bottomLeftVertex;
        Triangle triangle = new Triangle(vertexTop, vertexBottomLeft, vertexBottomRight);

        triangle.setColor(floatColor);
        return triangle;

    }

    private Rectangle generateFourRectangle(float z, FloatColor floatColor) {
        float dYMax = secondDependVertex.getCoordinateY() -0.1f;
        float dYMin = dependVertex.getCoordinateY() + 0.1f;
        Random random = new Random();
        float dy = random.nextFloat()*(dYMax - dYMin) + dYMin;
        float dx = 1.0f - Math.abs(dy);
        Vertex vertexBottomLeft = new Vertex(dependVertex.getCoordinateX() - dx, dy, z);
        dYMax = -0.2f;
        dYMin = vertexBottomLeft.getCoordinateY() + 0.2f;
        float dyTop = random.nextFloat()*(dYMax - dYMin) + dYMin;
        dx = Math.abs(vertexBottomLeft.getCoordinateY()) - Math.abs(dyTop);
        Vertex vertexTopLeft = new Vertex(vertexBottomLeft.getCoordinateX() - dx, dyTop, z);
        Vertex vertexBottomRight = new Vertex(vertexBottomLeft.getCoordinateX() + (1.0f + Math.abs(vertexBottomLeft.getCoordinateY())), 1.0f, z);
        Vertex vertexTopRight = new Vertex(vertexTopLeft.getCoordinateX() + (1.0f + Math.abs(vertexTopLeft.getCoordinateY())),1.0f, z);
        Rectangle rectangle = new Rectangle(vertexTopLeft, vertexBottomLeft, vertexBottomRight, vertexTopRight);
        rectangle.setColor(floatColor);
        return rectangle;
    }

    @Override
    protected Layer initializeFirstLayer() {
        Layer firstLayer = new Layer();
        Triangle triangle = generateTriangle(0.1f, getColor());
        ShadowTriangle shadowTriangle = triangle.generateShadow();
        firstLayer.addShape(shadowTriangle);
        firstLayer.addShape(triangle);
        return firstLayer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        Layer secondLayer = new Layer();
        ShadowTriangle shadowTriangle = ((Triangle)nextShape).generateShadow();
        secondLayer.addShape(shadowTriangle);
        secondLayer.addShape(nextShape);
        return secondLayer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        Layer thirdLayer = new Layer();
        ShadowTriangle shadowTriangle = ((Triangle)shape).generateShadow();
        thirdLayer.addShape(shadowTriangle);
        thirdLayer.addShape(shape);
        return thirdLayer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        Layer fourLayer = new Layer();
        Rectangle rectangle = generateFourRectangle(0.4f,getColor());
        ShadowRectangle shadowRectangle = rectangle.generateShadow();
        fourLayer.addShape(shadowRectangle);
        fourLayer.addShape(rectangle);

        return fourLayer;
    }
    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }


}
