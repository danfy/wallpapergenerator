package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 11/12/2015.
 */
public class Nature extends WallpaperPattern {
    private final Random mRandom;
    public Nature(Context context) {
        super(context, PatternsIds.NATURE_PATTERN);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.5625f;
        float centerX = 0.0f;
        float centerY = -0.3f;
        float width = 2f;
        float height = width/backgroundRatio;

        TexturedRectangle background = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.nature_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);

        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = mRandom.nextFloat()*(0.7f -(0.2f)) + (0.2f);
        width = 0.3f;
        height = 0.3f;

        TexturedRectangle moon = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        moon.setIsClickable(true);
        moon.setTextResourceId(R.drawable.nature_moon, mContext);
        layer.addShape(moon);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float seventhTreeRatio =0.353f;
        float centerX = mRandom.nextFloat() *(0.5f - (-0.5f)) + (-0.5f);
        float centerY = 0.0f;
        float height = 0.1f;
        float width = height*seventhTreeRatio;
        TexturedRectangle seventhTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        seventhTree.setIsClickable(true);
        seventhTree.setTextResourceId(R.drawable.nature_tree_7, mContext);
        Layer layer = new Layer();
        layer.addShape(seventhTree);

        float sixthTreeRatio = 0.35f;
        centerX = mRandom.nextFloat()*(0.5f -(-0.5f)) +(-0.5f);
        centerY = -0.05f;
        height = 0.15f;
        width = height*sixthTreeRatio;
        TexturedRectangle sixthTree= new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        sixthTree.setIsClickable(true);
        sixthTree.setTextResourceId(R.drawable.nature_tree_6, mContext);
        layer.addShape(sixthTree);

        float millRatio = 0.764f;
        centerX = mRandom.nextFloat()*(0.6f - (-0.6f)) + (-0.6f);
        centerY = 0.0f;
        height = 0.3f;
        width = height*millRatio;
        TexturedRectangle mill = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        mill.setIsClickable(true);
        mill.setTextResourceId(R.drawable.nature_mill, mContext);
        layer.addShape(mill);

        float ninthTreeRatio = 0.35f;
        centerX = mRandom.nextFloat()*(0.6f - 0.3f) + 0.3f;
        centerY = -0.2f;
        height = 0.2f;
        width = height*ninthTreeRatio;
        TexturedRectangle ninthTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        ninthTree.setIsClickable(true);
        ninthTree.setTextResourceId(R.drawable.nature_tree_9, mContext);
        layer.addShape(ninthTree);

        float fifthTreeRatio =0.355f;
        centerX = mRandom.nextFloat()*(-0.3f - (-0.6f)) + (-0.6f);
        centerY = -0.2f;
        height = 0.22f;
        width = height*fifthTreeRatio;
        TexturedRectangle fifthTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        fifthTree.setIsClickable(true);
        fifthTree.setTextResourceId(R.drawable.nature_tree_5, mContext);
        layer.addShape(fifthTree);

        float tentRatio =1.75f;
        centerX = 0.0f;
        centerY = -0.25f;
        height = 0.3f;
        width = height*tentRatio;
        TexturedRectangle tent= new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        tent.setIsClickable(true);
        tent.setTextResourceId(R.drawable.nature_tent, mContext);
        layer.addShape(tent);

        float womenRatio = 0.58f;
        centerX = -0.2f;
        centerY = -0.3f;
        height = 0.2f;
        width = height*womenRatio;
        TexturedRectangle women = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        women.setIsClickable(true);
        women.setTextResourceId(R.drawable.nature_women, mContext);
        layer.addShape(women);

        float fireRatio = 0.59f;
        centerX = 0.0f;
        centerY = -0.4f;
        height =0.3f;
        width = height*fireRatio;
        TexturedRectangle fire = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        fire.setIsClickable(true);
        fire.setTextResourceId(R.drawable.nature_fire, mContext);
        layer.addShape(fire);

        float manRatio = 1.46f;
        centerX = 0.15f;
        centerY = -0.4f;
        width = 0.3f;
        height = width/manRatio;
        TexturedRectangle man = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        man.setIsClickable(true);
        man.setTextResourceId(R.drawable.nature_man, mContext);
        layer.addShape(man);

        float fourthTreeRatio =0.605f;
        centerX = mRandom.nextFloat()*(0.7f - (0.5f)) + 0.5f;
        centerY = -0.3f;
        height = 0.3f;
        width = height*fourthTreeRatio;
        TexturedRectangle fourthTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        fourthTree.setIsClickable(true);
        fourthTree.setTextResourceId(R.drawable.nature_tree_4, mContext);
        layer.addShape(fourthTree);

        float thirdTreeRatio = 0.34f;
        centerX = mRandom.nextFloat()*(-0.5f - (-0.7f)) + (-0.7f);
        centerY = -0.3f;
        height = 0.3f;
        width = height*thirdTreeRatio;
        TexturedRectangle thirdTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        thirdTree.setIsClickable(true);
        thirdTree.setTextResourceId(R.drawable.nature_tree_3, mContext);
        layer.addShape(thirdTree);

        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float secondTreeRatio = 0.61f;
        float centerX = mRandom.nextFloat()*(-0.5f - (-0.7f)) + (-0.7f);
        float centerY = -0.5f;
        float height = 0.4f;
        float width = height*secondTreeRatio;
        TexturedRectangle secondTree = new TexturedRectangle(new Vertex(centerX, centerY,0.0f), width, height,0);
        secondTree.setIsClickable(true);
        secondTree.setTextResourceId(R.drawable.nature_tree_2, mContext);
        Layer layer = new Layer();
        layer.addShape(secondTree);

        float firstTreeRatio =0.353f;
        centerX = mRandom.nextFloat()*(0.7f - 0.5f) + 0.5f;
        centerY = -0.5f;
        height = 0.4f;
        width = height*firstTreeRatio;
        TexturedRectangle firstTree = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        firstTree.setIsClickable(true);
        firstTree.setTextResourceId(R.drawable.nature_tree_1, mContext);
        layer.addShape(firstTree);


        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
