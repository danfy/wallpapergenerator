package com.elisoft.glrendering.wallpapers.categories.gradient;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;

/**
 * Created by yvorobey on 8/31/2015.
 */
public class ColoredRadialGradient extends RadialGradient {

    public ColoredRadialGradient(Context context) {
        super(context, PatternsIds.COLORED_RADIAL_GRADIENT);

        setBackgroundColor(palete.getSomeColor());
    }

    public ColoredRadialGradient(Context context, int width, int height) {
        super(context, width, height);
    }

    @Override
    protected Layer initializeFirstLayer() {

        Layer layer = new Layer();
        layer.addShape(initializeBackgroundRectangle(width, height, palete.getSomeColor()));
        return layer;
    }
}
