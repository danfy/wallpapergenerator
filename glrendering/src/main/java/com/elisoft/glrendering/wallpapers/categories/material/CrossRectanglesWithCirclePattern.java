package com.elisoft.glrendering.wallpapers.categories.material;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Circle;
import com.elisoft.glrendering.shapes.ShadowCircle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;


import java.util.Random;


/**
 * Created by yvorobey on 6/22/2015.
 */
public class CrossRectanglesWithCirclePattern extends CrossRectanglesPattern {
    private static final float MAX_RADIUS = 0.6f;
    private static final float MIN_RADIUS = 0.5f;


  public CrossRectanglesWithCirclePattern(Context context) {
      super(context, PatternsIds.CROSSING_RECTANGLES_WITH_CIRCLE_PATTERN );
  }

    @Override
    protected Layer initializeFifthLayer() {
        Layer layer = new Layer();
        Circle circle = generateCircle(0.5f, getColor());
        ShadowCircle circleShadow = circle.generateShadow();
        layer.addShape(circleShadow);
        layer.addShape(circle);
        return layer;
    }
    private Circle generateCircle(float z, FloatColor floatColor) {
        Random random = new Random();
        float cx = random.nextFloat()*(1.0f - (-1.0f)) + (-1.0f);
        float cy = random.nextFloat()*(1.0f - (-1.0f)) + (-1.0f);
        float radius = random.nextFloat()*(MAX_RADIUS - MIN_RADIUS) + MIN_RADIUS;

        Vertex centerVertex = new Vertex(cx, cy, z);
        Circle circle = new Circle(centerVertex, 100, radius);
        circle.setColor(floatColor);
        return circle;
    }
}
