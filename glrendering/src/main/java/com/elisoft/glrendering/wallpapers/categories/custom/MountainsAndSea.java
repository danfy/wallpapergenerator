package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 11/13/2015.
 */
public class MountainsAndSea extends WallpaperPattern {
    private final Random mRandom;
    public MountainsAndSea(Context context) {
        super(context, PatternsIds.MOUNTAINS_AND_SEA);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        TexturedRectangle background = new TexturedRectangle(new Vertex(0.0f, 0.0f, 0.0f), 2.0f, 2.0f, 0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.mountains_and_sea_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);


        float centerX = mRandom.nextFloat()*(0.5f - (-0.5f)) + (-0.5f);
        float centerY = mRandom.nextFloat()*(0.8f - (0.2f)) + 0.2f;
        float height = mRandom.nextFloat()*(0.4f - (0.2f)) + 0.2f;
        float width = height;

        TexturedRectangle sun = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        sun.setIsClickable(true);
        sun.setTextResourceId(R.drawable.mountains_and_sea_sun, mContext);
        layer.addShape(sun);

        float firstMountainRatio = 1.24f;
        height = 0.4f;
        width = height*firstMountainRatio;
        centerX = mRandom.nextFloat()*(0.0f - (-0.5f))  + (-0.5f);
        centerY = 0.1f;
        TexturedRectangle firstMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        firstMountain.setIsClickable(true);
        firstMountain.setTextResourceId(R.drawable.mountains_and_sea_mountain_1, mContext);
        layer.addShape(firstMountain);

        float thirdMountainRatio =1.37f;
        height = 0.36f;
        centerX = centerX - width/2;
        width = height*thirdMountainRatio;
        centerY = 0.08f;
        TexturedRectangle thirdMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        thirdMountain.setIsClickable(true);
        thirdMountain.setTextResourceId(R.drawable.mountains_and_sea_mountain_3, mContext);
        layer.addShape(thirdMountain);

        float secondMountainRatio = 1.62f;
        height = 0.3f;
        centerX = centerX + width/3;
        width = height*secondMountainRatio;
        centerY = 0.05f;
        TexturedRectangle secondMaountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        secondMaountain.setIsClickable(true);
        secondMaountain.setTextResourceId(R.drawable.mountains_and_sea_mountain_2, mContext);
        layer.addShape(secondMaountain);

        float seaRatio = 1.7f;
        centerX = 0.0f;
        centerY = -0.6f;
         height = 1.0f;
         width = height*seaRatio;
        TexturedRectangle sea = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        sea.setIsClickable(false);
        sea.setTextResourceId(R.drawable.mountains_and_sea_sea, mContext);
        layer.addShape(sea);


        float shipRatio = 0.74f;
        height = 0.2f;
        width = height*shipRatio;
        centerX = mRandom.nextFloat()*(0.6f - (0.2f)) + 0.2f;
        centerY = 0.0f;
        TexturedRectangle ship = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        ship.setIsClickable(true);
        ship.setTextResourceId(R.drawable.mountains_and_sea_yacht, mContext);
        layer.addShape(ship);

        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float hillRatio = 2.15f;
        float centerX = 0.0f;
        float centerY = -0.6f;
        float width = 1.8f;
        float height = width/hillRatio;

        TexturedRectangle hill = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        hill.setIsClickable(true);
        hill.setTextResourceId(R.drawable.mountains_and_sea_hill_1, mContext);
        Layer layer = new Layer();
        layer.addShape(hill);

        float seventhMountainRatio = 4.02f;
        height = 0.4f;
        width = height*seventhMountainRatio;
        centerX = 0.5f;
        centerY = -0.8f;

        TexturedRectangle seventhMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        seventhMountain.setIsClickable(true);
        seventhMountain.setTextResourceId(R.drawable.mountains_and_sea_mountain_7, mContext);
        layer.addShape(seventhMountain);

        float eighthMountainRatio = 2.4f;
        height = 0.6f;
        width = height*eighthMountainRatio;
        centerX = 0.7f;
        centerY = -0.7f;
        TexturedRectangle eighthMountain = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        eighthMountain.setIsClickable(true);
        eighthMountain.setTextResourceId(R.drawable.mountains_and_sea_mountain_8, mContext);
        layer.addShape(eighthMountain);

        float balloonRatio = 0.684f;
        height = 0.3f;
        width = height*balloonRatio;
        centerX = mRandom.nextFloat()*(0.5f - (-0.5f)) +(-0.5f);
        centerY = mRandom.nextFloat()*(0.8f - 0.3f) + 0.3f;
        TexturedRectangle balloon = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        balloon.setIsClickable(true);
        balloon.setTextResourceId(R.drawable.mountains_and_sea_ballon, mContext);
        layer.addShape(balloon);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
