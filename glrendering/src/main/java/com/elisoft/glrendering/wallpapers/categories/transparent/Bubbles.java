package com.elisoft.glrendering.wallpapers.categories.transparent;

import android.content.Context;


import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Circle;
import com.elisoft.glrendering.shapes.GradientRadialRectangle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;



/**
 * Class represents bubbles pattern
 */
public class Bubbles extends WallpaperPattern {

    public Bubbles(Context context) {
        super(context, PatternsIds.BUBBLES);
        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());
    }
    public Bubbles(Context context, int width, int height ) {
        super(context, PatternsIds.BUBBLES);
        palete.setColorRange(ColorRange.BASICS);
        setBackgroundColor(palete.getSomeColor());
        this.width= width;
        this.height = height;
    }

    @Override
    protected Layer initializeFirstLayer() {

        return initializeFirstLayer(0.1f);
    }

    @Override
    protected Layer initializeSecondLayer() {

        return initializeSecondLayer(0.2f);
    }

    @Override
    protected Layer initializeThirdLayer() {

        return initializeThirdLayer(0.3f);
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    protected void initializeNewBackgroundColor() {
        setBackgroundColor(palete.getSomeColor());
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
    private Layer initializeFirstLayer(float z) {

        Layer layer = new Layer();
        FloatColor firstColor = palete.getSomeColor();
        FloatColor secondColor = FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.black));
        GradientRadialRectangle gradientRadialRectangle = new GradientRadialRectangle(width, height,firstColor, secondColor, 0.5f, 0.7f, 0.8f);
        layer.addShape(gradientRadialRectangle);
        return layer;

    }
    private Layer initializeSecondLayer(float z) {
        Random random = new Random();
        float minX = -0.8f;
        float maxX  = 0.8f;
        float minY = -0.8f;
        float maxY = 0.8f;
        float maxRadius = 0.2f;
        float minRadius = 0.1f;
        float x = 0;
        float y = 0;
        float radius = 0;
        Layer layer = new Layer();
        FloatColor floatColor = FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.white));
        for (int i = 0; i<10; i++) {
            x = random.nextFloat()*(maxX - minX) + minX;
            y = random.nextFloat() *(maxY - minY) + minY;
            radius = random.nextFloat()*(maxRadius - minRadius) +minRadius;
            layer.addShape(generateCircle(x,y,z,radius, floatColor, 0.5f));
        }


        return layer;
    }
    private Layer initializeThirdLayer(float z) {
        Random random = new Random();
        float minX = -0.8f;
        float maxX  = 0.8f;
        float minY = -0.8f;
        float maxY = 0.8f;
        float maxRadius = 0.4f;
        float minRadius = 0.3f;
        float x = 0;
        float y = 0;
        float radius = 0;
        Layer layer = new Layer();
        FloatColor floatColor = FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.white));
        for (int i = 0; i<5; i++) {
            x = random.nextFloat()*(maxX - minX) + minX;
            y = random.nextFloat() *(maxY - minY) + minY;
            radius = random.nextFloat()*(maxRadius - minRadius) +minRadius;
            layer.addShape(generateCircle(x,y,z,radius, floatColor, 0.2f));
        }

        return layer;
    }
    private Circle generateCircle(float cx, float cy, float z, float radius, FloatColor floatColor, float alpha) {
        Vertex centerVertex = new Vertex(cx, cy, z);
        Circle circle = new Circle(centerVertex, 100, radius);
        floatColor.setAlpha(alpha);
        circle.setIsTransparent(true);
        circle.setColor(floatColor);
        return circle;
    }
}
