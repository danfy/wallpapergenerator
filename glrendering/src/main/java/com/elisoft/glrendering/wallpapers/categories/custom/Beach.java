package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 11/10/2015.
 */
public class Beach extends WallpaperPattern {
    private final Random mRandom;
    public Beach(Context context) {
        super(context, PatternsIds.BEACH_PATTERN);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float beachRatio = 0.567f;
        float centerX = 0.0f;
        float centerY = -0.5f;
        float width = 2.0f;
        float height = width/beachRatio;
        TexturedRectangle texturedRectangle = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTextResourceId(R.drawable.beach_sky, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);

        centerX = mRandom.nextFloat()*(0.6f -(-0.6f)) + (-0.6f);
        centerY = 0.8f;
        width = 0.3f;
        height = width;
        TexturedRectangle sun = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        sun.setIsClickable(true);
        sun.setTextResourceId(R.drawable.beach_sun, mContext);
        layer.addShape(sun);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        Vertex topLeftSeaVertex = new Vertex(-1.0f, 0.0f, 0.0f);
        Vertex bottomLeftSeaVertex = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightSeaVertex = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightSeaVertex = new Vertex(1.0f, 0.0f, 0.0f);

        TexturedRectangle sea = new TexturedRectangle(topLeftSeaVertex, bottomLeftSeaVertex, bottomRightSeaVertex, topRightSeaVertex);
        sea.setIsClickable(true);
        sea.setTextResourceId(R.drawable.beach_sea, mContext);
        Layer layer = new Layer();
        layer.addShape(sea);

        Vertex topLeftBeachVertex = new Vertex(-1.0f, -0.2f, 0.0f);
        Vertex bottomLeftBeachVertex = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightBeachVertex = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightBeachVertex = new Vertex(1.0f, -0.2f,0.0f);

        TexturedRectangle beach = new TexturedRectangle(topLeftBeachVertex, bottomLeftBeachVertex, bottomRightBeachVertex, topRightBeachVertex);
        beach.setIsClickable(true);
        beach.setTextResourceId(R.drawable.beach_beach, mContext);
        layer.addShape(beach);

        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float chaiseRatio = 1.57f;
        float centerX = 0.5f;
        float centerY = -0.2f;
        float width = 0.3f;
        float height = width/chaiseRatio;
        TexturedRectangle chaise = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        chaise.setIsClickable(true);
        chaise.setTextResourceId(R.drawable.beach_chaise, mContext);
        Layer layer = new Layer();
        layer.addShape(chaise);

        centerX = 0.55f;
        centerY = -0.3f;
        width = 0.06f;
        height= width;
        TexturedRectangle ball = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        ball.setIsClickable(true);
        ball.setTextResourceId(R.drawable.beach_ball, mContext);
        layer.addShape(ball);

        float palmRatio =0.914f;
        centerX = mRandom.nextFloat()*(-0.4f - (-0.7f)) + (-0.7f);
        centerY = mRandom.nextFloat() *(0.0f - (-0.1f)) +(-0.1f);
        width = 0.2f;
        height = width/palmRatio;
        TexturedRectangle palm = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        palm.setIsClickable(true);
        palm.setTextResourceId(R.drawable.beach_palm, mContext);
        layer.addShape(palm);

        float shipRatio = 0.85f;
        centerX = mRandom.nextFloat()*(0.3f - 0.0f) + (0.0f);
        centerY = mRandom.nextFloat()*(0.0f - (-0.05f)) + (-0.05f);
        width = 0.2f;
        height = width/shipRatio;
        TexturedRectangle ship = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        ship.setIsClickable(true);
        ship.setTextResourceId(R.drawable.beach_yacht, mContext);
        layer.addShape(ship);

        float viewingRatio = 0.27f;
        centerX = mRandom.nextFloat()*(-0.2f - (-0.5f)) +(-0.5f);
        centerY = -0.4f;
        width = 0.2f;
        height = width/viewingRatio;
        TexturedRectangle viewing = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        viewing.setIsClickable(true);
        viewing.setTextResourceId(R.drawable.beach_viewing, mContext);
        layer.addShape(viewing);

        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);

    }
}
