package com.elisoft.glrendering.wallpapers.categories.custom;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 9/28/2015.
 */
public class Hills extends WallpaperPattern {
    private final Random random;


    public Hills(Context context) {
        super(context, PatternsIds.HILLS_PATTERN);
        random = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        Vertex topLeftVertex = new Vertex(-1.0f, 1.0f, 0.0f);
        Vertex bottomLeftVertex = new Vertex(-1.0f, -1.0f , 0.0f);
        Vertex bottomRightVertex = new Vertex(1.0f, -1.0f, 0.0f);
        Vertex topRightVertex = new Vertex(1.0f, 1.0f, 0.0f);

        TexturedRectangle texturedRectangle = new TexturedRectangle(topLeftVertex, bottomLeftVertex, bottomRightVertex, topRightVertex);
        texturedRectangle.setIsClickable(false);
        texturedRectangle.setTextResourceId(R.drawable.hills_sky, mContext);
        Layer layer = new Layer();
        layer.addShape(texturedRectangle);


        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float mountainRatio = 2.3f;
        float hillBackRatio = 1.48f;

        float height = random.nextFloat()*(0.4f - (0.2f)) + 0.2f;
        float width = height*hillBackRatio;
        float centerX = random.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        Vertex centerFirstBackHill = new Vertex(centerX, -0.1f + height/2, 0f);
        TexturedRectangle firstBackHill = new TexturedRectangle(centerFirstBackHill, width, height,0);
        firstBackHill.setTextResourceId(R.drawable.hills_back_mountain_1, mContext);
        Layer layer = new Layer();
        layer.addShape(firstBackHill);

        height = random.nextFloat()*(0.4f - (0.2f)) + 0.2f;
        width = height*hillBackRatio;
        centerX = random.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        Vertex centerSecondBackHill = new Vertex(centerX, -0.1f + height/2, 0f);
        TexturedRectangle secondBackHill = new TexturedRectangle(centerSecondBackHill, width, height, 0);
        secondBackHill.setTextResourceId(R.drawable.hills_back_mountain_2, mContext);
        layer.addShape(secondBackHill);



        Vertex topLeftVertex = new Vertex(-1.0f, -0.1f, 0f);
        Vertex bottomLeftVertex = new Vertex(-1.0f, -1.0f, 0.0f);
        Vertex bottomRightVertex = new Vertex(1.0f, -1.0f, 0f);
        Vertex topRightVertex = new Vertex(1.0f, -0.1f, 0f);

        TexturedRectangle ground = new TexturedRectangle(topLeftVertex, bottomLeftVertex, bottomRightVertex, topRightVertex);
        ground.setTextResourceId(R.drawable.hills_ground, mContext);
        layer.addShape(ground);

        height = random.nextFloat()*(0.7f - 0.5f) + 0.5f;
        width = height*mountainRatio;
        centerX = random.nextFloat()*(0.7f - (-0.7f)) + (-0.7f);
        Vertex centerMountain = new Vertex(centerX, -0.1f + height/2, 0f);
        TexturedRectangle mountain = new TexturedRectangle(centerMountain, width, height, 0f);
        mountain.setTextResourceId(R.drawable.hills_mountain, mContext);
        layer.addShape(mountain);

        float grassRatio = 20.87f;

        height = 0.08f;
        width = height*grassRatio;

        centerX = random.nextFloat()*(0.3f - (-0.3f)) +(-0.3f);
        float centerY = random.nextFloat()*(-0.1f - (-0.2f)) + (-0.2f);
        Vertex grassCenter = new Vertex(centerX, centerY, 0);
        TexturedRectangle grass = new TexturedRectangle(grassCenter, width, height, 0);
        grass.setTextResourceId(R.drawable.hills_grass, mContext);
        layer.addShape(grass);


//        float roadRatio = 1.05f;
//        height = 1.0f;
//        width = height*roadRatio;
//        centerX = 0.0f;
//        Vertex centerRoad = new Vertex(centerX-0.3f, -height/2, 0);
//        TexturedRectangle road = new TexturedRectangle(centerRoad, width, height, 0);
//        road.setTextResourceId(R.drawable.hills_road, mContext);
//        layer.addShape(road);

        float firstHillRatio = 1.8f;
        width = random.nextFloat()*(0.7f - 0.4f) + 0.4f;
        height = width/firstHillRatio;
        centerX = -1.0f + width/2;
        centerY = -0.3f;
        Vertex firstHillCenter = new Vertex(centerX, centerY, 0f);
        TexturedRectangle firstHill = new TexturedRectangle(firstHillCenter, width, height,0);
        firstHill.setTextResourceId(R.drawable.hills_hill_1, mContext);
        layer.addShape(firstHill);

        float secondHillRatio =2.29f;
        width = random.nextFloat()*(0.8f - 0.5f) + 0.5f;
        height = width/secondHillRatio;
        centerX = 1.0f - width/2;
        centerY = -0.2f;
        Vertex secondHillCenter = new Vertex(centerX, centerY, 0f);
        TexturedRectangle secondHill = new TexturedRectangle(secondHillCenter, width, height,0);
        secondHill.setTextResourceId(R.drawable.hills_hill_2, mContext);
        layer.addShape(secondHill);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float sixTreeRatio = 0.42f;
        float height = random.nextFloat()*(0.2f - 0.1f) + 0.1f;
        float width = height*sixTreeRatio;
        float centerX = random.nextFloat()*(-0.3f - (-0.6f)) + (-0.6f);
        float centerY = random.nextFloat()*(-0.1f-(-0.3f)) + (-0.3f);
        Vertex firstTreeCenter = new Vertex(centerX, centerY, 0f);
        TexturedRectangle firstTree = new TexturedRectangle(firstTreeCenter, width, height, 0);
        firstTree.setTextResourceId(R.drawable.hills_tree_6, mContext);
        Layer layer = new Layer();
        layer.addShape(firstTree);

        float oneTreeRatio = 0.434f;
        height = random.nextFloat() *(0.2f - 0.1f) + 0.1f;
        width = height*oneTreeRatio;
        centerX = random.nextFloat()*(0.6f - (0.4f)) + 0.4f;
        centerY = random.nextFloat()*(-0.1f - (-0.3f)) + (-0.3f);
        Vertex secondTreeCenter = new Vertex(centerX, centerY,0);
        TexturedRectangle secondTree = new TexturedRectangle(secondTreeCenter, width, height,0);
        secondTree.setTextResourceId(R.drawable.hills_tree_1, mContext);
        layer.addShape(secondTree);


        height = random.nextFloat() *(0.2f - 0.1f) + 0.1f;
        width = height*oneTreeRatio;
        centerX = random.nextFloat()*(0.6f - (0.4f)) + 0.4f;
        centerY = random.nextFloat()*(-0.3f - (-0.6f)) + (-0.6f);
        Vertex thirdTreeCenter = new Vertex(centerX, centerY, 0);
        TexturedRectangle thirdTree = new TexturedRectangle(thirdTreeCenter, width, height, 0);
        thirdTree.setTextResourceId(R.drawable.hills_tree_2, mContext);
        layer.addShape(thirdTree);


        float threeTreeRatio = 0.426f;
        height = random.nextFloat()*(0.3f - 0.2f) + 0.2f;
        width = height*threeTreeRatio;
        centerX = random.nextFloat()*(0.5f -(-0.5f)) + (-0.5f);
        centerY = random.nextFloat() *(-0.2f - (-0.7f)) + (-0.7f);
        Vertex fourthTreeCenter = new Vertex(centerX, centerY, 0);
        TexturedRectangle fourthTree = new TexturedRectangle(fourthTreeCenter, width, height, 0);
        fourthTree.setTextResourceId(R.drawable.hills_tree_3, mContext);
        layer.addShape(fourthTree);

        float fourthTreeRatio = 0.43f;
        height = random.nextFloat()*(0.4f-0.3f) + 0.3f;
        width = height*fourthTreeRatio;
        centerX = random.nextFloat()*(0.5f - 0.0f) + 0.0f;
        centerY = random.nextFloat()*(-0.3f - (-0.6f)) + (-0.6f);
        Vertex fifthTreeCenter = new Vertex(centerX, centerY, 0);
        TexturedRectangle fifthTree = new TexturedRectangle(fifthTreeCenter, width, height,0);
        fifthTree.setTextResourceId(R.drawable.hills_tree_4, mContext);
        layer.addShape(fifthTree);

        float fifthTreeRatio = 0.423f;
        height = random.nextFloat()*(0.35f - 0.25f) +0.25f;
        width = height *fifthTreeRatio;
        centerX = random.nextFloat() *(0.5f - (-0.5f)) + (-0.5f);
        centerY = random.nextFloat()*(-0.2f - (-0.7f)) +(-0.7f);
        Vertex sixthTreeCenter = new Vertex(centerX, centerY, 0);
        TexturedRectangle sixTree = new TexturedRectangle(sixthTreeCenter, width, height, 0);
        sixTree.setTextResourceId(R.drawable.hills_tree_5, mContext);
        layer.addShape(sixTree);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
