package com.elisoft.glrendering.wallpapers.categories.newyearholiday;

import android.content.Context;

import com.elisoft.glrendering.R;
import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.TexturedRectangle;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;

/**
 * Created by yvorobey on 12/24/2015.
 */
public class ChristmasRoom extends WallpaperPattern {
    private final Random mRandom;
    private float absfloarTopCoordinate;
    private float windowCenterY;
    private float windowHeight;
    private float windowWidth;
    private float windowCenterX;
    public ChristmasRoom(Context context) {
        super(context, PatternsIds.CHRISTMAS_ROOM);
        mRandom = new Random();
    }

    @Override
    protected Layer initializeFirstLayer() {
        float backgroundRatio = 0.56f;
        float centerX = 0.0f;
        float centerY = 0.5f;
        float width = 2f;
        float height = width/backgroundRatio;

        TexturedRectangle background = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        background.setIsClickable(false);
        background.setTextResourceId(R.drawable.christmas_room_bg, mContext);
        Layer layer = new Layer();
        layer.addShape(background);

        float windowRatio = 0.82f;
        centerX = windowCenterX = mRandom.nextFloat()*(0.2f - (-0.2f)) + (-0.2f);
        centerY = windowCenterY =  0.2f;
        height = windowHeight = 1.4f;
        width = windowWidth= height*windowRatio;

        TexturedRectangle window = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        window.setIsClickable(true);
        window.setTextResourceId(R.drawable.christmas_room_window, mContext);
        layer.addShape(window);

        float floorRatio = 10.2f;
        centerX = 0.0f;
        width = 2f;
        height = width/floorRatio;
        centerY = -(1.0f - height/2);
        absfloarTopCoordinate = 1.0f - height;
        TexturedRectangle floor = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        floor.setIsClickable(true);
        floor.setTextResourceId(R.drawable.christmas_room_futer, mContext);
        layer.addShape(floor);
        return layer;
    }

    @Override
    protected Layer initializeSecondLayer() {
        float fireplaceRatio = 1.40f;
        float firePlaceCenterX = mRandom.nextFloat()*(0.6f - (0.3f)) + (0.3f);
        float firePlaceHeight = 0.6f;
        float firePlaceWidth = firePlaceHeight*fireplaceRatio;
        float firePlaceCenterY = -(absfloarTopCoordinate - firePlaceHeight/3);
        float socksCenterY = firePlaceCenterY+ firePlaceHeight/3;
        float firePlaceTopCoord = firePlaceCenterY + firePlaceHeight/2f;
        TexturedRectangle firePlace = new TexturedRectangle(new Vertex(firePlaceCenterX, firePlaceCenterY, 0.0f), firePlaceWidth, firePlaceHeight, 0);
        firePlace.setIsClickable(true);
        firePlace.setTextResourceId(R.drawable.christmas_room_fireplace_1, mContext);
        Layer layer = new Layer();
        layer.addShape(firePlace);

        float socksRatio = 0.65f;
        float centerY = socksCenterY;
        float centerX = firePlaceCenterX - firePlaceWidth/4f;
        float height = 0.2f;
        float width = height*socksRatio;
        TexturedRectangle firstSocks = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        firstSocks.setIsClickable(true);
        firstSocks.setTextResourceId(R.drawable.christmas_room_socks_1, mContext);
        layer.addShape(firstSocks);

        centerY = socksCenterY;
        centerX = firePlaceCenterX;
        height = 0.2f;
        width = height*socksRatio;
        TexturedRectangle secondSocks = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        secondSocks.setIsClickable(true);
        secondSocks.setTextResourceId(R.drawable.christmas_room_socks_1, mContext);
        layer.addShape(secondSocks);

        centerY = socksCenterY;
        centerX = firePlaceCenterX + firePlaceWidth/4f;
        height = 0.2f;
        width = height*socksRatio;
        TexturedRectangle thirdSocks = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        thirdSocks.setIsClickable(true);
        thirdSocks.setTextResourceId(R.drawable.christmas_room_socks_1, mContext);
        layer.addShape(thirdSocks);

        float redCandleRatio = 0.256f;
        centerX = windowCenterX - windowWidth/8f;
        centerY = windowCenterY - windowHeight/5.8f;
        height = 0.15f;
        width = height*redCandleRatio;
        TexturedRectangle redCandle = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width,height,0);
        redCandle.setIsClickable(true);
        redCandle.setTextResourceId(R.drawable.christmas_room_candle_red, mContext);
        layer.addShape(redCandle);

        float greenCandleRatio = 0.59f;
        centerX = windowCenterX-windowWidth/6f;
        centerY  = windowCenterY - windowHeight/5.45f;
        height = 0.12f;
        width = height*greenCandleRatio;
        TexturedRectangle greenCandle = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        greenCandle.setIsClickable(true);
        greenCandle.setTextResourceId(R.drawable.christmas_room_candle_green, mContext);
        layer.addShape(greenCandle);

        float giftViolRatio = 0.84f;
        centerX = firePlaceCenterX + firePlaceWidth/6;
        height = 0.2f;
        width = height*giftViolRatio;
        centerY = firePlaceTopCoord +height/2;
        TexturedRectangle violGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        violGift.setIsClickable(true);
        violGift.setTextResourceId(R.drawable.christmas_room_gift_viol, mContext);
        layer.addShape(violGift);

        float giftsecondRedRatio = 0.84f;
        centerX = firePlaceCenterX + firePlaceWidth/4;
        height = 0.2f;
        width = height*giftsecondRedRatio;
        centerY = firePlaceTopCoord +height/2;
        TexturedRectangle giftSecondRedGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        giftSecondRedGift.setIsClickable(true);
        giftSecondRedGift.setTextResourceId(R.drawable.christmas_room_gift_2red, mContext);
        layer.addShape(giftSecondRedGift);
        return layer;
    }

    @Override
    protected Layer initializeThirdLayer() {
        float orangleGiftRatio = 0.413f;
        float centerX = mRandom.nextFloat()*(0.0f - (-0.8f)) + (-0.8f);
        float height = 0.3f;
        float width = height*orangleGiftRatio;
        float centerY = -(absfloarTopCoordinate - height/2);
        TexturedRectangle orangeGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height, 0);
        orangeGift.setIsClickable(true);
        orangeGift.setTextResourceId(R.drawable.christmas_room_gift_orange, mContext);
        Layer layer = new Layer();
        layer.addShape(orangeGift);

        float redGiftRatio = 0.84f;
        centerX = mRandom.nextFloat() *(0.0f - (-0.8f)) +(-0.8f);
        height = 0.2f;
        width = height*redGiftRatio;
        centerY = -(absfloarTopCoordinate - height/2);
        TexturedRectangle redGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        redGift.setIsClickable(true);
        redGift.setTextResourceId(R.drawable.christmas_room_gift_red, mContext);
        layer.addShape(redGift);

        float greenGiftRatio = 0.412f;
        centerX = mRandom.nextFloat() *(0.0f - (-0.8f)) +(-0.8f);
        height = 0.3f;
        width = height*greenGiftRatio;
        centerY = -(absfloarTopCoordinate - height/2);
        TexturedRectangle greenGift = new TexturedRectangle(new Vertex(centerX, centerY, 0.0f), width, height,0);
        greenGift.setIsClickable(true);
        greenGift.setTextResourceId(R.drawable.christmas_room_gift_green, mContext);
        layer.addShape(greenGift);
        return layer;
    }

    @Override
    protected Layer initializeFourthLayer() {
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
}
