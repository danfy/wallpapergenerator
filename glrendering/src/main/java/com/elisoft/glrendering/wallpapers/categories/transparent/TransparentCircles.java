package com.elisoft.glrendering.wallpapers.categories.transparent;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Circle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.utils.colors.ColorRange;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;
import com.elisoft.glrendering.wallpapers.categories.WallpaperPattern;

import java.util.Random;



/**
 * Class represents transparent colored circles.
 */
public class TransparentCircles extends WallpaperPattern {

    private Random random;

    public TransparentCircles(Context context) {
        super(context, PatternsIds.TRANSPARENT_CIRCLES);

        random = new Random();
        palete.setColorRange(ColorRange.BASICS);

        FloatColor floatColor = palete.getSomeColor();

        setBackgroundColor(floatColor);
    }

    @Override
    protected Layer initializeFirstLayer() {

        return  generateFirstLayer(0.1f);
    }

    @Override
    protected Layer initializeSecondLayer() {

        return generateSecondLayer(0.2f);
    }

    @Override
    protected Layer initializeThirdLayer() {

        return generateThirdLayer(0.3f);
    }

    @Override
    protected Layer initializeFourthLayer() {

//        Circle circle = generateFourthCircle(0.4f, FloatColor.generateFloatColor(mContext.getResources().getColor(R.color.green_500)));
//        layer.addShape(circle);
        return new Layer();
    }

    @Override
    protected Layer initializeFifthLayer() {
        return new Layer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        super.drawScene(mMVPMatrix);
    }
    private Layer generateFirstLayer(float z) {
        Layer layer = new Layer();
        float minX = -1.0f;
        float maxX = 1.0f;
        float minY = 0.7f;
        float maxY = 1.0f;
        float maxRadius = 1.0f;
        float minRadius = 0.6f;


        float centerX =random.nextFloat()*(maxX- minX) + minX;
        float centerY = random.nextFloat()*(maxY -minY) + minY;
        float radius = random.nextFloat()*(maxRadius - minRadius) + minRadius;
        Vertex centerVertex = new Vertex(centerX, centerY, z);
        Circle circle = new Circle(centerVertex, 180, radius);
        FloatColor floatColor = palete.getSomeColor();
        floatColor.setAlpha(0.7f);
        circle.setColor(floatColor);
        layer.addShape(circle);

        if(centerX >0) {
            maxX = -centerX/2;
        } else {
            minX =centerX/2;
        }
        maxY = -0.7f;
        minY = -1.0f;
        float secCircleX = random.nextFloat()*(maxX - minX) + minX;
        float secCircleY = random.nextFloat()*(maxY - minY) + minY;
        float secCircleRadius = random.nextFloat()*(maxRadius - minRadius) + minRadius;
        Vertex secCenterVertex = new Vertex(secCircleX, secCircleY, z);
        Circle secCircle = new Circle(secCenterVertex, 180, secCircleRadius);

        FloatColor secFloatColor = palete.getSomeColor();;
        secFloatColor.setAlpha(0.7f);
        secCircle.setColor(secFloatColor);
        layer.addShape(secCircle);
        return layer;
    }

    private Layer generateSecondLayer(float z) {
        Layer layer = new Layer();
        float maxY = 1.0f;

        float minY = -1.0f;
        float minRadius = 0.5f;
        float maxRadius = 0.9f;

        float firstCircleX = -1.0f;
        float firstCircleY = random.nextFloat()*(maxY - minY) + minY;
        float firstCircleRadius = random.nextFloat()*(maxRadius - minRadius) + minRadius;

        Vertex firstCircleVertex = new Vertex(firstCircleX, firstCircleY, z);
        Circle firstCircle = new Circle(firstCircleVertex, 180, firstCircleRadius);
        FloatColor firstFloatColor =palete.getSomeColor();;
        firstFloatColor.setAlpha(0.7f);
        firstCircle.setColor(firstFloatColor);
        firstCircle.setIsTransparent(true);
        layer.addShape(firstCircle);

        float secondCircleX = 1.0f;
        float secondCircleY = random.nextFloat()*(maxY - minY) + minY;
        float secondCircleRadius = random.nextFloat()*(maxRadius - minRadius) + minRadius;

        Vertex secondCircleVertex =new Vertex(secondCircleX, secondCircleY, z);
        Circle secondCircle = new Circle(secondCircleVertex, 180, secondCircleRadius);
        FloatColor secondFloatColor = palete.getSomeColor();;
        secondFloatColor.setAlpha(0.7f);
        secondCircle.setColor(secondFloatColor);
        secondCircle.setIsTransparent(true);
        layer.addShape(secondCircle);

        return layer;
    }
    private Layer generateThirdLayer(float z) {
        Layer layer = new Layer();
        float maxX = 0.5f;
        float minX =-0.5f;
        float maxY = 0.7f;
        float minY = 0.0f;
        float maxRadius = 0.7f;
        float minRadius = 0.4f;

        float firstX = random.nextFloat()*(maxX - minX) +minX;
        float firstY = random.nextFloat()*(maxY - minY) +minY;
        float firstRadius = random.nextFloat()*(maxRadius - minRadius) + minRadius;

        Vertex firstVertex = new Vertex(firstX, firstY, z);
        Circle firstCircle = new Circle(firstVertex, 180, firstRadius);
        FloatColor firstColor = palete.getSomeColor();;
        firstColor.setAlpha(0.7f);
        firstCircle.setIsTransparent(true);
        firstCircle.setColor(firstColor);
        layer.addShape(firstCircle);

        maxY = 0.0f;
        minY = -0.7f;

        float secondX = random.nextFloat()*(maxX - minX) + minX;
        float secondY = random.nextFloat()*(maxY - minY) +minY;
        float secondRadius = random.nextFloat()*(maxRadius - minRadius) + minRadius;

        Vertex secondVertex = new Vertex(secondX, secondY, z);
        FloatColor secondColor =palete.getSomeColor();;
        Circle secondCircle = new Circle(secondVertex, 180, secondRadius);
        secondColor.setAlpha(0.7f);
        secondCircle.setColor(secondColor);
        secondCircle.setIsTransparent(true);
        layer.addShape(secondCircle);



        return layer;
    }
}
