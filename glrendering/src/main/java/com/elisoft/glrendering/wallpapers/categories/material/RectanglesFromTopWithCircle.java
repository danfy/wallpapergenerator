package com.elisoft.glrendering.wallpapers.categories.material;

import android.content.Context;

import com.elisoft.glrendering.components.Layer;
import com.elisoft.glrendering.shapes.Circle;
import com.elisoft.glrendering.shapes.ShadowCircle;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.Vertex;
import com.elisoft.glrendering.wallpapers.categories.PatternsIds;

import java.util.Random;


/**
 * Created by yvorobey on 6/18/2015.
 */
public class RectanglesFromTopWithCircle extends RectanglesFromTop {
    private static final float MAX_RADIUS = 0.7f;
    private static final float MIN_RADIUS = 0.4f;
    private static final float MAX_X = 0.8f;
    private static final float MIN_X = -0.8f;

    public RectanglesFromTopWithCircle(Context context) {
        super(context, PatternsIds.RECTANGLES_FROM_TOP_WITH_CIRCLE_PATTERN);
    }

    @Override
    protected Layer initializeFifthLayer() {
        Layer fifthLayer = new Layer();
        Circle circle = generateCircle(0.5f, getColor());
        ShadowCircle shadowCircle= circle.generateShadow();
        fifthLayer.addShape(shadowCircle);
        fifthLayer.addShape(circle);
        return fifthLayer;

    }
    private Circle generateCircle(float z, FloatColor floatColor) {
        Random random = new Random();
        float cx = random.nextFloat()*(MAX_X - (MIN_X)) + (MIN_X);
        float cy = 1.0f;
        float radius = random.nextFloat()*(MAX_RADIUS - (MIN_RADIUS)) + MIN_RADIUS;
        Vertex centerVertex = new Vertex(cx, cy, z);
        Circle shape = new Circle(centerVertex, 100, radius);
        shape.setColor(floatColor);
        return shape;
    }
}
