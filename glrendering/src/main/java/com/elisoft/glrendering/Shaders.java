package com.elisoft.glrendering;

/**
 * Class represent different shaders for rendering
 */
public final class Shaders {
        public static final String POSITION_ATTRIBUTE = "vPosition";
        public static final String COLOR_UNIFORM = "u_Color";
        public static final String PROJECTION_UNIFORM = "u_projection";
        public static final String COLOR_ATTRIBUTE = "vColor";
        public static final String RADIUS_UNIFORM = "u_radius";
        public static final String LIGHT_UNIFORM = "u_lightPosition";
        public static final String FIRST_COLOR_UNIFORM  = "u_firstColor";
        public static final String SECOND_COLOR_UNIFORM = "u_secondColor";
        public static final String CENTER_UNIFORM = "u_center";
        public static final String RESOLUTION_UNIFORM = "u_resolution";
        public static final String POIN_SIZE_UNIFORM = "u_pointSize";
        public static final String TEXTURE_ATTRIBUTE = "aTexcoord";
        public static final String UNIFORM_SAMPLER = "tex_sampler";

        public static final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader


                    "attribute vec4 vPosition;" +
                            "uniform mat4 u_projection;" +
                    "void main() {" +
                    // the matrix must be included as a modifier of gl_Position
                    // Note that the uMVPMatrix factor *must be first* in order
                    // for the matrix multiplication product to be correct.

                    "  gl_Position = u_projection * vPosition;" +
                    "}";
    public static final String fragmentShaderCode =
            "precision mediump float;" +

                    "uniform vec4 u_Color;" +
                    "void main() {" +
                    "  gl_FragColor = u_Color;" +

                    "}";

        public static final String vertexShaderCodeShadow =
                // This matrix member variable provides a hook to manipulate
                // the coordinates of the objects that use this vertex shader
            "uniform mat4 u_projection;" +
                "attribute vec4 vPosition;" +
                        "attribute vec4 vColor;" +
                        "varying vec4 aColor;" +
                        "void main() {" +
                        // The matrix must be included as a modifier of gl_Position.
                        // Note that the uMVPMatrix factor *must be first* in order
                        // for the matrix multiplication product to be correct.
                        " aColor = vColor;"+
                        "  gl_Position =  u_projection * vPosition;" +
                        "}";
        public static final String fragmentShaderCodeShadow =
                "precision mediump float;" +
                        "varying vec4 aColor;"+

                        "void main() {" +

                        "  gl_FragColor = vec4(aColor.rgb, aColor.a*1.5);" +

                        "}";

    public static final String vertexShaderCodeCircleShadow =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader


            "attribute vec2 vPosition;" +
                    "attribute vec2 a_TexCoordinate;" +
                    "varying vec2 v_TexCoordinate;" +
                    "uniform mat4 u_projection;" +
                    "void main() {" +
                    // the matrix must be included as a modifier of gl_Position
                    // Note that the uMVPMatrix factor *must be first* in order
                    // for the matrix multiplication product to be correct.
                    "v_TexCoordinate = a_TexCoordinate;" +
                    "  gl_Position = u_projection * vec4(vPosition.x, vPosition.y, 0.0, 1.0);" +
                    "}";

        public static final String fragmentShadowCircle =
                "precision mediump float;" +
                        "uniform vec2 u_lightPosition;" +
                        "varying vec2 v_TexCoordinate;" +
                        "uniform float u_radius;" +
                        "void main() {" +
                        "vec2 center = vec2(0.5, 0.5);" +
                        "float distance  = length(v_TexCoordinate - center);" +

                        "    gl_FragColor = vec4(0.0, 0.0, 0.0,(0.5 -distance)*5.0);"+
    "}";

        public static final String v_radial_gradient =
                "attribute vec4 vPosition; " +

                        "uniform mat4 u_projection;" +
                        "void main() {" +

                        "gl_Position = u_projection * vPosition;"+
                        "}";
        public static final String f_radial_gradient =
                "precision mediump float;" +

                        "uniform vec4 u_firstColor;"+
                        "uniform vec4 u_secondColor;" +
                        "uniform float u_radius;" +
                        "uniform vec2 u_resolution;"+
                        "uniform vec2 u_center;" +
                        "void main() {" +
                        "vec2 center = u_center;" +
                        "vec2 position = (gl_FragCoord.xy/u_resolution.xy) - u_center;" +
                        "position.x *= u_resolution.x/u_resolution.y;" +
                        "float len = length(position);"+

                        "gl_FragColor = vec4(u_firstColor.rgb*(1.0-smoothstep(0.0,u_radius,len)) + u_secondColor.rgb,1.0);" +
//                        "float x = v_TexCoordinate.x;" +
//                        "float y = v_TexCoordinate.y;" +
//                        "float calculate = sqrt((x - u_center.x)*(x-u_center.x) + (y - u_center.y)*(y-u_center.y));" +
////                        "gl_FragColor = vec4(calculate, calculate, calculate, 1.0f);" +
//                        "gl_FragColor = vec4(x, x, x, 1.0f);"+
                        "}";

        public static final String v_lin_horiz_gradient =
                "attribute vec4 vPosition; " +
                        "uniform mat4 u_projection;" +
                        "void main() {" +
                        "gl_Position = u_projection * vPosition;"+
                        "}";
        public static final String f_lin_horiz_gradient =
                "precision mediump float;" +

                        "uniform vec4 u_firstColor;"+
                        "uniform vec4 u_secondColor;" +

                        "uniform vec2 u_resolution;"+

                        "void main() {" +

                        "vec2 position = gl_FragCoord.xy/u_resolution.xy;" +
                        "float len = position.x;"+
                        "gl_FragColor = vec4(u_firstColor.rgb*(1.0-len) + u_secondColor.rgb,1.0);" +
                        "}";

    public static final String v_lin_vert_gradient =
            "attribute vec4 vPosition; " +
                    "uniform mat4 u_projection;" +
                    "void main() {" +
                    "gl_Position = u_projection * vPosition;"+
                    "}";
    public static final String f_lin_vert_gradient =
            "precision mediump float;" +

                    "uniform vec4 u_firstColor;"+
                    "uniform vec4 u_secondColor;" +

                    "uniform vec2 u_resolution;"+

                    "void main() {" +

                    "vec2 position = gl_FragCoord.xy/u_resolution.xy;" +
                    "float len = position.y;"+
                    "gl_FragColor = vec4(u_firstColor.rgb*(1.0-len) + u_secondColor.rgb,1.0);" +
                    "}";

    public static final String v_stars =
            "attribute vec4 vPosition;" +
                    "uniform mat4 u_projection;" +
                    "uniform float u_pointSize;" +
                    "void main() {" +
                    "gl_PointSize = 3.0;"+
                    "gl_Position = u_projection * vPosition;" +
                    "}";
    public static final String f_stars =
            "precision mediump float;" +
                    "uniform vec4 u_Color;" +
                    "void main() { " +
                    "gl_FragColor = u_Color;"+
                    "}";

        public static final String VERTEX_SHADER_TEXTURE =
                "attribute vec4 vPosition;" +
                        "attribute vec2 aTexcoord;" +
                        "varying vec2 vTexcoord;" +
                        "uniform mat4 u_projection;" +
                        "void main() {" +
                        "  vTexcoord = aTexcoord;" +
                        "  gl_Position = u_projection * vPosition;" +

                        "}";
        public static final String FRAGMENT_SHADER_TEXTURE =
                "precision mediump float;" +
                        "uniform sampler2D tex_sampler;" +
                        "varying vec2 vTexcoord;" +
                        "void main() {" +
                        "  gl_FragColor = texture2D(tex_sampler, vTexcoord);" +
                        "}";

}
