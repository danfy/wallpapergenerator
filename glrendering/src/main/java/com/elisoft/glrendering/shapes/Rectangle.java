package com.elisoft.glrendering.shapes;




import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.ShaderProgram;
import com.elisoft.glrendering.utils.Vertex;


/**
 * Class represents rectangle shape
 */
public class Rectangle extends AbstractShape {
    private static final long serialVersionUID = 647261931L;
    protected Vertex centerPointRectangle;
    protected Vertex topLeftPoint;
    protected Vertex bottomLeftPoint;
    protected Vertex bottomRightPoint;
    protected Vertex topRightPoint;
    protected boolean isFlat = true;
    protected float angle = 0;
    private ShadowRectangle shadowRectangle;
    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);

        initializeShaderProgram();
    }

    @Override
    public void movePosition(float dx, float dy, float dz) {
        mVertexBuffer.clear();
        shapeCoordinates[0] +=dx;
        shapeCoordinates[1] +=dy;
        shapeCoordinates[3] +=dx;
        shapeCoordinates[4] +=dy;
        shapeCoordinates[6] +=dx;
        shapeCoordinates[7] +=dy;
        shapeCoordinates[9] +=dx;
        shapeCoordinates[10] +=dy;
        shapeCoordinates[12] +=dx;
        shapeCoordinates[13] +=dy;
        shapeCoordinates[15] +=dx;
        shapeCoordinates[16] +=dy;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
        if(shadowRectangle!=null) {
            shadowRectangle.moveShadowRectangle(dx, dy, dz);
        }
    }

    @Override
    public void scale(float scale) {
        mVertexBuffer.clear();
        shapeCoordinates[0] *=scale;
        shapeCoordinates[1] *=scale;
        shapeCoordinates[3] *=scale;
        shapeCoordinates[4] *=scale;
        shapeCoordinates[6] *=scale;
        shapeCoordinates[7] *=scale;
        shapeCoordinates[9] *=scale;
        shapeCoordinates[10] *=scale;
        shapeCoordinates[12] *=scale;
        shapeCoordinates[13] *=scale;
        shapeCoordinates[15] *=scale;
        shapeCoordinates[16] *=scale;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
        if(shadowRectangle!=null) {
            shadowRectangle.scaleShadowRectangle(scale);
        }
    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {
        float Bx = shapeCoordinates[12] - shapeCoordinates[3];
        float By = shapeCoordinates[13] - shapeCoordinates[4];
        float Cx = shapeCoordinates[6]-shapeCoordinates[3];
        float Cy = shapeCoordinates[7] - shapeCoordinates[4];

        float Px = x - shapeCoordinates[3];
        float Py = y - shapeCoordinates[4];
        float m = (Px * By - Bx*Py)/(Cx*By - Bx*Cy);
        if(m>=0 && m<=1) {
            float l = (Px - m*Cx)/Bx;
            if(l>0 && (m+l)<=1) {
                return true;
            }
        }
        Bx = shapeCoordinates[12] - shapeCoordinates[6];
        By = shapeCoordinates[13] - shapeCoordinates[7];
        float Dx = shapeCoordinates[9] - shapeCoordinates[6];
        float Dy = shapeCoordinates[10] - shapeCoordinates[7];
        Px = x - shapeCoordinates[6];
        Py = y - shapeCoordinates[7];
        m = (Px *By - Bx*Py)/(Dx*By - Bx*Dy);
        if(m>=0 && m<=1) {
            float l = (Px - m*Dx)/Bx;
            if(l>0 && (m+l)<=1) {
                return true;
            }
        }

        return false;
    }

    /**
     * Constructor for rectangle width corners degrees 90 with arguments: center point of rectangle, width, height and angle to rotate.
     * @param centerVertex - Center vertex of rectangle.
     * @param width - Width of rectangle.
     * @param height - Height of rectangle.
     * @param angle - Angle to rotate rectangle (etc. 0, 45, 135) in degrees.
     */
    public Rectangle(Vertex centerVertex, float width, float height, float angle, boolean isFlat) {
        this.isFlat = isFlat;
        shapeCoordinates = new float[6*COORDINATE_PER_VERTEX];
        centerPointRectangle = centerVertex;
        initializeRectVertexByAngle(width, height, angle);
        populateCoordinateArray();
        initializeVertexBuffer();
        initializeShaderProgram();
    }

    /**
     * Constructor for rectangle which constructs by 4 points: top-left, bottom-left, bottom-right
     * top-right points.
     * @param topLeftPoint - Coordinate of top-left corner of rectangle.
     * @param bottomLeftPoint -Coordinate of bottom-left corner of rectangle.
     * @param bottomRightPoint -Coordinate of bottom-right corner of rectangle.
     * @param topRightPoint - Coordinate of top-right corner of rectangle.
     */
    public Rectangle(Vertex topLeftPoint, Vertex bottomLeftPoint, Vertex bottomRightPoint, Vertex topRightPoint) {
        shapeCoordinates = new float[6*COORDINATE_PER_VERTEX];
        this.topLeftPoint = topLeftPoint;
        this.bottomLeftPoint = bottomLeftPoint;
        this.bottomRightPoint = bottomRightPoint;
        this.topRightPoint = topRightPoint;
        calculateCenterVertex();
        populateCoordinateArray();
        initializeVertexBuffer();
        initializeShaderProgram();

    }


    private void initializeRectVertexByAngle(float widthRect, float heightRect, float angle) {

        float dx = widthRect/2;
        float dy = heightRect/2;
        float z = centerPointRectangle.getCoordinateZ();

            topLeftPoint = new Vertex(centerPointRectangle.getCoordinateX() - dx, centerPointRectangle.getCoordinateY() + dy, z);
            bottomLeftPoint = new Vertex(centerPointRectangle.getCoordinateX() - dx, centerPointRectangle.getCoordinateY() - dy, z);
            bottomRightPoint = new Vertex(centerPointRectangle.getCoordinateX() + dx, centerPointRectangle.getCoordinateY() - dy, z);
            topRightPoint = new Vertex(centerPointRectangle.getCoordinateX() + dx, centerPointRectangle.getCoordinateY() + dy, z);
       if(angle !=0){
            float[] rotateMatrix = new float[16];
            Matrix.setIdentityM(rotateMatrix,0);
            rotateMatrix[0] = (float) Math.cos(angle);
            rotateMatrix[1] = (float) -Math.sin(angle);
            rotateMatrix[4] = (float) Math.sin(angle);
            rotateMatrix[5] = (float) Math.cos(angle);

            float[] topLeftCoordinates = new float[4];
            initialize4floatVector(topLeftCoordinates, topLeftPoint);

            Matrix.multiplyMV(topLeftCoordinates,0, rotateMatrix, 0, topLeftCoordinates, 0);
            topLeftPoint = new Vertex(topLeftCoordinates);
            float[] bottomLeftCoordinates = new float[4];
            initialize4floatVector(bottomLeftCoordinates, bottomLeftPoint);
            Matrix.multiplyMV(bottomLeftCoordinates, 0, rotateMatrix, 0, bottomLeftCoordinates, 0);
            bottomLeftPoint = new Vertex(bottomLeftCoordinates);
            float[] bottomRightCoordinates = new float[4];
            initialize4floatVector(bottomRightCoordinates, bottomRightPoint);
            Matrix.multiplyMV(bottomRightCoordinates, 0,rotateMatrix, 0, bottomRightCoordinates, 0);
            bottomRightPoint = new Vertex(bottomRightCoordinates);
            float[] topRightCoordinates = new float[4];
            initialize4floatVector(topRightCoordinates, topRightPoint);
            Matrix.multiplyMV(topRightCoordinates, 0,rotateMatrix,0,topRightCoordinates, 0);
            topRightPoint = new Vertex(topRightCoordinates);
            if(isFlat) {
                float[] centerPointCoordinate = new float[4];
                initialize4floatVector(centerPointCoordinate, centerPointRectangle);
                Matrix.multiplyMV(centerPointCoordinate, 0, rotateMatrix, 0, centerPointCoordinate, 0);
                centerPointRectangle = new Vertex(centerPointCoordinate);
            }
        }

        this.angle = angle;


    }
    private void initialize4floatVector(float[] coordiantes, Vertex vertex) {
        coordiantes[0] = vertex.getCoordinateX();
        coordiantes[1] = vertex.getCoordinateY();
        coordiantes[2] = vertex.getCoordinateZ();
        coordiantes[3] =0;

    }
    @Override
    protected void populateCoordinateArray() {
        System.arraycopy(centerPointRectangle.getCoordinatesFloatArray(), 0, shapeCoordinates,0, COORDINATE_PER_VERTEX);
        System.arraycopy(topLeftPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
        System.arraycopy(bottomLeftPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, 2 * COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
        System.arraycopy(bottomRightPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, 3 * COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
        System.arraycopy(topRightPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, 4 * COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
        System.arraycopy(topLeftPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, 5 * COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
    }

    @Override
    protected void calculateCenterVertex() {
        centerPointRectangle = new Vertex();
        float x1 =  topLeftPoint.getCoordinateX();
        float y1 =  topLeftPoint.getCoordinateY();

        float x2 = bottomLeftPoint.getCoordinateX();
        float y2 = bottomLeftPoint.getCoordinateY();

        float x3 = bottomRightPoint.getCoordinateX();
        float y3 = bottomRightPoint.getCoordinateY();

        float x4 = topRightPoint.getCoordinateX();
        float y4 = topRightPoint.getCoordinateY();

        float centerX = (x4 * y2 *(x3-x1) - x2* y4*(x3-x1) - y1 *(x3-x1) + x1*y3 - x1*y1)/(y3 - y1 - y4 *(x3-x1) + y2*(x3-x1));
        float centerY = (centerX * y4 - x2*y4 - centerX * y2 + x2*y2 + x4*y2 - x2*y2)/(x4-x2);
        centerPointRectangle.setCoordinateX(centerX);
        centerPointRectangle.setCoordinateY(centerY);
        //TODO Need review below initialization
        centerPointRectangle.setCoordinateZ(topLeftPoint.getCoordinateZ());
        ///////////////////////////////////////

    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram program = new ShaderProgram(Shaders.vertexShaderCode, Shaders.fragmentShaderCode); // Just for test
        mProgram = program.getProgram();
    }

    @Override
    public void draw() {
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);

        applyColorUniform();

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 6);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);

        applyColorUniform();
//        if(isRotate) {
//           Matrix.rotateM(rotateMatrix ,0, mMVPMatrix, 0, angle, 0.0f, 0.0f, 1.0f);
//            applyProjectionUniform(rotateMatrix);
//        } else {
            applyProjectionUniform(mMVPMatrix);
//        }
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0,6);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }
    public ShadowRectangle generateShadow() {
        shadowRectangle = new ShadowRectangle(topLeftPoint, bottomLeftPoint, bottomRightPoint, topRightPoint);
        return shadowRectangle;
    }

}
