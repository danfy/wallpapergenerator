package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;

import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.FloatColor;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;



/**
 * Abstract class represents abstract shape for render in OpenGL
 */
public abstract class AbstractShape implements Serializable{
    private int mId; //Id of shape
    protected boolean isShadow = false; //True if shape is shadow, false otherwise. Need for enabled or disabled glBlendFunc().
    protected boolean isTransparent = false;
    protected static final int COORDINATE_PER_VERTEX = 3;
    protected static final int VERTEX_STRIDE = COORDINATE_PER_VERTEX *4;
    protected static final int BYTES_PER_FLOAT = 4;
    protected static final int BYTES_PER_SHORT = 2;
    protected transient FloatBuffer mVertexBuffer;
    protected float[] color = new float[4];
    protected float[] shapeCoordinates;
    protected int mPositionHandler;
    protected int mProgram;

    public float[] getShapeCoordinates() {
        return shapeCoordinates;
    }

    /**
     * Method for populate coordinate array. Must be call before initialize mVertexBuffer.
     */
    protected abstract void populateCoordinateArray();
    protected abstract void calculateCenterVertex();

    /**
     * Method initialize FloatBuffer for vertex values
     */
    protected  void initializeVertexBuffer() {
        ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect(shapeCoordinates.length*BYTES_PER_FLOAT);
        vertexByteBuffer.order(ByteOrder.nativeOrder());
        mVertexBuffer = vertexByteBuffer.asFloatBuffer();
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
    }
    protected abstract void initializeShaderProgram();
    public abstract void draw();
    public abstract void draw(float[] mMVPMatrix);

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        this.mId = id;
    }
    public void setColor(FloatColor floatColor) {
        color[0] = floatColor.getRed();
        color[1] = floatColor.getGreen();
        color[2] = floatColor.getBlue();
        color[3] = floatColor.getAlpha();
    }
    public void applyColorUniform() {
        int colorHandler = GLES20.glGetUniformLocation(mProgram, Shaders.COLOR_UNIFORM);
        GLES20.glUniform4fv(colorHandler, 1, color, 0);
    }
    public void applyProjectionUniform(float[] mMVPMatrix) {
        int projectionHandler = GLES20.glGetUniformLocation(mProgram, Shaders.PROJECTION_UNIFORM);
        GLES20.glUniformMatrix4fv(projectionHandler, 1, false, mMVPMatrix, 0);
    }

    public boolean isShadow() {
        return isShadow;
    }

    public boolean isTransparent() {
        return isTransparent;
    }

    public void setIsTransparent(boolean isTransparent) {
        this.isTransparent = isTransparent;
    }

    public void initializeShapeFromFile(Context context) {
        initializeVertexBuffer();
    }

    public abstract void movePosition(float dx, float dy, float dz);

    public abstract void scale(float scale);

    public abstract boolean checkIfPointInShape(float x, float y);
}
