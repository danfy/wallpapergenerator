package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.ShaderProgram;
import com.elisoft.glrendering.utils.Vertex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


/**
 * Class represents shadow of triangle shape.
 */
public class ShadowTriangle extends Triangle {
    private static final long serialVersionUID = 4231245562L;
    protected float[] scale = {1f,0,0,0,
            0,1f,0,0,
            0,0,1f,0f,
            0.0f,0.0f,0,1f};

    protected float[] identity = new float[16];
    protected float[] view = new float[16];

    protected float[] center = new float[4];
    protected transient FloatBuffer colorAttributeBuffer;
    private boolean isInitialized = false;
    protected float color[] = { 0.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.0f,0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,


            0.0f, 0.0f,0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,


            0.0f, 0.0f,0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,


    };

    @Override
    public void initializeShapeFromFile(Context context) {

        super.initializeShapeFromFile(context);
        initializeShadowColorBuffer();
    }
    public void scaleShadow(float scale) {
        mVertexBuffer.clear();
        shapeCoordinates[0] *=scale;
        shapeCoordinates[3] *=scale;
        shapeCoordinates[6]*=scale;
        shapeCoordinates[9] *=scale;
        shapeCoordinates[1]*=scale;
        shapeCoordinates[4]*=scale;
        shapeCoordinates[7] *=scale;
        shapeCoordinates[10] *=scale;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
    }
    public void movePositionShadow(float dx, float dy, float dz) {
        mVertexBuffer.clear();
        shapeCoordinates[0] +=dx;
        shapeCoordinates[3] +=dx;
        shapeCoordinates[6] +=dx;
        shapeCoordinates[9] +=dx;
        shapeCoordinates[1] +=dy;
        shapeCoordinates[4] +=dy;
        shapeCoordinates[7] +=dy;
        shapeCoordinates[10] +=dy;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
    }

    public ShadowTriangle(Vertex topPoint, Vertex bottomLeftPoint, Vertex bottomRightPoint) {
        super(topPoint, bottomLeftPoint, bottomRightPoint);
        isShadow = true;
        initializeShadowColorBuffer();
    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram shaderProgram = new ShaderProgram(Shaders.vertexShaderCodeShadow, Shaders.fragmentShaderCodeShadow);
        mProgram = shaderProgram.getProgram();
    }

    @Override
    protected void initializeDrawListBuffer() {
        super.initializeDrawListBuffer();
    }
    private void initializeShadowColorBuffer() {
        ByteBuffer colorByteBuffer = ByteBuffer.allocateDirect(color.length*4);
        colorByteBuffer.order(ByteOrder.nativeOrder());
        colorAttributeBuffer = colorByteBuffer.asFloatBuffer();
        colorAttributeBuffer.put(color);
        colorAttributeBuffer.position(0);

    }

    @Override
    protected void initializeVertexBuffer() {
        super.initializeVertexBuffer();
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        if(!isInitialized) {
            System.arraycopy(mMVPMatrix, 0, identity, 0, mMVPMatrix.length);
            float dx = -centerPointTriangle.getCoordinateX();
            float dy = -centerPointTriangle.getCoordinateY();
            Matrix.translateM(identity, 0, -dx, -dy, 0.0f);
            Matrix.scaleM(identity, 0, 1.08f, 1.08f, 1f);
            Matrix.translateM(identity, 0, dx, dy, 0.0f);
            isInitialized = true;
        }
//        Matrix.multiplyMM(identity, 0, identity, 0, scale, 0);
//        Matrix.translateM(identity, 0, centerPointTriangle.getCoordinateX(), centerPointTriangle.getCoordinateY(), 0.0f);


        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);

        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
//        applyColorUniform();
        int colorHandler = GLES20.glGetAttribLocation(mProgram, Shaders.COLOR_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(colorHandler);
        GLES20.glVertexAttribPointer(colorHandler,4, GLES20.GL_FLOAT, false, 0, colorAttributeBuffer);
        applyProjectionUniform(identity);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }

    @Override
    public void movePosition(float dx, float dy, float dz) {
        //Do nothing
    }

    @Override
    public void scale(float scale) {
        //Do nothing
    }
}
