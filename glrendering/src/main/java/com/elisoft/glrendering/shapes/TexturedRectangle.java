package com.elisoft.glrendering.shapes;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;


import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.ShaderProgram;
import com.elisoft.glrendering.utils.Vertex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


/**
 * Created by yvorobey on 9/15/2015.
 */
public class TexturedRectangle extends AbstractShape {
    private static final String TAG = TexturedRectangle.class.getSimpleName();
    private static final long serialVersionUID = 641004242L;
    protected transient FloatBuffer textureBuffer;
    protected Vertex centerPointRectangle;
    protected Vertex topLeftPoint;
    protected Vertex bottomLeftPoint;
    protected Vertex bottomRightPoint;
    protected Vertex topRightPoint;
    protected float[] rotateMatrix = new float[16];
    protected boolean isRotate = false;
    protected float angle = 0;
    private int textResourceId;
    private int[] mTextures = new int[2];
    private transient Context mContext;
    protected int mTexCoordHandle;
    protected int mTexSamplerHandle;
    private String tag;
    private boolean initialized = false;
    private boolean isClickable = true;

    private static final float[] TEX_VERTICES = {
            0.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f
    };


    public TexturedRectangle (Vertex centerVertex , float width, float height, float angle) {
        shapeCoordinates = new float[4*COORDINATE_PER_VERTEX];
        centerPointRectangle = centerVertex;
        initializeRectVertexByAngle(width, height, angle);
        populateCoordinateArray();
        initializeVertexBuffer();
        initializeShaderProgram();
        initializeTextureBuffer();
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public TexturedRectangle (Vertex topLeftPoint, Vertex bottomLeftPoint, Vertex bottomRightPoint, Vertex topRightPoint) {
        shapeCoordinates = new float[4 * COORDINATE_PER_VERTEX];
        this.topLeftPoint = topLeftPoint;
        this.bottomLeftPoint = bottomLeftPoint;
        this.bottomRightPoint = bottomRightPoint;
        this.topRightPoint = topRightPoint;
        calculateCenterVertex();
        populateCoordinateArray();
        initializeVertexBuffer();
        initializeTextureBuffer();
        initializeShaderProgram();

    }

    private void initializeRectVertexByAngle(float widthRect, float heightRect, float angle) {
        float dx = widthRect/2;
        float dy = heightRect/2;
        float z = centerPointRectangle.getCoordinateZ();
        topLeftPoint = new Vertex(centerPointRectangle.getCoordinateX() - dx, centerPointRectangle.getCoordinateY() + dy, z);
        bottomLeftPoint = new Vertex(centerPointRectangle.getCoordinateX()-dx, centerPointRectangle.getCoordinateY() - dy, z);
        bottomRightPoint = new Vertex(centerPointRectangle.getCoordinateX() + dx, centerPointRectangle.getCoordinateY() -dy, z);
        topRightPoint = new Vertex(centerPointRectangle.getCoordinateX() + dx, centerPointRectangle.getCoordinateY() +dy, z);
        this.angle = angle;
        if(angle !=0f) {
            isRotate = true;
        }

    }

    public void setTextResourceId(int textResourceId, Context context) {

        mContext = context;
        this.textResourceId = textResourceId;
    }

    @Override
    protected void populateCoordinateArray() {
        System.arraycopy(bottomLeftPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, 0, COORDINATE_PER_VERTEX);
        System.arraycopy(bottomRightPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
        System.arraycopy(topLeftPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, 2*COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);

        System.arraycopy(topRightPoint.getCoordinatesFloatArray(), 0, shapeCoordinates, 3 * COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);

    }

    @Override
    protected void calculateCenterVertex() {
        centerPointRectangle = new Vertex();
        float x1 =  topLeftPoint.getCoordinateX();
        float y1 =  topLeftPoint.getCoordinateY();

        float x2 = bottomLeftPoint.getCoordinateX();
        float y2 = bottomLeftPoint.getCoordinateY();

        float x3 = bottomRightPoint.getCoordinateX();
        float y3 = bottomRightPoint.getCoordinateY();

        float x4 = topRightPoint.getCoordinateX();
        float y4 = topRightPoint.getCoordinateY();

        float centerX = (x4 * y2 *(x3-x1) - x2* y4*(x3-x1) - y1 *(x3-x1) + x1*y3 - x1*y1)/(y3 - y1 - y4 *(x3-x1) + y2*(x3-x1));
        float centerY = (centerX * y4 - x2*y4 - centerX * y2 + x2*y2 + x4*y2 - x2*y2)/(x4-x2);
        centerPointRectangle.setCoordinateX(centerX);
        centerPointRectangle.setCoordinateY(centerY);
        //TODO Need review below initialization
        centerPointRectangle.setCoordinateZ(topLeftPoint.getCoordinateZ());
        ///////////////////////////////////////
    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram program = new ShaderProgram(Shaders.VERTEX_SHADER_TEXTURE, Shaders.FRAGMENT_SHADER_TEXTURE);
        mProgram = program.getProgram();
    }

    @Override
    public void draw() {

    }
    @Override
    public void scale(float scale) {
        if(!isClickable){
            return;
        }
        mVertexBuffer.clear();
        shapeCoordinates[0] = shapeCoordinates[0] * scale;
        shapeCoordinates[3] = shapeCoordinates[3] * scale;
        shapeCoordinates[6] = shapeCoordinates[6] * scale;
        shapeCoordinates[9] = shapeCoordinates[9] * scale;

        shapeCoordinates[1] = shapeCoordinates[1] * scale;
        shapeCoordinates[4] = shapeCoordinates[4] * scale;
        shapeCoordinates[7] = shapeCoordinates[7] * scale;
        shapeCoordinates[10] = shapeCoordinates[10] * scale;

        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
//        Matrix.scaleM(currentMatrix, 0, scale, scale, 1.0f);

    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {


        if(x<shapeCoordinates[0] || x>shapeCoordinates[3]) {
            return false;
        }
        if(y<shapeCoordinates[1] || y>shapeCoordinates[7]) {
            return false;
        }
        return true;
    }

    @Override
    public void movePosition(float dx, float dy, float dz){
        if(!isClickable) {
            return;
        }
        mVertexBuffer.clear();
        shapeCoordinates[0] = shapeCoordinates[0] + dx;
        shapeCoordinates[3] = shapeCoordinates[3] + dx;
        shapeCoordinates[6] = shapeCoordinates[6] + dx;
        shapeCoordinates[9] = shapeCoordinates[9] + dx;

        shapeCoordinates[1] = shapeCoordinates[1] + dy;
        shapeCoordinates[4] = shapeCoordinates[4] + dy;
        shapeCoordinates[7] = shapeCoordinates[7] + dy;
        shapeCoordinates[10] = shapeCoordinates[10] + dy;

        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
//        Matrix.translateM(currentMatrix, 0 , dx, -dy, dz);
    }

    @Override
    public void draw(float[] mMVPMatrix) {

        if(!initialized) {

            loadTexture();

            initialized = true;
        }

        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        mTexCoordHandle = GLES20.glGetAttribLocation(mProgram, Shaders.TEXTURE_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mTexCoordHandle);
        GLES20.glVertexAttribPointer(mTexCoordHandle, 2, GLES20.GL_FLOAT, false, 0, textureBuffer);

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
        GLES20.glUniform1i(mTexSamplerHandle, 0);


//        applyColorUniform();
        if(isRotate) {
            Matrix.rotateM(rotateMatrix, 0, mMVPMatrix, 0, angle, 0.0f, 0.0f, 1.0f);
            applyProjectionUniform(rotateMatrix);
        } else {

            applyProjectionUniform(mMVPMatrix);
        }

        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0,4);

        GLES20.glDisableVertexAttribArray(mPositionHandler);
        GLES20.glDisableVertexAttribArray(mTexCoordHandle);
    }

    protected void initializeTextureBuffer() {
        textureBuffer = ByteBuffer.allocateDirect(TEX_VERTICES.length * BYTES_PER_FLOAT).order(ByteOrder.nativeOrder()).asFloatBuffer();
        textureBuffer.put(TEX_VERTICES).position(0);
    }

    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);

        initializeTextureBuffer();

        mContext = context;

        initializeShaderProgram();

    }

    public void setIsClickable(boolean isClickable) {
        this.isClickable = isClickable;
    }

    public boolean isClickable() {
        return isClickable;
    }
    private boolean loadTexture() {

        GLES20.glGenTextures(1,mTextures, 0);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;

                Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), textResourceId, options);

                GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextures[0]);
                GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
                initTexParams();
                bitmap.recycle();

        return true;
    }
    private void initTexParams() {
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D,
                GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);

        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
                GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
                GLES20.GL_CLAMP_TO_EDGE);
    }
}
