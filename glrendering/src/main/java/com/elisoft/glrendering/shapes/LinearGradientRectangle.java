package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;


import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.FloatColor;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;



/**
 * Created by yvorobey on 6/26/2015.
 */
public abstract class LinearGradientRectangle extends AbstractShape {
    protected static final short[] index =  {0,1,2, 0,2,3}; //Index fo drawing.
    protected float[] firstColor = new float[4];            //First color of gradient.
    protected float[] secondColor = new float[4];           //Second color of gradient.
    protected transient ShortBuffer indexBuffer;            //Buffer for indexes of drawing.
    protected float screenWidth;                            //Width of screen
    protected float screenHeight;                           //Height of screen


    /**
     * Protected constructor for linear two colors gradient.
     * @param viewWidth - Width of view (screen in FullScreen).
     * @param viewHeight - Height of view (screen in FullScreen).
     * @param firstColor - First color of gradient.
     * @param secondColor - Second color of gradient.
     */
    protected LinearGradientRectangle(float viewWidth, float viewHeight, FloatColor firstColor, FloatColor secondColor) {
        screenWidth = viewWidth;
        screenHeight = viewHeight;
        populateCoordinateArray();
        initializeFirstColor(firstColor);
        initializeSecondColor(secondColor);
        initializeVertexBuffer();
        initializeIndexBuffer();
        initializeShaderProgram();
    }
    @Override
    protected void populateCoordinateArray() {
        shapeCoordinates = new float[12];
        shapeCoordinates[0] = -1.0f;
        shapeCoordinates[1] = 1.0f;
        shapeCoordinates[2] = 0.0f;
        shapeCoordinates[3] = -1.0f;
        shapeCoordinates[4] = -1.0f;
        shapeCoordinates[5] = 0.0f;
        shapeCoordinates[6] = 1.0f;
        shapeCoordinates[7] = -1.0f;
        shapeCoordinates[8] = 0.0f;
        shapeCoordinates[9] = 1.0f;
        shapeCoordinates[10] = 1.0f;
        shapeCoordinates[11] =0.0f;
    }
    @Override
    protected void calculateCenterVertex() {
        //Do nothing. We draw rectangle like two triangles not 4 triangles.
    }
    private void initializeFirstColor(FloatColor floatColor) {
        firstColor[0] = floatColor.getRed();
        firstColor[1] = floatColor.getGreen();
        firstColor[2] = floatColor.getBlue();
        firstColor[3] = floatColor.getAlpha();
    }
    private void initializeSecondColor(FloatColor floatColor) {
        secondColor[0] = floatColor.getRed();
        secondColor[1] = floatColor.getGreen();
        secondColor[2] = floatColor.getBlue();
        secondColor[3] = floatColor.getAlpha();
    }
    private void initializeIndexBuffer() {
        ByteBuffer indexByteBuffer = ByteBuffer.allocateDirect(index.length*2);
        indexByteBuffer.order(ByteOrder.nativeOrder());
        indexBuffer = indexByteBuffer.asShortBuffer();
        indexBuffer.put(index);
        indexBuffer.position(0);
    }
    protected void applyFirstColorUniform() {
        int firstColorHandler = GLES20.glGetUniformLocation(mProgram, Shaders.FIRST_COLOR_UNIFORM);

        GLES20.glUniform4fv(firstColorHandler, 1, firstColor, 0);

    }
    protected void applySecondColorUniform() {
        int secondColorHandler = GLES20.glGetUniformLocation(mProgram, Shaders.SECOND_COLOR_UNIFORM);

        GLES20.glUniform4fv(secondColorHandler, 1, secondColor, 0);
    }
    protected void applyResolutionUniform() {
        int resolutionHandler = GLES20.glGetUniformLocation(mProgram, Shaders.RESOLUTION_UNIFORM);
        GLES20.glUniform2f(resolutionHandler, screenWidth,screenHeight);
    }

    @Override
    public void draw() {
        //Do nothing.
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        applyProjectionUniform(mMVPMatrix);
        applyFirstColorUniform();
        applySecondColorUniform();
        applyResolutionUniform();

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, index.length, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
        }

    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);
        initializeShaderProgram();
        initializeIndexBuffer();
    }
}
