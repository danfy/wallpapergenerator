package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.Matrix;

import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.ShaderProgram;
import com.elisoft.glrendering.utils.Vertex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;


/**
 * Class represents shadow of rectangle shape.
 */
public class ShadowRectangle extends Rectangle {
    private static final long serialVersionUID = 195023134L;
    protected float[] scale = {1,0,0,0,
            0,1,0,0,
            0,0,0,-1/2f,
            0,0,0,1};
    protected float[] identity = new float[16];
    protected transient FloatBuffer colorAttributeBuffer;
    protected float color[] = { 0.0f, 0.0f, 0.0f, 1.0f,
            0.0f, 0.0f,0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,


            0.0f, 0.0f,0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,


            0.0f, 0.0f,0.0f, 0.0f,
            0.0f, 0.0f, 0.0f, 0.0f,


    };

    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);
        initializeShadowColorBuffer();
    }

    @Override
    public void movePosition(float dx, float dy, float dz) {
        //Do nothing
    }

    @Override
    public void scale(float scale) {
        //Do nothing
    }
    public void moveShadowRectangle(float dx, float dy, float dz) {
        mVertexBuffer.clear();
        shapeCoordinates[0] +=dx;
        shapeCoordinates[1] +=dy;
        shapeCoordinates[3] +=dx;
        shapeCoordinates[4] +=dy;
        shapeCoordinates[6] +=dx;
        shapeCoordinates[7] +=dy;
        shapeCoordinates[9] +=dx;
        shapeCoordinates[10] +=dy;
        shapeCoordinates[12] +=dx;
        shapeCoordinates[13] +=dy;
        shapeCoordinates[15] +=dx;
        shapeCoordinates[16] +=dy;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
    }
    public void scaleShadowRectangle(float scale) {
        mVertexBuffer.clear();
        shapeCoordinates[0] *=scale;
        shapeCoordinates[1] *=scale;
        shapeCoordinates[3] *=scale;
        shapeCoordinates[4] *=scale;
        shapeCoordinates[6] *=scale;
        shapeCoordinates[7] *=scale;
        shapeCoordinates[9] *=scale;
        shapeCoordinates[10] *=scale;
        shapeCoordinates[12] *=scale;
        shapeCoordinates[13] *=scale;
        shapeCoordinates[15] *=scale;
        shapeCoordinates[16] *=scale;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
    }

    public ShadowRectangle(Vertex topLeftPoint, Vertex bottomLeftPoint, Vertex bottomRightPoint, Vertex topRightPoint) {
        super(topLeftPoint, bottomLeftPoint, bottomRightPoint, topRightPoint);
        isShadow = true;
        initializeShadowColorBuffer();
    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram shaderProgram = new ShaderProgram(Shaders.vertexShaderCodeShadow, Shaders.fragmentShaderCodeShadow);
        mProgram = shaderProgram.getProgram();

    }
    private void initializeShadowColorBuffer() {
        ByteBuffer colorByteBuffer = ByteBuffer.allocateDirect(color.length*4);
        colorByteBuffer.order(ByteOrder.nativeOrder());
        colorAttributeBuffer = colorByteBuffer.asFloatBuffer();
        colorAttributeBuffer.put(color);
        colorAttributeBuffer.position(0);
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        System.arraycopy(mMVPMatrix,0,identity, 0, mMVPMatrix.length);
        float dx = -centerPointRectangle.getCoordinateX();
        float dy = -centerPointRectangle.getCoordinateY();
        Matrix.translateM(identity, 0, -dx, -dy, 0.0f);
        Matrix.scaleM(identity, 0, 1.08f, 1.08f, 1f);
        Matrix.translateM(identity, 0 , dx, dy, 0.0f);
//        Matrix.multiplyMM(identity, 0, identity, 0, scale, 0);

        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
//        applyColorUniform();
        int colorHandler = GLES20.glGetAttribLocation(mProgram, Shaders.COLOR_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(colorHandler);
        GLES20.glVertexAttribPointer(colorHandler,4, GLES20.GL_FLOAT, false, 0, colorAttributeBuffer);
        applyProjectionUniform(identity);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, 6);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }
}
