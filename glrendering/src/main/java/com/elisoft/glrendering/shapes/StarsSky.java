package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;

import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.ShaderProgram;

import java.util.Random;



/**
 * Class represents stars sky with random position of pixel with size
 * of pixel point is 3.0f. Look at Shaders.v_stars shader code.
 */
public class StarsSky extends AbstractShape {

    private final Random random;
    private final int count;

    public StarsSky(int count) {
        random = new Random();
        this.count = count;

        populateCoordinateArray();
        initializeVertexBuffer();
        initializeShaderProgram();

    }
    @Override
    protected void populateCoordinateArray() {
        shapeCoordinates = new float[count*3];
        for(int i = 0; i<shapeCoordinates.length; i+=3) {
            shapeCoordinates[i] = random.nextFloat()*(1.0f - (-1.0f)) +(-1.0f);
            shapeCoordinates[i+1] = random.nextFloat()*(1.0f - (-1.0f)) +(-1.0f);
            shapeCoordinates[i+2] = 0.0f;
        }
    }

    @Override
    protected void calculateCenterVertex() {
        //Do nothing
    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram shaderProgram = new ShaderProgram(Shaders.v_stars, Shaders.f_stars);
        mProgram = shaderProgram.getProgram();
    }

    @Override
    public void draw() {

    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        applyProjectionUniform(mMVPMatrix);
        applyColorUniform();
//        applyPointSizeUniform();

//        GLES20.glDrawElements(GLES20.GL_POINTS, indexes.length, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, shapeCoordinates.length / COORDINATE_PER_VERTEX);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }

    @Override
    public void applyColorUniform() {
        float color[] = new float[4];
        color[0] = 1.0f;
        color[1] = 1.0f;
        color[2] = 1.0f;
        color[3] = 1.0f;
        int colorHandler =GLES20.glGetUniformLocation(mProgram, Shaders.COLOR_UNIFORM);
        GLES20.glUniform4fv(colorHandler, 1, color, 0);
    }
//    protected void applyPointSizeUniform() {
//        float pointSize = random.nextInt((4 - 1) + 1) + 1;
//        int pointSizeHandler = GLES20.glGetUniformLocation(mProgram, Shaders.POIN_SIZE_UNIFORM);
//        GLES20.glUniform1f(pointSizeHandler, pointSize);
//    }


    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);
        initializeShaderProgram();
    }

    @Override
    public void movePosition(float dx, float dy, float dz) {

    }

    @Override
    public void scale(float scale) {

    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {
        return false;
    }
}
