package com.elisoft.glrendering.shapes;


import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.ShaderProgram;

/**
 * Created by yvorobey on 6/26/2015.
 */
public class VerticalGradientRectangle extends LinearGradientRectangle {

    /**
     * Protected constructor for linear two colors gradient.
     *
     * @param viewWidth   - Width of view (screen in FullScreen).
     * @param viewHeight  - Height of view (screen in FullScreen).
     * @param firstColor  - First color of gradient.
     * @param secondColor - Second color of gradient.
     */
    public VerticalGradientRectangle(float viewWidth, float viewHeight, FloatColor firstColor, FloatColor secondColor) {
        super(viewWidth, viewHeight, firstColor, secondColor);
    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram shaderProgram =new ShaderProgram(Shaders.v_lin_vert_gradient, Shaders.f_lin_vert_gradient);
        mProgram = shaderProgram.getProgram();
    }

    @Override
    public void movePosition(float dx, float dy, float dz) {

    }

    @Override
    public void scale(float scale) {

    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {
        return false;
    }


}
