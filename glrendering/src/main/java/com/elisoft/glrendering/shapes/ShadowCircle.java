package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;

import com.elisoft.glrendering.Shaders;

import com.elisoft.glrendering.utils.OpenGLDebug;
import com.elisoft.glrendering.utils.ShaderProgram;
import com.elisoft.glrendering.utils.Vertex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;


/**
 * Class represents shadow circle shape.
 */
public class ShadowCircle extends AbstractShape {
    private static final long serialVersionUID = 7463212345L;
    protected float[] uvCoordinates = {0,0f, 0f,1f, 1f,1f, 0.0f,1f,1f,1f,0f};

    protected Vertex center;

    protected float radius;
    protected short[] index = {0,1,2, 0,2,3};
    protected transient ShortBuffer indexBuffer;

    protected transient FloatBuffer texBuffer;

    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);
        initializeShaderProgram();
        initializeVertexBuffer();
        initializeIndexBuffer();
        initializeTexCoordBuffer();
    }

    public void moveShadow(float dx, float dy, float dz) {
        if(mVertexBuffer!=null) {
            mVertexBuffer.clear();
            shapeCoordinates[0] += dx;
            shapeCoordinates[1] += dy;
            shapeCoordinates[3] += dx;
            shapeCoordinates[4] += dy;
            shapeCoordinates[6] += dx;
            shapeCoordinates[7] += dy;
            shapeCoordinates[9] += dx;
            shapeCoordinates[10] += dy;
            mVertexBuffer.put(shapeCoordinates);
            mVertexBuffer.position(0);
        }
    }
    public void scaleShadow(float scale) {
        if(mVertexBuffer!=null) {
            mVertexBuffer.clear();
            shapeCoordinates[0] *= scale;
            shapeCoordinates[1] *= scale;
            shapeCoordinates[3] *= scale;
            shapeCoordinates[4] *= scale;
            shapeCoordinates[6] *= scale;
            shapeCoordinates[7] *= scale;
            shapeCoordinates[9] *= scale;
            shapeCoordinates[10] *= scale;
            mVertexBuffer.put(shapeCoordinates);
            mVertexBuffer.position(0);
        }
    }

    @Override
    public void movePosition(float dx, float dy, float dz) {
        //Do nothing
    }

    @Override
    public void scale(float scale) {
        //Do nothing
    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {
        return false;
    }

    public ShadowCircle(Vertex center, float radius) {
        this.center = center;
        isShadow =true;
        this.radius = radius;
        populateCoordinateArray();
        initializeShaderProgram();
        initializeVertexBuffer();
        initializeIndexBuffer();
        initializeTexCoordBuffer();

    }
    @Override
    protected void populateCoordinateArray() {
        shapeCoordinates = new float[12];
        shapeCoordinates[0] = center.getCoordinateX() - radius;
        shapeCoordinates[1] = center.getCoordinateY() + radius;
        shapeCoordinates[2] = center.getCoordinateZ();

        shapeCoordinates[3] = shapeCoordinates[0];
        shapeCoordinates[4] = center.getCoordinateY()- radius;
        shapeCoordinates[5] = center.getCoordinateZ();

        shapeCoordinates[6] = center.getCoordinateX() + radius;
        shapeCoordinates[7] = shapeCoordinates[4];
        shapeCoordinates[8] = center.getCoordinateZ();

        shapeCoordinates[9] = shapeCoordinates[6];
        shapeCoordinates[10] = shapeCoordinates[1];
        shapeCoordinates[11] = center.getCoordinateZ();


    }

    @Override
    protected void calculateCenterVertex() {
        //Do nothing
    }

    @Override
    protected void initializeVertexBuffer() {
        ByteBuffer vertexByteBuffer = ByteBuffer.allocateDirect(shapeCoordinates.length *4);
        vertexByteBuffer.order(ByteOrder.nativeOrder());
        mVertexBuffer = vertexByteBuffer.asFloatBuffer();
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);

    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram shaderProgram = new ShaderProgram(Shaders.vertexShaderCodeCircleShadow, Shaders.fragmentShadowCircle);
        mProgram = shaderProgram.getProgram();
    }

    @Override
    public void draw() {

    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        OpenGLDebug.checkGLError("glGetAttribLocation");
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        int texCoordHandler = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");

        GLES20.glEnableVertexAttribArray(texCoordHandler);
        GLES20.glVertexAttribPointer(texCoordHandler, 2, GLES20.GL_FLOAT, false,0, texBuffer);
        applyProjectionUniform(mMVPMatrix);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, index.length, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
        GLES20.glDisableVertexAttribArray(mPositionHandler);



    }
    private void initializeIndexBuffer() {
        ByteBuffer indexByteBuffer = ByteBuffer.allocateDirect(index.length*2);
        indexByteBuffer.order(ByteOrder.nativeOrder());
        indexBuffer = indexByteBuffer.asShortBuffer();
        indexBuffer.put(index);
        indexBuffer.position(0);
    }
    private void initializeTexCoordBuffer() {
        ByteBuffer texByteBuffer = ByteBuffer.allocateDirect(uvCoordinates.length*4);
        texByteBuffer.order(ByteOrder.nativeOrder());
        texBuffer = texByteBuffer.asFloatBuffer();
        texBuffer.put(uvCoordinates);
        texBuffer.position(0);
    }
}
