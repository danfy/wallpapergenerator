package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;

import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.FloatColor;
import com.elisoft.glrendering.utils.ShaderProgram;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;


/**
 * Class represents rectangle with gradient
 */
public class GradientRadialRectangle extends AbstractShape {
    private static final long serialVersionUID = 10041234L;
    private static final short[] index =  {0,1,2, 0,2,3};

    private float[] firstColor = new float[4];
    private float[] secondColor = new float[4];
    protected float radius;
    protected float centerX; // X coordinate of center in uv texture coordinate
    protected float centerY; // Y coordinate of center in uv texture coordinate
    protected transient ShortBuffer indexBuffer;

    protected int texCoordinateHandler;
    protected float resolutionX;
    protected float resolutionY;

    /**
     * Constructor for gradient rectangle in coordinates (-1,1), (-1,-1), (1,-1), (1,1) of OpenGL.
     * @param widthScreen - Width of screen.
     * @param heightScreen - Height of screen.
     * @param firstColor - First color in gradient (main color).
     * @param secondColor - Second color int gradient (secondary).
     * @param centerX - Center coordinate X of radial gradient.
     * @param centerY - Center coordinate Y of radial gradient.
     * @param radiusGradient  - Radius of radial gradient.
     */
    public GradientRadialRectangle(float widthScreen, float heightScreen, FloatColor firstColor, FloatColor secondColor, float centerX, float centerY, float radiusGradient) {
        populateCoordinateArray();
        initializeFirstColor(firstColor);
        initializeSecondColor(secondColor);
        initializeCenterGradient(centerX, centerY);
        initializeRadiusGradient(radiusGradient);
        initializeVertexBuffer();
        initializeIndexBuffer();
        initializeShaderProgram();
        this.resolutionX = widthScreen;
        this.resolutionY = heightScreen;

    }

    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);
        initializeShaderProgram();
        initializeIndexBuffer();

    }

    @Override
    public void movePosition(float dx, float dy, float dz) {

    }

    @Override
    public void scale(float scale) {

    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {
        return false;
    }

    private void initializeRadiusGradient(float radius) {
        this.radius = radius;
    }

    private void initializeCenterGradient(float centerX, float centerY) {
        this.centerX = centerX;
        this.centerY = centerY;
    }
    private void initializeFirstColor(FloatColor floatColor) {
        firstColor[0] = floatColor.getRed();
        firstColor[1] = floatColor.getGreen();
        firstColor[2] = floatColor.getBlue();
        firstColor[3] = floatColor.getAlpha();
    }
    private void initializeSecondColor(FloatColor floatColor) {
        secondColor[0] = floatColor.getRed();
        secondColor[1] = floatColor.getGreen();
        secondColor[2] = floatColor.getBlue();
        secondColor[3] = floatColor.getAlpha();
    }
    private void initializeIndexBuffer() {
        ByteBuffer indexByteBuffer = ByteBuffer.allocateDirect(index.length*2);
        indexByteBuffer.order(ByteOrder.nativeOrder());
        indexBuffer = indexByteBuffer.asShortBuffer();
        indexBuffer.put(index);
        indexBuffer.position(0);
    }

    @Override
    protected void populateCoordinateArray() {
        shapeCoordinates = new float[12];
        shapeCoordinates[0] = -1.0f;
        shapeCoordinates[1] = 1.0f;
        shapeCoordinates[2] = 0.0f;
        shapeCoordinates[3] = -1.0f;
        shapeCoordinates[4] = -1.0f;
        shapeCoordinates[5] = 0.0f;
        shapeCoordinates[6] = 1.0f;
        shapeCoordinates[7] = -1.0f;
        shapeCoordinates[8] = 0.0f;
        shapeCoordinates[9] = 1.0f;
        shapeCoordinates[10] = 1.0f;
        shapeCoordinates[11] =0.0f;
    }

    @Override
    protected void calculateCenterVertex() {
        //Do nothing. We draw rectangle like two triangles not 4 triangles.
    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram shaderProgram = new ShaderProgram(Shaders.v_radial_gradient, Shaders.f_radial_gradient);
        mProgram = shaderProgram.getProgram();
    }

    @Override
    public void draw() {

    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
//        applyTexCoordinateAttribute();
        applyProjectionUniform(mMVPMatrix);
        applyFirstColorUniform();
        applySecondColorUniform();
        applyRadiusUniform();
        applyCenterUniform();
        applyResolutionUniform();

        GLES20.glDrawElements(GLES20.GL_TRIANGLES, index.length, GLES20.GL_UNSIGNED_SHORT, indexBuffer);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
        GLES20.glDisableVertexAttribArray(texCoordinateHandler);

    }

    private void applyFirstColorUniform() {
        int firstColorHandler = GLES20.glGetUniformLocation(mProgram, Shaders.FIRST_COLOR_UNIFORM);

        GLES20.glUniform4fv(firstColorHandler, 1, firstColor, 0);

    }
    private void applySecondColorUniform() {
        int secondColorHandler = GLES20.glGetUniformLocation(mProgram, Shaders.SECOND_COLOR_UNIFORM);

        GLES20.glUniform4fv(secondColorHandler, 1, secondColor, 0);
    }
    private void applyRadiusUniform() {
        int radiusHandler = GLES20.glGetUniformLocation(mProgram, Shaders.RADIUS_UNIFORM);

        GLES20.glUniform1f(radiusHandler, radius);
    }
    private void applyCenterUniform() {
        int centerHandler = GLES20.glGetUniformLocation(mProgram, Shaders.CENTER_UNIFORM);

        GLES20.glUniform2f(centerHandler, centerX, centerY);
    }
    private void applyResolutionUniform() {
        int resolutionHandler = GLES20.glGetUniformLocation(mProgram, Shaders.RESOLUTION_UNIFORM);
        GLES20.glUniform2f(resolutionHandler, resolutionX,resolutionY);
    }
}
