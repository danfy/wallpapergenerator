package com.elisoft.glrendering.shapes;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;


import com.elisoft.glrendering.Shaders;
import com.elisoft.glrendering.utils.ShaderProgram;
import com.elisoft.glrendering.utils.Vertex;


/**
 * Class represents circle shape
 */
public class Circle extends AbstractShape {
    private static final long serialVersionUID = 103952134L;
    private static final String TAG = Circle.class.getSimpleName();
    protected Vertex centerPointCircle; // Vertex of center point of circle
    protected int vertexCount; //Vertex count of circle
    protected float radius; //Radius of circle
    private ShadowCircle shadowCircle;
    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);
        initializeVertexBuffer();
        initializeShaderProgram();
    }

    public Circle(Vertex center, int vertexCount, float radius) {
        shapeCoordinates = new float[vertexCount*COORDINATE_PER_VERTEX];
        this.vertexCount = vertexCount;
        this.radius = radius;
        centerPointCircle = center;
        populateCoordinateArray();
        initializeVertexBuffer();
        initializeShaderProgram();
    }
    @Override
    protected void populateCoordinateArray() {
        generateCircleVertexes();
    }

    @Override
    protected void calculateCenterVertex() {
        //Nothing to do
    }

    @Override
    protected void initializeShaderProgram() {
        ShaderProgram shaderProgram = new ShaderProgram(Shaders.vertexShaderCode, Shaders.fragmentShaderCode);
        mProgram = shaderProgram.getProgram();
    }

    @Override
    public void draw() {
        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        applyColorUniform();
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexCount);
        GLES20.glDisableVertexAttribArray(mPositionHandler);

    }
    @Override
    public void movePosition(float dx, float dy, float dz) {
        mVertexBuffer.clear();
        for(int i = 0; i< vertexCount; i++) {
            shapeCoordinates[(i*3)+0] +=dx;
            shapeCoordinates[(i*3)+1] +=dy;
        }
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
        if(shadowCircle!=null) {
            shadowCircle.moveShadow(dx, dy, dz);
        }
    }

    @Override
    public void scale(float scale) {
        mVertexBuffer.clear();
        for(int i = 0; i<vertexCount; i++) {
            shapeCoordinates[(i*3) +0] *=scale;
            shapeCoordinates[(i*3) + 1] *=scale;

        }
        radius*=scale;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
        if(shadowCircle!=null) {
            shadowCircle.scaleShadow(scale);
        }
    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {

        if(x>shapeCoordinates[0]+radius || x< shapeCoordinates[0] - radius) {

            return false;
        }
        if(y>shapeCoordinates[1] + radius || y<shapeCoordinates[1] - radius) {
            return false;
        }

        return true;
    }

    @Override
    public void draw(float[] mMVPMatrix) {

        GLES20.glUseProgram(mProgram);
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        applyColorUniform();
        applyProjectionUniform(mMVPMatrix);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_FAN, 0, vertexCount);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }

    private void generateCircleVertexes() {
        shapeCoordinates[0] = centerPointCircle.getCoordinateX();
        shapeCoordinates[1] = centerPointCircle.getCoordinateY();
        shapeCoordinates[2] = centerPointCircle.getCoordinateZ();
        for(int i = 1; i<vertexCount-1;i++) {
            float percent = (i/ (float) (vertexCount -1));
            float radians = (float) (percent * 2 * Math.PI);
            shapeCoordinates[(i*3) +0] = (float)(centerPointCircle.getCoordinateX() + radius *Math.cos(radians));
            shapeCoordinates[(i*3) +1] =(float)(centerPointCircle.getCoordinateY() + radius *Math.sin(radians));
            shapeCoordinates[(i*3) +2] = centerPointCircle.getCoordinateZ();


        }
        shapeCoordinates[shapeCoordinates.length-3] =shapeCoordinates[3];
        shapeCoordinates[shapeCoordinates.length-2] =shapeCoordinates[4];
        shapeCoordinates[shapeCoordinates.length-1] =shapeCoordinates[5];
    }
    public ShadowCircle generateShadow() {
        shadowCircle = new ShadowCircle(centerPointCircle, radius*1.08f);
        return shadowCircle;
    }

}
