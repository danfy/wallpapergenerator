package com.elisoft.glrendering.shapes;


import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import com.elisoft.glrendering.Shaders;

import com.elisoft.glrendering.utils.OpenGLDebug;
import com.elisoft.glrendering.utils.ShaderProgram;
import com.elisoft.glrendering.utils.Vertex;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;



/**
 * Class represents triangle shape. Draw like 3 sub-triangles with
 * common point in center of shape triangle.
 */
public class Triangle extends AbstractShape {

    private static final long serialVersionUID = 7489912345L; //Used for serialization

    private static final String TAG =Triangle.class.getSimpleName();
    protected Vertex centerPointTriangle;       // Vertex of central point of triangle
    protected  Vertex topPointTriangle;          // Vertex of top point of triangle
    protected  Vertex bottomLeftPointTriangle;   // Vertex of bottom left point of triangle
    protected  Vertex bottomRightPointTriangle;  // Vertex of bottom right point of triangle
    protected final short drawOrder[] = {0,1,2,0,2,3,0,3,1}; //Draw indexes. Needs for vertex drawing order
    protected transient ShortBuffer drawListBuffer;               //Buffer for draw indexes.
    private ShadowTriangle shadowTriangle;
    protected float[] scale = {1,0,0,0,
            0,1,0,0,
            0,0,1,0,
            0,0,0,1};

    @Override
    public void initializeShapeFromFile(Context context) {
        super.initializeShapeFromFile(context);
        initializeDrawListBuffer();
        initializeShaderProgram();
    }

    public Triangle(Vertex topPoint, Vertex bottomLeftPoint, Vertex bottomRightPoint) {
        shapeCoordinates = new float[4*COORDINATE_PER_VERTEX];
        topPointTriangle = topPoint;
        bottomLeftPointTriangle = bottomLeftPoint;
        bottomRightPointTriangle = bottomRightPoint;

        calculateCenterVertex();
        populateCoordinateArray();
        initializeVertexBuffer();
        initializeDrawListBuffer();
        initializeShaderProgram();
    }


    @Override
    protected void populateCoordinateArray() {
        //Copy coordinate arrays of each point to main array shapeCoordinate
        System.arraycopy(centerPointTriangle.getCoordinatesFloatArray(), 0, shapeCoordinates, 0, COORDINATE_PER_VERTEX);
        System.arraycopy(topPointTriangle.getCoordinatesFloatArray(), 0, shapeCoordinates, COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
        System.arraycopy(bottomLeftPointTriangle.getCoordinatesFloatArray(), 0, shapeCoordinates, 2 * COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);
        System.arraycopy(bottomRightPointTriangle.getCoordinatesFloatArray(), 0, shapeCoordinates, 3 * COORDINATE_PER_VERTEX, COORDINATE_PER_VERTEX);

    }

    @Override
    protected void calculateCenterVertex() {
        centerPointTriangle = new Vertex();
        centerPointTriangle.setCoordinateX((topPointTriangle.getCoordinateX() + bottomLeftPointTriangle.getCoordinateX() + bottomRightPointTriangle.getCoordinateX()) / 3);
        centerPointTriangle.setCoordinateY((topPointTriangle.getCoordinateY() + bottomLeftPointTriangle.getCoordinateY() + bottomRightPointTriangle.getCoordinateY()) / 3);
        centerPointTriangle.setCoordinateZ((topPointTriangle.getCoordinateZ() + bottomLeftPointTriangle.getCoordinateZ() + bottomRightPointTriangle.getCoordinateZ()) / 3);

    }
    @Override
    public void movePosition(float dx, float dy, float dz) {
        mVertexBuffer.clear();
        shapeCoordinates[0] +=dx;
        shapeCoordinates[3] +=dx;
        shapeCoordinates[6] +=dx;
        shapeCoordinates[9] +=dx;
        shapeCoordinates[1] +=dy;
        shapeCoordinates[4] +=dy;
        shapeCoordinates[7] +=dy;
        shapeCoordinates[10] +=dy;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
        if(shadowTriangle!=null) {
            shadowTriangle.movePositionShadow(dx, dy, dz);
        }
    }

    @Override
    public void scale(float scale) {
        mVertexBuffer.clear();
        shapeCoordinates[0] *=scale;
        shapeCoordinates[3] *=scale;
        shapeCoordinates[6]*=scale;
        shapeCoordinates[9] *=scale;
        shapeCoordinates[1]*=scale;
        shapeCoordinates[4]*=scale;
        shapeCoordinates[7] *=scale;
        shapeCoordinates[10] *=scale;
        mVertexBuffer.put(shapeCoordinates);
        mVertexBuffer.position(0);
        if(shadowTriangle!=null) {
            shadowTriangle.scaleShadow(scale);
        }
    }

    @Override
    public boolean checkIfPointInShape(float x, float y) {


        float Bx = shapeCoordinates[3] - shapeCoordinates[9];
        float By = shapeCoordinates[4] - shapeCoordinates[10];
        float Ax = shapeCoordinates[6] - shapeCoordinates[9];
        float Ay = shapeCoordinates[7] - shapeCoordinates[10];
        float Px = x - shapeCoordinates[9];
        float Py = y - shapeCoordinates[10];

        float m = (Px*By - Bx*Py)/(Ax*By - Bx*Ay);
        if(m>=0 && m<=1) {

            float l = (Px-m*Ax)/Bx;
            if(l>0 && (m+l)<=1) {
                return true;
            }
        }
        return false;


    }


    @Override
    protected void initializeShaderProgram() {

        ShaderProgram program = new ShaderProgram(Shaders.vertexShaderCode, Shaders.fragmentShaderCode); // Just for test
        mProgram = program.getProgram();
    }

    @Override
    public void draw() {
        GLES20.glUseProgram(mProgram);

        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);

        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        OpenGLDebug.checkGLError("glVertexAttribPointer");
        applyColorUniform();
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        OpenGLDebug.checkGLError("glDrawElements");
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }

    @Override
    public void draw(float[] mMVPMatrix) {
        GLES20.glUseProgram(mProgram);
        OpenGLDebug.checkGLError("glUseProgram");

        mPositionHandler = GLES20.glGetAttribLocation(mProgram, Shaders.POSITION_ATTRIBUTE);
        GLES20.glEnableVertexAttribArray(mPositionHandler);
        OpenGLDebug.checkGLError("glEnableVertexAttribArray");
        GLES20.glVertexAttribPointer(mPositionHandler, COORDINATE_PER_VERTEX, GLES20.GL_FLOAT, false, VERTEX_STRIDE, mVertexBuffer);
        applyColorUniform();
        applyProjectionUniform(mMVPMatrix);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawOrder.length, GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        GLES20.glDisableVertexAttribArray(mPositionHandler);
    }

    protected void initializeDrawListBuffer() {
       ByteBuffer drawListByteBuffer = ByteBuffer.allocateDirect(drawOrder.length*BYTES_PER_SHORT);
       drawListByteBuffer.order(ByteOrder.nativeOrder());
       drawListBuffer = drawListByteBuffer.asShortBuffer();
       drawListBuffer.put(drawOrder);
       drawListBuffer.position(0);
   }
    public ShadowTriangle generateShadow() {
//        Matrix.scaleM(scale, 0, 2f, 2f, 1.0f);
//        float[] top = new float[4];
//        float[] bottomLeft = new float[4];
//        float[] bottomRight = new float[4];
//        System.arraycopy(topPointTriangle.getCoordinatesFloatArray(), 0, top, 0,3);
//        System.arraycopy(bottomLeftPointTriangle.getCoordinatesFloatArray(), 0, bottomLeft, 0, 3);
//        System.arraycopy(bottomRightPointTriangle.getCoordinatesFloatArray(), 0,  bottomRight,0 ,3);
//        top[3] = 1f;
//        bottomLeft[3] = 1f;
//        bottomRight[3] = 1f;
//        float[] outputTop = new float[4];
//        float[] outputBottomLeft = new float[4];
//        float[] outputBottomRight = new float[4];
//
//        Log.d(TAG, topPointTriangle.getCoordinatesFloatArray().length + "");
//        Matrix.multiplyMV(outputTop,0,scale,0, top, 0);
//        Matrix.multiplyMV(outputBottomLeft, 0, scale, 0, bottomLeft, 0);
//        Matrix.multiplyMV(outputBottomRight, 0, scale, 0, bottomRight, 0);
//        Vertex vertexTopShadow = new Vertex(top);
//        Vertex vertexBottomLeftShadow = new Vertex(bottomLeft);
//        Vertex vertexBottomRightShadow = new Vertex(bottomRight);

//        for(int i = 0 ; i<16; i++) {
//
//
//            Log.d(TAG,"Matrix i = " + scale[i]);
//        }

        shadowTriangle = new ShadowTriangle(topPointTriangle, bottomLeftPointTriangle, bottomRightPointTriangle);




        return shadowTriangle;
    }
}
