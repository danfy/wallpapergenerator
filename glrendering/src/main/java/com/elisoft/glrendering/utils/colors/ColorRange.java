package com.elisoft.glrendering.utils.colors;

import com.elisoft.glrendering.R;

import java.util.Random;



/**
 * Created by mBykov on 19.06.2015.
 */
public enum ColorRange {
    RED (R.array.red), PINK(R.array.pink),PUPRLE(R.array.purple),PUPRLE_DEEP(R.array.deep_purple), INDIGO (R.array.indigo), BLUE(R.array.blue),
    BLUE_LIGHT(R.array.light_blue),CYAN (R.array.cyan), TEAL (R.array.teal), GREEN (R.array.green),LIME (R.array.lime),
    YELLOW (R.array.yellow), AMBER (R.array.amber), ORANGE (R.array.orange), ORANGE_DEEP (R.array.deep_orange), BROWN (R.array.brown), GREY (R.array.grey),
    BLUE_GREY (R.array.blue_grey), BASICS (R.array.basic_colors);
    private static final ColorRange[] VALUES = values();
    private static final int SIZE = VALUES.length;
    private static final Random RANDOM = new Random();
    private int value;

    private ColorRange(int value) {
        this.value = value;
    }
    public int getValue() {
        return this.value;
    }
    public static ColorRange getRandomRange()  {
        return VALUES[RANDOM.nextInt(SIZE)];
    }

}
