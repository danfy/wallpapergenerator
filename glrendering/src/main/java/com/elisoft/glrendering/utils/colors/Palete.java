package com.elisoft.glrendering.utils.colors;

import android.content.Context;

import com.elisoft.glrendering.utils.FloatColor;



import java.util.ArrayList;



/**
 * How to use:
 * <p>Palete p= new Palete (Context contex);
 * <p>p.SetColorRange (ColorRange.RED); or p.SetColorRange (new ColorRange [] {ColorRange.RED, ColorRange.Blue});
 * <p>p.getColor (int position); // return random FloatColor from  range;<p/>
 * <p>p.getSomeColor (); // return random FloatColor from  range;<p/>
 */
public class Palete {
    Generator generator;
    ColorLoader colorLoader;
    ArrayList<FloatColor> palete;
    ArrayList<Integer> generatedColorsListPositions;
    private int iter;

    public Palete(Context context) {
        colorLoader = new ColorLoader(context);
        palete = new ArrayList<FloatColor>();
        generatedColorsListPositions = new ArrayList<Integer>();
        generator = new SimpleColorsGenerator();
        this.setColorRange(ColorRange.values());
    }

    private void generateColors() {
        generatedColorsListPositions.clear();
        generatedColorsListPositions= generator.generate(palete.size());
        iter=0;
    }
/**
 */
    /**
    *@return Return FloatColor from some position in palete;
     *
    */
    public FloatColor getColor(int position) {
        if (position>=generatedColorsListPositions.size()){
            position=position % generatedColorsListPositions.size();
        }
        return palete.get(generatedColorsListPositions.get(position));
    }
    public FloatColor getSomeColor (){
        if (iter>=generatedColorsListPositions.size()){
            iter=0;
        }
        FloatColor color =palete.get(generatedColorsListPositions.get(iter));
            iter++;
        return color;
    }

    public void setColorRange(ColorRange range) {
        palete.clear();
        palete.addAll(colorLoader.load(range));
        generateColors();
    }

    public void setColorRange(ColorRange[] ranges) {
        palete.clear();
        palete.addAll(colorLoader.load(ranges));
        generateColors();
    }

    public int getPaleteSize (){
        return  (palete!=null)?palete.size():0;
    }

}
