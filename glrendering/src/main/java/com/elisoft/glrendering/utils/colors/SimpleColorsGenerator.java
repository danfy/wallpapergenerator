package com.elisoft.glrendering.utils.colors;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by mBykov on 19.06.2015.
 */
class SimpleColorsGenerator implements Generator {

    @Override
    public ArrayList<Integer> generate(int size) {
        ArrayList<Integer> gen = new ArrayList<Integer>();
        for (int i = 0; i < size; i++) {
            gen.add(i);
        }
        Collections.shuffle(gen);
        return gen;
    }
}
