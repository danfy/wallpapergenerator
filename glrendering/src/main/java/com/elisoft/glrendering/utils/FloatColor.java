package com.elisoft.glrendering.utils;

import android.graphics.Color;

import java.io.Serializable;

/**
 * Color class handle values of red, green, blue, alpha in float 
 */
public class FloatColor implements Serializable {
    private static final long serialVersionUID = 2008931234L;
    private static final float MAX_COLOR_VALUE = 255f;
    private float red;
    private float green;
    private float blue;
    private float alpha;

    public FloatColor() {

    }

    public FloatColor(float red, float green, float blue, float alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    /**
     * Method generate FloatColor object from int values of red, green,blue,alpha
     * @param red int value of red
     * @param green int value of green
     * @param blue int value of blue
     * @param alpha int value of alpha
     * @return FloatColor object
     */
    public static FloatColor generateFloatColor(int red, int green, int blue, int alpha) {
        FloatColor floatColor = new FloatColor();
        floatColor.red = red / MAX_COLOR_VALUE;
        floatColor.green = green / MAX_COLOR_VALUE;
        floatColor.blue = blue / MAX_COLOR_VALUE;
        floatColor.alpha = alpha/MAX_COLOR_VALUE;
        return floatColor;
    }

    /**
     * Method generate FloatColor object from resources
     * @param color color resource
     * @return FloatColor object
     */
    public static FloatColor generateFloatColor(int color) {
        FloatColor floatColor = new FloatColor();
        floatColor.red = Color.red(color) / MAX_COLOR_VALUE;
        floatColor.green = Color.green(color) / MAX_COLOR_VALUE;
        floatColor.blue = Color.blue(color) / MAX_COLOR_VALUE;
        floatColor.alpha = Color.alpha(color) / MAX_COLOR_VALUE;
        return floatColor;
    }


    public float getRed() {
        return red;
    }

    public void setRed(float red) {
        this.red = red;
    }

    public float getGreen() {
        return green;
    }

    public void setGreen(float green) {
        this.green = green;
    }

    public float getBlue() {
        return blue;
    }

    public void setBlue(float blue) {
        this.blue = blue;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }
}
