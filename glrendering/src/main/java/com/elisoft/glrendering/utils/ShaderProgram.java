package com.elisoft.glrendering.utils;


import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Class handle render program openGL with initializing vertex shader and fragment shader
 */
public class ShaderProgram {
    private static final String TAG = ShaderProgram.class.getSimpleName();
    private String vertexShader, fragmentShader;
    private int mProgram, mVertexShader, mFragmentShader;
    private String log = "";
    /**
     * Constructor create ShaderProgram from strings values of vertexShader and fragmentShader
     * @param vertexShader - string with vertexShader code
     * @param fragmentShader - string with fragmentShader code
     */
    public ShaderProgram(String vertexShader, String fragmentShader) {
        setUpShaderCodes(vertexShader, fragmentShader);
    }

    /**
     * Constructor create ShaderProgram object with parameters from raw resources of vertex shader and fragment shader.
     * @param rawResVertexS - id of raw resource vertexShader
     * @param rawResFragmentS - id of raw resource fragmentShader
     * @param context - context
     */
    public ShaderProgram(int rawResVertexS, int rawResFragmentS, Context context) {
        StringBuffer vs = new StringBuffer();
        StringBuffer fs = new StringBuffer();
        // read the files
        try {
            // Read the file from the resource

            InputStream inputStream = context.getResources().openRawResource(rawResVertexS);
            // setup Bufferedreader
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

            String read = in.readLine();
            while (read != null) {
                vs.append(read + "\n");
                read = in.readLine();
            }

            vs.deleteCharAt(vs.length() - 1);


            inputStream = context.getResources().openRawResource(rawResFragmentS);
            // setup Bufferedreader
            in = new BufferedReader(new InputStreamReader(inputStream));

            read = in.readLine();
            while (read != null) {
                fs.append(read + "\n");
                read = in.readLine();
            }

            fs.deleteCharAt(fs.length() - 1);
        } catch (Exception e) {

        }


        // Setup everything
        setUpShaderCodes(vs.toString(), fs.toString());

    }
    /**
     * Utility method for compiling a OpenGL shader.
     *
     * When developing shaders, use the checkGlError()
     * method to debug shader coding errors.
     *
     * @param type - Vertex or fragment shader type.
     * @param shaderCode - String containing the shader code.
     * @return - Returns an id for the shader.
     */
    private int loadShader(int type, String shaderCode) {
        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);
        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);


        OpenGLDebug.checkGLError("glCompileShader");

        return shader;

    }

    /**
     * Initialize class string values of shader code
     * @param vertexShader - string with vertexShader code
     * @param fragmentShader - string with fragmentShader code
     */
    private void setUpShaderCodes(String vertexShader, String fragmentShader) {
        this.vertexShader = vertexShader;
        this.fragmentShader = fragmentShader;
        // create the program
        if (createProgram(this.vertexShader, this.fragmentShader) != 1) {
            throw new RuntimeException("Error at creating shaders " + log);
        };

    }

    /**
     * Method create Program and attach vertex shader and fragment shader
     * @param vertexShader - string with vertex shader code
     * @param fragmentShader - string with fragment shader code
     * @return -program creating status(1 if program creating is successful, 0 otherwise)
     */
    private int createProgram(String vertexShader, String fragmentShader) {

        mVertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexShader);
        if (mVertexShader == 0) {
            return 0;
        }
        mFragmentShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader);
        if (mFragmentShader == 0) {
            return 0;
        }
        mProgram = GLES20.glCreateProgram();
        if (mProgram != 0) {
            GLES20.glAttachShader(mProgram, mVertexShader);
            GLES20.glAttachShader(mProgram, mFragmentShader);
            GLES20.glLinkProgram(mProgram);
            OpenGLDebug.checkGLError("glLinkProgram");
            int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(mProgram, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {

                log = GLES20.glGetProgramInfoLog(mProgram);


                GLES20.glDeleteProgram(mProgram);
                mProgram = 0;

//                return 0;
                throw new RuntimeException("Program doesn't create " + log);
            }
        } else {


        }
        return 1;
    }

    /**
     * Return program handle
     * @return program handle
     */
    public int getProgram() {
        return mProgram;
    }
}
