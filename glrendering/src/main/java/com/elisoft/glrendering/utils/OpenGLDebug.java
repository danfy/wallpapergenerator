package com.elisoft.glrendering.utils;

import android.opengl.GLES20;
import android.util.Log;


/**
 * Class fo debugging errors in openGL commands
 */
public  class OpenGLDebug {
    private static final String TAG = OpenGLDebug.class.getSimpleName();
    /**
     * Utility method for debugging OpenGL calls. Provide the name of the call
     * just after making it:
     *
     * Example: mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
     * OpenGLDebug.checkGLError("glGetUniformLocation");
     *
     * If operation is not successful, the check throws an error.
     * @param glOperation - Name of the OpenGL call to check.
     */
    public static void checkGLError(String glOperation) {
        int error;

        while ((error = GLES20.glGetError())!= GLES20.GL_NO_ERROR) {

//            throw new RuntimeException(glOperation + " : glError " + error);
        }
    }
}
