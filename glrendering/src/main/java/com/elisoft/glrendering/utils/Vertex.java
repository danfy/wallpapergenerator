package com.elisoft.glrendering.utils;

import java.io.Serializable;

/**
 * Class represent coordinate of point
 */
public class Vertex implements Serializable {
    private float coordinateX; // coordinate X of point
    private float coordinateY; // coordinate Y of point
    private float coordinateZ; // coordinate Z of point
    private float[] coordinatesFloatArray = new float[3];

    public Vertex() {

    }

    public Vertex(float[] coordinates) {


        coordinateX = coordinatesFloatArray[0] = coordinates[0] ;
        coordinateY = coordinatesFloatArray[1] = coordinates[1];
        coordinateZ = coordinatesFloatArray[2] = coordinates[2];
    }

    public Vertex(float x, float y, float z) {
        coordinateX = coordinatesFloatArray[0]=x;
        coordinateY =coordinatesFloatArray[1]= y;
        coordinateZ =coordinatesFloatArray[2] =z;
    }

    public float getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(float coordinateX) {

        this.coordinateX = coordinatesFloatArray[0]=coordinateX;
    }

    public float getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(float coordinateY) {
        this.coordinateY = coordinatesFloatArray[1]= coordinateY;
    }

    public float getCoordinateZ() {
        return coordinateZ;
    }

    public void setCoordinateZ(float coordinateZ) {
        this.coordinateZ = coordinatesFloatArray[2]= coordinateZ;
    }

    public float[] getCoordinatesFloatArray() {
        return coordinatesFloatArray;
    }

}
