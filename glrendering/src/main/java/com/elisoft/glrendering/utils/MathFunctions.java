package com.elisoft.glrendering.utils;

/**
 * Created by yvorobey on 7/8/2015.
 */
public class MathFunctions {

    public static float BlinnWyvillFuction(float x) {
        float x2 = x*x;
        float x4 = x2*x2;
        float x6 = x4*x2;

        final float fa = ( 4.0f/9.0f);
        final float fb = (17.0f/9.0f);
        final float fc = (22.0f/9.0f);

        float y = fa*x6 - fb*x4 + fc*x2;
        return y;
    }

    public static float doubleCubicSeat (float x, float a, float b){

        float epsilon = 0.00001f;
        float min_param_a = 0.0f + epsilon;
        float max_param_a = 1.0f - epsilon;
        float min_param_b = 0.0f;
        float max_param_b = 1.0f;
        a = Math.min(max_param_a, Math.max(min_param_a, a));
        b = Math.min(max_param_b, Math.max(min_param_b, b));

        float y = 0;
        if (x <= a){
            y = b - b* (float)Math.pow(1f - x / a, 3.0f);
        } else {
            y = b + (1-b)*(float)Math.pow((x-a)/(1-a), 3.0);
        }
        return y;
    }
    public static float parabola(float x) {
        float y = (float) Math.pow(x, 2);
        return y;
    }
}
