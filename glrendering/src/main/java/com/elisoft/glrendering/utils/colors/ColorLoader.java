package com.elisoft.glrendering.utils.colors;

import android.content.Context;

import com.elisoft.glrendering.utils.FloatColor;

import java.util.ArrayList;



/**
 * Created by mBykov on 19.06.2015.
 */
class ColorLoader {
    Context context;

    public ColorLoader(Context context) {
        this.context = context;
    }

    ArrayList<FloatColor> load(ColorRange[] ranges) {
        ArrayList<FloatColor> floatColors = new ArrayList<FloatColor>();
        for (ColorRange range : ranges) {
            floatColors.addAll(loadind(range));
        }
        return floatColors;
    }

    private ArrayList<FloatColor> loadind(ColorRange range) {
        ArrayList<FloatColor> floatColors = new ArrayList<FloatColor>();
        int[] colors = context.getResources().getIntArray(range.getValue());
      //  int[] colors = context.getResources().getIntArray();
     //   int[] colors = context.getResources().getIntArray();
        for (int color: colors){
            floatColors.add(FloatColor.generateFloatColor(color));
        }
       /* for (int i = 0; i < c.length; i++) {
            // floatColors.add(FloatColor.generateFloatColor(context.getResources().getColor(colorsIds[i])));

           // floatColors.add(FloatColor.generateFloatColor(context.getResources().getColor(R.color.red_500)));
        }*/
        return floatColors;
    }

    ArrayList<FloatColor> load(ColorRange range) {
        return loadind(range);
    }
}
