package com.elisoft.glrendering.utils.colors;

import java.util.ArrayList;

/**
 * Created by mBykov on 19.06.2015.
 */
public interface Generator {
    public ArrayList<Integer> generate(int size);
}
